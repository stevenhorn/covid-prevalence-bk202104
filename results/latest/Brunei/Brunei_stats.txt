0 Divergences 
Passed validation 
Computed from 80 by 385 log-likelihood matrix

         Estimate       SE
elpd_loo  -452.10    65.42
p_loo       55.14        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      343   89.1%
 (0.5, 0.7]   (ok)         36    9.4%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    2    0.5%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.240   0.050   0.152    0.316  ...    43.0      72.0      18.0   1.03
pu         0.771   0.055   0.701    0.875  ...     4.0       5.0      38.0   1.46
mu         0.257   0.044   0.177    0.349  ...     9.0       9.0      32.0   1.18
mus        0.348   0.041   0.265    0.413  ...   147.0     152.0     100.0   1.00
gamma      0.467   0.072   0.370    0.615  ...    59.0      93.0     100.0   1.05
Is_begin  14.743   8.784   0.574   28.278  ...    10.0       8.0      24.0   1.24
Ia_begin  47.502  16.179  17.369   70.471  ...   105.0      93.0      40.0   1.01
E_begin   63.672  37.224   1.375  133.688  ...     9.0       8.0      41.0   1.19

[8 rows x 11 columns]