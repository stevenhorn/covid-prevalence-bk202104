0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1110.07    34.27
p_loo       35.81        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      366   95.3%
 (0.5, 0.7]   (ok)         13    3.4%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.262  0.054   0.184    0.350  ...    45.0      54.0      37.0   1.11
pu        0.883  0.019   0.849    0.900  ...    18.0      24.0      40.0   1.08
mu        0.136  0.025   0.095    0.176  ...    17.0      16.0      26.0   1.10
mus       0.153  0.032   0.106    0.209  ...   101.0     121.0      97.0   1.01
gamma     0.188  0.042   0.119    0.269  ...    64.0      79.0      55.0   1.06
Is_begin  0.722  0.724   0.000    2.071  ...    81.0      46.0      30.0   1.00
Ia_begin  1.508  1.887   0.005    4.663  ...   115.0      92.0      49.0   0.99
E_begin   0.849  1.019   0.006    2.810  ...   114.0     105.0      77.0   0.99

[8 rows x 11 columns]