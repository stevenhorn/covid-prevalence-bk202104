29 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1574.97    51.88
p_loo       47.40        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      283   73.7%
 (0.5, 0.7]   (ok)         46   12.0%
   (0.7, 1]   (bad)        34    8.9%
   (1, Inf)   (very bad)   21    5.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.236  0.034   0.192    0.297  ...     6.0       9.0      16.0   1.21
pu        0.808  0.045   0.740    0.853  ...     3.0       3.0      39.0   2.00
mu        0.129  0.017   0.097    0.148  ...     5.0       6.0      42.0   1.41
mus       0.116  0.083   0.024    0.233  ...     3.0       3.0      14.0   2.33
gamma     0.269  0.040   0.187    0.338  ...    34.0      40.0      76.0   1.16
Is_begin  1.175  0.900   0.025    2.787  ...     5.0       5.0      14.0   1.39
Ia_begin  0.451  0.402   0.031    1.347  ...     8.0       9.0      42.0   1.22
E_begin   0.748  0.670   0.001    2.001  ...     4.0       3.0      22.0   2.03

[8 rows x 11 columns]