0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1664.53    36.23
p_loo       19.12        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      375   97.7%
 (0.5, 0.7]   (ok)          6    1.6%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.208  0.018   0.179    0.242  ...     7.0       8.0      15.0   1.21
pu        0.814  0.004   0.806    0.818  ...    17.0      17.0      14.0   1.09
mu        0.217  0.010   0.206    0.232  ...     3.0       4.0      20.0   1.86
mus       0.154  0.005   0.144    0.161  ...     5.0       4.0      14.0   1.49
gamma     0.079  0.001   0.076    0.081  ...     4.0       4.0      36.0   1.53
Is_begin  1.675  0.379   1.031    2.424  ...    24.0      25.0      38.0   1.02
Ia_begin  2.920  2.083   0.087    7.394  ...     5.0       5.0      14.0   1.58
E_begin   1.198  1.061   0.305    3.620  ...     3.0       4.0      14.0   1.67

[8 rows x 11 columns]