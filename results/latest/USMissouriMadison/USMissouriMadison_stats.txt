0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1205.09    72.93
p_loo       44.70        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      363   94.5%
 (0.5, 0.7]   (ok)         16    4.2%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.172  0.020   0.150    0.212  ...    65.0      68.0      31.0   1.10
pu        0.706  0.004   0.700    0.712  ...    89.0      67.0      58.0   1.03
mu        0.102  0.015   0.077    0.137  ...    24.0      27.0      39.0   1.04
mus       0.207  0.036   0.145    0.278  ...    46.0      58.0      16.0   1.07
gamma     0.427  0.079   0.271    0.552  ...   115.0     127.0      81.0   1.00
Is_begin  0.496  0.454   0.035    1.446  ...    67.0      67.0      58.0   0.98
Ia_begin  0.601  0.671   0.004    1.862  ...    50.0      59.0      41.0   1.01
E_begin   0.281  0.295   0.003    0.737  ...    94.0     105.0      39.0   0.99

[8 rows x 11 columns]