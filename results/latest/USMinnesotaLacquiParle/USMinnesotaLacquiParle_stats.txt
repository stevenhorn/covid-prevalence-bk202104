0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -612.48    27.38
p_loo       24.98        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      357   93.0%
 (0.5, 0.7]   (ok)         24    6.2%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.260  0.046   0.190    0.350  ...   111.0     118.0      40.0   0.98
pu        0.775  0.023   0.729    0.812  ...    19.0      19.0      60.0   1.10
mu        0.140  0.026   0.095    0.188  ...    22.0      18.0      18.0   1.08
mus       0.172  0.033   0.111    0.230  ...    82.0      95.0      51.0   1.00
gamma     0.211  0.046   0.131    0.292  ...   129.0     152.0      65.0   0.98
Is_begin  0.578  0.643   0.002    1.777  ...    56.0      44.0      38.0   1.00
Ia_begin  0.952  1.568   0.009    3.648  ...    42.0      62.0      49.0   1.01
E_begin   0.391  0.473   0.004    0.941  ...    91.0      62.0      55.0   1.02

[8 rows x 11 columns]