0 Divergences 
Passed validation 
Computed from 80 by 385 log-likelihood matrix

         Estimate       SE
elpd_loo -3520.22    25.85
p_loo       20.32        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      367   95.3%
 (0.5, 0.7]   (ok)         14    3.6%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    1    0.3%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.243   0.054   0.156    0.335  ...   152.0     152.0      83.0   1.04
pu         0.810   0.056   0.707    0.892  ...    53.0      48.0      15.0   1.09
mu         0.100   0.014   0.071    0.116  ...    50.0      57.0      33.0   1.10
mus        0.145   0.023   0.106    0.186  ...   152.0     152.0      87.0   1.07
gamma      0.223   0.031   0.171    0.283  ...    91.0      94.0      93.0   0.99
Is_begin  24.491  16.996   2.663   61.664  ...    90.0      76.0      65.0   1.02
Ia_begin  68.271  44.463   5.302  149.951  ...    60.0      56.0      60.0   1.03
E_begin   49.842  40.674   0.642  118.434  ...    77.0      66.0      80.0   1.03

[8 rows x 11 columns]