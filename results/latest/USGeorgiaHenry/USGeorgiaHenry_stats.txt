0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1777.55    66.16
p_loo       41.35        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      349   90.9%
 (0.5, 0.7]   (ok)         32    8.3%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.246  0.051   0.169    0.337  ...    83.0      93.0      59.0   1.07
pu        0.763  0.030   0.716    0.819  ...    53.0      58.0      60.0   1.02
mu        0.122  0.023   0.094    0.175  ...    16.0      17.0      39.0   1.11
mus       0.251  0.045   0.178    0.340  ...   152.0     152.0      80.0   1.00
gamma     0.352  0.055   0.260    0.454  ...    83.0     102.0      57.0   1.05
Is_begin  0.924  0.496   0.039    1.890  ...   152.0     152.0      85.0   1.00
Ia_begin  2.631  1.890   0.181    5.735  ...    60.0      44.0      59.0   1.07
E_begin   2.329  2.347   0.058    6.987  ...    57.0      49.0      60.0   1.01

[8 rows x 11 columns]