0 Divergences 
Passed validation 
Computed from 80 by 385 log-likelihood matrix

         Estimate       SE
elpd_loo -2936.10    33.61
p_loo       21.16        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      333   86.5%
 (0.5, 0.7]   (ok)         43   11.2%
   (0.7, 1]   (bad)         9    2.3%
   (1, Inf)   (very bad)    0    0.0%

              mean        sd    hdi_3%  ...  ess_bulk  ess_tail  r_hat
pa           0.230     0.052     0.154  ...      95.0      58.0   1.03
pu           0.736     0.025     0.700  ...      19.0      30.0   1.09
mu           0.218     0.026     0.176  ...      18.0      19.0   1.10
mus          0.238     0.033     0.183  ...      82.0      57.0   0.99
gamma        0.360     0.053     0.270  ...     152.0      93.0   0.99
Is_begin  1555.781   920.808   105.975  ...      50.0      30.0   1.03
Ia_begin  3978.255  1583.448  1237.368  ...      93.0      60.0   0.99
E_begin   4705.803  2480.001   861.827  ...     102.0      96.0   1.00

[8 rows x 11 columns]