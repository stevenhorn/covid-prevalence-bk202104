0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1034.37    49.76
p_loo       38.17        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      362   94.3%
 (0.5, 0.7]   (ok)         14    3.6%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.246  0.058   0.155    0.337  ...   105.0      88.0      42.0   1.00
pu        0.784  0.026   0.738    0.831  ...   107.0     111.0      60.0   1.00
mu        0.126  0.020   0.094    0.169  ...    44.0      40.0      43.0   1.03
mus       0.185  0.039   0.113    0.236  ...    71.0      88.0      59.0   1.00
gamma     0.248  0.039   0.171    0.319  ...    86.0     105.0      81.0   1.00
Is_begin  0.366  0.456   0.002    1.235  ...    79.0      42.0      80.0   1.05
Ia_begin  0.553  0.860   0.002    1.696  ...   100.0      24.0      28.0   1.08
E_begin   0.263  0.381   0.000    0.973  ...   100.0      76.0      60.0   1.00

[8 rows x 11 columns]