4 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -922.99    46.32
p_loo       40.00        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      309   80.5%
 (0.5, 0.7]   (ok)         43   11.2%
   (0.7, 1]   (bad)        21    5.5%
   (1, Inf)   (very bad)   11    2.9%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.245  0.056   0.153    0.311  ...     4.0       5.0      22.0   1.46
pu        0.718  0.016   0.701    0.745  ...    15.0       7.0      20.0   1.40
mu        0.137  0.018   0.094    0.158  ...    14.0      17.0      24.0   1.53
mus       0.187  0.026   0.144    0.240  ...    20.0      18.0      54.0   1.15
gamma     0.165  0.028   0.142    0.241  ...     6.0       6.0      59.0   1.33
Is_begin  0.726  0.529   0.009    1.518  ...     5.0       4.0      22.0   1.56
Ia_begin  0.569  0.489   0.125    1.516  ...    42.0      29.0      59.0   1.38
E_begin   0.371  0.323   0.000    0.898  ...     6.0       6.0      20.0   1.41

[8 rows x 11 columns]