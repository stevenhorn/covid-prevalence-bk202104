0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -645.17    29.34
p_loo       27.94        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      359   93.5%
 (0.5, 0.7]   (ok)         17    4.4%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.255  0.043   0.171    0.322  ...   133.0     124.0      60.0   1.07
pu        0.863  0.019   0.830    0.889  ...    38.0      46.0      58.0   1.04
mu        0.164  0.029   0.122    0.215  ...    15.0      16.0      60.0   1.10
mus       0.204  0.037   0.149    0.278  ...   118.0     118.0      97.0   1.08
gamma     0.267  0.037   0.213    0.330  ...    95.0      95.0      93.0   0.99
Is_begin  0.705  0.784   0.002    2.179  ...    98.0      85.0      60.0   1.03
Ia_begin  1.556  1.815   0.015    5.408  ...    72.0      58.0      42.0   1.00
E_begin   0.576  0.621   0.045    1.675  ...    51.0      41.0      57.0   1.03

[8 rows x 11 columns]