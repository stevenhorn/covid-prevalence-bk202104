0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -163.86    37.28
p_loo       42.98        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      342   89.1%
 (0.5, 0.7]   (ok)         34    8.9%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.228  0.046   0.156    0.323  ...   152.0     152.0      93.0   1.00
pu        0.727  0.016   0.701    0.757  ...    90.0      87.0      96.0   1.02
mu        0.130  0.027   0.091    0.186  ...    66.0      72.0      81.0   1.02
mus       0.267  0.030   0.225    0.311  ...   152.0     105.0      88.0   1.01
gamma     0.289  0.042   0.221    0.363  ...    72.0     101.0      60.0   1.01
Is_begin  0.347  0.363   0.004    1.114  ...    67.0      50.0      57.0   1.00
Ia_begin  0.614  0.731   0.001    2.297  ...    58.0      77.0      54.0   1.00
E_begin   0.289  0.368   0.002    0.995  ...   101.0      79.0      42.0   0.98

[8 rows x 11 columns]