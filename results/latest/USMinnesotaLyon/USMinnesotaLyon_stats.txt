0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1114.26    32.12
p_loo       31.38        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      361   94.0%
 (0.5, 0.7]   (ok)         19    4.9%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.199  0.027   0.155    0.245  ...   110.0     111.0      58.0   1.00
pu        0.716  0.012   0.700    0.735  ...   152.0     152.0      59.0   1.02
mu        0.128  0.020   0.092    0.159  ...    63.0      62.0      60.0   0.99
mus       0.190  0.032   0.135    0.240  ...    65.0      59.0      58.0   1.01
gamma     0.262  0.042   0.188    0.338  ...   152.0     152.0     100.0   1.03
Is_begin  0.497  0.533   0.002    1.827  ...    94.0      66.0      40.0   0.99
Ia_begin  0.719  0.847   0.015    2.249  ...    92.0     108.0      48.0   1.00
E_begin   0.329  0.315   0.007    0.976  ...    58.0      59.0      60.0   1.02

[8 rows x 11 columns]