0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1235.32    56.15
p_loo       43.04        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      358   93.2%
 (0.5, 0.7]   (ok)         21    5.5%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.186  0.032   0.150    0.255  ...    77.0      84.0      35.0   0.99
pu        0.711  0.010   0.700    0.726  ...   152.0     117.0      58.0   1.02
mu        0.122  0.023   0.081    0.158  ...    67.0      69.0      67.0   1.00
mus       0.189  0.045   0.115    0.272  ...    74.0      77.0      59.0   1.03
gamma     0.232  0.048   0.156    0.322  ...    67.0      91.0      36.0   1.02
Is_begin  0.494  0.475   0.001    1.250  ...    41.0      11.0      14.0   1.14
Ia_begin  1.052  1.106   0.000    3.661  ...    91.0      60.0      14.0   1.02
E_begin   0.351  0.403   0.011    0.963  ...    71.0      33.0      87.0   1.07

[8 rows x 11 columns]