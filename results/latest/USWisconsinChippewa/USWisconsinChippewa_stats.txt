0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1281.34    36.78
p_loo       28.43        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      350   91.1%
 (0.5, 0.7]   (ok)         27    7.0%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.247  0.057   0.152    0.340  ...    99.0      86.0      55.0   1.07
pu        0.786  0.025   0.744    0.821  ...    49.0      52.0      40.0   1.00
mu        0.113  0.021   0.081    0.158  ...    67.0      76.0      72.0   1.06
mus       0.177  0.033   0.130    0.257  ...    88.0      87.0      59.0   1.01
gamma     0.201  0.038   0.137    0.266  ...   121.0     117.0      91.0   1.01
Is_begin  0.884  0.765   0.037    2.208  ...   123.0     152.0      60.0   1.03
Ia_begin  1.484  1.424   0.030    4.029  ...   100.0      48.0      59.0   1.01
E_begin   0.768  0.779   0.033    2.192  ...    95.0     122.0      74.0   1.03

[8 rows x 11 columns]