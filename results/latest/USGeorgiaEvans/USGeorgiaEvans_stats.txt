0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -826.42    52.88
p_loo       33.92        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      359   93.5%
 (0.5, 0.7]   (ok)         21    5.5%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.263  0.052   0.154    0.338  ...    70.0      70.0      59.0   1.00
pu        0.828  0.018   0.794    0.861  ...    56.0      57.0      87.0   1.03
mu        0.145  0.020   0.113    0.179  ...     8.0      10.0      24.0   1.19
mus       0.195  0.034   0.127    0.253  ...    54.0      55.0      99.0   1.00
gamma     0.275  0.054   0.195    0.402  ...    68.0      79.0      34.0   1.07
Is_begin  0.499  0.536   0.017    1.557  ...    94.0      68.0      56.0   1.04
Ia_begin  0.998  1.176   0.004    3.534  ...   100.0      77.0      36.0   0.99
E_begin   0.402  0.488   0.005    1.464  ...    68.0      49.0      59.0   0.99

[8 rows x 11 columns]