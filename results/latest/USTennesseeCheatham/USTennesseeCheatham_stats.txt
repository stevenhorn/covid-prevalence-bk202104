0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1272.45    28.31
p_loo       26.59        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      351   91.4%
 (0.5, 0.7]   (ok)         27    7.0%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.260  0.055   0.169    0.345  ...    91.0      85.0      38.0   1.08
pu        0.775  0.023   0.748    0.827  ...    65.0      64.0      61.0   1.03
mu        0.136  0.023   0.090    0.173  ...    14.0      16.0      39.0   1.12
mus       0.165  0.029   0.114    0.218  ...   109.0     126.0      96.0   1.02
gamma     0.194  0.034   0.125    0.247  ...   117.0     117.0      83.0   1.00
Is_begin  0.613  0.570   0.026    1.701  ...   125.0      67.0      20.0   1.04
Ia_begin  0.645  0.565   0.014    1.734  ...   126.0      98.0      60.0   0.98
E_begin   0.427  0.562   0.002    1.636  ...   111.0      87.0      32.0   1.02

[8 rows x 11 columns]