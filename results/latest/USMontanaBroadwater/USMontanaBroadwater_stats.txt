0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -588.86    23.80
p_loo       24.04        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      365   95.1%
 (0.5, 0.7]   (ok)         18    4.7%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.248  0.048   0.175    0.333  ...    77.0      79.0      87.0   1.00
pu        0.815  0.021   0.781    0.862  ...    26.0      26.0      31.0   1.01
mu        0.127  0.030   0.081    0.186  ...    35.0      32.0      47.0   0.99
mus       0.169  0.033   0.119    0.220  ...    73.0     110.0      72.0   1.01
gamma     0.185  0.036   0.123    0.244  ...   121.0     142.0      91.0   0.99
Is_begin  0.857  0.702   0.028    2.351  ...   152.0     152.0      43.0   0.99
Ia_begin  1.616  2.127   0.039    6.476  ...    93.0     106.0      75.0   1.00
E_begin   0.672  0.722   0.004    1.878  ...   120.0     122.0      58.0   1.01

[8 rows x 11 columns]