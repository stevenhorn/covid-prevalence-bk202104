0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -841.54    25.42
p_loo       26.81        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      349   90.9%
 (0.5, 0.7]   (ok)         31    8.1%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.239  0.048   0.161    0.326  ...    52.0      57.0      65.0   1.00
pu        0.735  0.019   0.703    0.761  ...    48.0      41.0      38.0   0.99
mu        0.129  0.024   0.094    0.176  ...    26.0      26.0      60.0   1.05
mus       0.188  0.030   0.137    0.256  ...    84.0      89.0      57.0   1.00
gamma     0.211  0.041   0.155    0.313  ...    81.0      77.0      77.0   0.99
Is_begin  0.583  0.452   0.019    1.397  ...    99.0      81.0      31.0   1.03
Ia_begin  1.135  1.076   0.000    3.129  ...    49.0      31.0      22.0   1.03
E_begin   0.492  0.527   0.003    1.482  ...   107.0      73.0      42.0   1.00

[8 rows x 11 columns]