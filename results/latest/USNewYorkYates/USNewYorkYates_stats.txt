0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -693.33    32.73
p_loo       26.77        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      344   89.6%
 (0.5, 0.7]   (ok)         33    8.6%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.290  0.040   0.220    0.348  ...   152.0     152.0      75.0   1.04
pu        0.889  0.011   0.869    0.900  ...    73.0      52.0      49.0   1.03
mu        0.164  0.023   0.123    0.206  ...    36.0      37.0      60.0   1.04
mus       0.198  0.032   0.140    0.259  ...   120.0     152.0      59.0   1.06
gamma     0.253  0.051   0.173    0.350  ...   152.0     152.0      83.0   0.99
Is_begin  0.915  0.700   0.035    2.075  ...    62.0      88.0      59.0   1.00
Ia_begin  2.075  1.768   0.029    5.761  ...    81.0     112.0      79.0   0.98
E_begin   1.010  1.125   0.004    3.884  ...    63.0      58.0      30.0   1.03

[8 rows x 11 columns]