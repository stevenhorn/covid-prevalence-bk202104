0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1173.62    29.07
p_loo       28.40        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      356   92.7%
 (0.5, 0.7]   (ok)         21    5.5%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.248  0.054   0.173    0.349  ...   152.0     152.0      59.0   0.99
pu        0.774  0.024   0.734    0.816  ...    19.0      18.0      30.0   1.09
mu        0.113  0.025   0.077    0.156  ...    16.0      14.0      55.0   1.11
mus       0.167  0.033   0.109    0.226  ...   152.0     152.0      45.0   1.11
gamma     0.186  0.038   0.135    0.267  ...    71.0      79.0      73.0   1.00
Is_begin  0.802  0.751   0.006    2.228  ...   114.0     152.0      69.0   1.01
Ia_begin  1.324  1.426   0.066    4.284  ...   118.0      93.0      33.0   0.99
E_begin   0.668  0.783   0.005    2.393  ...   119.0     103.0      61.0   0.99

[8 rows x 11 columns]