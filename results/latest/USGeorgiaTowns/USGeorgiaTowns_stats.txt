0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -910.57    35.64
p_loo       25.41        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      367   95.6%
 (0.5, 0.7]   (ok)         13    3.4%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.249  0.048   0.164    0.328  ...    71.0      71.0      91.0   0.98
pu        0.799  0.021   0.769    0.846  ...    15.0      14.0      66.0   1.10
mu        0.108  0.021   0.067    0.146  ...    13.0      13.0      25.0   1.14
mus       0.164  0.033   0.110    0.225  ...    53.0      70.0      83.0   1.02
gamma     0.182  0.033   0.113    0.234  ...    48.0      49.0      87.0   1.03
Is_begin  0.806  0.627   0.010    1.872  ...   152.0     152.0      61.0   1.02
Ia_begin  1.541  1.653   0.022    4.341  ...    90.0      68.0      60.0   1.00
E_begin   0.826  0.870   0.041    2.702  ...    87.0      59.0      96.0   1.01

[8 rows x 11 columns]