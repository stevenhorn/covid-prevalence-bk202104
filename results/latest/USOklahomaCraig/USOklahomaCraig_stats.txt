0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1116.90    57.48
p_loo       41.49        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      358   93.2%
 (0.5, 0.7]   (ok)         22    5.7%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.232  0.044   0.152    0.300  ...    42.0      57.0      61.0   1.03
pu        0.725  0.018   0.702    0.758  ...   108.0     115.0      38.0   1.05
mu        0.148  0.021   0.104    0.181  ...    32.0      32.0      32.0   1.06
mus       0.261  0.036   0.197    0.320  ...   111.0     106.0      95.0   1.00
gamma     0.389  0.065   0.282    0.513  ...    98.0      83.0      61.0   1.04
Is_begin  0.826  0.893   0.003    3.009  ...   105.0      63.0      60.0   1.01
Ia_begin  1.401  1.444   0.011    4.665  ...   102.0      99.0     100.0   0.99
E_begin   0.631  0.701   0.008    1.934  ...   112.0     102.0      61.0   1.01

[8 rows x 11 columns]