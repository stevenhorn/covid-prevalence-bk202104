0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -2243.50    46.22
p_loo       26.17        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      348   90.6%
 (0.5, 0.7]   (ok)         31    8.1%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    0    0.0%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.231   0.048   0.154    0.318  ...    41.0      42.0      59.0   1.01
pu         0.730   0.021   0.701    0.762  ...     9.0       9.0      30.0   1.18
mu         0.104   0.025   0.067    0.152  ...    20.0      20.0      40.0   1.09
mus        0.178   0.028   0.126    0.225  ...    95.0      96.0      97.0   1.01
gamma      0.231   0.035   0.164    0.281  ...   152.0     133.0      69.0   1.00
Is_begin  19.284  11.223   1.693   38.081  ...    76.0      67.0      60.0   1.02
Ia_begin  39.407  20.038   8.482   71.800  ...    69.0      59.0      76.0   1.04
E_begin   49.129  25.729  10.945  108.512  ...    65.0      63.0      59.0   1.02

[8 rows x 11 columns]