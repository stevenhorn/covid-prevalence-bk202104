0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1458.78    67.01
p_loo       39.91        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      359   93.5%
 (0.5, 0.7]   (ok)         21    5.5%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.194  0.033   0.151    0.258  ...    34.0      40.0      60.0   1.04
pu        0.714  0.012   0.701    0.744  ...    51.0      62.0      59.0   1.05
mu        0.095  0.015   0.067    0.118  ...    21.0      22.0      14.0   1.04
mus       0.215  0.034   0.162    0.301  ...    49.0      52.0      60.0   1.01
gamma     0.471  0.073   0.350    0.612  ...   152.0     152.0      40.0   1.01
Is_begin  0.451  0.332   0.027    1.137  ...    56.0      56.0      48.0   1.00
Ia_begin  0.777  1.120   0.019    3.026  ...    78.0      27.0      59.0   1.05
E_begin   0.310  0.443   0.003    0.956  ...    65.0      63.0      55.0   1.01

[8 rows x 11 columns]