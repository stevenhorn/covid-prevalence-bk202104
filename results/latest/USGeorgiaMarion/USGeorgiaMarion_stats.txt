0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -777.13    49.62
p_loo       36.02        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      350   91.1%
 (0.5, 0.7]   (ok)         27    7.0%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.254  0.052   0.154    0.335  ...    51.0      54.0      31.0   1.01
pu        0.833  0.027   0.781    0.877  ...     4.0       5.0      21.0   1.54
mu        0.140  0.024   0.104    0.181  ...     9.0       9.0      36.0   1.17
mus       0.187  0.027   0.147    0.245  ...    12.0      13.0      35.0   1.13
gamma     0.269  0.047   0.193    0.349  ...    74.0      68.0      40.0   1.04
Is_begin  1.094  0.894   0.098    2.663  ...    81.0      66.0      81.0   1.00
Ia_begin  2.756  2.355   0.128    7.967  ...    55.0      52.0      60.0   1.05
E_begin   1.534  1.749   0.010    4.750  ...    50.0      48.0      60.0   1.03

[8 rows x 11 columns]