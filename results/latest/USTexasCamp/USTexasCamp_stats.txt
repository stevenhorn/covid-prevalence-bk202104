0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1055.29    53.59
p_loo       39.44        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      365   95.1%
 (0.5, 0.7]   (ok)         14    3.6%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.273  0.051   0.178    0.350  ...    66.0      70.0      49.0   1.01
pu        0.824  0.021   0.787    0.866  ...    43.0      41.0      59.0   1.01
mu        0.130  0.022   0.100    0.185  ...    17.0      17.0      42.0   1.12
mus       0.188  0.033   0.131    0.241  ...    87.0      98.0      88.0   1.02
gamma     0.269  0.048   0.179    0.341  ...    85.0      86.0      87.0   1.00
Is_begin  0.607  0.619   0.002    2.095  ...    92.0      55.0      47.0   1.02
Ia_begin  1.242  1.510   0.005    4.515  ...   114.0     102.0      48.0   1.01
E_begin   0.468  0.448   0.002    1.415  ...    98.0      63.0      22.0   1.01

[8 rows x 11 columns]