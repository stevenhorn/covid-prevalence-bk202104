0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1863.86    23.72
p_loo       31.00        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      351   91.4%
 (0.5, 0.7]   (ok)         29    7.6%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.246  0.057   0.160    0.350  ...    62.0      82.0      22.0   1.09
pu        0.739  0.033   0.701    0.801  ...    20.0      37.0      32.0   1.05
mu        0.103  0.017   0.071    0.130  ...    15.0      17.0      33.0   1.07
mus       0.187  0.034   0.129    0.242  ...   152.0     152.0      81.0   1.01
gamma     0.274  0.039   0.209    0.344  ...   152.0     152.0      65.0   1.01
Is_begin  2.440  1.230   0.437    4.758  ...   152.0     152.0     100.0   1.09
Ia_begin  5.644  3.402   0.296   10.695  ...   152.0     152.0      76.0   1.01
E_begin   7.308  6.283   0.116   17.389  ...   108.0     132.0      86.0   1.01

[8 rows x 11 columns]