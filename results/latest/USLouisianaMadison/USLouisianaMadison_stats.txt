0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1160.00    53.84
p_loo       39.74        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      367   95.6%
 (0.5, 0.7]   (ok)         13    3.4%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.210  0.048   0.150    0.289  ...    54.0      41.0      14.0   1.04
pu        0.719  0.013   0.702    0.744  ...    75.0      64.0      93.0   1.08
mu        0.181  0.021   0.146    0.227  ...    50.0      46.0      84.0   1.03
mus       0.256  0.035   0.183    0.309  ...    67.0      67.0      49.0   1.03
gamma     0.372  0.051   0.292    0.461  ...    21.0      20.0      91.0   1.08
Is_begin  0.771  0.787   0.007    2.638  ...    95.0     114.0     100.0   1.00
Ia_begin  1.160  1.164   0.009    3.719  ...    48.0      64.0      53.0   1.05
E_begin   0.480  0.574   0.018    1.712  ...    54.0      46.0      69.0   1.04

[8 rows x 11 columns]