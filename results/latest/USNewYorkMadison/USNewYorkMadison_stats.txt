0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1157.76    48.19
p_loo       37.20        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      358   93.2%
 (0.5, 0.7]   (ok)         20    5.2%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.238  0.047   0.157    0.310  ...    34.0      58.0      56.0   1.07
pu        0.771  0.043   0.701    0.834  ...    17.0      14.0      38.0   1.12
mu        0.149  0.024   0.105    0.191  ...    47.0      55.0      54.0   1.01
mus       0.221  0.037   0.158    0.280  ...    65.0      67.0      59.0   1.00
gamma     0.298  0.055   0.215    0.420  ...   121.0     152.0      72.0   1.00
Is_begin  1.955  1.266   0.143    4.040  ...   145.0     152.0      88.0   1.01
Ia_begin  6.100  3.189   1.232   11.394  ...    30.0      46.0      54.0   1.03
E_begin   5.611  4.724   0.418   13.552  ...    22.0      43.0      18.0   1.04

[8 rows x 11 columns]