4 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1079.45    31.27
p_loo       33.77        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      367   95.6%
 (0.5, 0.7]   (ok)         13    3.4%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.240  0.066   0.153    0.341  ...    37.0      34.0      47.0   1.24
pu        0.855  0.022   0.799    0.887  ...     6.0       6.0      24.0   1.34
mu        0.158  0.024   0.118    0.198  ...    10.0      12.0      48.0   1.12
mus       0.201  0.037   0.136    0.253  ...    76.0      75.0      96.0   1.14
gamma     0.237  0.040   0.164    0.299  ...    38.0      40.0      95.0   1.06
Is_begin  0.552  0.598   0.019    1.656  ...    59.0      48.0      84.0   1.06
Ia_begin  1.038  1.095   0.006    3.516  ...    26.0      31.0      73.0   1.06
E_begin   0.477  0.586   0.005    1.390  ...    73.0      61.0      56.0   1.02

[8 rows x 11 columns]