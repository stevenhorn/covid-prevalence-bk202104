28 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -2384.32    80.34
p_loo       41.17        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      356   92.7%
 (0.5, 0.7]   (ok)         20    5.2%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    1    0.3%

              mean        sd    hdi_3%  ...  ess_bulk  ess_tail  r_hat
pa           0.236     0.050     0.155  ...      23.0      40.0   1.10
pu           0.818     0.051     0.745  ...      31.0      60.0   1.05
mu           0.127     0.020     0.091  ...       4.0      15.0   1.55
mus          0.218     0.032     0.168  ...      21.0      59.0   1.07
gamma        0.271     0.048     0.202  ...      26.0      69.0   1.08
Is_begin   978.066   687.741   105.129  ...      66.0      60.0   1.02
Ia_begin  4453.022  2069.822  2049.699  ...      52.0      55.0   1.05
E_begin   2689.425  1734.355   698.376  ...      49.0      59.0   1.00

[8 rows x 11 columns]