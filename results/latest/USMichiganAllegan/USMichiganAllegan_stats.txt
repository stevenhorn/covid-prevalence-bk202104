0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1423.80    28.65
p_loo       21.70        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      354   92.2%
 (0.5, 0.7]   (ok)         28    7.3%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.251  0.058   0.164    0.344  ...    50.0      52.0      58.0   1.05
pu        0.802  0.025   0.751    0.838  ...    32.0      35.0      49.0   1.05
mu        0.124  0.019   0.095    0.157  ...    10.0       9.0      93.0   1.24
mus       0.161  0.032   0.118    0.223  ...   106.0     152.0      60.0   0.99
gamma     0.201  0.030   0.149    0.264  ...   129.0     114.0      86.0   1.00
Is_begin  0.928  0.685   0.063    2.104  ...    66.0      56.0      58.0   1.05
Ia_begin  1.988  1.910   0.059    5.434  ...    26.0      31.0      87.0   1.09
E_begin   0.989  0.957   0.010    2.849  ...    59.0      31.0      59.0   1.07

[8 rows x 11 columns]