0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1237.03    35.21
p_loo       36.54        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      349   90.9%
 (0.5, 0.7]   (ok)         27    7.0%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.198  0.039   0.155    0.259  ...     6.0       5.0      38.0   1.38
pu        0.721  0.011   0.702    0.739  ...    20.0      17.0      22.0   1.25
mu        0.187  0.060   0.096    0.269  ...     3.0       3.0      14.0   2.21
mus       0.221  0.047   0.146    0.299  ...     4.0       4.0      17.0   1.68
gamma     0.180  0.057   0.106    0.270  ...     3.0       3.0      14.0   2.27
Is_begin  1.115  0.844   0.077    2.666  ...     6.0       6.0      24.0   1.31
Ia_begin  1.743  1.817   0.039    5.423  ...     5.0       4.0      49.0   1.52
E_begin   0.926  0.660   0.097    2.107  ...     7.0       6.0      30.0   1.29

[8 rows x 11 columns]