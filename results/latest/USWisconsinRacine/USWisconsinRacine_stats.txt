0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1863.99    53.80
p_loo       33.01        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      344   89.6%
 (0.5, 0.7]   (ok)         32    8.3%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.244  0.062   0.153    0.333  ...    37.0      30.0      34.0   1.09
pu        0.737  0.023   0.701    0.775  ...    87.0      93.0      95.0   1.01
mu        0.132  0.024   0.099    0.189  ...    17.0      16.0      40.0   1.11
mus       0.196  0.033   0.137    0.244  ...   152.0     152.0      91.0   0.99
gamma     0.256  0.043   0.180    0.331  ...   131.0     152.0      59.0   1.08
Is_begin  0.765  0.651   0.025    1.923  ...    68.0      40.0      96.0   1.07
Ia_begin  0.622  0.472   0.027    1.455  ...   152.0     116.0      60.0   1.02
E_begin   0.578  0.524   0.005    1.728  ...    58.0      43.0      60.0   1.05

[8 rows x 11 columns]