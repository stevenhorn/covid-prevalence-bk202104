0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1164.86    45.10
p_loo       61.70        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      363   94.5%
 (0.5, 0.7]   (ok)         17    4.4%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.240  0.050   0.161    0.329  ...    55.0      58.0      97.0   1.04
pu        0.751  0.032   0.701    0.796  ...     4.0       5.0      34.0   1.54
mu        0.149  0.025   0.113    0.194  ...    11.0      14.0      48.0   1.11
mus       0.206  0.035   0.149    0.265  ...    88.0      84.0     100.0   1.01
gamma     0.312  0.052   0.239    0.410  ...   100.0      93.0      53.0   1.05
Is_begin  1.566  1.325   0.121    4.196  ...    37.0     127.0      74.0   1.09
Ia_begin  5.389  2.972   1.542   10.913  ...    17.0      17.0      96.0   1.10
E_begin   3.864  3.386   0.040   10.558  ...    24.0      16.0      60.0   1.09

[8 rows x 11 columns]