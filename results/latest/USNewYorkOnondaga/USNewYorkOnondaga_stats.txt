0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1697.06    30.02
p_loo       28.55        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      335   87.2%
 (0.5, 0.7]   (ok)         38    9.9%
   (0.7, 1]   (bad)        11    2.9%
   (1, Inf)   (very bad)    0    0.0%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.230   0.054   0.151    0.319  ...   132.0     107.0      45.0   0.99
pu         0.757   0.039   0.700    0.822  ...    74.0      71.0      38.0   1.11
mu         0.112   0.023   0.083    0.166  ...    17.0      29.0      32.0   1.19
mus        0.198   0.034   0.145    0.257  ...   152.0     152.0      67.0   1.03
gamma      0.259   0.050   0.155    0.342  ...    87.0     152.0      60.0   1.02
Is_begin   9.153   7.411   0.001   22.533  ...    11.0       9.0      20.0   1.17
Ia_begin  31.900  13.987   9.417   58.119  ...   104.0     102.0      40.0   1.00
E_begin   38.429  26.962   2.720   81.776  ...    69.0      70.0      96.0   1.01

[8 rows x 11 columns]