0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -988.28    28.14
p_loo       25.54        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      353   91.9%
 (0.5, 0.7]   (ok)         24    6.2%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.247  0.045   0.184    0.348  ...    15.0      22.0      15.0   1.08
pu        0.760  0.022   0.710    0.793  ...    12.0      16.0      49.0   1.09
mu        0.128  0.020   0.095    0.169  ...    54.0      57.0      59.0   1.02
mus       0.164  0.025   0.125    0.209  ...   117.0     129.0      38.0   1.00
gamma     0.193  0.035   0.145    0.270  ...   111.0     111.0      81.0   1.00
Is_begin  0.932  0.817   0.008    2.390  ...   133.0     151.0      96.0   1.00
Ia_begin  1.662  1.536   0.063    4.471  ...    96.0     102.0      54.0   1.01
E_begin   0.850  0.890   0.022    2.638  ...    38.0      68.0      49.0   1.04

[8 rows x 11 columns]