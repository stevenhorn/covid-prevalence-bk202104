0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -419.48    59.01
p_loo       36.63        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      364   94.8%
 (0.5, 0.7]   (ok)         15    3.9%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.250  0.047   0.161    0.331  ...    73.0      70.0      40.0   1.01
pu        0.858  0.015   0.828    0.882  ...    88.0      90.0     100.0   1.02
mu        0.121  0.021   0.087    0.164  ...    82.0      81.0      83.0   0.99
mus       0.180  0.042   0.121    0.262  ...   134.0     135.0      58.0   1.03
gamma     0.269  0.058   0.196    0.387  ...   152.0     152.0      54.0   0.98
Is_begin  0.507  0.534   0.003    1.320  ...    87.0      79.0      72.0   1.00
Ia_begin  0.776  0.901   0.008    2.774  ...    36.0      30.0      88.0   1.07
E_begin   0.388  0.444   0.003    1.309  ...    66.0      70.0      59.0   1.00

[8 rows x 11 columns]