0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -986.08    36.74
p_loo       29.79        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      352   91.7%
 (0.5, 0.7]   (ok)         25    6.5%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.261  0.053   0.177    0.347  ...    11.0      14.0      85.0   1.10
pu        0.739  0.027   0.700    0.785  ...     9.0      11.0      14.0   1.13
mu        0.107  0.018   0.082    0.148  ...    18.0      22.0      43.0   1.03
mus       0.189  0.036   0.132    0.255  ...   126.0     138.0      96.0   1.02
gamma     0.251  0.043   0.180    0.341  ...   120.0     139.0      56.0   0.98
Is_begin  0.627  0.658   0.007    2.345  ...    71.0      43.0      55.0   1.02
Ia_begin  1.122  1.151   0.003    3.513  ...    43.0      46.0      60.0   1.04
E_begin   0.402  0.500   0.006    1.174  ...    54.0      36.0      58.0   1.05

[8 rows x 11 columns]