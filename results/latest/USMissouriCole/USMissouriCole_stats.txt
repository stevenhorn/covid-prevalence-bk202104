0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1527.63    66.26
p_loo       38.75        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      364   94.8%
 (0.5, 0.7]   (ok)         14    3.6%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.183  0.026   0.150    0.238  ...   152.0     152.0      42.0   0.98
pu        0.711  0.009   0.700    0.724  ...   152.0     152.0      47.0   0.98
mu        0.101  0.016   0.071    0.124  ...    54.0      47.0      59.0   1.04
mus       0.212  0.028   0.169    0.268  ...   152.0     152.0      37.0   1.03
gamma     0.326  0.057   0.217    0.423  ...    99.0     125.0      60.0   1.05
Is_begin  0.832  0.747   0.075    2.094  ...   152.0      95.0      83.0   0.99
Ia_begin  0.596  0.494   0.080    1.660  ...   129.0     135.0      93.0   0.99
E_begin   0.421  0.469   0.003    1.247  ...    81.0      92.0      88.0   1.00

[8 rows x 11 columns]