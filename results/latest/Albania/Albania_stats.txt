0 Divergences 
Passed validation 
Computed from 80 by 385 log-likelihood matrix

         Estimate       SE
elpd_loo -1935.38    43.08
p_loo       31.61        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      337   87.5%
 (0.5, 0.7]   (ok)         36    9.4%
   (0.7, 1]   (bad)        10    2.6%
   (1, Inf)   (very bad)    2    0.5%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.250   0.050   0.168    0.321  ...    60.0      64.0      53.0   1.08
pu         0.830   0.061   0.718    0.898  ...    15.0      12.0      19.0   1.12
mu         0.177   0.032   0.129    0.232  ...     4.0       5.0      38.0   1.50
mus        0.221   0.037   0.144    0.281  ...    78.0      86.0      59.0   1.05
gamma      0.261   0.041   0.187    0.329  ...    21.0      19.0      61.0   1.08
Is_begin  12.697   5.824   2.427   21.349  ...    71.0      94.0      80.0   1.01
Ia_begin  34.317  15.589  14.802   68.038  ...   144.0     122.0      95.0   1.02
E_begin   49.466  34.342   0.582  105.369  ...    24.0      28.0      22.0   1.06

[8 rows x 11 columns]