0 Divergences 
Passed validation 
Computed from 80 by 385 log-likelihood matrix

         Estimate       SE
elpd_loo -2218.98    32.41
p_loo       26.84        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      333   86.5%
 (0.5, 0.7]   (ok)         42   10.9%
   (0.7, 1]   (bad)        10    2.6%
   (1, Inf)   (very bad)    0    0.0%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.258   0.056   0.171    0.349  ...   115.0      98.0      35.0   1.01
pu         0.848   0.045   0.771    0.899  ...    24.0      17.0      39.0   1.13
mu         0.132   0.019   0.091    0.164  ...     5.0       5.0      29.0   1.52
mus        0.176   0.041   0.116    0.271  ...    46.0      60.0      50.0   1.05
gamma      0.182   0.037   0.127    0.258  ...    87.0      93.0      99.0   1.03
Is_begin  10.233   7.214   0.284   23.680  ...    63.0      48.0      44.0   1.00
Ia_begin  13.286  12.068   0.310   40.677  ...    68.0      35.0      60.0   1.04
E_begin   10.458  10.143   0.296   31.690  ...    85.0      33.0      40.0   1.04

[8 rows x 11 columns]