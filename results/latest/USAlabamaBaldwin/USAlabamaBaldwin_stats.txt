0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1708.86    46.12
p_loo       41.20        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      347   90.4%
 (0.5, 0.7]   (ok)         25    6.5%
   (0.7, 1]   (bad)         9    2.3%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.259  0.056   0.160    0.340  ...    64.0      66.0      60.0   1.03
pu        0.797  0.028   0.742    0.840  ...    26.0      29.0      43.0   1.06
mu        0.135  0.027   0.096    0.196  ...    14.0      13.0      51.0   1.14
mus       0.206  0.031   0.160    0.274  ...    58.0      56.0      96.0   1.00
gamma     0.266  0.046   0.191    0.345  ...   125.0     141.0      61.0   1.00
Is_begin  1.198  0.978   0.049    3.298  ...    52.0      27.0      40.0   1.08
Ia_begin  0.806  0.500   0.052    1.629  ...   123.0     108.0      60.0   1.06
E_begin   0.796  0.938   0.004    2.719  ...    88.0      48.0      60.0   1.04

[8 rows x 11 columns]