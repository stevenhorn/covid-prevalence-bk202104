0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1536.65    35.11
p_loo       31.80        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      360   93.8%
 (0.5, 0.7]   (ok)         20    5.2%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.246  0.055   0.150    0.329  ...   104.0     117.0      58.0   1.02
pu        0.784  0.044   0.707    0.856  ...    19.0      20.0      29.0   1.09
mu        0.121  0.023   0.083    0.162  ...    16.0      15.0      39.0   1.11
mus       0.174  0.031   0.126    0.230  ...   152.0     152.0      92.0   1.02
gamma     0.217  0.039   0.151    0.281  ...   129.0     139.0      96.0   1.01
Is_begin  1.412  1.223   0.025    4.075  ...   135.0      87.0      40.0   1.02
Ia_begin  3.594  2.799   0.201    8.841  ...   116.0      97.0      93.0   0.99
E_begin   2.336  2.576   0.034    6.801  ...    70.0      39.0      43.0   1.05

[8 rows x 11 columns]