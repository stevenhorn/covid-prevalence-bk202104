0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1009.92    26.35
p_loo       24.32        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      360   93.8%
 (0.5, 0.7]   (ok)         17    4.4%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.246  0.056   0.150    0.343  ...    70.0      75.0      59.0   1.01
pu        0.778  0.021   0.738    0.815  ...    85.0      89.0     100.0   0.99
mu        0.133  0.023   0.097    0.178  ...    23.0      18.0      60.0   1.09
mus       0.167  0.029   0.118    0.222  ...   106.0      97.0      91.0   1.02
gamma     0.212  0.039   0.154    0.291  ...   152.0     138.0      96.0   0.98
Is_begin  0.681  0.689   0.002    1.904  ...    98.0      77.0      59.0   0.99
Ia_begin  1.373  1.475   0.022    3.814  ...    54.0      54.0      60.0   1.02
E_begin   0.431  0.519   0.003    1.172  ...    81.0     107.0      67.0   0.99

[8 rows x 11 columns]