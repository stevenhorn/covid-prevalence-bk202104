0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -972.48    49.07
p_loo       62.30        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      361   94.0%
 (0.5, 0.7]   (ok)         15    3.9%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    4    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.251  0.060   0.169    0.348  ...    80.0      95.0      30.0   1.08
pu        0.821  0.029   0.781    0.873  ...     5.0       5.0      15.0   1.41
mu        0.102  0.015   0.082    0.135  ...   107.0     109.0      79.0   1.01
mus       0.232  0.039   0.154    0.307  ...    72.0      78.0      59.0   1.03
gamma     0.292  0.054   0.209    0.418  ...    11.0      13.0      19.0   1.13
Is_begin  0.820  0.629   0.014    1.848  ...    68.0      58.0      60.0   1.02
Ia_begin  1.385  1.137   0.000    3.806  ...   148.0     108.0      59.0   1.02
E_begin   0.660  0.764   0.012    1.975  ...   100.0      81.0      57.0   1.01

[8 rows x 11 columns]