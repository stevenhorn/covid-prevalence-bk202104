0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -801.11    23.05
p_loo       22.12        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      355   92.4%
 (0.5, 0.7]   (ok)         26    6.8%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.264  0.051   0.160    0.346  ...    41.0      36.0      40.0   1.06
pu        0.821  0.024   0.768    0.856  ...    13.0      14.0      40.0   1.16
mu        0.134  0.027   0.087    0.183  ...    37.0      50.0      15.0   1.01
mus       0.173  0.040   0.105    0.247  ...    58.0      51.0      40.0   1.00
gamma     0.205  0.040   0.138    0.280  ...    98.0     124.0      57.0   1.00
Is_begin  0.720  0.643   0.001    2.027  ...   149.0      94.0      59.0   1.00
Ia_begin  1.574  1.571   0.015    3.635  ...   106.0      80.0      60.0   0.99
E_begin   0.676  1.057   0.001    2.331  ...    98.0      55.0      17.0   1.06

[8 rows x 11 columns]