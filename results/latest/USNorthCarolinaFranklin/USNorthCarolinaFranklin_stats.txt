0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1383.64    37.78
p_loo       31.10        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      348   90.6%
 (0.5, 0.7]   (ok)         29    7.6%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.242  0.054   0.151    0.323  ...    63.0      55.0      38.0   1.02
pu        0.765  0.042   0.707    0.834  ...    11.0       9.0      22.0   1.18
mu        0.115  0.020   0.080    0.150  ...    19.0      17.0      57.0   1.09
mus       0.161  0.027   0.111    0.196  ...   103.0     152.0      60.0   0.99
gamma     0.208  0.034   0.149    0.276  ...    99.0     104.0      95.0   1.02
Is_begin  0.779  0.593   0.009    1.845  ...   113.0      79.0      60.0   0.99
Ia_begin  1.396  0.974   0.066    3.089  ...    66.0      48.0      81.0   1.01
E_begin   1.099  1.147   0.025    2.940  ...   120.0      64.0      93.0   0.99

[8 rows x 11 columns]