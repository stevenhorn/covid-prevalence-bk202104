0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -899.10    40.61
p_loo       34.64        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      348   90.6%
 (0.5, 0.7]   (ok)         31    8.1%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.250  0.052   0.155    0.325  ...    97.0     130.0      87.0   1.04
pu        0.797  0.020   0.759    0.828  ...    84.0      82.0      58.0   1.02
mu        0.114  0.020   0.090    0.154  ...    26.0      33.0      60.0   1.06
mus       0.241  0.037   0.178    0.307  ...   112.0     128.0      83.0   0.99
gamma     0.363  0.057   0.250    0.454  ...   107.0     106.0      59.0   1.03
Is_begin  0.932  0.826   0.010    2.308  ...    85.0      80.0      60.0   1.00
Ia_begin  1.943  1.785   0.053    5.611  ...    93.0      99.0      86.0   1.00
E_begin   1.120  1.077   0.009    3.423  ...    68.0      42.0      60.0   1.03

[8 rows x 11 columns]