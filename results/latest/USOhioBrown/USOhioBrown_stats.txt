0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1034.37    32.79
p_loo       24.46        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      353   91.9%
 (0.5, 0.7]   (ok)         26    6.8%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.251  0.055   0.168    0.339  ...   116.0     115.0      29.0   1.00
pu        0.839  0.017   0.799    0.864  ...    47.0      53.0      79.0   1.02
mu        0.127  0.024   0.076    0.166  ...    62.0      63.0      48.0   0.99
mus       0.162  0.031   0.110    0.216  ...   152.0     147.0      96.0   1.03
gamma     0.173  0.037   0.106    0.228  ...    64.0      57.0      60.0   1.03
Is_begin  0.691  0.668   0.019    2.044  ...   103.0      76.0      87.0   1.04
Ia_begin  1.416  1.576   0.046    5.102  ...    78.0      57.0      95.0   1.04
E_begin   0.633  0.750   0.001    2.161  ...    63.0      29.0      65.0   1.05

[8 rows x 11 columns]