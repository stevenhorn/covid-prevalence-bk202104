0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1211.45    28.48
p_loo       21.35        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      364   94.8%
 (0.5, 0.7]   (ok)         16    4.2%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.286  0.040   0.212    0.344  ...    78.0      78.0      67.0   0.99
pu        0.888  0.010   0.872    0.900  ...    50.0      71.0      77.0   1.02
mu        0.126  0.024   0.074    0.158  ...    17.0      17.0      39.0   1.10
mus       0.151  0.029   0.105    0.213  ...   120.0     140.0      80.0   1.03
gamma     0.165  0.031   0.118    0.227  ...    55.0      40.0      60.0   1.07
Is_begin  0.882  0.823   0.016    2.568  ...   104.0      81.0      54.0   1.00
Ia_begin  0.727  0.541   0.016    1.572  ...    73.0      59.0      40.0   1.04
E_begin   0.720  0.715   0.009    1.845  ...   113.0     106.0      38.0   1.00

[8 rows x 11 columns]