0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -855.91    40.42
p_loo       36.72        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      365   95.1%
 (0.5, 0.7]   (ok)         16    4.2%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.205  0.039   0.151    0.266  ...   152.0     148.0      59.0   1.00
pu        0.717  0.012   0.700    0.738  ...    99.0      86.0      60.0   1.01
mu        0.137  0.024   0.102    0.179  ...    53.0      52.0      59.0   1.02
mus       0.231  0.032   0.176    0.292  ...   152.0     152.0      97.0   0.99
gamma     0.311  0.048   0.228    0.395  ...    59.0      72.0      43.0   1.03
Is_begin  0.534  0.517   0.017    1.540  ...    77.0      80.0      56.0   1.07
Ia_begin  0.711  0.751   0.007    1.528  ...    57.0      82.0      60.0   0.99
E_begin   0.369  0.354   0.010    1.117  ...    84.0      64.0      60.0   0.99

[8 rows x 11 columns]