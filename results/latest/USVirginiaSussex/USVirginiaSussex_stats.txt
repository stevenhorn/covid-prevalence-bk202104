0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -995.91    28.41
p_loo       33.82        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      353   91.9%
 (0.5, 0.7]   (ok)         20    5.2%
   (0.7, 1]   (bad)         9    2.3%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.252  0.051   0.164    0.340  ...    34.0      49.0      60.0   1.05
pu        0.747  0.030   0.705    0.801  ...    10.0      10.0      52.0   1.17
mu        0.165  0.026   0.122    0.211  ...    32.0      32.0      36.0   1.05
mus       0.243  0.037   0.183    0.304  ...    69.0      71.0      60.0   1.00
gamma     0.380  0.058   0.290    0.481  ...    80.0      82.0      60.0   0.99
Is_begin  0.672  0.705   0.003    2.257  ...   140.0     105.0      59.0   1.00
Ia_begin  1.092  1.108   0.005    3.748  ...    96.0      53.0      40.0   0.99
E_begin   0.479  0.492   0.003    1.388  ...   110.0      66.0      59.0   0.99

[8 rows x 11 columns]