0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1210.72    37.47
p_loo       25.85        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      360   93.8%
 (0.5, 0.7]   (ok)         19    4.9%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.260  0.060   0.175    0.344  ...    51.0      52.0      55.0   1.00
pu        0.845  0.021   0.807    0.878  ...    12.0      13.0      46.0   1.15
mu        0.116  0.020   0.084    0.144  ...    21.0      19.0      22.0   1.14
mus       0.177  0.040   0.123    0.255  ...   136.0     145.0      60.0   0.99
gamma     0.195  0.033   0.134    0.259  ...    57.0      55.0      56.0   1.01
Is_begin  0.816  0.873   0.002    2.360  ...   106.0      59.0      41.0   1.05
Ia_begin  2.008  1.921   0.040    5.640  ...    79.0      84.0      26.0   1.06
E_begin   0.837  0.920   0.026    2.630  ...    43.0      52.0      51.0   1.03

[8 rows x 11 columns]