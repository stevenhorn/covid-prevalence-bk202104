0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -852.57    27.96
p_loo       27.52        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      363   94.5%
 (0.5, 0.7]   (ok)         15    3.9%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.192  0.034   0.150    0.263  ...    22.0      21.0      59.0   1.10
pu        0.716  0.012   0.701    0.734  ...    74.0      55.0      42.0   1.00
mu        0.132  0.027   0.079    0.178  ...    37.0      38.0      36.0   1.07
mus       0.215  0.034   0.156    0.281  ...   111.0     102.0      46.0   1.06
gamma     0.266  0.041   0.192    0.340  ...   118.0     108.0      60.0   0.99
Is_begin  0.523  0.629   0.015    1.638  ...    88.0      35.0      59.0   1.06
Ia_begin  0.988  0.977   0.015    3.097  ...   117.0     106.0      88.0   0.99
E_begin   0.421  0.480   0.013    1.344  ...    96.0      71.0      96.0   1.03

[8 rows x 11 columns]