0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1018.46    27.41
p_loo       28.04        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      357   93.0%
 (0.5, 0.7]   (ok)         25    6.5%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.248  0.056   0.150    0.333  ...   152.0     152.0      46.0   1.05
pu        0.816  0.024   0.779    0.858  ...    64.0      69.0      60.0   1.00
mu        0.118  0.025   0.075    0.160  ...    17.0      13.0      56.0   1.14
mus       0.182  0.038   0.123    0.240  ...   152.0     152.0      99.0   1.00
gamma     0.190  0.045   0.121    0.271  ...    69.0      91.0      88.0   1.00
Is_begin  0.660  0.551   0.009    1.665  ...    63.0      52.0      38.0   1.02
Ia_begin  1.313  1.266   0.005    4.189  ...    83.0     113.0      58.0   1.07
E_begin   0.567  0.562   0.010    1.566  ...   127.0     103.0     102.0   1.00

[8 rows x 11 columns]