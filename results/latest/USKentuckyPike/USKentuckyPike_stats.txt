3 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1296.61    34.48
p_loo       27.02        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      369   96.1%
 (0.5, 0.7]   (ok)          9    2.3%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.263  0.055   0.172    0.342  ...    56.0      57.0      56.0   1.16
pu        0.821  0.018   0.793    0.854  ...    75.0      72.0      57.0   0.98
mu        0.121  0.023   0.093    0.165  ...     6.0       8.0      21.0   1.24
mus       0.150  0.033   0.090    0.203  ...   152.0     152.0     100.0   1.00
gamma     0.152  0.033   0.084    0.198  ...    41.0      28.0      53.0   1.07
Is_begin  0.635  0.559   0.023    1.881  ...    87.0     104.0     100.0   1.01
Ia_begin  1.618  1.816   0.084    5.058  ...   104.0     152.0      65.0   1.02
E_begin   0.749  0.929   0.016    2.031  ...    96.0     124.0      59.0   1.01

[8 rows x 11 columns]