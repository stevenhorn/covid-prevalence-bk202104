0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1103.57    70.92
p_loo       40.72        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      359   93.5%
 (0.5, 0.7]   (ok)         22    5.7%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.238  0.045   0.154    0.315  ...    52.0      47.0      22.0   1.07
pu        0.810  0.021   0.778    0.847  ...    19.0      16.0      39.0   1.12
mu        0.117  0.020   0.086    0.157  ...    11.0      10.0      60.0   1.15
mus       0.212  0.041   0.149    0.289  ...    41.0      39.0      87.0   1.05
gamma     0.242  0.041   0.181    0.319  ...    55.0      60.0      59.0   1.00
Is_begin  0.651  0.612   0.003    1.695  ...   108.0      73.0      58.0   1.00
Ia_begin  1.260  1.396   0.002    3.527  ...    46.0      17.0      46.0   1.10
E_begin   0.655  0.811   0.001    1.821  ...    41.0      12.0      59.0   1.14

[8 rows x 11 columns]