0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1444.97    62.76
p_loo       46.86        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      364   94.8%
 (0.5, 0.7]   (ok)         16    4.2%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.245  0.046   0.173    0.325  ...    41.0      42.0      60.0   1.07
pu        0.745  0.025   0.701    0.784  ...    33.0      28.0      24.0   1.10
mu        0.129  0.023   0.085    0.174  ...    30.0      29.0      37.0   1.15
mus       0.235  0.048   0.142    0.327  ...    74.0      72.0      59.0   1.00
gamma     0.303  0.047   0.224    0.388  ...    85.0     106.0      96.0   1.01
Is_begin  1.016  0.671   0.032    2.310  ...    45.0      39.0      38.0   1.02
Ia_begin  2.069  2.121   0.018    4.878  ...    54.0      53.0      33.0   1.07
E_begin   0.980  0.993   0.029    2.528  ...    77.0      45.0      58.0   1.01

[8 rows x 11 columns]