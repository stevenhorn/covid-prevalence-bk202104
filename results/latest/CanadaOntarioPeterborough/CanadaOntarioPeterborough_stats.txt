0 Divergences 
Passed validation 
Computed from 80 by 390 log-likelihood matrix

         Estimate       SE
elpd_loo  -831.16    38.87
p_loo       37.06        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      373   95.6%
 (0.5, 0.7]   (ok)         14    3.6%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    2    0.5%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.266   0.061   0.169    0.350  ...    52.0      42.0      40.0   1.06
pu         0.881   0.018   0.844    0.900  ...    68.0      75.0      60.0   1.01
mu         0.171   0.028   0.122    0.221  ...     9.0      10.0      49.0   1.17
mus        0.207   0.037   0.157    0.281  ...    77.0      88.0     102.0   1.01
gamma      0.285   0.049   0.198    0.365  ...    91.0      91.0      43.0   1.06
Is_begin   6.946   5.616   0.002   19.208  ...    19.0      78.0      58.0   1.05
Ia_begin  43.917  24.860   9.499   80.235  ...    56.0      53.0      97.0   1.04
E_begin   18.064  13.198   0.027   42.072  ...    49.0      29.0      40.0   1.06

[8 rows x 11 columns]