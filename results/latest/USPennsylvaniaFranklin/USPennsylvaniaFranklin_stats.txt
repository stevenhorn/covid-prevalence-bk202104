0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1529.01    28.68
p_loo       29.47        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      357   93.0%
 (0.5, 0.7]   (ok)         22    5.7%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.246  0.053   0.162    0.336  ...    34.0      42.0      59.0   1.04
pu        0.774  0.029   0.708    0.810  ...    28.0      26.0      60.0   1.06
mu        0.118  0.025   0.079    0.169  ...    28.0      25.0      40.0   1.06
mus       0.173  0.038   0.118    0.241  ...   152.0     152.0      49.0   0.99
gamma     0.218  0.040   0.154    0.294  ...   102.0     117.0      72.0   1.00
Is_begin  1.121  0.889   0.023    2.999  ...   112.0     148.0      59.0   1.01
Ia_begin  2.063  2.202   0.028    6.580  ...    44.0      35.0      55.0   1.03
E_begin   1.267  1.186   0.061    3.450  ...    56.0      58.0      73.0   1.01

[8 rows x 11 columns]