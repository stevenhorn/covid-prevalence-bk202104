0 Divergences 
Passed validation 
Computed from 80 by 385 log-likelihood matrix

         Estimate       SE
elpd_loo  -307.86    36.35
p_loo       34.16        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      355   92.2%
 (0.5, 0.7]   (ok)         24    6.2%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.269  0.052   0.181    0.346  ...    89.0      96.0      69.0   1.07
pu        0.877  0.016   0.843    0.899  ...    77.0      70.0      46.0   1.00
mu        0.139  0.022   0.105    0.184  ...    19.0      25.0      45.0   1.06
mus       0.186  0.036   0.124    0.255  ...    92.0      87.0      69.0   1.15
gamma     0.311  0.047   0.232    0.404  ...    58.0      65.0      96.0   1.02
Is_begin  1.199  1.099   0.005    3.319  ...    79.0      57.0      59.0   1.00
Ia_begin  0.779  0.737   0.001    2.362  ...    63.0      51.0      60.0   1.03
E_begin   0.880  1.182   0.006    3.447  ...   108.0      70.0      59.0   1.03

[8 rows x 11 columns]