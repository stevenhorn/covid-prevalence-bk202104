4 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -2056.19    39.91
p_loo       31.06        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      348   90.6%
 (0.5, 0.7]   (ok)         31    8.1%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    1    0.3%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.238   0.052   0.156    0.337  ...    30.0      31.0      60.0   1.05
pu         0.751   0.042   0.701    0.830  ...     6.0       7.0      22.0   1.30
mu         0.127   0.022   0.091    0.167  ...     8.0       8.0      33.0   1.22
mus        0.182   0.042   0.116    0.268  ...   124.0     140.0      69.0   1.00
gamma      0.275   0.061   0.174    0.391  ...    21.0      39.0      22.0   1.07
Is_begin   6.223   4.750   0.199   13.093  ...    61.0      26.0      38.0   1.06
Ia_begin  19.231  10.170   1.468   35.046  ...    56.0      58.0      42.0   1.08
E_begin   16.262  14.493   0.250   46.888  ...    74.0      51.0      56.0   1.04

[8 rows x 11 columns]