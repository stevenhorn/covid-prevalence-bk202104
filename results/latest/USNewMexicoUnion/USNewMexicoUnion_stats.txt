0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -618.46    50.61
p_loo       43.94        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      351   91.4%
 (0.5, 0.7]   (ok)         27    7.0%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.270  0.054   0.174    0.348  ...    33.0      32.0      15.0   1.02
pu        0.792  0.045   0.704    0.850  ...     3.0       4.0      17.0   1.88
mu        0.177  0.031   0.123    0.230  ...     8.0       8.0      60.0   1.21
mus       0.224  0.038   0.142    0.286  ...    84.0      82.0      40.0   1.21
gamma     0.306  0.053   0.229    0.421  ...    55.0      59.0      60.0   1.02
Is_begin  0.740  0.619   0.006    2.111  ...    80.0      62.0      86.0   1.05
Ia_begin  1.526  1.658   0.010    5.144  ...    75.0      42.0      43.0   1.05
E_begin   0.695  0.697   0.025    1.942  ...    45.0      23.0      81.0   1.07

[8 rows x 11 columns]