0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1668.34    41.57
p_loo       37.02        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      358   93.2%
 (0.5, 0.7]   (ok)         20    5.2%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.260  0.056   0.164    0.346  ...   120.0     121.0      59.0   1.04
pu        0.809  0.037   0.733    0.873  ...    33.0      30.0      42.0   1.06
mu        0.127  0.025   0.073    0.166  ...    59.0      58.0      51.0   1.01
mus       0.165  0.027   0.114    0.213  ...    95.0      95.0      93.0   0.99
gamma     0.199  0.038   0.146    0.282  ...   152.0     152.0      69.0   1.02
Is_begin  0.752  0.667   0.004    1.987  ...   103.0     106.0      58.0   1.00
Ia_begin  1.525  1.583   0.064    4.829  ...    74.0      60.0      81.0   1.01
E_begin   0.780  0.933   0.002    2.479  ...   107.0      57.0      57.0   1.01

[8 rows x 11 columns]