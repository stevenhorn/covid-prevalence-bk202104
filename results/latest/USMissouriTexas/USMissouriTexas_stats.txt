0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1170.91    63.51
p_loo       43.50        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      372   96.9%
 (0.5, 0.7]   (ok)          8    2.1%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.198  0.042   0.151    0.275  ...    94.0     126.0      38.0   1.02
pu        0.713  0.013   0.700    0.738  ...   152.0     152.0      93.0   0.99
mu        0.112  0.014   0.088    0.135  ...    19.0      20.0      59.0   1.08
mus       0.240  0.037   0.160    0.293  ...    79.0      78.0      58.0   1.00
gamma     0.435  0.068   0.308    0.540  ...    66.0      67.0      61.0   1.01
Is_begin  0.533  0.533   0.004    1.481  ...    76.0      69.0      93.0   1.01
Ia_begin  0.885  0.918   0.004    2.961  ...    85.0      83.0     100.0   1.02
E_begin   0.422  0.682   0.004    1.392  ...    99.0      50.0      55.0   1.02

[8 rows x 11 columns]