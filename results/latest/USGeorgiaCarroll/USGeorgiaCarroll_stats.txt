0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1679.81    68.75
p_loo       41.22        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      363   94.5%
 (0.5, 0.7]   (ok)         17    4.4%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.239  0.051   0.150    0.333  ...    96.0      97.0      33.0   1.00
pu        0.745  0.030   0.701    0.800  ...    52.0      50.0      56.0   1.01
mu        0.126  0.016   0.090    0.148  ...    40.0      39.0      43.0   1.04
mus       0.288  0.048   0.189    0.376  ...    81.0      80.0      96.0   1.01
gamma     0.425  0.070   0.299    0.528  ...   123.0     127.0      60.0   1.03
Is_begin  2.885  2.578   0.119    8.371  ...   108.0      81.0      69.0   1.03
Ia_begin  5.948  4.085   0.227   13.671  ...    74.0      55.0      95.0   1.04
E_begin   3.887  3.760   0.021   11.716  ...    42.0      28.0      56.0   1.07

[8 rows x 11 columns]