0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1454.63    28.12
p_loo       28.28        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      356   92.7%
 (0.5, 0.7]   (ok)         19    4.9%
   (0.7, 1]   (bad)         9    2.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.258  0.056   0.166    0.345  ...   110.0      98.0      60.0   1.01
pu        0.757  0.023   0.711    0.794  ...    69.0      68.0      59.0   1.02
mu        0.145  0.036   0.080    0.219  ...     4.0       4.0      14.0   1.60
mus       0.185  0.037   0.136    0.270  ...    25.0      30.0      88.0   1.07
gamma     0.235  0.041   0.164    0.312  ...    54.0      42.0      55.0   1.04
Is_begin  1.894  1.473   0.012    4.400  ...   152.0     114.0      60.0   1.01
Ia_begin  6.203  3.412   0.573   12.113  ...   109.0     140.0      83.0   0.99
E_begin   4.862  4.472   0.277   14.848  ...    56.0      78.0      40.0   1.00

[8 rows x 11 columns]