2 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1889.83    26.95
p_loo       20.35        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      352   91.7%
 (0.5, 0.7]   (ok)         25    6.5%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.281  0.038   0.214    0.344  ...    30.0      38.0      58.0   1.06
pu        0.881  0.024   0.828    0.900  ...    12.0      19.0      19.0   1.18
mu        0.140  0.040   0.082    0.204  ...     3.0       3.0      17.0   2.06
mus       0.159  0.025   0.120    0.204  ...    28.0      36.0      58.0   1.05
gamma     0.157  0.035   0.108    0.232  ...    15.0      16.0      38.0   1.10
Is_begin  3.999  3.144   0.096    9.486  ...    21.0      22.0      32.0   1.07
Ia_begin  7.949  6.032   0.061   18.865  ...    21.0      14.0      59.0   1.15
E_begin   6.485  6.627   0.207   18.093  ...    10.0       8.0      34.0   1.21

[8 rows x 11 columns]