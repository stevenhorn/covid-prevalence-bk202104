0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -746.30    30.17
p_loo       32.43        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      356   92.7%
 (0.5, 0.7]   (ok)         22    5.7%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.262  0.058   0.165    0.349  ...   137.0     139.0      38.0   1.06
pu        0.847  0.042   0.772    0.897  ...    85.0      87.0      47.0   1.01
mu        0.131  0.025   0.081    0.179  ...    57.0      56.0      84.0   1.01
mus       0.164  0.041   0.100    0.225  ...   146.0     127.0      79.0   0.99
gamma     0.209  0.035   0.149    0.275  ...    42.0      69.0      40.0   1.04
Is_begin  0.924  0.741   0.030    2.418  ...   118.0      76.0      47.0   1.01
Ia_begin  1.962  2.227   0.034    6.724  ...   105.0     152.0      73.0   0.98
E_begin   1.263  2.108   0.014    2.959  ...    91.0      95.0      99.0   0.99

[8 rows x 11 columns]