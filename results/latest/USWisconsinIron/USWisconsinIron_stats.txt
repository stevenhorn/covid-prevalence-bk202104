0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -818.20    38.59
p_loo       36.17        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      343   89.3%
 (0.5, 0.7]   (ok)         37    9.6%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.248  0.051   0.165    0.343  ...   152.0     152.0     102.0   0.99
pu        0.765  0.025   0.715    0.806  ...    74.0      78.0      39.0   1.03
mu        0.152  0.026   0.112    0.208  ...    78.0      77.0      88.0   1.03
mus       0.192  0.032   0.149    0.263  ...   120.0     124.0     100.0   1.00
gamma     0.240  0.042   0.165    0.315  ...   144.0     144.0      42.0   1.01
Is_begin  0.570  0.534   0.015    1.515  ...   106.0     123.0      91.0   1.02
Ia_begin  1.314  1.321   0.037    3.595  ...   121.0     152.0      95.0   0.98
E_begin   0.564  0.519   0.012    1.614  ...   116.0     114.0      75.0   0.99

[8 rows x 11 columns]