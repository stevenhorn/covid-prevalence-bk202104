0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -926.47    32.23
p_loo       26.55        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      329   85.7%
 (0.5, 0.7]   (ok)         47   12.2%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.239  0.060   0.153    0.325  ...    13.0      13.0      56.0   1.14
pu        0.857  0.020   0.817    0.888  ...     7.0       7.0      26.0   1.27
mu        0.118  0.017   0.092    0.157  ...    70.0      68.0      60.0   0.99
mus       0.173  0.029   0.129    0.239  ...    36.0      48.0      87.0   1.02
gamma     0.207  0.052   0.126    0.312  ...   152.0     152.0     100.0   0.99
Is_begin  0.636  0.620   0.017    1.623  ...    81.0      80.0      59.0   1.00
Ia_begin  1.128  1.299   0.008    4.230  ...    86.0      46.0      53.0   0.99
E_begin   0.505  0.523   0.005    1.749  ...    80.0      45.0      60.0   1.04

[8 rows x 11 columns]