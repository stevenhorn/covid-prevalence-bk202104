0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -656.77    61.83
p_loo       51.01        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      373   97.1%
 (0.5, 0.7]   (ok)          9    2.3%
   (0.7, 1]   (bad)         0    0.0%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.228  0.053   0.150    0.313  ...    51.0      41.0      40.0   1.08
pu        0.731  0.025   0.701    0.770  ...    41.0      31.0      39.0   1.06
mu        0.155  0.032   0.105    0.203  ...     9.0      10.0      15.0   1.19
mus       0.269  0.042   0.191    0.343  ...    24.0      20.0      31.0   1.09
gamma     0.385  0.065   0.295    0.521  ...    22.0      21.0      57.0   1.08
Is_begin  0.553  0.578   0.000    1.684  ...    41.0      41.0      39.0   1.06
Ia_begin  0.699  1.172   0.008    2.426  ...    19.0       6.0      88.0   1.33
E_begin   0.281  0.389   0.001    0.967  ...    57.0      30.0      43.0   1.11

[8 rows x 11 columns]