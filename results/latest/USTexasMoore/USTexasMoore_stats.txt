0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1374.44    45.95
p_loo       44.38        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      356   92.7%
 (0.5, 0.7]   (ok)         23    6.0%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.234  0.046   0.163    0.312  ...   123.0     133.0      96.0   1.00
pu        0.758  0.034   0.702    0.800  ...    18.0      18.0      74.0   1.10
mu        0.125  0.020   0.094    0.164  ...    19.0      23.0      69.0   1.09
mus       0.192  0.038   0.137    0.268  ...    18.0      17.0      57.0   1.10
gamma     0.297  0.053   0.187    0.387  ...    53.0      51.0      45.0   1.03
Is_begin  0.566  0.640   0.012    1.961  ...    76.0      54.0      48.0   1.01
Ia_begin  1.524  1.671   0.003    4.539  ...    85.0      73.0      43.0   1.00
E_begin   0.628  0.704   0.007    2.211  ...    86.0      74.0      59.0   1.02

[8 rows x 11 columns]