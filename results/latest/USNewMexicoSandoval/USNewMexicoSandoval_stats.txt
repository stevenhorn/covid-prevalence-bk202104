0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1444.83    35.16
p_loo       35.89        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      361   94.0%
 (0.5, 0.7]   (ok)         16    4.2%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.238  0.053   0.151    0.330  ...    87.0      80.0      24.0   1.03
pu        0.760  0.037   0.703    0.826  ...    85.0     105.0     100.0   1.02
mu        0.123  0.023   0.082    0.154  ...    11.0      10.0      40.0   1.19
mus       0.199  0.035   0.128    0.254  ...   132.0     130.0      54.0   1.01
gamma     0.303  0.052   0.211    0.395  ...   122.0     136.0      39.0   1.01
Is_begin  0.811  0.555   0.028    1.741  ...    61.0      43.0      21.0   1.07
Ia_begin  1.914  1.186   0.194    3.963  ...    88.0      88.0      59.0   1.06
E_begin   2.012  1.904   0.073    5.905  ...    86.0      67.0      38.0   1.01

[8 rows x 11 columns]