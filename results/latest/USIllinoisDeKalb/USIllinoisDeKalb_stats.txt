0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1314.32    27.36
p_loo       25.72        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      362   94.3%
 (0.5, 0.7]   (ok)         16    4.2%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.244  0.054   0.153    0.323  ...    75.0      76.0      46.0   1.11
pu        0.803  0.034   0.728    0.852  ...    27.0      30.0      19.0   1.10
mu        0.107  0.021   0.077    0.150  ...    27.0      31.0      45.0   1.04
mus       0.171  0.031   0.124    0.234  ...    90.0     122.0     100.0   1.02
gamma     0.205  0.034   0.145    0.276  ...   117.0     118.0      86.0   0.99
Is_begin  1.215  1.255   0.006    3.453  ...    69.0     100.0      60.0   1.04
Ia_begin  2.594  2.174   0.180    6.449  ...    60.0      67.0      43.0   1.03
E_begin   1.487  1.467   0.084    4.503  ...    89.0      75.0      87.0   1.01

[8 rows x 11 columns]