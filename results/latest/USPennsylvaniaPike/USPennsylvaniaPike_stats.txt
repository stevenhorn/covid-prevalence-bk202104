0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -979.93    26.34
p_loo       28.97        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      345   89.8%
 (0.5, 0.7]   (ok)         29    7.6%
   (0.7, 1]   (bad)        10    2.6%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.251  0.060   0.164    0.348  ...   152.0     152.0      24.0   1.14
pu        0.790  0.047   0.705    0.863  ...     9.0       9.0      59.0   1.18
mu        0.127  0.022   0.092    0.171  ...    10.0       8.0      21.0   1.19
mus       0.183  0.028   0.130    0.231  ...   121.0     105.0      88.0   1.00
gamma     0.295  0.055   0.199    0.386  ...    79.0      66.0      42.0   1.01
Is_begin  1.871  1.282   0.075    4.275  ...    57.0      50.0      56.0   1.01
Ia_begin  0.956  0.565   0.112    2.044  ...    99.0     126.0      49.0   1.00
E_begin   2.366  2.088   0.027    6.473  ...    88.0      95.0      91.0   1.01

[8 rows x 11 columns]