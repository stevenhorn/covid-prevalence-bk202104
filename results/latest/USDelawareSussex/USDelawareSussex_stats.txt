0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1843.25    36.15
p_loo       36.63        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      342   89.1%
 (0.5, 0.7]   (ok)         34    8.9%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.256  0.057   0.159    0.346  ...   110.0      93.0      40.0   1.02
pu        0.765  0.028   0.712    0.808  ...    60.0      61.0      67.0   1.00
mu        0.124  0.019   0.090    0.164  ...    44.0      44.0      59.0   1.05
mus       0.188  0.028   0.133    0.238  ...   141.0     146.0      86.0   1.01
gamma     0.290  0.038   0.226    0.362  ...   152.0     152.0      75.0   1.05
Is_begin  1.744  0.903   0.201    3.125  ...    84.0      78.0      95.0   1.01
Ia_begin  3.195  2.671   0.063    8.326  ...    89.0      50.0      29.0   1.06
E_begin   2.907  2.622   0.149    8.960  ...    32.0      14.0      41.0   1.10

[8 rows x 11 columns]