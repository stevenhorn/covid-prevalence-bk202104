0 Divergences 
Passed validation 
Computed from 80 by 390 log-likelihood matrix

         Estimate       SE
elpd_loo  -348.19    44.74
p_loo       38.98        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      365   93.6%
 (0.5, 0.7]   (ok)         20    5.1%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.248  0.047   0.163    0.319  ...   115.0     108.0      93.0   1.04
pu        0.789  0.034   0.723    0.845  ...    55.0      58.0      59.0   0.99
mu        0.159  0.031   0.097    0.221  ...    43.0      43.0      53.0   1.02
mus       0.284  0.050   0.199    0.368  ...    46.0      46.0      59.0   1.01
gamma     0.335  0.059   0.239    0.432  ...    66.0      63.0      58.0   1.02
Is_begin  3.357  2.945   0.080    9.446  ...    65.0      45.0      36.0   1.02
Ia_begin  5.625  5.492   0.036   18.999  ...    24.0      13.0      22.0   1.11
E_begin   2.921  2.676   0.036    7.966  ...    37.0      39.0      60.0   1.02

[8 rows x 11 columns]