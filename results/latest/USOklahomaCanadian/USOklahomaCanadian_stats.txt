6 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1622.45    32.74
p_loo       26.68        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      363   94.5%
 (0.5, 0.7]   (ok)         15    3.9%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.244  0.059   0.150    0.332  ...    11.0      10.0      22.0   1.20
pu        0.754  0.033   0.703    0.804  ...    25.0      27.0      22.0   1.05
mu        0.135  0.028   0.076    0.180  ...    28.0      29.0      23.0   1.01
mus       0.172  0.032   0.121    0.238  ...    53.0      69.0      60.0   1.00
gamma     0.188  0.036   0.142    0.268  ...    76.0      82.0      93.0   0.99
Is_begin  0.751  0.742   0.014    2.140  ...    84.0      45.0      17.0   1.02
Ia_begin  0.667  0.529   0.008    1.708  ...   123.0      86.0      55.0   1.00
E_begin   0.403  0.342   0.002    1.071  ...    60.0      59.0      56.0   0.99

[8 rows x 11 columns]