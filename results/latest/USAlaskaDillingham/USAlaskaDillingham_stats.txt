0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -604.61    56.69
p_loo       47.57        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      359   93.5%
 (0.5, 0.7]   (ok)         20    5.2%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.216  0.056   0.150    0.318  ...    25.0      23.0      43.0   1.07
pu        0.742  0.038   0.701    0.816  ...     4.0       5.0      14.0   1.39
mu        0.154  0.034   0.083    0.212  ...    18.0      18.0      59.0   1.08
mus       0.244  0.041   0.185    0.334  ...    23.0      26.0      61.0   1.07
gamma     0.284  0.047   0.196    0.364  ...   152.0     136.0      54.0   1.00
Is_begin  0.312  0.419   0.004    1.334  ...   107.0      73.0      48.0   1.00
Ia_begin  0.508  0.615   0.005    1.886  ...    57.0      85.0      36.0   1.00
E_begin   0.253  0.364   0.002    0.933  ...    85.0     121.0      59.0   1.00

[8 rows x 11 columns]