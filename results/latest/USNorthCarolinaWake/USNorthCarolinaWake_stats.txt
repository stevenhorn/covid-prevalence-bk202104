56 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -2238.51    31.87
p_loo       25.02        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      339   88.3%
 (0.5, 0.7]   (ok)         31    8.1%
   (0.7, 1]   (bad)        12    3.1%
   (1, Inf)   (very bad)    2    0.5%

            mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.261  0.038   0.173    0.328  ...    15.0      15.0      55.0   1.55
pu         0.821  0.027   0.789    0.864  ...     4.0       4.0      22.0   1.77
mu         0.117  0.012   0.091    0.133  ...     6.0       6.0      20.0   1.83
mus        0.174  0.030   0.107    0.200  ...     5.0       8.0      59.0   1.69
gamma      0.191  0.032   0.131    0.248  ...    56.0      42.0      55.0   1.80
Is_begin   4.840  2.633   0.711    9.631  ...    68.0      17.0      39.0   1.74
Ia_begin  18.589  9.407   2.158   29.187  ...     3.0       4.0      34.0   1.58
E_begin    7.293  4.633   0.221   14.539  ...    27.0      19.0      18.0   1.61

[8 rows x 11 columns]