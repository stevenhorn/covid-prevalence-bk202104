0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1096.52    45.05
p_loo       47.50        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      352   91.7%
 (0.5, 0.7]   (ok)         26    6.8%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.253  0.060   0.150    0.333  ...   152.0     152.0      48.0   1.00
pu        0.788  0.041   0.701    0.847  ...    21.0      23.0      31.0   1.08
mu        0.177  0.027   0.129    0.224  ...    62.0      67.0      60.0   1.03
mus       0.250  0.037   0.174    0.309  ...    83.0     133.0      91.0   1.02
gamma     0.389  0.078   0.234    0.513  ...   126.0     122.0      88.0   1.01
Is_begin  0.944  0.988   0.015    2.919  ...   146.0     100.0      59.0   1.01
Ia_begin  2.284  2.250   0.065    7.641  ...   116.0     147.0      86.0   0.99
E_begin   1.064  1.017   0.033    2.943  ...   116.0      86.0      57.0   0.98

[8 rows x 11 columns]