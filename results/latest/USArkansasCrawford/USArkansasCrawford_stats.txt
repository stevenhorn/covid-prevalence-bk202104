0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1254.03    29.24
p_loo       22.97        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      359   93.5%
 (0.5, 0.7]   (ok)         18    4.7%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.261  0.056   0.160    0.346  ...    52.0      51.0      38.0   1.05
pu        0.791  0.021   0.753    0.833  ...    17.0      18.0      67.0   1.10
mu        0.109  0.021   0.073    0.143  ...    11.0       9.0      40.0   1.20
mus       0.163  0.033   0.117    0.233  ...    66.0      66.0      59.0   0.98
gamma     0.160  0.032   0.113    0.228  ...    88.0      85.0      96.0   1.02
Is_begin  0.597  0.629   0.004    1.908  ...    91.0     105.0      59.0   1.02
Ia_begin  1.016  1.108   0.023    3.256  ...    27.0      18.0      24.0   1.09
E_begin   0.466  0.588   0.002    1.597  ...    48.0      33.0      37.0   1.05

[8 rows x 11 columns]