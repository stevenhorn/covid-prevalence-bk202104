0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1035.93    34.70
p_loo       26.84        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      354   92.2%
 (0.5, 0.7]   (ok)         23    6.0%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.293  0.039   0.239    0.349  ...   123.0      97.0      40.0   1.01
pu        0.890  0.010   0.873    0.900  ...   152.0     131.0      42.0   1.04
mu        0.143  0.020   0.107    0.177  ...    38.0      38.0      39.0   1.00
mus       0.185  0.030   0.130    0.232  ...   124.0     148.0      61.0   1.02
gamma     0.215  0.042   0.155    0.304  ...    95.0     152.0      87.0   1.00
Is_begin  1.495  1.073   0.034    3.647  ...    41.0      30.0      44.0   1.06
Ia_begin  3.089  2.716   0.012    8.051  ...    52.0      39.0      60.0   0.99
E_begin   1.830  1.844   0.000    5.900  ...    88.0      58.0      60.0   1.00

[8 rows x 11 columns]