0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1292.97    28.95
p_loo       22.49        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      347   90.4%
 (0.5, 0.7]   (ok)         28    7.3%
   (0.7, 1]   (bad)         9    2.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.263  0.051   0.168    0.330  ...    58.0      62.0      59.0   1.06
pu        0.806  0.017   0.779    0.839  ...    37.0      34.0      87.0   1.05
mu        0.145  0.025   0.107    0.196  ...    15.0      15.0      58.0   1.12
mus       0.183  0.039   0.124    0.260  ...    88.0     112.0      60.0   1.02
gamma     0.214  0.035   0.151    0.267  ...   147.0     137.0     100.0   0.99
Is_begin  1.142  1.087   0.003    3.196  ...    56.0      94.0      22.0   1.00
Ia_begin  0.728  0.569   0.080    1.824  ...   152.0     152.0      23.0   0.99
E_begin   0.641  0.720   0.003    2.279  ...   119.0      56.0      40.0   1.03

[8 rows x 11 columns]