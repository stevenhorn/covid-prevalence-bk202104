1 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1392.31    61.46
p_loo       55.20        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      358   93.2%
 (0.5, 0.7]   (ok)         18    4.7%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.216  0.044   0.159    0.298  ...   130.0     125.0      66.0   1.03
pu        0.724  0.021   0.701    0.762  ...    20.0      31.0      57.0   1.08
mu        0.111  0.022   0.077    0.147  ...    33.0      31.0      40.0   1.05
mus       0.244  0.045   0.157    0.322  ...    71.0      71.0      46.0   1.02
gamma     0.378  0.072   0.240    0.484  ...    57.0      58.0      78.0   1.01
Is_begin  0.883  1.096   0.011    2.869  ...    74.0      73.0      59.0   0.99
Ia_begin  1.644  1.801   0.008    4.867  ...   117.0      48.0      35.0   1.06
E_begin   0.585  0.824   0.002    2.012  ...    89.0      34.0      54.0   1.07

[8 rows x 11 columns]