0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -740.02    66.07
p_loo       41.15        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      359   93.5%
 (0.5, 0.7]   (ok)         21    5.5%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.173  0.018   0.151    0.207  ...   102.0      83.0      96.0   1.01
pu        0.707  0.005   0.700    0.716  ...   147.0     120.0      96.0   1.01
mu        0.126  0.023   0.083    0.165  ...    79.0      86.0      63.0   1.01
mus       0.217  0.037   0.142    0.276  ...    55.0      50.0      60.0   1.04
gamma     0.226  0.035   0.155    0.278  ...    99.0      73.0      60.0   1.03
Is_begin  0.577  0.563   0.019    1.526  ...    55.0      68.0      49.0   1.04
Ia_begin  1.218  1.357   0.013    4.022  ...    17.0      24.0      74.0   1.08
E_begin   0.419  0.508   0.011    1.417  ...    19.0      28.0      40.0   1.05

[8 rows x 11 columns]