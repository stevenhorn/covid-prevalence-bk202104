0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -936.55    23.80
p_loo       25.28        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      350   91.1%
 (0.5, 0.7]   (ok)         30    7.8%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.259  0.059   0.163    0.346  ...    76.0      65.0      38.0   1.03
pu        0.782  0.039   0.708    0.844  ...    53.0      51.0      60.0   1.01
mu        0.144  0.024   0.106    0.186  ...    66.0      60.0      49.0   1.00
mus       0.197  0.029   0.143    0.240  ...   152.0     152.0      80.0   0.99
gamma     0.273  0.046   0.200    0.365  ...   128.0     119.0      99.0   1.00
Is_begin  1.537  1.093   0.207    3.481  ...   105.0      75.0      95.0   1.00
Ia_begin  0.827  0.517   0.046    1.814  ...    78.0      72.0      54.0   1.00
E_begin   1.270  1.293   0.013    3.512  ...    74.0      43.0      60.0   1.04

[8 rows x 11 columns]