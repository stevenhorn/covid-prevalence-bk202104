0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -244.69    61.33
p_loo       50.30        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      348   90.6%
 (0.5, 0.7]   (ok)         26    6.8%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    5    1.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.252  0.055   0.173    0.346  ...    76.0      79.0      59.0   0.99
pu        0.874  0.013   0.849    0.896  ...    63.0      57.0      23.0   1.00
mu        0.084  0.016   0.061    0.116  ...    80.0      72.0      60.0   1.03
mus       0.243  0.044   0.171    0.324  ...    45.0      35.0      87.0   1.05
gamma     0.317  0.053   0.205    0.392  ...   121.0     116.0      19.0   1.10
Is_begin  0.035  0.069   0.000    0.144  ...    37.0      32.0      48.0   1.10
Ia_begin  0.028  0.049   0.000    0.132  ...    41.0      28.0      42.0   1.06
E_begin   0.020  0.046   0.000    0.060  ...    55.0      56.0      22.0   1.12

[8 rows x 11 columns]