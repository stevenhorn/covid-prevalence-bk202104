0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -475.85    28.16
p_loo       30.90        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      348   90.6%
 (0.5, 0.7]   (ok)         32    8.3%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.250  0.057   0.152    0.339  ...    77.0      83.0      60.0   1.13
pu        0.825  0.026   0.767    0.867  ...     8.0      10.0      24.0   1.19
mu        0.144  0.026   0.097    0.194  ...     7.0       7.0      39.0   1.29
mus       0.180  0.032   0.126    0.247  ...   152.0     152.0      59.0   1.04
gamma     0.214  0.040   0.154    0.297  ...    87.0      79.0      93.0   1.01
Is_begin  0.500  0.511   0.008    1.518  ...   101.0      60.0      40.0   1.04
Ia_begin  0.974  1.025   0.001    3.351  ...   109.0     125.0      49.0   1.00
E_begin   0.388  0.474   0.005    1.335  ...    62.0      29.0      76.0   1.07

[8 rows x 11 columns]