1 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1071.77    69.82
p_loo       37.92        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      353   91.9%
 (0.5, 0.7]   (ok)         26    6.8%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.243  0.053   0.155    0.328  ...    39.0      39.0      39.0   1.03
pu        0.812  0.017   0.780    0.838  ...    61.0      63.0      44.0   1.01
mu        0.138  0.022   0.104    0.173  ...     8.0       9.0      47.0   1.19
mus       0.296  0.045   0.234    0.375  ...    18.0      17.0      66.0   1.09
gamma     0.461  0.074   0.336    0.594  ...   106.0     102.0      74.0   1.01
Is_begin  0.869  0.744   0.078    2.518  ...    87.0      76.0      59.0   1.00
Ia_begin  1.447  1.750   0.041    5.143  ...    66.0      59.0      56.0   1.03
E_begin   0.539  0.765   0.003    1.617  ...    71.0      53.0      56.0   1.03

[8 rows x 11 columns]