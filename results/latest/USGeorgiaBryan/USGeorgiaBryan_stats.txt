0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1145.54    33.18
p_loo       29.81        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      343   89.3%
 (0.5, 0.7]   (ok)         35    9.1%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.257  0.046   0.180    0.344  ...    69.0      69.0      60.0   1.02
pu        0.801  0.021   0.764    0.834  ...    45.0      49.0      60.0   1.03
mu        0.126  0.024   0.089    0.167  ...    24.0      17.0      56.0   1.12
mus       0.173  0.033   0.102    0.231  ...   152.0     152.0      48.0   1.16
gamma     0.211  0.049   0.133    0.306  ...   152.0     152.0      59.0   1.06
Is_begin  1.013  0.862   0.020    2.632  ...   135.0      93.0      31.0   1.00
Ia_begin  2.770  2.244   0.128    6.904  ...    84.0      68.0      54.0   1.06
E_begin   1.102  1.077   0.014    2.746  ...    92.0      42.0      40.0   1.01

[8 rows x 11 columns]