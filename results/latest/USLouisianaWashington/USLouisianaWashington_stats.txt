0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1462.40    50.13
p_loo       55.97        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      368   95.8%
 (0.5, 0.7]   (ok)         12    3.1%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.236  0.042   0.172    0.310  ...     9.0       9.0      24.0   1.21
pu        0.778  0.023   0.727    0.813  ...     7.0       6.0      14.0   1.30
mu        0.188  0.064   0.109    0.259  ...     3.0       4.0      40.0   1.83
mus       0.201  0.029   0.151    0.255  ...    14.0      16.0      60.0   1.14
gamma     0.206  0.080   0.122    0.355  ...     3.0       4.0      38.0   1.86
Is_begin  1.911  1.089   0.101    3.980  ...     5.0       6.0      38.0   1.35
Ia_begin  0.478  0.464   0.005    1.214  ...    10.0       8.0      29.0   1.26
E_begin   0.865  0.856   0.006    2.635  ...    14.0       9.0      48.0   1.19

[8 rows x 11 columns]