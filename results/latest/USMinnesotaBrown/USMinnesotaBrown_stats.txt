0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -915.69    28.29
p_loo       21.61        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      367   95.6%
 (0.5, 0.7]   (ok)         15    3.9%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.251  0.057   0.159    0.339  ...    63.0      70.0      40.0   1.00
pu        0.753  0.025   0.718    0.800  ...    58.0      58.0      99.0   1.00
mu        0.120  0.021   0.092    0.170  ...    29.0      32.0      60.0   1.05
mus       0.180  0.030   0.135    0.238  ...   112.0     117.0      83.0   0.99
gamma     0.221  0.028   0.170    0.274  ...   149.0     140.0     100.0   1.00
Is_begin  0.789  0.694   0.001    1.993  ...   152.0     152.0      72.0   1.01
Ia_begin  1.261  1.321   0.039    3.924  ...    38.0      35.0      59.0   1.06
E_begin   0.548  0.592   0.016    1.679  ...   114.0     139.0     100.0   1.01

[8 rows x 11 columns]