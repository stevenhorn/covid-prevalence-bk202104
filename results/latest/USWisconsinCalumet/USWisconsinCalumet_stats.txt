0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1223.45    43.97
p_loo       31.82        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      360   93.8%
 (0.5, 0.7]   (ok)         19    4.9%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.250  0.053   0.160    0.330  ...   115.0     107.0      93.0   0.99
pu        0.760  0.024   0.721    0.797  ...    66.0      72.0      32.0   1.01
mu        0.112  0.020   0.063    0.146  ...    34.0      36.0      58.0   1.01
mus       0.172  0.033   0.118    0.234  ...    90.0      97.0      60.0   1.00
gamma     0.185  0.040   0.114    0.259  ...   106.0     108.0      93.0   1.01
Is_begin  0.566  0.552   0.000    1.536  ...   105.0      52.0      57.0   0.99
Ia_begin  1.212  1.139   0.031    3.666  ...   106.0      68.0      74.0   1.03
E_begin   0.523  0.515   0.012    1.388  ...    96.0      38.0      60.0   1.06

[8 rows x 11 columns]