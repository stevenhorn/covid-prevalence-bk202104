0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -944.68    47.15
p_loo       37.52        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      353   91.9%
 (0.5, 0.7]   (ok)         21    5.5%
   (0.7, 1]   (bad)         9    2.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.171  0.016   0.150    0.203  ...   152.0     152.0      60.0   1.02
pu        0.706  0.004   0.700    0.712  ...   118.0     102.0      36.0   1.03
mu        0.088  0.011   0.073    0.113  ...    89.0      98.0      93.0   1.02
mus       0.287  0.043   0.226    0.380  ...    97.0     104.0      60.0   0.98
gamma     0.390  0.041   0.302    0.456  ...    94.0      89.0      93.0   0.99
Is_begin  0.493  0.627   0.001    1.808  ...    82.0     152.0      45.0   0.99
Ia_begin  0.543  0.634   0.010    1.688  ...    69.0      57.0      38.0   1.02
E_begin   0.260  0.320   0.001    0.951  ...    66.0      51.0      42.0   1.04

[8 rows x 11 columns]