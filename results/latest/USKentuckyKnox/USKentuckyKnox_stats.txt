0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1226.98    43.35
p_loo       31.44        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      356   92.7%
 (0.5, 0.7]   (ok)         24    6.2%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.243  0.055   0.153    0.337  ...    57.0      63.0      42.0   1.07
pu        0.748  0.026   0.704    0.788  ...    44.0      46.0      24.0   1.10
mu        0.120  0.025   0.072    0.161  ...    63.0      60.0      61.0   1.01
mus       0.164  0.030   0.122    0.222  ...    73.0      73.0      99.0   0.99
gamma     0.176  0.038   0.090    0.223  ...    99.0      91.0      81.0   1.00
Is_begin  0.486  0.506   0.011    1.484  ...    91.0      83.0      87.0   1.01
Ia_begin  0.833  0.916   0.031    2.882  ...   128.0     128.0     102.0   1.01
E_begin   0.395  0.401   0.003    1.099  ...    84.0      67.0      60.0   0.99

[8 rows x 11 columns]