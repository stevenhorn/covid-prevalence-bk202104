0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1133.29    63.95
p_loo       51.56        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      356   92.7%
 (0.5, 0.7]   (ok)         21    5.5%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.239  0.052   0.154    0.323  ...    96.0      94.0      58.0   1.02
pu        0.741  0.025   0.704    0.785  ...    87.0      97.0      83.0   1.01
mu        0.188  0.027   0.149    0.249  ...    44.0      43.0      72.0   1.01
mus       0.270  0.045   0.197    0.350  ...    64.0      75.0      86.0   1.02
gamma     0.385  0.063   0.276    0.513  ...   152.0     152.0      88.0   0.99
Is_begin  1.064  1.167   0.006    2.968  ...    58.0      44.0      39.0   1.05
Ia_begin  1.691  1.642   0.001    5.378  ...   118.0      78.0      40.0   1.00
E_begin   0.842  1.114   0.003    2.821  ...    64.0      22.0      59.0   1.08

[8 rows x 11 columns]