1 Divergences 
Passed validation 
Computed from 80 by 385 log-likelihood matrix

         Estimate       SE
elpd_loo -4015.95    59.01
p_loo      396.16        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      362   94.0%
 (0.5, 0.7]   (ok)         15    3.9%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    3    0.8%

             mean       sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.251    0.064   0.153    0.346  ...   119.0     152.0      38.0   1.15
pu          0.778    0.057   0.704    0.886  ...     6.0       7.0      74.0   1.26
mu          0.117    0.021   0.078    0.144  ...     4.0       4.0      15.0   1.60
mus         0.146    0.026   0.099    0.190  ...    40.0      43.0      43.0   1.06
gamma       0.189    0.027   0.145    0.239  ...    19.0      21.0      57.0   1.06
Is_begin   34.320   25.414   4.170   84.972  ...     9.0       9.0      49.0   1.19
Ia_begin  108.716   74.788   9.951  219.242  ...     4.0       5.0      15.0   1.50
E_begin   160.667  184.421   1.575  468.394  ...     5.0       4.0      17.0   1.60

[8 rows x 11 columns]