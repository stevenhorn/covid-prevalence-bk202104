0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -114.79    39.59
p_loo       37.74        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      364   94.8%
 (0.5, 0.7]   (ok)         13    3.4%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.289  0.043   0.213    0.345  ...    76.0      85.0      79.0   1.00
pu        0.889  0.008   0.875    0.899  ...    95.0      96.0      70.0   0.99
mu        0.121  0.026   0.078    0.166  ...    73.0      72.0      79.0   1.03
mus       0.212  0.038   0.135    0.269  ...    83.0     108.0      60.0   0.99
gamma     0.259  0.039   0.200    0.347  ...    81.0      81.0      38.0   1.02
Is_begin  0.457  0.472   0.008    1.149  ...    57.0      70.0      85.0   1.06
Ia_begin  0.617  0.774   0.000    1.812  ...    96.0      89.0      59.0   1.01
E_begin   0.273  0.300   0.003    0.949  ...    98.0      95.0      77.0   0.98

[8 rows x 11 columns]