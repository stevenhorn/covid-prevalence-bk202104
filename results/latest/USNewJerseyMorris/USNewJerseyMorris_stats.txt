0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1822.57    29.89
p_loo       29.37        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      342   89.1%
 (0.5, 0.7]   (ok)         34    8.9%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    1    0.3%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.233   0.056   0.154    0.326  ...    51.0      33.0      42.0   1.05
pu         0.737   0.031   0.701    0.797  ...    35.0      33.0      57.0   1.00
mu         0.111   0.020   0.082    0.150  ...    47.0      45.0      57.0   1.04
mus        0.197   0.033   0.139    0.259  ...    85.0      88.0      60.0   1.01
gamma      0.315   0.054   0.219    0.402  ...   114.0     107.0      68.0   1.01
Is_begin  21.825  10.921   2.677   40.379  ...    61.0      58.0      38.0   1.02
Ia_begin  58.201  18.688  33.018   93.340  ...    70.0      65.0      69.0   1.01
E_begin   96.176  53.557  17.443  177.448  ...    33.0      90.0      17.0   1.02

[8 rows x 11 columns]