0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -845.74    25.57
p_loo       26.59        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      346   90.1%
 (0.5, 0.7]   (ok)         33    8.6%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.257  0.057   0.179    0.350  ...    51.0      48.0      40.0   1.06
pu        0.789  0.025   0.745    0.837  ...    29.0      31.0      35.0   1.10
mu        0.137  0.026   0.094    0.185  ...    25.0      24.0      23.0   1.10
mus       0.186  0.034   0.131    0.245  ...    93.0     109.0      60.0   0.99
gamma     0.211  0.042   0.140    0.277  ...   152.0     152.0      56.0   1.02
Is_begin  0.805  0.796   0.030    2.662  ...   118.0     122.0      87.0   0.98
Ia_begin  0.635  0.549   0.018    1.640  ...   110.0      88.0      59.0   1.01
E_begin   0.464  0.601   0.002    1.691  ...    84.0      70.0      60.0   1.03

[8 rows x 11 columns]