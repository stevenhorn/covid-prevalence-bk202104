1 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -973.37    31.89
p_loo       25.42        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      369   96.1%
 (0.5, 0.7]   (ok)         14    3.6%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.218  0.033   0.151    0.276  ...    18.0      18.0      14.0   1.21
pu        0.714  0.012   0.700    0.737  ...    13.0      15.0      15.0   1.15
mu        0.169  0.055   0.102    0.247  ...     3.0       4.0      24.0   1.91
mus       0.154  0.040   0.093    0.228  ...     3.0       4.0      17.0   1.73
gamma     0.111  0.028   0.075    0.145  ...     3.0       3.0      19.0   2.20
Is_begin  0.422  0.500   0.019    1.411  ...    30.0      23.0      56.0   1.31
Ia_begin  0.545  0.620   0.068    2.085  ...     6.0       5.0      35.0   1.49
E_begin   0.366  0.440   0.004    1.149  ...     5.0       4.0      14.0   1.85

[8 rows x 11 columns]