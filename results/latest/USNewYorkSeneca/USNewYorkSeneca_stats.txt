0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -838.94    43.06
p_loo       32.23        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      367   95.6%
 (0.5, 0.7]   (ok)         12    3.1%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.262  0.046   0.190    0.342  ...    36.0      34.0      56.0   1.05
pu        0.885  0.013   0.857    0.900  ...    32.0      45.0      24.0   1.07
mu        0.134  0.020   0.102    0.172  ...    30.0      29.0      54.0   1.02
mus       0.184  0.030   0.126    0.229  ...   152.0     152.0      93.0   1.07
gamma     0.237  0.041   0.168    0.297  ...   152.0     152.0      74.0   1.11
Is_begin  1.146  0.725   0.005    2.582  ...    55.0      50.0      58.0   1.01
Ia_begin  2.485  2.192   0.068    6.686  ...    77.0      62.0      43.0   0.99
E_begin   1.426  1.755   0.044    3.948  ...    80.0      60.0      60.0   1.01

[8 rows x 11 columns]