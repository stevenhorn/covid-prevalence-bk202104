0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1094.44    57.64
p_loo       52.17        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      353   91.9%
 (0.5, 0.7]   (ok)         20    5.2%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    4    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.179  0.029   0.150    0.236  ...    72.0      73.0      59.0   1.00
pu        0.710  0.009   0.700    0.728  ...    85.0      67.0      31.0   1.05
mu        0.098  0.019   0.062    0.129  ...    16.0      16.0      60.0   1.09
mus       0.203  0.030   0.143    0.252  ...    64.0      63.0      60.0   0.99
gamma     0.281  0.058   0.194    0.399  ...    83.0      78.0      60.0   1.00
Is_begin  0.293  0.393   0.010    1.161  ...    14.0      26.0      43.0   1.25
Ia_begin  0.369  0.430   0.001    1.268  ...    35.0      21.0      43.0   1.08
E_begin   0.186  0.215   0.002    0.598  ...    11.0      11.0      59.0   1.19

[8 rows x 11 columns]