0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1365.46    33.02
p_loo       33.14        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      344   89.6%
 (0.5, 0.7]   (ok)         30    7.8%
   (0.7, 1]   (bad)         9    2.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.241  0.055   0.162    0.335  ...    42.0      29.0      17.0   1.06
pu        0.748  0.028   0.707    0.792  ...    71.0      76.0      60.0   1.02
mu        0.111  0.024   0.065    0.154  ...    24.0      19.0      57.0   1.09
mus       0.179  0.039   0.112    0.248  ...   107.0     146.0      54.0   1.01
gamma     0.199  0.038   0.139    0.276  ...   100.0      94.0      45.0   1.04
Is_begin  0.797  0.878   0.002    2.738  ...   135.0      56.0      34.0   1.03
Ia_begin  1.527  1.449   0.000    3.919  ...    98.0     105.0      40.0   1.00
E_begin   0.800  1.068   0.008    3.194  ...   103.0      60.0      40.0   1.00

[8 rows x 11 columns]