0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1100.15    36.25
p_loo       29.68        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      360   93.8%
 (0.5, 0.7]   (ok)         18    4.7%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.244  0.055   0.161    0.329  ...   119.0     107.0      60.0   1.01
pu        0.819  0.018   0.787    0.850  ...    69.0      66.0      60.0   0.99
mu        0.119  0.022   0.079    0.152  ...    23.0      26.0      22.0   1.07
mus       0.163  0.032   0.108    0.224  ...   132.0     152.0      54.0   1.02
gamma     0.184  0.034   0.122    0.233  ...   152.0     149.0     100.0   0.99
Is_begin  0.662  0.602   0.008    1.782  ...   113.0     107.0      60.0   1.03
Ia_begin  1.300  1.407   0.000    4.023  ...   105.0      80.0      38.0   1.02
E_begin   0.685  0.721   0.022    2.410  ...   118.0     149.0     100.0   1.04

[8 rows x 11 columns]