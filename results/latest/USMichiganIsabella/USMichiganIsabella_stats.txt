0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1265.66    28.46
p_loo       24.39        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      358   93.2%
 (0.5, 0.7]   (ok)         18    4.7%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.245  0.055   0.159    0.336  ...    90.0      86.0      93.0   1.08
pu        0.794  0.032   0.744    0.847  ...    52.0      53.0      58.0   1.02
mu        0.147  0.024   0.105    0.186  ...    28.0      27.0      36.0   1.09
mus       0.200  0.036   0.142    0.263  ...   103.0     114.0      91.0   1.00
gamma     0.284  0.054   0.190    0.391  ...   152.0     146.0      58.0   0.98
Is_begin  0.978  1.014   0.007    3.266  ...    87.0      42.0      22.0   1.01
Ia_begin  2.218  1.988   0.104    6.356  ...    86.0      71.0     100.0   1.00
E_begin   1.085  1.145   0.002    3.646  ...   101.0      75.0      59.0   1.03

[8 rows x 11 columns]