0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -803.48    31.05
p_loo       30.96        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      359   93.5%
 (0.5, 0.7]   (ok)         22    5.7%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.253  0.055   0.165    0.342  ...    40.0      61.0      60.0   1.04
pu        0.836  0.022   0.796    0.873  ...    13.0      14.0      48.0   1.15
mu        0.127  0.024   0.094    0.181  ...    26.0      16.0      52.0   1.12
mus       0.178  0.034   0.101    0.229  ...   101.0     105.0      83.0   0.98
gamma     0.220  0.032   0.141    0.263  ...    99.0     101.0      88.0   1.04
Is_begin  0.786  0.632   0.036    1.937  ...    65.0      76.0      96.0   0.99
Ia_begin  1.383  1.471   0.023    4.055  ...    59.0      66.0      59.0   1.05
E_begin   0.517  0.673   0.016    1.393  ...    87.0      78.0      49.0   1.02

[8 rows x 11 columns]