0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -977.46    44.37
p_loo       33.46        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      363   94.5%
 (0.5, 0.7]   (ok)         14    3.6%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.251  0.053   0.170    0.349  ...    22.0      23.0      34.0   1.05
pu        0.834  0.024   0.780    0.867  ...    15.0      21.0      20.0   1.06
mu        0.116  0.022   0.081    0.155  ...    14.0      14.0      60.0   1.13
mus       0.152  0.028   0.103    0.203  ...   152.0     143.0      86.0   1.00
gamma     0.167  0.031   0.115    0.225  ...    90.0     113.0      19.0   1.05
Is_begin  0.725  0.664   0.004    2.013  ...    99.0      28.0      19.0   1.06
Ia_begin  0.972  1.125   0.003    2.852  ...    42.0      22.0      37.0   1.02
E_begin   0.536  0.613   0.028    1.566  ...    53.0      58.0      49.0   1.00

[8 rows x 11 columns]