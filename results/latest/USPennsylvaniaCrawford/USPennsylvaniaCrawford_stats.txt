0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1268.61    71.90
p_loo       46.75        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      357   93.0%
 (0.5, 0.7]   (ok)         21    5.5%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.258  0.046   0.172    0.338  ...    73.0      77.0      93.0   1.01
pu        0.794  0.024   0.744    0.828  ...    43.0      43.0      39.0   1.01
mu        0.124  0.022   0.086    0.158  ...    11.0      12.0      40.0   1.16
mus       0.298  0.048   0.203    0.389  ...    15.0      16.0      46.0   1.11
gamma     0.323  0.057   0.201    0.405  ...    59.0      70.0      60.0   1.01
Is_begin  0.946  1.051   0.012    3.010  ...   121.0      94.0      59.0   0.99
Ia_begin  1.604  1.854   0.035    4.768  ...   103.0      85.0      58.0   1.00
E_begin   0.666  0.874   0.022    2.159  ...    87.0      65.0      91.0   1.05

[8 rows x 11 columns]