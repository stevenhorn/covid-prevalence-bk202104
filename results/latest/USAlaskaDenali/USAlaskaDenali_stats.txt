0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -326.93    44.32
p_loo       45.45        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      350   91.1%
 (0.5, 0.7]   (ok)         25    6.5%
   (0.7, 1]   (bad)         8    2.1%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.252  0.058   0.151    0.336  ...    64.0      70.0      57.0   0.99
pu        0.748  0.036   0.700    0.815  ...    59.0      55.0      60.0   1.01
mu        0.127  0.023   0.089    0.168  ...    74.0      75.0     100.0   1.00
mus       0.217  0.035   0.153    0.285  ...    74.0      86.0      44.0   1.02
gamma     0.230  0.041   0.154    0.296  ...    86.0      87.0      87.0   1.01
Is_begin  0.322  0.315   0.003    0.980  ...    80.0      71.0      60.0   1.03
Ia_begin  0.474  0.522   0.024    1.257  ...    69.0      74.0      60.0   1.00
E_begin   0.268  0.381   0.012    0.895  ...    95.0      93.0      56.0   0.98

[8 rows x 11 columns]