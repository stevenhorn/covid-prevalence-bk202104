0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -692.94    44.30
p_loo       41.64        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      369   96.1%
 (0.5, 0.7]   (ok)         11    2.9%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.240  0.053   0.168    0.349  ...    18.0      21.0      16.0   1.09
pu        0.741  0.027   0.702    0.784  ...    26.0      23.0      49.0   1.03
mu        0.130  0.022   0.096    0.173  ...    13.0      16.0      59.0   1.11
mus       0.204  0.034   0.152    0.259  ...   116.0     101.0      47.0   1.07
gamma     0.236  0.046   0.165    0.331  ...   152.0     152.0     100.0   1.01
Is_begin  0.318  0.389   0.003    1.020  ...    74.0      57.0      60.0   1.03
Ia_begin  0.543  0.795   0.001    2.655  ...    42.0      28.0      22.0   1.05
E_begin   0.241  0.447   0.000    1.044  ...    44.0      25.0      56.0   1.04

[8 rows x 11 columns]