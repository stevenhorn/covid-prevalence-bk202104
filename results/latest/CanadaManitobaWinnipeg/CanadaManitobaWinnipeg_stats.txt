0 Divergences 
Passed validation 
Computed from 80 by 390 log-likelihood matrix

         Estimate       SE
elpd_loo -1470.83    36.95
p_loo       30.42        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      368   94.4%
 (0.5, 0.7]   (ok)         20    5.1%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    0    0.0%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.273   0.048   0.188    0.344  ...   100.0      92.0      57.0   1.11
pu         0.872   0.028   0.825    0.900  ...    43.0      61.0      74.0   1.05
mu         0.148   0.016   0.117    0.173  ...    36.0      34.0      54.0   1.05
mus        0.190   0.032   0.134    0.251  ...   149.0     152.0     100.0   1.03
gamma      0.257   0.039   0.197    0.339  ...   152.0     152.0      91.0   1.02
Is_begin   9.007   5.692   0.234   17.813  ...    99.0      99.0      86.0   1.00
Ia_begin  66.026  27.888  27.497  128.607  ...    72.0      98.0      21.0   1.05
E_begin   41.181  28.636   1.255   92.489  ...    62.0      68.0      60.0   1.02

[8 rows x 11 columns]