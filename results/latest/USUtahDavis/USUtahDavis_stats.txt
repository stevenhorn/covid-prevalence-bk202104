0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1717.85    43.81
p_loo       29.62        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      334   87.0%
 (0.5, 0.7]   (ok)         40   10.4%
   (0.7, 1]   (bad)         9    2.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.250  0.059   0.153    0.340  ...    18.0      18.0      38.0   1.08
pu        0.762  0.030   0.704    0.807  ...    10.0      10.0      15.0   1.17
mu        0.114  0.026   0.069    0.154  ...     5.0       5.0      17.0   1.39
mus       0.191  0.036   0.134    0.250  ...   107.0      96.0      38.0   1.01
gamma     0.236  0.042   0.173    0.325  ...   152.0     152.0      93.0   1.00
Is_begin  2.800  1.977   0.143    5.825  ...   110.0      76.0      40.0   1.00
Ia_begin  7.503  4.567   0.129   15.663  ...    38.0      27.0      14.0   1.03
E_begin   6.167  6.080   0.373   17.962  ...    70.0      60.0      60.0   1.01

[8 rows x 11 columns]