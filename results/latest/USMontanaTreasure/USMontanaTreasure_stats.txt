0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -266.22    47.81
p_loo       53.16        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      347   90.4%
 (0.5, 0.7]   (ok)         29    7.6%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.218  0.044   0.159    0.316  ...    49.0      93.0      88.0   1.12
pu        0.741  0.036   0.701    0.822  ...     5.0       8.0      16.0   1.25
mu        0.135  0.029   0.078    0.174  ...    24.0      17.0     100.0   1.11
mus       0.233  0.041   0.163    0.317  ...   133.0     131.0      49.0   1.04
gamma     0.254  0.035   0.190    0.319  ...   102.0      97.0      96.0   1.02
Is_begin  0.346  0.455   0.001    1.291  ...    73.0      69.0      51.0   1.01
Ia_begin  0.507  0.608   0.008    1.825  ...    85.0      60.0      49.0   1.04
E_begin   0.272  0.368   0.001    0.790  ...    38.0      27.0      37.0   1.07

[8 rows x 11 columns]