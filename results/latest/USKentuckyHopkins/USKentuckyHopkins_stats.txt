0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1261.17    30.78
p_loo       29.00        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      358   93.2%
 (0.5, 0.7]   (ok)         18    4.7%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.242  0.050   0.166    0.337  ...    69.0      72.0      43.0   1.00
pu        0.745  0.023   0.704    0.779  ...    31.0      30.0      48.0   1.05
mu        0.142  0.027   0.103    0.194  ...    45.0      38.0      60.0   1.00
mus       0.198  0.030   0.135    0.247  ...   152.0     152.0      86.0   0.98
gamma     0.281  0.046   0.202    0.347  ...    94.0     100.0      97.0   1.02
Is_begin  1.503  1.264   0.020    3.821  ...   101.0      61.0      80.0   1.03
Ia_begin  4.857  3.733   0.521   12.427  ...    66.0      77.0      56.0   0.99
E_begin   3.730  3.619   0.002    9.477  ...    61.0      46.0      17.0   1.04

[8 rows x 11 columns]