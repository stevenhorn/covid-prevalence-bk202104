0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -748.94    26.30
p_loo       24.71        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      352   91.7%
 (0.5, 0.7]   (ok)         27    7.0%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.225  0.045   0.154    0.301  ...    98.0     114.0      60.0   0.98
pu        0.722  0.021   0.701    0.760  ...    98.0      71.0      57.0   1.01
mu        0.127  0.021   0.090    0.168  ...    40.0      38.0      34.0   1.02
mus       0.201  0.040   0.131    0.278  ...     7.0       9.0      38.0   1.21
gamma     0.253  0.044   0.172    0.321  ...    77.0      77.0      56.0   1.01
Is_begin  0.580  0.701   0.007    2.216  ...    76.0      65.0      31.0   1.00
Ia_begin  1.237  1.204   0.013    3.206  ...    82.0     100.0      59.0   1.02
E_begin   0.448  0.509   0.006    1.516  ...    87.0      75.0      37.0   0.99

[8 rows x 11 columns]