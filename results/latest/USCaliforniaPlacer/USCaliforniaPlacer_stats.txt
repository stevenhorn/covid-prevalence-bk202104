0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1770.93    27.35
p_loo       23.19        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      343   89.3%
 (0.5, 0.7]   (ok)         35    9.1%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.256  0.053   0.162    0.347  ...   123.0     152.0      32.0   1.07
pu        0.847  0.037   0.755    0.891  ...    61.0      66.0      57.0   0.99
mu        0.126  0.023   0.087    0.163  ...    38.0      24.0      58.0   1.10
mus       0.158  0.023   0.116    0.197  ...   148.0     152.0      87.0   1.11
gamma     0.204  0.044   0.131    0.301  ...   152.0     152.0      81.0   1.05
Is_begin  1.345  0.927   0.107    3.087  ...   152.0     152.0      54.0   1.00
Ia_begin  0.776  0.555   0.033    1.857  ...   152.0     152.0      91.0   1.01
E_begin   0.840  0.733   0.028    2.351  ...   108.0      78.0      60.0   1.00

[8 rows x 11 columns]