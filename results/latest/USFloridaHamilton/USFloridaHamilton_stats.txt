0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1075.36    38.05
p_loo       40.67        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      342   89.1%
 (0.5, 0.7]   (ok)         32    8.3%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.236  0.049   0.150    0.327  ...    25.0      30.0      45.0   1.08
pu        0.742  0.022   0.711    0.782  ...    41.0      39.0      56.0   1.02
mu        0.131  0.020   0.091    0.156  ...    37.0      41.0      43.0   1.02
mus       0.209  0.032   0.145    0.271  ...   152.0     152.0      88.0   1.00
gamma     0.308  0.054   0.230    0.409  ...    47.0      49.0      59.0   1.03
Is_begin  0.345  0.374   0.000    1.172  ...    92.0      97.0      59.0   1.01
Ia_begin  0.411  0.519   0.010    1.369  ...    64.0      67.0      36.0   1.05
E_begin   0.233  0.303   0.001    0.921  ...   114.0      79.0      43.0   1.00

[8 rows x 11 columns]