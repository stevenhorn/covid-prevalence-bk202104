0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -976.65    36.70
p_loo       42.67        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      365   95.1%
 (0.5, 0.7]   (ok)         15    3.9%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.247  0.039   0.179    0.312  ...     8.0      10.0      25.0   1.32
pu        0.755  0.023   0.716    0.783  ...     6.0       6.0      38.0   1.43
mu        0.140  0.027   0.098    0.186  ...     6.0       6.0      43.0   1.32
mus       0.169  0.022   0.132    0.208  ...     4.0       5.0      22.0   1.50
gamma     0.169  0.030   0.111    0.217  ...    28.0      23.0      29.0   1.09
Is_begin  0.808  0.418   0.137    1.608  ...    17.0      15.0      30.0   1.07
Ia_begin  0.747  1.159   0.033    2.321  ...    32.0       8.0      40.0   1.27
E_begin   0.704  0.760   0.011    1.927  ...    32.0      26.0      22.0   1.09

[8 rows x 11 columns]