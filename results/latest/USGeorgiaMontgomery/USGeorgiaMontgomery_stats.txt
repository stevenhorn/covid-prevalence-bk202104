0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -826.21    41.51
p_loo       36.61        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      360   93.8%
 (0.5, 0.7]   (ok)         21    5.5%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.248  0.052   0.166    0.346  ...    29.0      31.0      50.0   1.05
pu        0.831  0.017   0.792    0.854  ...    46.0      45.0      42.0   1.02
mu        0.113  0.017   0.079    0.144  ...    48.0      48.0      26.0   1.06
mus       0.178  0.033   0.118    0.245  ...    46.0      65.0      24.0   1.10
gamma     0.252  0.043   0.173    0.331  ...    72.0      76.0      54.0   1.01
Is_begin  0.401  0.454   0.009    1.145  ...   106.0      88.0      93.0   0.99
Ia_begin  0.722  0.792   0.001    2.518  ...    95.0      66.0      97.0   1.03
E_begin   0.418  0.460   0.000    1.413  ...    56.0      62.0      60.0   1.03

[8 rows x 11 columns]