0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -893.05    21.87
p_loo       21.74        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      348   90.6%
 (0.5, 0.7]   (ok)         31    8.1%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.256  0.053   0.156    0.340  ...    73.0      64.0      24.0   1.01
pu        0.811  0.021   0.771    0.846  ...    15.0      16.0      59.0   1.12
mu        0.126  0.019   0.088    0.164  ...    51.0      30.0      60.0   1.08
mus       0.161  0.032   0.102    0.211  ...   152.0     152.0      60.0   1.00
gamma     0.176  0.035   0.118    0.236  ...    88.0      69.0      59.0   1.01
Is_begin  0.671  0.541   0.025    1.615  ...   133.0     152.0      39.0   1.03
Ia_begin  1.254  1.715   0.001    4.787  ...    88.0      44.0      42.0   1.03
E_begin   0.547  0.636   0.010    1.668  ...   101.0      96.0     100.0   0.99

[8 rows x 11 columns]