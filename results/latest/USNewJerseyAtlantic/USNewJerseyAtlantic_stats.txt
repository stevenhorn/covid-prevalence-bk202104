0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1638.36    24.72
p_loo       29.79        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      336   87.5%
 (0.5, 0.7]   (ok)         41   10.7%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.252  0.058   0.160    0.347  ...    46.0      45.0      39.0   1.00
pu        0.762  0.030   0.706    0.808  ...    38.0      43.0      86.0   1.05
mu        0.102  0.020   0.070    0.139  ...    43.0      41.0      59.0   1.05
mus       0.166  0.025   0.116    0.211  ...   152.0     152.0     100.0   1.00
gamma     0.235  0.041   0.172    0.313  ...   152.0     152.0      49.0   1.00
Is_begin  0.785  0.603   0.040    2.011  ...    55.0      47.0      43.0   1.00
Ia_begin  1.733  1.412   0.084    4.628  ...    62.0      46.0      24.0   1.03
E_begin   1.793  2.117   0.022    6.008  ...    85.0      39.0      49.0   0.99

[8 rows x 11 columns]