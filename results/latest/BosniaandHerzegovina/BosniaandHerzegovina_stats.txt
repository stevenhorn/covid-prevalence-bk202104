0 Divergences 
Passed validation 
Computed from 80 by 385 log-likelihood matrix

         Estimate       SE
elpd_loo -2523.01    29.66
p_loo       24.86        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      355   92.2%
 (0.5, 0.7]   (ok)         27    7.0%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    0    0.0%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.244   0.051   0.176    0.342  ...    56.0      62.0      58.0   1.04
pu         0.828   0.052   0.716    0.897  ...     5.0       6.0      40.0   1.34
mu         0.128   0.023   0.079    0.166  ...     8.0       9.0      24.0   1.21
mus        0.161   0.028   0.117    0.222  ...   152.0     152.0     100.0   1.01
gamma      0.232   0.042   0.156    0.299  ...   123.0     150.0      88.0   0.99
Is_begin  13.392  11.865   0.020   34.003  ...    13.0       8.0      22.0   1.20
Ia_begin  39.920  23.156   4.897   87.861  ...    23.0      24.0      59.0   1.10
E_begin   26.341  22.387   2.767   72.628  ...    54.0      32.0      88.0   1.08

[8 rows x 11 columns]