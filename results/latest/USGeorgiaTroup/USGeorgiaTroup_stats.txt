0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1506.88    67.92
p_loo       37.60        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      359   93.5%
 (0.5, 0.7]   (ok)         22    5.7%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.242  0.059   0.152    0.329  ...    53.0      58.0      43.0   0.99
pu        0.760  0.032   0.708    0.815  ...    24.0      26.0      37.0   1.06
mu        0.124  0.020   0.091    0.160  ...    28.0      30.0      32.0   1.04
mus       0.262  0.042   0.197    0.340  ...    92.0      87.0      58.0   1.00
gamma     0.410  0.065   0.307    0.515  ...    81.0      80.0      63.0   1.00
Is_begin  0.651  0.455   0.037    1.311  ...    66.0      56.0      59.0   1.04
Ia_begin  1.389  1.221   0.047    3.298  ...    85.0      64.0      40.0   1.01
E_begin   0.743  0.633   0.037    1.982  ...    58.0      58.0      56.0   1.04

[8 rows x 11 columns]