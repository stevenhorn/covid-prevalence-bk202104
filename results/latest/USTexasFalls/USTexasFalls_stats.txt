0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1390.50    69.64
p_loo       53.76        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      370   96.4%
 (0.5, 0.7]   (ok)          9    2.3%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.177  0.024   0.150    0.222  ...   152.0     116.0      40.0   1.00
pu        0.707  0.008   0.700    0.725  ...   106.0      27.0      23.0   1.05
mu        0.103  0.018   0.080    0.141  ...    36.0      36.0      39.0   1.06
mus       0.235  0.035   0.174    0.300  ...    88.0      73.0      54.0   1.03
gamma     0.410  0.075   0.280    0.550  ...   152.0     152.0      36.0   1.12
Is_begin  0.583  0.516   0.004    1.666  ...    94.0      76.0      58.0   1.00
Ia_begin  0.959  1.003   0.009    2.448  ...   110.0      95.0      40.0   0.98
E_begin   0.379  0.435   0.004    1.481  ...   120.0     106.0      93.0   1.00

[8 rows x 11 columns]