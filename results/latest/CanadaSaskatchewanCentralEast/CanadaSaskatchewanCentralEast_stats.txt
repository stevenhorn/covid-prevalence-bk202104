1 Divergences 
Passed validation 
Computed from 80 by 247 log-likelihood matrix

         Estimate       SE
elpd_loo  -686.34    19.72
p_loo       18.06        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      226   91.5%
 (0.5, 0.7]   (ok)         14    5.7%
   (0.7, 1]   (bad)         7    2.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.262  0.049   0.179    0.341  ...   125.0     128.0      57.0   1.00
pu        0.850  0.046   0.750    0.899  ...    47.0      45.0      40.0   1.06
mu        0.096  0.018   0.066    0.127  ...    26.0      21.0      56.0   1.09
mus       0.144  0.026   0.102    0.185  ...    72.0      76.0      88.0   1.02
gamma     0.161  0.030   0.112    0.212  ...    85.0      80.0      93.0   1.00
Is_begin  2.059  1.903   0.004    4.778  ...    77.0      33.0      24.0   1.04
Ia_begin  2.162  1.656   0.033    4.902  ...    70.0      47.0      31.0   1.02
E_begin   1.571  1.417   0.036    4.146  ...    41.0      36.0      59.0   1.02

[8 rows x 11 columns]