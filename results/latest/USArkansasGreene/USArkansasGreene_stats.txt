0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1306.07    53.11
p_loo       37.18        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      369   96.1%
 (0.5, 0.7]   (ok)         10    2.6%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.240  0.046   0.164    0.317  ...   152.0     128.0      91.0   1.10
pu        0.748  0.025   0.705    0.790  ...    33.0      30.0      40.0   1.04
mu        0.119  0.027   0.082    0.173  ...    29.0      36.0      35.0   1.04
mus       0.198  0.041   0.133    0.278  ...    30.0      35.0      43.0   1.04
gamma     0.223  0.043   0.146    0.309  ...    92.0      84.0     100.0   1.00
Is_begin  0.605  0.586   0.011    1.912  ...    96.0      75.0      60.0   1.02
Ia_begin  1.153  1.049   0.003    3.548  ...    99.0      52.0      40.0   1.01
E_begin   0.445  0.613   0.004    1.899  ...    51.0      28.0      22.0   1.07

[8 rows x 11 columns]