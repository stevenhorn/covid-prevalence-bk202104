0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -960.71    24.43
p_loo       23.64        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      341   88.8%
 (0.5, 0.7]   (ok)         33    8.6%
   (0.7, 1]   (bad)         9    2.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.251  0.054   0.167    0.348  ...    31.0      31.0      30.0   1.05
pu        0.817  0.016   0.794    0.854  ...    35.0      33.0      60.0   1.01
mu        0.124  0.027   0.075    0.167  ...     5.0       5.0      38.0   1.49
mus       0.155  0.027   0.107    0.201  ...    81.0      74.0      36.0   1.03
gamma     0.179  0.034   0.126    0.237  ...    52.0      56.0      86.0   0.98
Is_begin  0.511  0.522   0.004    1.690  ...    96.0      67.0      59.0   1.00
Ia_begin  0.831  0.812   0.004    2.406  ...    65.0      48.0      60.0   1.03
E_begin   0.365  0.385   0.002    1.177  ...    73.0      34.0      19.0   1.05

[8 rows x 11 columns]