0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -982.63    29.94
p_loo       29.14        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      361   94.0%
 (0.5, 0.7]   (ok)         16    4.2%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.246  0.054   0.158    0.337  ...    77.0      79.0      69.0   1.07
pu        0.853  0.026   0.798    0.891  ...    25.0      24.0      60.0   1.04
mu        0.124  0.024   0.087    0.170  ...    14.0      12.0      40.0   1.12
mus       0.172  0.025   0.131    0.227  ...   152.0     152.0      77.0   1.03
gamma     0.220  0.037   0.162    0.292  ...   113.0     132.0      39.0   1.02
Is_begin  1.540  1.301   0.033    4.372  ...    89.0      65.0      57.0   1.03
Ia_begin  0.863  0.582   0.153    2.039  ...    72.0      69.0      59.0   1.04
E_begin   1.041  1.027   0.028    3.032  ...   142.0      96.0      83.0   0.98

[8 rows x 11 columns]