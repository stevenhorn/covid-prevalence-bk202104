0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -983.40    62.49
p_loo       46.29        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      354   92.2%
 (0.5, 0.7]   (ok)         24    6.2%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.199  0.033   0.152    0.251  ...    15.0      18.0      42.0   1.08
pu        0.714  0.012   0.700    0.734  ...    62.0      43.0      14.0   1.03
mu        0.137  0.021   0.105    0.174  ...    32.0      29.0      29.0   1.05
mus       0.243  0.040   0.179    0.314  ...    67.0      74.0      54.0   0.99
gamma     0.355  0.072   0.220    0.460  ...   123.0     113.0      40.0   1.00
Is_begin  0.744  0.674   0.005    1.992  ...    77.0      53.0      38.0   1.02
Ia_begin  0.936  0.914   0.006    2.417  ...    48.0      44.0      43.0   1.02
E_begin   0.473  0.451   0.003    1.324  ...    53.0      52.0      59.0   0.99

[8 rows x 11 columns]