0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1167.53    49.65
p_loo       39.68        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      375   97.7%
 (0.5, 0.7]   (ok)          5    1.3%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.161  0.014   0.150    0.193  ...    43.0      12.0      54.0   1.12
pu        0.705  0.004   0.700    0.715  ...    35.0      17.0      15.0   1.08
mu        0.147  0.036   0.089    0.205  ...     3.0       3.0      19.0   2.10
mus       0.182  0.022   0.148    0.220  ...    16.0      16.0      20.0   1.11
gamma     0.144  0.033   0.099    0.197  ...     3.0       3.0      18.0   2.03
Is_begin  0.521  0.561   0.065    1.643  ...    18.0      21.0      49.0   1.25
Ia_begin  0.617  0.434   0.034    1.413  ...     7.0       6.0      24.0   1.43
E_begin   0.543  0.504   0.024    1.418  ...    53.0      43.0      43.0   1.03

[8 rows x 11 columns]