0 Divergences 
Passed validation 
Computed from 80 by 390 log-likelihood matrix

         Estimate       SE
elpd_loo  -675.50    37.10
p_loo       39.92        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      363   93.1%
 (0.5, 0.7]   (ok)         20    5.1%
   (0.7, 1]   (bad)         6    1.5%
   (1, Inf)   (very bad)    1    0.3%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.289   0.036   0.218    0.343  ...   130.0     102.0      54.0   0.99
pu         0.884   0.016   0.850    0.900  ...    61.0     103.0      38.0   0.99
mu         0.164   0.026   0.123    0.216  ...    13.0      14.0      44.0   1.11
mus        0.193   0.033   0.128    0.240  ...   110.0     102.0      56.0   1.03
gamma      0.293   0.037   0.234    0.366  ...    57.0      81.0      56.0   1.02
Is_begin   5.019   3.525   0.261   11.313  ...   106.0     101.0      91.0   1.00
Ia_begin  25.427  18.096   2.488   62.010  ...    59.0     118.0      43.0   1.11
E_begin   10.410   8.522   0.037   26.401  ...    77.0      61.0      59.0   1.02

[8 rows x 11 columns]