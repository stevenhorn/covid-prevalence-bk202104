0 Divergences 
Passed validation 
Computed from 80 by 390 log-likelihood matrix

         Estimate       SE
elpd_loo -1098.30    36.01
p_loo       29.46        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      365   93.6%
 (0.5, 0.7]   (ok)         21    5.4%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    0    0.0%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.291   0.038   0.203    0.338  ...   102.0     105.0      42.0   1.02
pu         0.891   0.008   0.877    0.900  ...    71.0      39.0      18.0   1.05
mu         0.208   0.027   0.172    0.265  ...    22.0      30.0      60.0   1.06
mus        0.211   0.033   0.163    0.267  ...    57.0      55.0      54.0   1.04
gamma      0.288   0.051   0.221    0.394  ...   110.0     111.0      87.0   1.01
Is_begin   3.274   2.446   0.093    6.878  ...    99.0      88.0      60.0   1.02
Ia_begin  11.791  14.200   0.338   39.324  ...   106.0      75.0      92.0   1.01
E_begin    3.954   3.518   0.089   11.527  ...   101.0      84.0      93.0   1.00

[8 rows x 11 columns]