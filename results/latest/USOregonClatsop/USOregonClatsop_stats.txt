0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -900.43    52.03
p_loo       37.03        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      365   95.1%
 (0.5, 0.7]   (ok)         15    3.9%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.276  0.056   0.177    0.348  ...    40.0      57.0      96.0   1.15
pu        0.883  0.018   0.854    0.900  ...    25.0      37.0      95.0   1.05
mu        0.187  0.021   0.156    0.237  ...    56.0      56.0      67.0   1.02
mus       0.261  0.037   0.197    0.339  ...   152.0     152.0      88.0   1.05
gamma     0.386  0.072   0.296    0.557  ...   152.0     152.0      88.0   0.99
Is_begin  0.986  0.637   0.038    2.016  ...    60.0      52.0      32.0   1.04
Ia_begin  2.292  2.129   0.034    6.323  ...    46.0     132.0      78.0   1.03
E_begin   1.189  1.536   0.020    3.620  ...   107.0      62.0      40.0   1.01

[8 rows x 11 columns]