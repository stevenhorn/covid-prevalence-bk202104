0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -864.78    25.35
p_loo       21.50        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      358   93.2%
 (0.5, 0.7]   (ok)         22    5.7%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.256  0.046   0.177    0.336  ...    94.0      91.0      40.0   1.03
pu        0.838  0.019   0.798    0.867  ...    55.0      68.0      37.0   1.01
mu        0.126  0.026   0.087    0.177  ...    40.0      41.0      88.0   1.01
mus       0.177  0.035   0.127    0.249  ...   152.0     152.0      53.0   1.09
gamma     0.206  0.039   0.141    0.272  ...   102.0      93.0      51.0   1.01
Is_begin  0.616  0.528   0.006    1.768  ...   105.0     107.0      79.0   0.99
Ia_begin  1.249  1.273   0.008    3.791  ...    74.0     109.0      48.0   1.00
E_begin   0.545  0.531   0.006    1.656  ...    99.0     116.0      77.0   0.99

[8 rows x 11 columns]