0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -966.17    66.77
p_loo       48.85        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      360   93.8%
 (0.5, 0.7]   (ok)         17    4.4%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.217  0.057   0.150    0.334  ...    56.0      51.0      18.0   1.03
pu        0.721  0.017   0.701    0.754  ...    69.0      75.0      38.0   1.03
mu        0.148  0.017   0.117    0.177  ...    53.0      51.0      60.0   1.00
mus       0.280  0.039   0.229    0.365  ...   152.0     152.0      91.0   0.99
gamma     0.450  0.075   0.358    0.625  ...   152.0     152.0      95.0   1.02
Is_begin  0.460  0.490   0.010    1.432  ...    77.0      69.0      93.0   1.00
Ia_begin  0.751  0.910   0.023    2.289  ...    50.0      56.0      60.0   1.03
E_begin   0.308  0.347   0.007    0.852  ...    97.0      65.0      57.0   1.04

[8 rows x 11 columns]