0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1262.65    30.34
p_loo       29.35        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      350   91.1%
 (0.5, 0.7]   (ok)         25    6.5%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.210  0.038   0.153    0.272  ...    92.0      87.0      93.0   1.00
pu        0.717  0.013   0.700    0.744  ...    51.0      51.0      60.0   1.01
mu        0.142  0.028   0.093    0.190  ...     6.0       6.0      34.0   1.39
mus       0.201  0.029   0.139    0.246  ...    44.0      40.0      84.0   1.04
gamma     0.285  0.044   0.209    0.365  ...   151.0     152.0     100.0   1.01
Is_begin  0.430  0.469   0.014    1.539  ...    71.0     102.0      59.0   1.02
Ia_begin  0.695  0.781   0.006    2.366  ...    56.0      72.0      60.0   1.04
E_begin   0.318  0.316   0.008    1.008  ...    62.0      74.0      60.0   1.02

[8 rows x 11 columns]