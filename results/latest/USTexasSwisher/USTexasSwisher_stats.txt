0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -928.67    62.68
p_loo       35.77        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      361   94.0%
 (0.5, 0.7]   (ok)         14    3.6%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.241  0.055   0.155    0.334  ...    72.0      70.0      39.0   1.01
pu        0.750  0.028   0.703    0.798  ...    63.0      55.0      55.0   1.03
mu        0.126  0.022   0.090    0.172  ...     7.0       6.0      27.0   1.33
mus       0.174  0.034   0.134    0.242  ...    95.0      57.0      56.0   1.02
gamma     0.224  0.052   0.132    0.298  ...    86.0     106.0      67.0   0.99
Is_begin  0.664  0.741   0.003    2.032  ...    79.0      30.0      40.0   1.01
Ia_begin  1.328  1.575   0.008    4.727  ...    78.0      59.0      59.0   1.00
E_begin   0.568  0.767   0.014    2.069  ...    56.0      39.0      60.0   1.00

[8 rows x 11 columns]