0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1086.45    34.20
p_loo       28.37        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      343   89.3%
 (0.5, 0.7]   (ok)         37    9.6%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.228  0.035   0.176    0.293  ...    21.0      21.0      43.0   1.07
pu        0.762  0.016   0.726    0.784  ...    39.0      44.0      42.0   0.99
mu        0.191  0.026   0.158    0.242  ...     4.0       4.0      15.0   1.57
mus       0.191  0.030   0.149    0.247  ...     4.0       4.0      32.0   1.54
gamma     0.198  0.043   0.136    0.270  ...     5.0       5.0      57.0   1.44
Is_begin  1.000  0.672   0.063    2.172  ...     8.0       8.0      14.0   1.18
Ia_begin  3.390  2.270   0.385    6.965  ...    29.0      20.0      59.0   1.07
E_begin   1.675  1.291   0.092    3.828  ...    18.0      10.0      25.0   1.18

[8 rows x 11 columns]