3 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -2145.68    29.67
p_loo       21.55        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      358   93.2%
 (0.5, 0.7]   (ok)         19    4.9%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.251  0.056   0.166    0.340  ...    16.0      29.0      61.0   1.10
pu        0.830  0.026   0.775    0.862  ...    11.0      11.0      22.0   1.22
mu        0.119  0.021   0.078    0.155  ...     7.0       7.0      20.0   1.28
mus       0.155  0.024   0.114    0.198  ...    34.0      37.0      93.0   1.06
gamma     0.187  0.032   0.140    0.251  ...    25.0      17.0      32.0   1.10
Is_begin  4.388  3.005   0.586   10.412  ...    33.0      43.0      24.0   1.10
Ia_begin  7.662  5.708   0.303   18.397  ...    33.0      36.0      40.0   1.10
E_begin   6.037  6.111   0.093   13.503  ...    86.0      43.0      60.0   1.04

[8 rows x 11 columns]