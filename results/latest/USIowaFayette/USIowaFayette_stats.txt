0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -849.62    32.20
p_loo       25.60        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      365   95.1%
 (0.5, 0.7]   (ok)         12    3.1%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.263  0.055   0.163    0.343  ...     8.0       8.0      17.0   1.20
pu        0.832  0.015   0.809    0.865  ...     7.0       7.0      19.0   1.29
mu        0.143  0.020   0.110    0.179  ...    13.0      13.0      38.0   1.11
mus       0.215  0.031   0.168    0.290  ...    64.0      59.0      77.0   1.00
gamma     0.255  0.044   0.177    0.348  ...    81.0      82.0      81.0   1.01
Is_begin  0.733  0.619   0.009    1.919  ...    52.0      43.0      54.0   1.03
Ia_begin  1.935  1.622   0.087    4.755  ...    58.0      48.0      42.0   1.02
E_begin   0.871  0.898   0.029    2.464  ...    70.0      58.0      59.0   1.04

[8 rows x 11 columns]