0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -898.07    29.00
p_loo       28.72        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      341   88.8%
 (0.5, 0.7]   (ok)         34    8.9%
   (0.7, 1]   (bad)         8    2.1%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.206  0.036   0.155    0.270  ...    73.0      77.0      69.0   1.02
pu        0.723  0.016   0.702    0.749  ...    94.0      97.0      58.0   0.99
mu        0.134  0.027   0.087    0.182  ...    53.0      50.0      29.0   1.01
mus       0.200  0.033   0.148    0.265  ...   100.0     152.0      61.0   0.99
gamma     0.263  0.046   0.177    0.341  ...   152.0     152.0      88.0   0.99
Is_begin  0.531  0.531   0.000    1.286  ...   111.0     152.0      35.0   1.06
Ia_begin  0.943  1.171   0.004    3.136  ...    96.0      91.0      60.0   1.00
E_begin   0.413  0.458   0.006    1.050  ...    45.0      92.0      60.0   1.01

[8 rows x 11 columns]