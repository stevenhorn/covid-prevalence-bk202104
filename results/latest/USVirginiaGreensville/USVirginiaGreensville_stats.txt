0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1100.66    33.66
p_loo       40.39        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      349   90.9%
 (0.5, 0.7]   (ok)         22    5.7%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    6    1.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.211  0.039   0.150    0.287  ...    89.0      77.0      57.0   1.00
pu        0.718  0.014   0.700    0.741  ...    57.0      47.0      38.0   1.00
mu        0.188  0.028   0.136    0.233  ...    11.0      13.0      56.0   1.14
mus       0.256  0.028   0.203    0.306  ...    32.0      32.0      75.0   1.07
gamma     0.428  0.062   0.334    0.540  ...   148.0     152.0      75.0   0.99
Is_begin  0.687  0.618   0.002    1.820  ...    52.0      43.0      40.0   1.09
Ia_begin  1.139  1.401   0.003    4.297  ...    64.0      26.0      24.0   1.08
E_begin   0.504  0.842   0.003    2.636  ...    73.0      38.0      38.0   1.05

[8 rows x 11 columns]