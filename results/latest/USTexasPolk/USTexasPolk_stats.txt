0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1523.00    58.63
p_loo       43.36        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      364   94.8%
 (0.5, 0.7]   (ok)         16    4.2%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.263  0.045   0.180    0.331  ...    40.0      42.0      51.0   1.03
pu        0.759  0.025   0.725    0.809  ...    14.0      15.0      59.0   1.11
mu        0.182  0.030   0.136    0.233  ...     4.0       4.0      27.0   1.52
mus       0.263  0.035   0.215    0.344  ...    20.0      24.0      31.0   1.09
gamma     0.400  0.081   0.277    0.542  ...    38.0      43.0      55.0   1.02
Is_begin  1.031  0.930   0.066    2.996  ...    19.0      13.0      54.0   1.15
Ia_begin  2.221  1.936   0.049    5.377  ...     6.0       6.0      20.0   1.33
E_begin   1.125  1.090   0.010    3.419  ...    21.0       9.0      22.0   1.25

[8 rows x 11 columns]