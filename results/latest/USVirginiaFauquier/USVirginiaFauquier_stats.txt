0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1210.03    27.61
p_loo       24.53        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      357   93.0%
 (0.5, 0.7]   (ok)         24    6.2%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.254  0.055   0.166    0.342  ...    67.0      60.0      45.0   1.02
pu        0.832  0.049   0.742    0.894  ...    17.0      18.0      88.0   1.11
mu        0.116  0.021   0.081    0.152  ...    47.0      47.0      59.0   1.05
mus       0.161  0.033   0.099    0.211  ...    60.0      66.0      83.0   1.03
gamma     0.199  0.041   0.144    0.290  ...   152.0     152.0      80.0   1.00
Is_begin  0.905  0.951   0.001    2.543  ...   118.0      32.0      15.0   1.05
Ia_begin  2.043  1.936   0.169    5.927  ...    73.0      50.0      86.0   1.01
E_begin   1.324  1.294   0.030    3.962  ...    36.0      54.0      23.0   1.02

[8 rows x 11 columns]