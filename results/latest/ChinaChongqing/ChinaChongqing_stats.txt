0 Divergences 
Passed validation 
Computed from 80 by 385 log-likelihood matrix

         Estimate       SE
elpd_loo    68.23    57.14
p_loo       53.45        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      352   91.4%
 (0.5, 0.7]   (ok)         25    6.5%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.263  0.037   0.204    0.336  ...   152.0     152.0      80.0   1.01
pu        0.880  0.014   0.856    0.899  ...    94.0      90.0      96.0   1.00
mu        0.159  0.031   0.111    0.217  ...   126.0     138.0      86.0   1.05
mus       0.251  0.037   0.175    0.310  ...   109.0     106.0      59.0   0.99
gamma     0.284  0.043   0.199    0.355  ...   152.0     152.0      49.0   1.00
Is_begin  1.107  0.815   0.023    2.538  ...   142.0     103.0      97.0   0.99
Ia_begin  3.111  2.562   0.008    8.235  ...    96.0      51.0      14.0   1.00
E_begin   1.015  0.885   0.015    2.484  ...    79.0      67.0      59.0   1.02

[8 rows x 11 columns]