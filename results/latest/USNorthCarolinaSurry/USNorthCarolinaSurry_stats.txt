0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1356.33    28.33
p_loo       17.58        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      354   92.2%
 (0.5, 0.7]   (ok)         26    6.8%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.256  0.055   0.173    0.348  ...    23.0      21.0      34.0   1.07
pu        0.766  0.036   0.711    0.816  ...     5.0       5.0      20.0   1.36
mu        0.103  0.021   0.066    0.137  ...    13.0      13.0      93.0   1.12
mus       0.153  0.030   0.102    0.199  ...   100.0      88.0      40.0   0.98
gamma     0.164  0.027   0.116    0.225  ...    48.0      50.0      58.0   1.00
Is_begin  0.514  0.466   0.041    1.336  ...    96.0     128.0      91.0   1.01
Ia_begin  0.898  0.987   0.000    2.346  ...   103.0      69.0      22.0   1.03
E_begin   0.448  0.537   0.009    1.806  ...    20.0      87.0      25.0   1.03

[8 rows x 11 columns]