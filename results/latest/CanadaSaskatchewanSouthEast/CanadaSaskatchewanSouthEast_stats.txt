0 Divergences 
Failed validation 
Computed from 80 by 247 log-likelihood matrix

         Estimate       SE
elpd_loo  -621.46    18.67
p_loo       20.28        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      227   91.9%
 (0.5, 0.7]   (ok)         19    7.7%
   (0.7, 1]   (bad)         1    0.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.275  0.044   0.181    0.339  ...   123.0     129.0      57.0   1.02
pu        0.881  0.018   0.843    0.898  ...    42.0      64.0      76.0   1.05
mu        0.153  0.030   0.106    0.206  ...     8.0       8.0      48.0   1.23
mus       0.185  0.029   0.134    0.242  ...   152.0     152.0      93.0   0.98
gamma     0.268  0.049   0.187    0.360  ...    56.0      67.0      88.0   1.04
Is_begin  1.012  1.234   0.032    3.421  ...   103.0     136.0      95.0   1.02
Ia_begin  1.303  1.403   0.009    3.781  ...   108.0     122.0      40.0   1.00
E_begin   0.603  0.834   0.002    1.727  ...    97.0      71.0      60.0   1.01

[8 rows x 11 columns]