0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1441.66    35.01
p_loo       24.47        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      358   93.2%
 (0.5, 0.7]   (ok)         23    6.0%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.272  0.047   0.190    0.348  ...    10.0      10.0      39.0   1.16
pu        0.749  0.024   0.702    0.786  ...    10.0      11.0      25.0   1.18
mu        0.107  0.019   0.076    0.145  ...    25.0      26.0      46.0   1.06
mus       0.145  0.033   0.079    0.210  ...    70.0      77.0      28.0   1.03
gamma     0.151  0.027   0.106    0.202  ...    48.0      40.0      54.0   1.04
Is_begin  0.466  0.418   0.005    1.264  ...    87.0      72.0      54.0   1.02
Ia_begin  0.869  1.049   0.004    2.707  ...   108.0      61.0      48.0   1.00
E_begin   0.493  0.525   0.011    1.729  ...    47.0      35.0      91.0   1.00

[8 rows x 11 columns]