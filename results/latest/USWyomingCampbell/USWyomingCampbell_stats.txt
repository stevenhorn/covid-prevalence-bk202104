0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1299.37    66.58
p_loo       34.92        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      357   93.0%
 (0.5, 0.7]   (ok)         23    6.0%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.264  0.051   0.169    0.349  ...    21.0      21.0      16.0   1.05
pu        0.797  0.020   0.753    0.827  ...    15.0      15.0      49.0   1.10
mu        0.111  0.018   0.082    0.137  ...    54.0      51.0      49.0   1.00
mus       0.194  0.034   0.138    0.254  ...    13.0      12.0      38.0   1.15
gamma     0.223  0.043   0.156    0.310  ...    24.0      30.0      59.0   1.08
Is_begin  0.926  0.922   0.036    2.718  ...    53.0      32.0      14.0   1.07
Ia_begin  1.883  2.050   0.004    6.509  ...    73.0      20.0      33.0   1.06
E_begin   0.861  0.898   0.021    2.043  ...    36.0     114.0      44.0   1.03

[8 rows x 11 columns]