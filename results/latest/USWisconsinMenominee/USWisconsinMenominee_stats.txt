0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -741.48    36.76
p_loo       29.60        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      361   94.0%
 (0.5, 0.7]   (ok)         18    4.7%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.196  0.040   0.150    0.274  ...    65.0      45.0      60.0   1.06
pu        0.717  0.011   0.701    0.740  ...    34.0      81.0      36.0   1.22
mu        0.114  0.020   0.081    0.149  ...    99.0     100.0      72.0   1.08
mus       0.180  0.033   0.126    0.238  ...   152.0     152.0      44.0   1.00
gamma     0.196  0.040   0.128    0.255  ...    82.0      60.0      53.0   1.02
Is_begin  0.609  0.582   0.008    1.757  ...   107.0      82.0      59.0   1.03
Ia_begin  0.819  0.819   0.025    2.383  ...   102.0     119.0      99.0   0.98
E_begin   0.447  0.441   0.005    1.236  ...   121.0      91.0      57.0   1.00

[8 rows x 11 columns]