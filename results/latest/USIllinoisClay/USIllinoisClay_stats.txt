0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -852.02    38.57
p_loo       34.44        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      359   93.5%
 (0.5, 0.7]   (ok)         18    4.7%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.244  0.051   0.163    0.328  ...    16.0      18.0      20.0   1.10
pu        0.772  0.020   0.738    0.808  ...     8.0       9.0      27.0   1.18
mu        0.121  0.020   0.082    0.157  ...    42.0      43.0      40.0   1.04
mus       0.188  0.040   0.111    0.256  ...    76.0      78.0      69.0   0.99
gamma     0.214  0.046   0.145    0.297  ...   108.0     104.0      86.0   0.98
Is_begin  0.704  0.752   0.008    2.400  ...    73.0      18.0      56.0   1.08
Ia_begin  0.914  1.204   0.017    2.725  ...   104.0      97.0      93.0   0.99
E_begin   0.358  0.419   0.021    1.098  ...    58.0      87.0      61.0   1.03

[8 rows x 11 columns]