0 Divergences 
Passed validation 
Computed from 80 by 390 log-likelihood matrix

         Estimate       SE
elpd_loo  -912.64    28.37
p_loo       38.43        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      349   89.5%
 (0.5, 0.7]   (ok)         34    8.7%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.254  0.056   0.164    0.337  ...    85.0      84.0      56.0   1.02
pu        0.798  0.040   0.735    0.871  ...    17.0      15.0      21.0   1.12
mu        0.267  0.027   0.224    0.321  ...    33.0      32.0      57.0   1.02
mus       0.307  0.034   0.238    0.353  ...   135.0     139.0      88.0   0.98
gamma     0.549  0.081   0.407    0.662  ...   152.0     152.0      81.0   1.00
Is_begin  1.654  1.241   0.091    3.970  ...   128.0      91.0      60.0   0.98
Ia_begin  3.783  3.392   0.080   10.828  ...    68.0      41.0      32.0   1.04
E_begin   3.676  4.849   0.042   13.564  ...    77.0      48.0      42.0   1.04

[8 rows x 11 columns]