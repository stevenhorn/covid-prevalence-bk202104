0 Divergences 
Passed validation 
Computed from 80 by 390 log-likelihood matrix

         Estimate       SE
elpd_loo -1552.20    30.14
p_loo       27.23        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      356   91.3%
 (0.5, 0.7]   (ok)         26    6.7%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    3    0.8%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.257   0.048   0.172    0.340  ...    85.0      82.0      60.0   1.06
pu         0.808   0.054   0.708    0.888  ...    61.0      64.0      55.0   1.03
mu         0.123   0.017   0.098    0.154  ...    60.0      58.0      56.0   1.03
mus        0.163   0.026   0.120    0.211  ...   127.0     152.0      81.0   1.05
gamma      0.211   0.046   0.137    0.290  ...   152.0     152.0      87.0   1.09
Is_begin   7.016   5.513   0.158   18.121  ...    68.0      47.0      48.0   1.03
Ia_begin  41.133  24.835   2.976   84.199  ...    85.0      77.0      96.0   0.99
E_begin   22.264  17.406   0.300   46.895  ...    84.0      64.0      57.0   0.99

[8 rows x 11 columns]