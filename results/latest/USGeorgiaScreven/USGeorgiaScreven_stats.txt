0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -917.01    56.28
p_loo       39.39        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      360   93.8%
 (0.5, 0.7]   (ok)         20    5.2%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.249  0.055   0.160    0.337  ...    68.0      69.0      57.0   0.99
pu        0.881  0.013   0.857    0.900  ...    60.0      49.0      17.0   1.06
mu        0.122  0.017   0.096    0.159  ...    24.0      31.0      36.0   1.04
mus       0.179  0.033   0.130    0.246  ...    94.0     101.0      77.0   0.98
gamma     0.222  0.053   0.140    0.286  ...    43.0      99.0      32.0   1.02
Is_begin  0.724  0.722   0.010    2.374  ...   150.0     146.0      69.0   1.04
Ia_begin  1.699  1.566   0.007    4.666  ...    83.0      46.0      25.0   1.05
E_begin   0.779  0.913   0.033    1.994  ...   101.0      95.0     100.0   1.01

[8 rows x 11 columns]