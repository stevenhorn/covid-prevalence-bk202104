0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo   409.68    96.75
p_loo       83.34        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      362   94.3%
 (0.5, 0.7]   (ok)         12    3.1%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    5    1.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.268  0.053   0.151    0.332  ...    29.0      74.0      56.0   1.13
pu        0.885  0.016   0.855    0.900  ...    87.0      53.0      22.0   1.00
mu        0.150  0.025   0.098    0.195  ...    58.0      56.0      60.0   1.02
mus       0.241  0.044   0.162    0.310  ...   108.0     120.0      59.0   0.98
gamma     0.255  0.046   0.178    0.349  ...    86.0      84.0      59.0   1.02
Is_begin  0.281  0.343   0.000    0.838  ...    83.0      23.0      30.0   1.08
Ia_begin  0.652  0.763   0.001    2.157  ...    24.0      23.0      59.0   1.07
E_begin   0.250  0.334   0.001    0.858  ...    47.0      12.0      83.0   1.14

[8 rows x 11 columns]