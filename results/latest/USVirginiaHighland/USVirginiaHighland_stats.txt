0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -378.93    35.08
p_loo       29.72        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      361   94.0%
 (0.5, 0.7]   (ok)         18    4.7%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.276  0.055   0.173    0.349  ...    77.0     118.0      86.0   1.00
pu        0.882  0.018   0.842    0.899  ...    58.0      51.0      79.0   1.05
mu        0.138  0.032   0.083    0.195  ...    45.0      50.0      74.0   1.04
mus       0.173  0.036   0.109    0.234  ...   151.0     152.0      87.0   1.02
gamma     0.201  0.042   0.129    0.275  ...    80.0      80.0      40.0   0.99
Is_begin  0.691  1.135   0.000    1.842  ...    99.0      76.0      60.0   1.02
Ia_begin  1.357  1.399   0.032    3.644  ...    78.0      84.0      88.0   1.00
E_begin   0.707  0.856   0.013    2.306  ...    98.0     103.0     100.0   0.99

[8 rows x 11 columns]