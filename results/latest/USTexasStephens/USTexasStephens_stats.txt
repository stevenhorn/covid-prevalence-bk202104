0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1115.24    69.29
p_loo       55.21        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      372   96.9%
 (0.5, 0.7]   (ok)          7    1.8%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.194  0.037   0.152    0.262  ...    41.0      31.0      59.0   1.06
pu        0.716  0.013   0.701    0.741  ...    22.0      24.0      33.0   1.06
mu        0.200  0.025   0.163    0.243  ...     8.0       8.0      28.0   1.21
mus       0.274  0.055   0.205    0.385  ...     6.0       8.0      15.0   1.30
gamma     0.342  0.070   0.242    0.476  ...    11.0       9.0      14.0   1.20
Is_begin  0.557  0.720   0.016    2.295  ...    38.0      28.0      59.0   1.01
Ia_begin  1.428  1.410   0.126    3.298  ...    17.0      27.0      21.0   1.08
E_begin   0.608  0.519   0.072    1.661  ...    43.0      38.0      58.0   1.04

[8 rows x 11 columns]