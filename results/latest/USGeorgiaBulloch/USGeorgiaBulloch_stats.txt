0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1459.94    72.65
p_loo       49.07        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      343   89.3%
 (0.5, 0.7]   (ok)         33    8.6%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.250  0.058   0.152    0.334  ...    85.0      85.0      57.0   1.01
pu        0.750  0.035   0.700    0.809  ...    25.0      24.0      19.0   1.05
mu        0.161  0.020   0.131    0.205  ...    24.0      27.0      59.0   1.02
mus       0.325  0.040   0.258    0.407  ...    96.0      90.0      68.0   1.01
gamma     0.577  0.095   0.415    0.761  ...   114.0     112.0     100.0   1.01
Is_begin  0.792  0.777   0.004    2.396  ...    42.0      25.0      57.0   1.06
Ia_begin  1.258  1.434   0.038    3.750  ...    71.0      80.0      87.0   1.03
E_begin   0.520  0.648   0.000    1.420  ...    67.0      12.0      46.0   1.17

[8 rows x 11 columns]