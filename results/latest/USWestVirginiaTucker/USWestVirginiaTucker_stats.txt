0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -617.52    31.96
p_loo       30.85        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      364   94.8%
 (0.5, 0.7]   (ok)         14    3.6%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.248  0.055   0.150    0.345  ...    31.0      38.0      22.0   1.06
pu        0.847  0.016   0.820    0.878  ...    47.0      47.0      58.0   1.08
mu        0.143  0.029   0.097    0.198  ...    71.0      74.0      30.0   1.09
mus       0.174  0.032   0.117    0.226  ...   152.0     140.0      38.0   1.01
gamma     0.201  0.044   0.129    0.279  ...   118.0     138.0      95.0   1.01
Is_begin  0.980  1.025   0.002    3.240  ...   110.0      78.0      79.0   1.00
Ia_begin  0.646  0.581   0.021    1.839  ...   134.0      93.0      87.0   1.00
E_begin   0.531  0.646   0.010    1.666  ...   101.0      81.0      83.0   1.01

[8 rows x 11 columns]