0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1974.92    26.03
p_loo       27.36        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      346   90.1%
 (0.5, 0.7]   (ok)         31    8.1%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    0    0.0%

             mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.224   0.050   0.151    0.308  ...    85.0      96.0      60.0   0.99
pu          0.734   0.026   0.701    0.783  ...    64.0      64.0      56.0   0.98
mu          0.112   0.020   0.079    0.153  ...    37.0      38.0      72.0   1.03
mus         0.198   0.035   0.140    0.263  ...   142.0     152.0      57.0   1.00
gamma       0.369   0.053   0.285    0.466  ...    85.0     140.0      57.0   0.99
Is_begin   24.095  17.426   0.007   53.369  ...    71.0      52.0      38.0   1.00
Ia_begin   77.446  28.553  19.234  124.679  ...    67.0      63.0      87.0   1.03
E_begin   114.999  51.834  35.272  211.401  ...    89.0      90.0      77.0   0.98

[8 rows x 11 columns]