0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -2111.63    55.30
p_loo       39.71        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      356   92.7%
 (0.5, 0.7]   (ok)         20    5.2%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    1    0.3%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.236   0.063   0.150    0.337  ...   116.0     114.0      57.0   1.11
pu         0.763   0.034   0.704    0.824  ...    43.0      41.0      42.0   1.01
mu         0.112   0.020   0.071    0.147  ...    35.0      34.0      22.0   1.00
mus        0.209   0.048   0.138    0.289  ...   123.0      95.0      54.0   0.99
gamma      0.296   0.047   0.202    0.379  ...   152.0     147.0     100.0   1.00
Is_begin   8.381   6.871   0.091   25.256  ...    65.0      39.0      30.0   1.04
Ia_begin  24.456  11.826   5.617   44.994  ...   140.0     116.0      27.0   1.02
E_begin   25.435  19.891   0.118   70.417  ...    72.0      59.0      74.0   1.05

[8 rows x 11 columns]