0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -2551.44    68.67
p_loo       49.42        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      338   88.0%
 (0.5, 0.7]   (ok)         37    9.6%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    3    0.8%

             mean      sd   hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.174   0.023    0.150    0.215  ...   152.0     110.0      77.0   1.01
pu          0.706   0.005    0.700    0.717  ...    58.0      32.0      63.0   1.04
mu          0.079   0.013    0.058    0.101  ...    27.0      31.0      40.0   1.01
mus         0.311   0.049    0.235    0.407  ...   130.0     152.0      60.0   1.12
gamma       0.883   0.058    0.778    0.979  ...    89.0      91.0      60.0   1.02
Is_begin    4.307   1.416    1.835    6.620  ...   109.0      96.0      59.0   1.10
Ia_begin   25.711   3.468   19.632   32.687  ...   152.0     152.0      81.0   0.99
E_begin   214.666  36.146  157.651  286.460  ...    88.0      93.0     100.0   0.98

[8 rows x 11 columns]