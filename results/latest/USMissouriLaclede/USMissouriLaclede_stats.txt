0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1321.41    62.28
p_loo       42.33        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      353   91.9%
 (0.5, 0.7]   (ok)         21    5.5%
   (0.7, 1]   (bad)         8    2.1%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.195  0.042   0.150    0.273  ...   104.0      89.0      59.0   0.99
pu        0.714  0.014   0.700    0.743  ...   143.0      64.0      60.0   1.03
mu        0.122  0.016   0.095    0.154  ...    38.0      43.0      39.0   1.01
mus       0.248  0.040   0.181    0.306  ...   104.0     119.0      60.0   1.00
gamma     0.404  0.064   0.303    0.522  ...   152.0     152.0      88.0   0.99
Is_begin  0.382  0.480   0.007    1.185  ...    56.0      44.0      77.0   0.98
Ia_begin  0.469  0.619   0.003    1.738  ...    50.0      33.0      30.0   1.02
E_begin   0.195  0.314   0.002    0.748  ...    59.0      39.0      59.0   1.03

[8 rows x 11 columns]