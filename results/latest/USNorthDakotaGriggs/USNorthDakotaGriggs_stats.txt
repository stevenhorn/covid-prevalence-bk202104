0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -537.17    39.16
p_loo       40.10        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      348   90.6%
 (0.5, 0.7]   (ok)         28    7.3%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.253  0.054   0.172    0.347  ...   107.0      97.0      43.0   1.02
pu        0.791  0.022   0.753    0.824  ...    90.0     108.0      60.0   1.08
mu        0.137  0.033   0.084    0.203  ...    25.0      14.0      60.0   1.11
mus       0.195  0.035   0.135    0.271  ...    84.0      90.0      60.0   1.03
gamma     0.219  0.047   0.131    0.292  ...    82.0     110.0     100.0   1.03
Is_begin  0.302  0.285   0.001    0.802  ...    86.0      66.0      40.0   0.99
Ia_begin  0.594  0.640   0.000    1.766  ...    81.0      45.0      83.0   1.02
E_begin   0.232  0.277   0.001    0.872  ...    78.0      72.0      91.0   1.00

[8 rows x 11 columns]