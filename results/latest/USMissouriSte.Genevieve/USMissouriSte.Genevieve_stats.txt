0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1095.68    70.69
p_loo       44.40        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      356   92.7%
 (0.5, 0.7]   (ok)         20    5.2%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.199  0.032   0.153    0.258  ...   152.0     152.0      95.0   0.99
pu        0.717  0.016   0.700    0.748  ...    85.0      68.0      40.0   1.07
mu        0.115  0.013   0.091    0.137  ...    57.0      52.0      40.0   1.04
mus       0.230  0.039   0.156    0.291  ...   102.0     152.0      59.0   1.11
gamma     0.386  0.063   0.297    0.509  ...    87.0     109.0      60.0   0.99
Is_begin  0.808  0.643   0.003    1.861  ...   152.0     152.0     100.0   1.01
Ia_begin  1.279  1.490   0.001    4.775  ...   128.0     103.0      39.0   1.01
E_begin   0.468  0.373   0.029    1.296  ...   114.0     113.0      59.0   1.01

[8 rows x 11 columns]