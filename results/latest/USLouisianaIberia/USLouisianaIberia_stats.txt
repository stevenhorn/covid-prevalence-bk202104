0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1531.50    30.06
p_loo       32.10        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      357   93.0%
 (0.5, 0.7]   (ok)         19    4.9%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.242  0.055   0.161    0.349  ...    54.0      52.0      29.0   1.00
pu        0.779  0.023   0.734    0.813  ...    25.0      22.0      30.0   1.08
mu        0.128  0.029   0.091    0.190  ...     6.0       6.0      24.0   1.37
mus       0.177  0.029   0.134    0.225  ...    57.0      47.0      59.0   1.02
gamma     0.241  0.048   0.160    0.309  ...    61.0      59.0      43.0   1.02
Is_begin  1.633  1.167   0.122    3.263  ...    53.0      46.0      42.0   1.02
Ia_begin  0.771  0.606   0.016    1.870  ...    90.0      66.0      60.0   0.99
E_begin   1.235  1.099   0.012    3.603  ...    80.0      63.0      36.0   1.03

[8 rows x 11 columns]