0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -933.70    32.87
p_loo       28.91        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      346   90.1%
 (0.5, 0.7]   (ok)         30    7.8%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.251  0.060   0.151    0.344  ...    89.0      65.0      39.0   1.07
pu        0.807  0.026   0.773    0.852  ...    32.0      32.0      53.0   1.07
mu        0.115  0.019   0.081    0.148  ...    13.0      11.0      57.0   1.15
mus       0.156  0.033   0.103    0.210  ...    92.0     152.0      60.0   1.02
gamma     0.169  0.034   0.119    0.231  ...   152.0     152.0      46.0   1.07
Is_begin  0.664  0.537   0.032    1.774  ...   105.0      88.0      96.0   1.00
Ia_begin  1.355  1.381   0.028    4.191  ...   100.0     103.0      97.0   1.01
E_begin   0.696  0.783   0.005    2.208  ...    99.0      82.0      50.0   1.01

[8 rows x 11 columns]