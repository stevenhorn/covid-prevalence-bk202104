0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1005.41    61.57
p_loo       54.98        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      368   95.8%
 (0.5, 0.7]   (ok)          9    2.3%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.207  0.035   0.151    0.268  ...    60.0      57.0      42.0   1.05
pu        0.717  0.023   0.700    0.775  ...    10.0       5.0      18.0   1.45
mu        0.130  0.021   0.090    0.166  ...    14.0      12.0      65.0   1.13
mus       0.231  0.039   0.162    0.310  ...    11.0      13.0      18.0   1.15
gamma     0.308  0.059   0.217    0.412  ...    32.0      29.0      48.0   1.06
Is_begin  0.597  0.605   0.003    1.683  ...    72.0      36.0      29.0   1.04
Ia_begin  0.975  0.977   0.012    2.722  ...    55.0      33.0      43.0   1.01
E_begin   0.424  0.462   0.002    1.389  ...    38.0      37.0      59.0   0.99

[8 rows x 11 columns]