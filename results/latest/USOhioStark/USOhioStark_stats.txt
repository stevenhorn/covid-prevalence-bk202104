0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1711.29    42.35
p_loo       28.58        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      348   90.6%
 (0.5, 0.7]   (ok)         32    8.3%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.251  0.060   0.155    0.345  ...   152.0     152.0      60.0   1.03
pu        0.788  0.046   0.709    0.861  ...    35.0      38.0      60.0   1.03
mu        0.104  0.019   0.075    0.138  ...    32.0      35.0      40.0   1.03
mus       0.186  0.030   0.126    0.230  ...    82.0      81.0      60.0   1.01
gamma     0.222  0.034   0.161    0.288  ...    72.0      73.0      55.0   1.01
Is_begin  0.758  0.583   0.022    1.880  ...   145.0     152.0      80.0   0.99
Ia_begin  1.657  1.070   0.129    3.569  ...   124.0      56.0      37.0   1.03
E_begin   1.287  1.088   0.047    3.304  ...   101.0      81.0      40.0   1.05

[8 rows x 11 columns]