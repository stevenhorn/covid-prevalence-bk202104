0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -2792.53    26.90
p_loo       21.76        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      357   93.0%
 (0.5, 0.7]   (ok)         21    5.5%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.251  0.047   0.173    0.319  ...    60.0      53.0      56.0   1.03
pu        0.752  0.026   0.705    0.795  ...    15.0      16.0      40.0   1.11
mu        0.112  0.021   0.075    0.146  ...    22.0      22.0      59.0   1.02
mus       0.167  0.027   0.129    0.222  ...    73.0      73.0      81.0   1.01
gamma     0.241  0.038   0.188    0.333  ...   132.0     147.0      97.0   0.98
Is_begin  3.515  2.950   0.066    8.807  ...    24.0      10.0      59.0   1.17
Ia_begin  8.386  5.694   0.423   16.455  ...    36.0      19.0      47.0   1.08
E_begin   6.395  6.147   0.486   17.240  ...    94.0      54.0      39.0   1.00

[8 rows x 11 columns]