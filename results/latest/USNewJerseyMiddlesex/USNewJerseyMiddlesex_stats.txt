40 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -2051.71    35.75
p_loo       24.54        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      309   80.5%
 (0.5, 0.7]   (ok)         51   13.3%
   (0.7, 1]   (bad)        14    3.6%
   (1, Inf)   (very bad)   10    2.6%

             mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.215   0.042   0.150    0.300  ...    10.0      32.0      23.0   1.60
pu          0.732   0.030   0.700    0.797  ...    12.0      18.0      24.0   1.76
mu          0.112   0.015   0.082    0.126  ...     6.0       6.0      14.0   1.77
mus         0.175   0.025   0.134    0.228  ...    10.0       7.0      38.0   1.95
gamma       0.337   0.039   0.265    0.410  ...     7.0      10.0      25.0   2.12
Is_begin   37.964  18.374   1.402   65.794  ...    12.0      13.0      24.0   1.94
Ia_begin   70.305  46.011  31.795  148.429  ...     3.0       4.0      15.0   1.87
E_begin   124.307  85.211   4.228  310.101  ...    23.0      59.0      46.0   1.82

[8 rows x 11 columns]