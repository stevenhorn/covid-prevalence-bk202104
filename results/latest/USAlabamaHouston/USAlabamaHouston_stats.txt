0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1644.29    59.05
p_loo       45.72        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      365   95.1%
 (0.5, 0.7]   (ok)         15    3.9%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.265  0.051   0.166    0.350  ...    40.0      36.0      39.0   1.07
pu        0.794  0.020   0.751    0.823  ...    13.0      14.0      57.0   1.13
mu        0.109  0.023   0.073    0.158  ...    27.0      27.0      14.0   1.12
mus       0.165  0.034   0.116    0.241  ...    90.0     148.0      59.0   0.99
gamma     0.179  0.033   0.130    0.238  ...    75.0      64.0      79.0   1.04
Is_begin  0.777  0.723   0.010    2.118  ...    94.0      77.0      48.0   1.04
Ia_begin  1.483  1.810   0.018    5.112  ...   111.0     115.0      84.0   1.01
E_begin   0.798  0.872   0.007    2.034  ...    92.0      80.0      83.0   1.00

[8 rows x 11 columns]