0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1103.23    76.48
p_loo       40.60        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      370   96.4%
 (0.5, 0.7]   (ok)         13    3.4%
   (0.7, 1]   (bad)         0    0.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.183  0.028   0.151    0.243  ...    96.0      96.0      86.0   1.00
pu        0.709  0.009   0.700    0.726  ...   136.0     146.0      48.0   1.01
mu        0.100  0.012   0.082    0.126  ...    46.0      46.0      76.0   1.03
mus       0.226  0.038   0.153    0.294  ...    56.0      54.0      31.0   1.01
gamma     0.414  0.071   0.292    0.523  ...    72.0      60.0      88.0   1.02
Is_begin  0.663  0.579   0.020    1.601  ...   101.0      94.0      93.0   0.99
Ia_begin  0.817  0.920   0.001    2.758  ...    69.0      56.0      58.0   1.03
E_begin   0.403  0.392   0.007    0.960  ...    58.0      43.0      59.0   1.02

[8 rows x 11 columns]