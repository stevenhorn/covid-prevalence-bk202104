0 Divergences 
Passed validation 
Computed from 80 by 385 log-likelihood matrix

         Estimate       SE
elpd_loo -2434.78    40.87
p_loo       25.68        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      348   90.4%
 (0.5, 0.7]   (ok)         32    8.3%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    0    0.0%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.262   0.052   0.190    0.348  ...    60.0      51.0      40.0   1.01
pu         0.805   0.052   0.708    0.857  ...     3.0       4.0      26.0   1.69
mu         0.159   0.020   0.133    0.192  ...     6.0       6.0      49.0   1.35
mus        0.194   0.031   0.156    0.262  ...   104.0      93.0      39.0   1.00
gamma      0.322   0.050   0.246    0.423  ...    29.0      33.0      17.0   1.05
Is_begin  14.472  10.590   0.054   34.535  ...    81.0      61.0      36.0   1.05
Ia_begin  41.241  20.475   3.355   74.149  ...    81.0      75.0      91.0   1.03
E_begin   35.519  28.270   1.273   79.657  ...    89.0      62.0      60.0   1.02

[8 rows x 11 columns]