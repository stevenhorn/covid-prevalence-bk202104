0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -922.50    41.54
p_loo       34.89        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      349   90.9%
 (0.5, 0.7]   (ok)         29    7.6%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.268  0.054   0.173    0.348  ...    39.0      31.0      22.0   1.07
pu        0.881  0.014   0.859    0.898  ...    18.0      18.0      38.0   1.13
mu        0.168  0.033   0.114    0.217  ...    15.0      14.0      85.0   1.10
mus       0.200  0.042   0.129    0.264  ...    41.0      48.0      74.0   1.05
gamma     0.260  0.041   0.194    0.327  ...    57.0      52.0      80.0   1.05
Is_begin  1.515  1.053   0.025    3.273  ...    49.0      45.0      57.0   1.01
Ia_begin  3.482  2.651   0.053    8.177  ...    86.0      62.0      40.0   1.02
E_begin   1.916  1.664   0.034    4.977  ...    97.0      52.0      43.0   1.04

[8 rows x 11 columns]