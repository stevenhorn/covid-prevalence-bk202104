0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -985.00    34.34
p_loo       35.55        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      358   93.2%
 (0.5, 0.7]   (ok)         20    5.2%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.256  0.048   0.159    0.322  ...    35.0      36.0      60.0   1.04
pu        0.741  0.023   0.706    0.779  ...    25.0      25.0      31.0   1.02
mu        0.160  0.027   0.110    0.205  ...    29.0      29.0      59.0   1.02
mus       0.235  0.036   0.166    0.292  ...    70.0      75.0      59.0   1.01
gamma     0.289  0.048   0.217    0.373  ...    34.0      42.0      60.0   1.06
Is_begin  0.546  0.588   0.000    1.485  ...    65.0      34.0      14.0   1.02
Ia_begin  1.089  1.013   0.074    3.062  ...    62.0      57.0      45.0   1.02
E_begin   0.483  0.549   0.003    1.642  ...    56.0      37.0      53.0   1.05

[8 rows x 11 columns]