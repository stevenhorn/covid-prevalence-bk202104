0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1454.86    34.93
p_loo       33.67        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      356   92.7%
 (0.5, 0.7]   (ok)         22    5.7%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.250  0.055   0.156    0.346  ...    23.0      39.0      37.0   1.05
pu        0.794  0.040   0.710    0.848  ...     9.0       9.0      53.0   1.23
mu        0.114  0.019   0.082    0.153  ...    51.0      51.0      58.0   1.06
mus       0.167  0.033   0.107    0.233  ...   114.0     114.0      48.0   1.01
gamma     0.215  0.036   0.146    0.269  ...   128.0     124.0      97.0   1.00
Is_begin  1.541  1.021   0.000    3.396  ...    63.0      41.0      22.0   1.08
Ia_begin  0.811  0.579   0.041    1.925  ...    87.0      68.0      43.0   1.04
E_begin   1.237  1.057   0.006    3.160  ...    58.0      53.0      40.0   1.03

[8 rows x 11 columns]