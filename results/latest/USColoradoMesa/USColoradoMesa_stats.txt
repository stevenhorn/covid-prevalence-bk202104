0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1408.85    33.06
p_loo       25.96        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      363   94.5%
 (0.5, 0.7]   (ok)         15    3.9%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.262  0.051   0.178    0.344  ...    51.0      51.0      80.0   1.03
pu        0.836  0.014   0.811    0.860  ...    34.0      32.0      42.0   1.00
mu        0.125  0.022   0.085    0.159  ...    18.0      18.0      80.0   1.08
mus       0.167  0.030   0.113    0.220  ...    94.0     101.0      60.0   1.03
gamma     0.178  0.031   0.134    0.232  ...    74.0      70.0      60.0   1.01
Is_begin  0.922  0.775   0.027    2.467  ...   152.0     152.0      42.0   1.02
Ia_begin  2.071  1.721   0.070    5.267  ...    84.0      70.0      60.0   1.03
E_begin   0.984  1.052   0.044    3.421  ...    55.0     109.0      20.0   1.01

[8 rows x 11 columns]