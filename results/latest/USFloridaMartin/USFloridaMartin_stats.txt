0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1576.50    31.60
p_loo       31.61        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      346   90.1%
 (0.5, 0.7]   (ok)         33    8.6%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.241  0.054   0.151    0.322  ...    40.0      34.0      39.0   1.06
pu        0.809  0.045   0.738    0.874  ...     5.0       5.0      19.0   1.44
mu        0.114  0.029   0.066    0.162  ...     5.0       6.0      38.0   1.30
mus       0.174  0.029   0.128    0.235  ...    57.0      53.0      83.0   1.03
gamma     0.213  0.040   0.150    0.294  ...   147.0     136.0      60.0   1.01
Is_begin  1.411  1.164   0.016    3.198  ...    74.0      59.0      35.0   1.00
Ia_begin  3.469  2.985   0.259    8.803  ...    59.0      54.0     100.0   1.02
E_begin   2.344  2.205   0.068    5.363  ...    61.0      41.0      60.0   1.02

[8 rows x 11 columns]