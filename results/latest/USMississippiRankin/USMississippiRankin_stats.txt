1 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1579.93    24.53
p_loo       22.20        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      357   93.0%
 (0.5, 0.7]   (ok)         21    5.5%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.254  0.052   0.156    0.347  ...    75.0      71.0      54.0   1.01
pu        0.815  0.027   0.761    0.854  ...     7.0       8.0      57.0   1.24
mu        0.119  0.022   0.085    0.164  ...     4.0       4.0      59.0   1.67
mus       0.177  0.033   0.120    0.229  ...    49.0      49.0      58.0   1.02
gamma     0.200  0.033   0.146    0.273  ...    86.0      96.0      47.0   1.01
Is_begin  1.522  0.986   0.118    3.412  ...    82.0      61.0      60.0   1.02
Ia_begin  0.724  0.512   0.003    1.568  ...    68.0      58.0      40.0   1.03
E_begin   1.087  1.097   0.014    3.658  ...    65.0      75.0      54.0   1.08

[8 rows x 11 columns]