0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -870.22    38.12
p_loo       35.91        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      358   93.2%
 (0.5, 0.7]   (ok)         25    6.5%
   (0.7, 1]   (bad)         0    0.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.241  0.054   0.153    0.330  ...    83.0      78.0      43.0   1.00
pu        0.793  0.027   0.732    0.832  ...    93.0      98.0      97.0   0.99
mu        0.148  0.029   0.102    0.204  ...    31.0      28.0      27.0   1.05
mus       0.211  0.033   0.151    0.267  ...    71.0      82.0      60.0   1.04
gamma     0.267  0.049   0.202    0.371  ...    55.0      63.0      61.0   1.08
Is_begin  0.822  0.857   0.001    2.373  ...    63.0     108.0      18.0   1.08
Ia_begin  1.303  1.264   0.009    3.307  ...    35.0      45.0      40.0   1.05
E_begin   0.651  0.653   0.003    1.853  ...    64.0      74.0      59.0   1.03

[8 rows x 11 columns]