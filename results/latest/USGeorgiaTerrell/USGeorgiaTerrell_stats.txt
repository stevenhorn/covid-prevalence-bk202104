0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -890.29    50.33
p_loo       43.57        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      362   94.3%
 (0.5, 0.7]   (ok)         15    3.9%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.242  0.054   0.160    0.339  ...    43.0      46.0      79.0   1.08
pu        0.772  0.037   0.711    0.831  ...     3.0       4.0      22.0   1.74
mu        0.146  0.023   0.110    0.181  ...    39.0      38.0      58.0   1.01
mus       0.238  0.046   0.160    0.315  ...    68.0      64.0      59.0   1.02
gamma     0.344  0.064   0.223    0.442  ...    48.0      49.0      59.0   1.10
Is_begin  1.820  1.327   0.060    4.207  ...    51.0      37.0      69.0   1.04
Ia_begin  0.744  0.586   0.001    1.978  ...    95.0      78.0      58.0   0.99
E_begin   1.992  1.965   0.058    6.560  ...    92.0      38.0      58.0   1.03

[8 rows x 11 columns]