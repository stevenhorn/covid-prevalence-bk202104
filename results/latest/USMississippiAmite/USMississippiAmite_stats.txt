0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -877.74    28.06
p_loo       30.16        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      357   93.0%
 (0.5, 0.7]   (ok)         19    4.9%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.246  0.057   0.153    0.336  ...    98.0      95.0      59.0   1.06
pu        0.772  0.032   0.713    0.830  ...    29.0      33.0      57.0   1.02
mu        0.133  0.028   0.097    0.193  ...    36.0      37.0      51.0   1.03
mus       0.190  0.028   0.149    0.244  ...   100.0     103.0     100.0   1.00
gamma     0.235  0.044   0.153    0.313  ...    73.0     112.0      45.0   1.03
Is_begin  0.853  0.765   0.015    2.393  ...    84.0      69.0     100.0   1.03
Ia_begin  1.715  1.531   0.064    4.505  ...   121.0     102.0     100.0   1.00
E_begin   0.852  0.927   0.007    2.581  ...   110.0     137.0      57.0   1.02

[8 rows x 11 columns]