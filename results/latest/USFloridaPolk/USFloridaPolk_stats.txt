0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1946.33    36.13
p_loo       25.09        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      354   92.2%
 (0.5, 0.7]   (ok)         27    7.0%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.249  0.053   0.173    0.349  ...    58.0      60.0      32.0   1.11
pu        0.765  0.031   0.717    0.819  ...    46.0      46.0      59.0   1.01
mu        0.116  0.026   0.066    0.152  ...    10.0      10.0      15.0   1.15
mus       0.170  0.031   0.116    0.226  ...   111.0     132.0      44.0   1.08
gamma     0.227  0.042   0.162    0.297  ...   105.0      94.0      55.0   1.03
Is_begin  1.691  1.352   0.048    3.855  ...   152.0     152.0      97.0   0.98
Ia_begin  5.981  3.381   0.790   10.922  ...    94.0     108.0      93.0   1.01
E_begin   5.328  4.452   0.579   13.259  ...   109.0     123.0     102.0   1.00

[8 rows x 11 columns]