1 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1478.12    26.29
p_loo       24.69        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      360   93.8%
 (0.5, 0.7]   (ok)         21    5.5%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.254  0.057   0.167    0.343  ...    91.0      83.0      45.0   1.03
pu        0.827  0.050   0.712    0.893  ...    11.0      11.0      40.0   1.15
mu        0.132  0.028   0.085    0.180  ...    23.0      15.0      21.0   1.12
mus       0.175  0.028   0.128    0.229  ...    30.0      30.0      84.0   1.07
gamma     0.235  0.043   0.158    0.301  ...    58.0      51.0      96.0   1.03
Is_begin  0.741  0.686   0.001    1.795  ...   114.0      89.0      40.0   1.01
Ia_begin  1.549  1.101   0.103    3.701  ...    68.0      66.0      59.0   0.99
E_begin   1.114  1.109   0.035    2.813  ...    84.0      60.0      60.0   1.00

[8 rows x 11 columns]