0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -924.56    28.60
p_loo       33.70        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      354   92.2%
 (0.5, 0.7]   (ok)         24    6.2%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.252  0.058   0.161    0.348  ...    87.0      83.0      58.0   1.00
pu        0.761  0.028   0.710    0.806  ...    53.0      55.0      49.0   1.01
mu        0.158  0.026   0.103    0.194  ...    18.0      18.0      40.0   1.10
mus       0.275  0.039   0.213    0.348  ...    71.0      74.0      91.0   1.00
gamma     0.405  0.064   0.309    0.507  ...   101.0      96.0      99.0   0.99
Is_begin  0.799  0.762   0.009    2.350  ...   100.0     123.0      95.0   0.99
Ia_begin  1.140  1.108   0.015    3.527  ...   135.0      85.0      59.0   1.03
E_begin   0.510  0.572   0.005    1.514  ...    87.0      97.0      60.0   1.00

[8 rows x 11 columns]