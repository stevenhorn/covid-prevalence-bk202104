0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1391.40    45.64
p_loo       30.57        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      357   93.0%
 (0.5, 0.7]   (ok)         22    5.7%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.177  0.012   0.164    0.202  ...     4.0       7.0      17.0   1.33
pu        0.715  0.011   0.703    0.733  ...     3.0       3.0      22.0   1.92
mu        0.188  0.021   0.160    0.213  ...     3.0       3.0      15.0   2.26
mus       0.194  0.024   0.162    0.231  ...     3.0       3.0      29.0   2.03
gamma     0.167  0.013   0.144    0.187  ...     3.0       3.0      27.0   2.38
Is_begin  0.987  0.516   0.382    2.021  ...     7.0       6.0      46.0   1.29
Ia_begin  1.201  0.755   0.251    2.339  ...     3.0       3.0      15.0   2.23
E_begin   0.258  0.163   0.057    0.512  ...     3.0       3.0      14.0   1.94

[8 rows x 11 columns]