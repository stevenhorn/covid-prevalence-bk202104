0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1031.52    29.54
p_loo       26.45        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      354   92.2%
 (0.5, 0.7]   (ok)         27    7.0%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.250  0.059   0.165    0.346  ...    63.0      62.0      54.0   1.05
pu        0.767  0.020   0.736    0.806  ...    42.0      42.0      40.0   1.00
mu        0.127  0.024   0.093    0.177  ...    31.0      45.0      57.0   1.09
mus       0.179  0.033   0.129    0.244  ...   152.0     152.0      46.0   1.07
gamma     0.204  0.038   0.151    0.283  ...    66.0      67.0      40.0   1.03
Is_begin  0.882  0.840   0.025    2.246  ...    60.0      96.0      59.0   1.01
Ia_begin  1.616  1.719   0.040    5.906  ...    80.0      76.0      93.0   1.00
E_begin   0.713  0.760   0.004    2.247  ...    73.0      82.0      60.0   0.99

[8 rows x 11 columns]