0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -459.47    45.39
p_loo       46.02        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      364   94.8%
 (0.5, 0.7]   (ok)         14    3.6%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    4    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.296  0.042   0.207    0.348  ...   112.0     103.0      83.0   1.00
pu        0.892  0.007   0.879    0.900  ...    46.0      55.0      55.0   1.08
mu        0.132  0.024   0.085    0.172  ...    67.0      67.0      56.0   1.04
mus       0.178  0.038   0.121    0.261  ...    77.0     152.0      59.0   1.02
gamma     0.211  0.045   0.141    0.315  ...   145.0     152.0      59.0   1.10
Is_begin  0.729  0.885   0.002    2.559  ...    96.0      64.0      60.0   1.00
Ia_begin  1.072  1.149   0.004    3.147  ...    85.0      67.0      38.0   1.05
E_begin   0.558  0.804   0.001    1.958  ...   101.0      87.0      60.0   1.00

[8 rows x 11 columns]