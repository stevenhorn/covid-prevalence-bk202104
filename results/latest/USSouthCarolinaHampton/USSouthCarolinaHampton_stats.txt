0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -979.22    23.16
p_loo       24.81        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      359   93.5%
 (0.5, 0.7]   (ok)         20    5.2%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.259  0.049   0.172    0.335  ...    58.0      51.0      60.0   1.03
pu        0.816  0.017   0.790    0.848  ...    27.0      28.0      60.0   1.06
mu        0.123  0.019   0.090    0.156  ...    16.0      19.0      59.0   1.09
mus       0.178  0.031   0.135    0.243  ...    85.0      86.0      88.0   1.06
gamma     0.226  0.047   0.150    0.315  ...   123.0     128.0      40.0   0.99
Is_begin  0.921  0.739   0.048    2.343  ...    73.0      67.0      88.0   0.99
Ia_begin  1.337  1.405   0.009    4.271  ...    67.0      57.0     100.0   1.01
E_begin   0.793  0.660   0.031    2.309  ...    93.0      47.0      93.0   1.04

[8 rows x 11 columns]