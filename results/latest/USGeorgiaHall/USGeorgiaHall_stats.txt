0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1791.82    28.99
p_loo       26.88        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      355   92.4%
 (0.5, 0.7]   (ok)         20    5.2%
   (0.7, 1]   (bad)         9    2.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.216  0.037   0.158    0.288  ...    62.0      35.0      39.0   1.05
pu        0.720  0.016   0.700    0.750  ...    30.0      21.0      24.0   1.09
mu        0.114  0.020   0.079    0.150  ...    27.0      27.0      38.0   1.05
mus       0.190  0.032   0.144    0.266  ...    93.0      87.0      60.0   1.01
gamma     0.271  0.039   0.206    0.336  ...   152.0     150.0      96.0   1.00
Is_begin  1.684  1.088   0.059    3.516  ...   139.0     147.0      93.0   1.01
Ia_begin  3.388  2.042   0.103    6.617  ...   152.0     151.0      97.0   1.00
E_begin   3.225  2.764   0.281    9.259  ...    88.0      55.0      59.0   1.01

[8 rows x 11 columns]