0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1075.22    51.88
p_loo       42.83        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      349   90.9%
 (0.5, 0.7]   (ok)         28    7.3%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.257  0.059   0.171    0.343  ...    41.0      45.0      80.0   1.06
pu        0.841  0.027   0.790    0.884  ...    22.0      24.0      33.0   1.06
mu        0.156  0.023   0.115    0.197  ...     7.0       8.0      40.0   1.23
mus       0.220  0.039   0.143    0.278  ...    38.0      40.0      38.0   1.04
gamma     0.274  0.061   0.181    0.380  ...   118.0      75.0      45.0   1.02
Is_begin  0.855  0.713   0.016    2.313  ...    40.0      27.0      31.0   1.04
Ia_begin  1.994  1.877   0.001    5.750  ...    25.0      20.0      16.0   1.07
E_begin   0.851  0.725   0.019    2.197  ...    57.0      56.0      93.0   1.00

[8 rows x 11 columns]