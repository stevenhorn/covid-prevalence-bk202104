0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -2160.26    31.26
p_loo       25.08        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      345   89.8%
 (0.5, 0.7]   (ok)         36    9.4%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.247  0.060   0.162    0.344  ...    45.0      67.0      53.0   0.99
pu        0.746  0.032   0.700    0.799  ...    35.0      36.0      30.0   1.04
mu        0.134  0.033   0.087    0.189  ...     4.0       4.0      15.0   1.53
mus       0.168  0.027   0.127    0.218  ...    30.0      34.0      57.0   1.08
gamma     0.233  0.041   0.169    0.313  ...   113.0     118.0      74.0   1.01
Is_begin  2.135  2.034   0.043    6.261  ...    58.0      45.0      57.0   1.01
Ia_begin  2.756  3.469   0.027    9.503  ...    32.0      18.0      40.0   1.08
E_begin   2.878  4.527   0.004   11.899  ...    53.0      44.0      59.0   1.04

[8 rows x 11 columns]