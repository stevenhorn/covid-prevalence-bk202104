0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -537.34    49.81
p_loo       44.90        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      352   91.7%
 (0.5, 0.7]   (ok)         25    6.5%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.264  0.050   0.185    0.348  ...    57.0      60.0      99.0   1.00
pu        0.829  0.025   0.779    0.868  ...    11.0      14.0      58.0   1.14
mu        0.131  0.024   0.090    0.174  ...    17.0      15.0      36.0   1.13
mus       0.198  0.034   0.147    0.260  ...    58.0      52.0      19.0   1.03
gamma     0.248  0.046   0.172    0.323  ...    96.0      88.0      60.0   0.98
Is_begin  0.500  0.520   0.006    1.539  ...    98.0      71.0      60.0   0.98
Ia_begin  0.896  0.881   0.018    2.212  ...    88.0     109.0      88.0   0.98
E_begin   0.477  0.521   0.003    1.530  ...    99.0      80.0      73.0   1.03

[8 rows x 11 columns]