0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1084.41    37.88
p_loo       35.55        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      348   90.6%
 (0.5, 0.7]   (ok)         31    8.1%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.235  0.055   0.152    0.335  ...    48.0      50.0      43.0   1.05
pu        0.740  0.020   0.704    0.771  ...    47.0      46.0      54.0   1.03
mu        0.127  0.020   0.087    0.155  ...    29.0      28.0      36.0   1.03
mus       0.188  0.031   0.131    0.242  ...    72.0      78.0      87.0   1.01
gamma     0.247  0.039   0.191    0.329  ...    91.0      96.0      53.0   1.01
Is_begin  0.291  0.279   0.007    0.827  ...   143.0     149.0      95.0   1.00
Ia_begin  0.412  0.403   0.005    1.074  ...    52.0      50.0      77.0   1.04
E_begin   0.245  0.281   0.007    0.874  ...   109.0      54.0      42.0   1.03

[8 rows x 11 columns]