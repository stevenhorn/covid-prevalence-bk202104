0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1219.25    33.03
p_loo       36.00        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      364   94.8%
 (0.5, 0.7]   (ok)         16    4.2%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.229  0.047   0.156    0.317  ...    75.0      70.0      37.0   1.04
pu        0.726  0.018   0.700    0.756  ...    78.0      68.0      42.0   1.03
mu        0.160  0.024   0.117    0.208  ...    31.0      31.0      40.0   1.02
mus       0.216  0.041   0.148    0.279  ...    49.0      47.0      59.0   1.01
gamma     0.289  0.056   0.196    0.391  ...    29.0     108.0      80.0   1.05
Is_begin  0.753  0.732   0.002    2.135  ...    89.0      65.0      47.0   1.04
Ia_begin  1.327  1.581   0.008    4.292  ...    39.0      41.0      60.0   1.15
E_begin   0.605  0.955   0.000    1.626  ...    66.0      41.0      57.0   1.04

[8 rows x 11 columns]