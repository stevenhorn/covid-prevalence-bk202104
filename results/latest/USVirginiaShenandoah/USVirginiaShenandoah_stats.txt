0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1130.95    23.69
p_loo       29.37        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      349   90.9%
 (0.5, 0.7]   (ok)         24    6.2%
   (0.7, 1]   (bad)        11    2.9%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.253  0.056   0.151    0.336  ...    20.0      16.0      22.0   1.13
pu        0.790  0.037   0.718    0.851  ...     7.0       8.0      20.0   1.26
mu        0.114  0.020   0.071    0.142  ...    17.0      15.0      60.0   1.14
mus       0.183  0.027   0.134    0.238  ...   130.0     114.0      59.0   1.01
gamma     0.232  0.035   0.170    0.285  ...   152.0     152.0     100.0   0.99
Is_begin  0.712  0.755   0.003    2.185  ...   141.0     116.0      40.0   1.01
Ia_begin  1.151  1.239   0.069    4.114  ...   101.0      79.0      45.0   1.00
E_begin   0.554  0.540   0.002    1.536  ...    75.0      65.0      59.0   1.06

[8 rows x 11 columns]