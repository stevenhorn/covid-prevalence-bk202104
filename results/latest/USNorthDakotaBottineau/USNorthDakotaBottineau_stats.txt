0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -652.01    31.04
p_loo       30.48        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      357   93.0%
 (0.5, 0.7]   (ok)         18    4.7%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.266  0.053   0.159    0.333  ...    44.0      39.0      14.0   1.03
pu        0.810  0.018   0.783    0.850  ...    22.0      26.0      21.0   1.06
mu        0.138  0.025   0.091    0.176  ...    40.0      40.0      72.0   1.04
mus       0.186  0.038   0.125    0.266  ...    55.0      59.0      81.0   1.01
gamma     0.221  0.054   0.128    0.311  ...    44.0      41.0      38.0   1.05
Is_begin  0.490  0.572   0.008    1.628  ...    72.0      56.0      60.0   1.06
Ia_begin  0.995  1.190   0.002    3.622  ...    81.0      53.0      40.0   1.01
E_begin   0.360  0.390   0.002    1.160  ...    82.0      58.0      40.0   1.00

[8 rows x 11 columns]