0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -993.49    30.65
p_loo       27.13        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      347   90.4%
 (0.5, 0.7]   (ok)         29    7.6%
   (0.7, 1]   (bad)         8    2.1%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.247  0.055   0.153    0.327  ...    50.0      48.0      43.0   1.03
pu        0.777  0.022   0.733    0.822  ...    67.0      67.0      59.0   1.00
mu        0.140  0.023   0.101    0.179  ...     8.0       8.0      54.0   1.23
mus       0.195  0.031   0.150    0.245  ...    56.0      64.0      77.0   1.01
gamma     0.245  0.042   0.180    0.328  ...   121.0     113.0      91.0   1.07
Is_begin  1.133  0.999   0.014    2.890  ...   113.0     143.0      29.0   1.00
Ia_begin  1.974  1.297   0.086    4.458  ...    72.0      60.0      91.0   1.03
E_begin   0.982  1.052   0.001    3.279  ...    43.0      34.0      31.0   1.04

[8 rows x 11 columns]