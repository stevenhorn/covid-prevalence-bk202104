0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -968.16    75.80
p_loo       56.64        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      349   90.9%
 (0.5, 0.7]   (ok)         28    7.3%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    4    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.222  0.043   0.155    0.293  ...    88.0     103.0      60.0   1.05
pu        0.729  0.021   0.700    0.767  ...    64.0      54.0      17.0   0.99
mu        0.137  0.020   0.108    0.170  ...    74.0      84.0      96.0   1.06
mus       0.265  0.047   0.175    0.352  ...   107.0     118.0      92.0   1.00
gamma     0.401  0.078   0.246    0.528  ...   110.0     101.0      99.0   1.00
Is_begin  0.931  0.811   0.017    2.432  ...   126.0     124.0      80.0   1.01
Ia_begin  1.324  1.201   0.031    3.818  ...   120.0     125.0      88.0   1.00
E_begin   0.660  0.738   0.011    2.261  ...   101.0     118.0      74.0   1.01

[8 rows x 11 columns]