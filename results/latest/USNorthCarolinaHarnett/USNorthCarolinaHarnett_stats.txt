0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1542.87    39.47
p_loo       35.39        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      361   94.0%
 (0.5, 0.7]   (ok)         17    4.4%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.254  0.052   0.160    0.328  ...   152.0     152.0      97.0   1.00
pu        0.796  0.033   0.724    0.850  ...    23.0      29.0      61.0   1.10
mu        0.110  0.020   0.081    0.145  ...    47.0      42.0      51.0   1.01
mus       0.151  0.025   0.107    0.197  ...   152.0     152.0      60.0   1.02
gamma     0.176  0.032   0.124    0.239  ...    57.0      44.0      87.0   1.05
Is_begin  0.674  0.579   0.054    1.887  ...   136.0     119.0      60.0   0.99
Ia_begin  1.968  1.768   0.019    5.052  ...   124.0     129.0      35.0   1.02
E_begin   1.354  1.521   0.003    4.704  ...    73.0      93.0      44.0   1.02

[8 rows x 11 columns]