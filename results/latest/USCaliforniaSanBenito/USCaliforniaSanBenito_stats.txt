0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1252.22    26.32
p_loo       22.86        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      360   93.8%
 (0.5, 0.7]   (ok)         20    5.2%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.250  0.054   0.151    0.339  ...   106.0      92.0      40.0   1.00
pu        0.825  0.018   0.801    0.861  ...    57.0      57.0      59.0   0.99
mu        0.146  0.043   0.086    0.213  ...     3.0       3.0      27.0   1.96
mus       0.159  0.034   0.091    0.219  ...    59.0      68.0      57.0   1.01
gamma     0.176  0.039   0.120    0.263  ...     9.0       8.0      18.0   1.21
Is_begin  1.137  0.833   0.037    2.706  ...   152.0     141.0      83.0   0.99
Ia_begin  0.675  0.601   0.003    1.888  ...    94.0      55.0      59.0   1.02
E_begin   0.719  0.702   0.003    2.223  ...   103.0     126.0      60.0   1.00

[8 rows x 11 columns]