0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1155.54    31.35
p_loo       24.11        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      356   92.7%
 (0.5, 0.7]   (ok)         19    4.9%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    4    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.245  0.053   0.167    0.329  ...    38.0      40.0      40.0   1.03
pu        0.818  0.020   0.785    0.858  ...    58.0      62.0      49.0   1.00
mu        0.114  0.019   0.083    0.152  ...    26.0      30.0      59.0   1.04
mus       0.159  0.028   0.113    0.207  ...   118.0     127.0      91.0   1.08
gamma     0.165  0.029   0.123    0.225  ...    66.0      63.0      99.0   1.01
Is_begin  0.858  0.847   0.083    2.433  ...    47.0      59.0      32.0   1.00
Ia_begin  1.905  1.703   0.009    4.905  ...    53.0      22.0      15.0   1.06
E_begin   0.812  0.977   0.007    2.630  ...    43.0      21.0      16.0   1.06

[8 rows x 11 columns]