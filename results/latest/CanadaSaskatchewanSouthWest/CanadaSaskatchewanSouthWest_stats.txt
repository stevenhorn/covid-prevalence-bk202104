0 Divergences 
Passed validation 
Computed from 80 by 247 log-likelihood matrix

         Estimate       SE
elpd_loo  -493.30    26.46
p_loo       27.87        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      218   88.3%
 (0.5, 0.7]   (ok)         24    9.7%
   (0.7, 1]   (bad)         2    0.8%
   (1, Inf)   (very bad)    3    1.2%

            mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.239  0.048   0.167    0.326  ...    85.0     101.0      97.0   1.02
pu         0.777  0.042   0.717    0.860  ...    20.0      25.0      79.0   1.08
mu         0.228  0.035   0.187    0.309  ...    34.0      31.0      45.0   1.02
mus        0.302  0.042   0.234    0.384  ...   136.0     130.0      59.0   1.01
gamma      0.472  0.066   0.367    0.601  ...   147.0     127.0      56.0   1.02
Is_begin   8.152  3.514   1.355   13.320  ...    72.0      71.0      43.0   1.01
Ia_begin   3.181  2.172   0.127    7.480  ...    69.0      74.0      60.0   1.04
E_begin   11.144  7.712   0.545   26.748  ...    18.0      20.0      59.0   1.11

[8 rows x 11 columns]