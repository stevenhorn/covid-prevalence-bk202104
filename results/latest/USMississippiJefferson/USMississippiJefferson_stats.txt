0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -767.26    30.18
p_loo       26.40        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      368   95.8%
 (0.5, 0.7]   (ok)         13    3.4%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.262  0.051   0.174    0.338  ...    51.0      49.0      48.0   1.04
pu        0.820  0.022   0.778    0.856  ...    22.0      34.0      33.0   1.03
mu        0.125  0.018   0.102    0.169  ...    43.0      44.0      56.0   1.03
mus       0.178  0.042   0.124    0.293  ...   102.0     144.0      22.0   1.02
gamma     0.209  0.037   0.152    0.289  ...   151.0     152.0      59.0   1.00
Is_begin  0.726  0.628   0.032    2.051  ...    87.0      72.0      60.0   1.00
Ia_begin  1.193  1.342   0.003    3.220  ...   115.0      92.0      87.0   1.02
E_begin   0.656  0.861   0.001    2.818  ...   115.0      76.0      43.0   1.02

[8 rows x 11 columns]