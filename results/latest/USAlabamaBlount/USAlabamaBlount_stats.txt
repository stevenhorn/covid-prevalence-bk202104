0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1364.66    44.85
p_loo       32.44        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      352   91.7%
 (0.5, 0.7]   (ok)         28    7.3%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.256  0.068   0.160    0.349  ...    37.0      14.0      23.0   1.12
pu        0.756  0.025   0.715    0.799  ...    44.0      44.0      91.0   0.99
mu        0.116  0.023   0.082    0.158  ...    38.0      41.0      77.0   0.99
mus       0.184  0.038   0.125    0.254  ...    98.0     106.0      39.0   0.99
gamma     0.194  0.049   0.116    0.274  ...   109.0      98.0     100.0   1.00
Is_begin  0.776  0.808   0.008    2.392  ...   117.0     129.0      58.0   0.98
Ia_begin  1.349  1.103   0.051    3.353  ...   102.0     133.0      86.0   0.99
E_begin   0.643  0.624   0.023    1.921  ...    51.0      98.0      59.0   1.01

[8 rows x 11 columns]