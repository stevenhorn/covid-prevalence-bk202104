0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1297.05    35.56
p_loo       34.61        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      356   92.7%
 (0.5, 0.7]   (ok)         17    4.4%
   (0.7, 1]   (bad)         9    2.3%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.257  0.064   0.160    0.349  ...    88.0      79.0      40.0   1.04
pu        0.744  0.037   0.701    0.814  ...     7.0       8.0      54.0   1.22
mu        0.109  0.020   0.075    0.148  ...    32.0      31.0      60.0   1.00
mus       0.174  0.027   0.130    0.226  ...   152.0     152.0      45.0   1.00
gamma     0.225  0.041   0.156    0.291  ...   104.0     105.0      79.0   1.00
Is_begin  0.959  0.775   0.023    2.597  ...   103.0      87.0      93.0   1.01
Ia_begin  2.402  2.387   0.036    7.284  ...   113.0      63.0      24.0   1.00
E_begin   1.445  1.437   0.020    4.588  ...    98.0      69.0      93.0   1.02

[8 rows x 11 columns]