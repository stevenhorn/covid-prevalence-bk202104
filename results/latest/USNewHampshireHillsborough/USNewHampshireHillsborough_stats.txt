0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1880.91    37.89
p_loo       32.73        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      356   92.7%
 (0.5, 0.7]   (ok)         19    4.9%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.235  0.051   0.156    0.327  ...    91.0      88.0      60.0   1.02
pu        0.756  0.038   0.702    0.815  ...    23.0      23.0      29.0   1.12
mu        0.103  0.015   0.071    0.125  ...    24.0      23.0      40.0   1.08
mus       0.172  0.032   0.116    0.228  ...   109.0     152.0      91.0   1.01
gamma     0.249  0.042   0.179    0.324  ...   152.0     142.0      60.0   1.03
Is_begin  2.258  2.095   0.037    5.791  ...    80.0      31.0      48.0   1.07
Ia_begin  6.328  3.687   1.698   14.566  ...    81.0      37.0      38.0   1.05
E_begin   6.395  5.368   0.362   15.212  ...    82.0      70.0      77.0   1.01

[8 rows x 11 columns]