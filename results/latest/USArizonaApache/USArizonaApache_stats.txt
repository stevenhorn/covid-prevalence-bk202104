0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1685.84    41.32
p_loo       32.92        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      355   92.4%
 (0.5, 0.7]   (ok)         22    5.7%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.169  0.018   0.150    0.204  ...   106.0      44.0      68.0   1.03
pu        0.707  0.006   0.700    0.718  ...    64.0      90.0      72.0   1.04
mu        0.088  0.015   0.063    0.122  ...    55.0      56.0      40.0   0.98
mus       0.156  0.031   0.102    0.210  ...   146.0     152.0      59.0   1.00
gamma     0.204  0.038   0.142    0.276  ...    67.0      70.0      84.0   1.02
Is_begin  0.820  0.784   0.002    2.195  ...    92.0      38.0      69.0   1.06
Ia_begin  0.689  0.483   0.025    1.526  ...   125.0     114.0      57.0   1.02
E_begin   0.598  0.790   0.025    2.346  ...   102.0      87.0      54.0   1.01

[8 rows x 11 columns]