0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1309.74    58.81
p_loo       32.91        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      365   95.1%
 (0.5, 0.7]   (ok)         17    4.4%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.236  0.049   0.166    0.321  ...    48.0      59.0      88.0   1.08
pu        0.735  0.019   0.701    0.769  ...    68.0      64.0      34.0   1.05
mu        0.134  0.022   0.098    0.170  ...     8.0       7.0      43.0   1.25
mus       0.213  0.041   0.161    0.313  ...    69.0      60.0      30.0   1.00
gamma     0.248  0.044   0.177    0.334  ...    94.0      92.0      96.0   1.01
Is_begin  0.798  0.847   0.002    2.556  ...    27.0      22.0      22.0   1.11
Ia_begin  0.649  0.455   0.012    1.428  ...    88.0      79.0      59.0   1.01
E_begin   0.492  0.567   0.006    1.763  ...    75.0      41.0      60.0   1.04

[8 rows x 11 columns]