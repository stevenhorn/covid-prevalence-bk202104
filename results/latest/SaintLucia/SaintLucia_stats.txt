0 Divergences 
Failed validation 
Computed from 80 by 385 log-likelihood matrix

         Estimate       SE
elpd_loo -1325.78    52.17
p_loo       43.00        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      347   90.1%
 (0.5, 0.7]   (ok)         29    7.5%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.307  0.032   0.238    0.348  ...    57.0      27.0      40.0   1.03
pu        0.893  0.005   0.882    0.900  ...   106.0     102.0      53.0   1.00
mu        0.163  0.024   0.129    0.215  ...     6.0       6.0      15.0   1.37
mus       0.182  0.030   0.134    0.233  ...    61.0      62.0      60.0   1.02
gamma     0.228  0.039   0.153    0.288  ...    13.0      17.0      26.0   1.10
Is_begin  1.358  1.045   0.066    3.204  ...    62.0      57.0      60.0   1.08
Ia_begin  0.685  0.719   0.010    1.893  ...    60.0      39.0      37.0   1.12
E_begin   0.690  0.647   0.007    1.988  ...    52.0      45.0      38.0   0.99

[8 rows x 11 columns]