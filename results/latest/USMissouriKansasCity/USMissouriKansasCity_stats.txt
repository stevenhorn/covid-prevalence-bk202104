0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -2270.11    67.47
p_loo       45.63        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      349   90.9%
 (0.5, 0.7]   (ok)         30    7.8%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.210  0.045   0.152    0.300  ...   109.0      81.0      58.0   1.03
pu        0.724  0.020   0.700    0.767  ...   114.0      91.0      57.0   1.02
mu        0.113  0.018   0.085    0.144  ...    57.0      55.0      54.0   1.00
mus       0.249  0.038   0.185    0.320  ...   152.0     152.0      87.0   1.01
gamma     0.407  0.071   0.306    0.567  ...    61.0      60.0      59.0   1.00
Is_begin  1.570  1.061   0.121    3.604  ...    98.0      89.0      59.0   1.01
Ia_begin  2.233  1.831   0.021    6.246  ...   109.0      50.0      80.0   1.04
E_begin   1.184  1.204   0.004    3.550  ...    79.0      57.0      35.0   1.00

[8 rows x 11 columns]