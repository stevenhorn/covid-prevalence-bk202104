0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1463.98    31.44
p_loo       35.67        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      347   90.4%
 (0.5, 0.7]   (ok)         30    7.8%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.256  0.053   0.174    0.341  ...   118.0     106.0      59.0   1.01
pu        0.785  0.047   0.711    0.864  ...    15.0      16.0      37.0   1.13
mu        0.188  0.032   0.133    0.246  ...     7.0       7.0      48.0   1.28
mus       0.259  0.039   0.195    0.318  ...   152.0     147.0      48.0   1.00
gamma     0.401  0.050   0.313    0.473  ...    89.0      91.0      60.0   1.01
Is_begin  0.868  0.639   0.089    2.052  ...   122.0      88.0      91.0   0.99
Ia_begin  1.778  1.139   0.198    3.860  ...    56.0      48.0      65.0   1.02
E_begin   1.715  1.575   0.080    5.036  ...    74.0      62.0      57.0   1.03

[8 rows x 11 columns]