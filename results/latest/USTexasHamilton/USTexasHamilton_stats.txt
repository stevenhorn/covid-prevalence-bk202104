0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1014.29    66.16
p_loo       51.45        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      367   95.6%
 (0.5, 0.7]   (ok)         11    2.9%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    4    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.261  0.052   0.169    0.350  ...    57.0      52.0      22.0   1.05
pu        0.789  0.028   0.741    0.832  ...    31.0      38.0      49.0   1.08
mu        0.128  0.025   0.079    0.166  ...    14.0      16.0      59.0   1.12
mus       0.226  0.033   0.167    0.285  ...    60.0      72.0      59.0   1.02
gamma     0.294  0.050   0.231    0.419  ...   113.0     146.0      88.0   1.00
Is_begin  0.634  0.725   0.020    2.149  ...    67.0      46.0      38.0   1.06
Ia_begin  0.981  1.543   0.001    3.145  ...    77.0      56.0      59.0   1.02
E_begin   0.590  0.669   0.011    1.552  ...    86.0      63.0      60.0   1.00

[8 rows x 11 columns]