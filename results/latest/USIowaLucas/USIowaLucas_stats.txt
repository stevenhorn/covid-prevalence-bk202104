0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -720.05    25.55
p_loo       28.55        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      355   92.4%
 (0.5, 0.7]   (ok)         23    6.0%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.243  0.046   0.163    0.314  ...    91.0      85.0      93.0   1.00
pu        0.778  0.038   0.714    0.838  ...    12.0      12.0      17.0   1.15
mu        0.140  0.025   0.095    0.182  ...    11.0      11.0      21.0   1.14
mus       0.209  0.042   0.132    0.293  ...   101.0     119.0     100.0   1.00
gamma     0.249  0.037   0.194    0.336  ...    75.0      79.0     102.0   1.03
Is_begin  0.425  0.516   0.000    1.463  ...    52.0      27.0      59.0   1.08
Ia_begin  0.646  1.034   0.005    2.410  ...    75.0      63.0      91.0   1.07
E_begin   0.332  0.565   0.000    1.050  ...    71.0      59.0      60.0   1.07

[8 rows x 11 columns]