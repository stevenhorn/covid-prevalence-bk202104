0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1143.93    27.11
p_loo       26.73        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      358   93.2%
 (0.5, 0.7]   (ok)         22    5.7%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.249  0.051   0.155    0.322  ...    65.0      67.0      91.0   1.01
pu        0.765  0.021   0.726    0.803  ...    17.0      19.0      17.0   1.05
mu        0.123  0.019   0.086    0.152  ...    17.0      16.0      45.0   1.08
mus       0.161  0.038   0.104    0.235  ...    44.0      57.0      39.0   1.07
gamma     0.176  0.037   0.114    0.237  ...    46.0      40.0      31.0   1.02
Is_begin  0.962  0.660   0.066    2.346  ...    66.0      64.0      60.0   1.02
Ia_begin  2.003  1.525   0.125    4.308  ...    67.0      75.0      54.0   1.01
E_begin   1.260  1.246   0.031    3.892  ...    46.0      58.0      40.0   1.04

[8 rows x 11 columns]