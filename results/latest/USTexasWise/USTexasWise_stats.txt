0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1524.18    41.46
p_loo       31.50        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      356   92.7%
 (0.5, 0.7]   (ok)         24    6.2%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.243  0.051   0.169    0.335  ...   152.0     152.0      60.0   1.00
pu        0.762  0.023   0.726    0.804  ...   152.0     152.0      99.0   1.08
mu        0.137  0.031   0.088    0.190  ...    59.0      57.0      60.0   1.06
mus       0.169  0.035   0.109    0.234  ...   152.0     152.0      58.0   1.03
gamma     0.177  0.039   0.114    0.258  ...    94.0      91.0      93.0   1.02
Is_begin  0.675  0.669   0.001    2.307  ...    91.0      80.0      79.0   1.02
Ia_begin  1.361  1.451   0.033    4.899  ...    53.0      69.0      54.0   1.03
E_begin   0.654  0.786   0.017    2.035  ...    66.0      69.0      46.0   1.00

[8 rows x 11 columns]