0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -820.70    21.26
p_loo       23.14        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      354   92.2%
 (0.5, 0.7]   (ok)         25    6.5%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.252  0.055   0.167    0.347  ...    73.0      72.0      43.0   1.02
pu        0.812  0.026   0.760    0.856  ...    30.0      24.0      58.0   1.07
mu        0.124  0.021   0.091    0.166  ...    17.0      25.0      53.0   1.09
mus       0.160  0.028   0.111    0.209  ...   107.0     121.0      96.0   1.01
gamma     0.201  0.041   0.135    0.273  ...    30.0      36.0      58.0   1.05
Is_begin  0.866  0.693   0.014    2.355  ...    87.0      95.0      96.0   1.02
Ia_begin  1.566  1.461   0.030    4.128  ...    88.0      89.0      39.0   0.99
E_begin   0.867  0.946   0.008    2.421  ...   105.0      55.0      40.0   1.00

[8 rows x 11 columns]