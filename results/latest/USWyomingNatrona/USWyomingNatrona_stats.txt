0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1320.23    35.08
p_loo       28.37        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      357   93.0%
 (0.5, 0.7]   (ok)         24    6.2%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.255  0.061   0.164    0.349  ...    44.0      52.0      55.0   1.03
pu        0.783  0.026   0.734    0.832  ...     9.0       9.0      48.0   1.18
mu        0.137  0.027   0.101    0.191  ...    10.0      10.0      22.0   1.22
mus       0.191  0.035   0.130    0.236  ...   125.0      70.0      59.0   1.06
gamma     0.221  0.038   0.153    0.288  ...    24.0      18.0      59.0   1.09
Is_begin  0.908  0.844   0.040    2.947  ...    72.0      40.0      14.0   1.03
Ia_begin  1.770  1.424   0.009    4.252  ...    47.0      27.0      14.0   1.04
E_begin   0.829  0.738   0.045    2.081  ...    39.0      35.0      57.0   1.01

[8 rows x 11 columns]