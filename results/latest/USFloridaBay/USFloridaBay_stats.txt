0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1709.40    39.26
p_loo       36.12        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      362   94.3%
 (0.5, 0.7]   (ok)         17    4.4%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.239  0.048   0.159    0.319  ...    52.0      75.0      99.0   1.02
pu        0.771  0.021   0.737    0.815  ...    50.0      53.0      40.0   1.01
mu        0.125  0.027   0.071    0.161  ...     8.0       7.0      24.0   1.30
mus       0.190  0.031   0.143    0.259  ...    67.0      82.0      40.0   1.01
gamma     0.237  0.038   0.173    0.316  ...    81.0      78.0      75.0   1.00
Is_begin  0.822  0.632   0.020    1.912  ...   125.0     140.0      54.0   1.00
Ia_begin  1.543  1.356   0.095    4.205  ...   100.0      94.0      55.0   0.99
E_begin   0.744  0.759   0.010    2.253  ...   112.0      91.0      57.0   1.01

[8 rows x 11 columns]