0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1168.22    66.80
p_loo       52.06        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      365   95.1%
 (0.5, 0.7]   (ok)          8    2.1%
   (0.7, 1]   (bad)         9    2.3%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.183  0.026   0.150    0.235  ...   101.0     128.0      59.0   1.10
pu        0.710  0.011   0.700    0.735  ...   108.0      96.0      38.0   1.04
mu        0.112  0.016   0.085    0.144  ...    57.0      57.0      49.0   1.00
mus       0.202  0.036   0.136    0.263  ...   132.0     152.0      38.0   1.01
gamma     0.253  0.052   0.181    0.351  ...    77.0      98.0      64.0   1.02
Is_begin  0.668  0.548   0.043    1.600  ...    82.0      71.0      60.0   1.02
Ia_begin  1.434  1.857   0.001    5.340  ...   107.0      89.0      59.0   1.01
E_begin   0.580  0.665   0.005    1.361  ...   104.0     125.0      65.0   1.00

[8 rows x 11 columns]