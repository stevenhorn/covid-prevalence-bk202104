0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo   -62.36    30.04
p_loo       35.85        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      358   93.2%
 (0.5, 0.7]   (ok)         22    5.7%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.268  0.055   0.154    0.337  ...    50.0      53.0      35.0   1.06
pu        0.880  0.017   0.845    0.900  ...    38.0      28.0      59.0   1.05
mu        0.146  0.027   0.104    0.192  ...    58.0      63.0      40.0   1.06
mus       0.161  0.040   0.095    0.228  ...   152.0     152.0      48.0   0.98
gamma     0.182  0.037   0.119    0.254  ...    24.0      24.0      43.0   1.09
Is_begin  0.605  0.663   0.001    1.894  ...    95.0      45.0      17.0   1.00
Ia_begin  1.464  1.015   0.097    3.223  ...    53.0      47.0      40.0   1.01
E_begin   0.666  0.714   0.005    1.989  ...    89.0      93.0      69.0   1.05

[8 rows x 11 columns]