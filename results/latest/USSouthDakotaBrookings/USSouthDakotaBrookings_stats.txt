0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1083.41    26.70
p_loo       27.24        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      354   92.2%
 (0.5, 0.7]   (ok)         20    5.2%
   (0.7, 1]   (bad)         8    2.1%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.223  0.041   0.157    0.289  ...   102.0     115.0      20.0   1.11
pu        0.723  0.016   0.702    0.756  ...   107.0     106.0      97.0   0.99
mu        0.123  0.020   0.093    0.167  ...    58.0      66.0      22.0   1.06
mus       0.215  0.034   0.149    0.269  ...   126.0     110.0      60.0   1.02
gamma     0.284  0.052   0.209    0.385  ...    73.0      73.0      60.0   0.98
Is_begin  0.669  0.750   0.005    2.006  ...   109.0     126.0      38.0   0.98
Ia_begin  1.176  1.204   0.017    2.859  ...   106.0      87.0      99.0   1.02
E_begin   0.576  0.605   0.005    1.723  ...    92.0      81.0      58.0   1.03

[8 rows x 11 columns]