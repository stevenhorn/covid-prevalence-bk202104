0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1088.85    48.26
p_loo       35.33        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      354   92.2%
 (0.5, 0.7]   (ok)         22    5.7%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.255  0.049   0.183    0.342  ...   104.0     102.0      36.0   1.06
pu        0.775  0.029   0.722    0.818  ...    11.0      11.0      56.0   1.15
mu        0.107  0.019   0.077    0.139  ...    41.0      41.0      91.0   1.02
mus       0.172  0.034   0.112    0.240  ...   152.0     152.0      54.0   1.01
gamma     0.188  0.038   0.116    0.264  ...    94.0      76.0      59.0   1.02
Is_begin  0.871  0.724   0.012    2.250  ...   151.0      99.0      58.0   1.02
Ia_begin  1.668  1.545   0.025    4.693  ...   144.0     152.0     100.0   1.00
E_begin   0.946  1.159   0.006    2.914  ...   127.0     119.0      87.0   0.99

[8 rows x 11 columns]