0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -785.95    34.40
p_loo       32.32        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      360   93.8%
 (0.5, 0.7]   (ok)         16    4.2%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.169  0.018   0.150    0.204  ...    63.0      44.0      25.0   1.06
pu        0.707  0.006   0.700    0.719  ...    55.0      44.0      60.0   1.10
mu        0.105  0.019   0.067    0.134  ...    11.0      11.0      46.0   1.14
mus       0.188  0.030   0.150    0.253  ...    60.0      52.0      40.0   1.00
gamma     0.236  0.040   0.172    0.313  ...    67.0      66.0      61.0   1.04
Is_begin  0.502  0.538   0.001    1.419  ...   100.0      54.0      26.0   1.03
Ia_begin  0.618  0.554   0.016    1.791  ...    89.0      80.0      73.0   0.99
E_begin   0.282  0.324   0.015    1.081  ...    94.0      72.0     102.0   0.99

[8 rows x 11 columns]