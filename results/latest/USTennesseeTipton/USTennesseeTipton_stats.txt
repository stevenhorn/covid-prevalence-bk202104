0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1529.97    53.25
p_loo       42.92        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      357   93.0%
 (0.5, 0.7]   (ok)         20    5.2%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.252  0.053   0.166    0.334  ...    52.0      55.0      59.0   1.00
pu        0.772  0.023   0.733    0.811  ...    29.0      30.0      60.0   1.04
mu        0.142  0.032   0.082    0.190  ...     4.0       5.0      42.0   1.47
mus       0.229  0.038   0.163    0.294  ...    92.0      85.0     100.0   1.02
gamma     0.273  0.060   0.181    0.409  ...   152.0     152.0      70.0   1.00
Is_begin  0.713  0.888   0.013    1.830  ...    93.0      65.0      83.0   1.02
Ia_begin  0.565  0.578   0.014    1.709  ...   144.0      83.0     100.0   1.01
E_begin   0.479  0.571   0.001    1.346  ...    97.0      57.0      60.0   1.07

[8 rows x 11 columns]