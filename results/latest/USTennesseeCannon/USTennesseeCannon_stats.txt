0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -986.32    26.87
p_loo       22.83        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      350   91.1%
 (0.5, 0.7]   (ok)         27    7.0%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.247  0.057   0.154    0.333  ...    60.0      61.0      59.0   1.02
pu        0.784  0.023   0.749    0.822  ...    58.0      58.0      39.0   0.99
mu        0.116  0.027   0.078    0.169  ...    14.0      16.0      59.0   1.08
mus       0.158  0.032   0.100    0.214  ...    51.0      42.0      60.0   1.07
gamma     0.179  0.041   0.116    0.252  ...    78.0      83.0      73.0   1.03
Is_begin  0.641  0.632   0.003    1.715  ...   120.0      90.0      59.0   1.01
Ia_begin  1.171  1.134   0.017    3.205  ...   111.0      83.0      88.0   0.98
E_begin   0.522  0.573   0.016    1.599  ...    83.0      74.0      93.0   0.98

[8 rows x 11 columns]