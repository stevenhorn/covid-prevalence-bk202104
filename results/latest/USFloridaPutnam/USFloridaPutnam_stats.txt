1 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1291.67    28.32
p_loo       27.56        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      365   95.1%
 (0.5, 0.7]   (ok)         14    3.6%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.251  0.053   0.161    0.347  ...    39.0      34.0      24.0   1.02
pu        0.783  0.030   0.717    0.823  ...     8.0      13.0      23.0   1.16
mu        0.140  0.020   0.106    0.178  ...    34.0      32.0      51.0   0.99
mus       0.177  0.025   0.137    0.220  ...   103.0     114.0      60.0   1.05
gamma     0.242  0.045   0.164    0.322  ...   116.0     151.0      66.0   1.03
Is_begin  1.377  1.104   0.008    3.402  ...   136.0     130.0      59.0   1.00
Ia_begin  3.044  2.196   0.013    6.643  ...   133.0     121.0      56.0   0.99
E_begin   1.946  1.374   0.027    3.944  ...   110.0      88.0      72.0   1.04

[8 rows x 11 columns]