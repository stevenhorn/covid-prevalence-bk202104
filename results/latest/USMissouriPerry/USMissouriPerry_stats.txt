0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1169.37    59.35
p_loo       43.32        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      346   90.1%
 (0.5, 0.7]   (ok)         32    8.3%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.219  0.044   0.160    0.296  ...    73.0      93.0      96.0   0.99
pu        0.720  0.015   0.701    0.746  ...    72.0      67.0      56.0   1.01
mu        0.146  0.026   0.104    0.190  ...     9.0       8.0      60.0   1.22
mus       0.239  0.044   0.158    0.324  ...    53.0      52.0      40.0   1.04
gamma     0.326  0.059   0.224    0.437  ...    94.0      91.0      93.0   1.07
Is_begin  0.882  0.922   0.013    2.617  ...    89.0      50.0      32.0   0.99
Ia_begin  1.648  1.364   0.005    4.059  ...    71.0      50.0      57.0   0.99
E_begin   0.675  0.617   0.000    1.748  ...    55.0      36.0      22.0   1.01

[8 rows x 11 columns]