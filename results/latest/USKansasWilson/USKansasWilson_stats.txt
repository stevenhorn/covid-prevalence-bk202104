0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1018.81    50.46
p_loo       39.06        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      352   91.7%
 (0.5, 0.7]   (ok)         23    6.0%
   (0.7, 1]   (bad)         8    2.1%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.239  0.053   0.157    0.332  ...    10.0      10.0      31.0   1.19
pu        0.749  0.028   0.710    0.798  ...    30.0      30.0      85.0   1.04
mu        0.122  0.018   0.079    0.152  ...    11.0      12.0      24.0   1.14
mus       0.167  0.028   0.120    0.218  ...    64.0      69.0      49.0   1.01
gamma     0.181  0.032   0.127    0.242  ...    51.0      48.0      74.0   1.06
Is_begin  0.527  0.530   0.014    1.660  ...    86.0     129.0      79.0   1.08
Ia_begin  0.746  0.744   0.021    1.959  ...    49.0      38.0      16.0   1.07
E_begin   0.389  0.541   0.008    1.422  ...    71.0      20.0      80.0   1.09

[8 rows x 11 columns]