0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -682.47    60.66
p_loo       45.98        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      360   93.8%
 (0.5, 0.7]   (ok)         18    4.7%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.243  0.054   0.152    0.329  ...   152.0     152.0     100.0   1.01
pu        0.750  0.029   0.700    0.794  ...    83.0      88.0      33.0   1.00
mu        0.154  0.029   0.107    0.208  ...    92.0      91.0      59.0   1.02
mus       0.261  0.042   0.165    0.325  ...    87.0      87.0      58.0   1.01
gamma     0.364  0.060   0.275    0.478  ...   119.0     120.0      96.0   1.01
Is_begin  0.923  0.788   0.079    2.127  ...   115.0     111.0      93.0   1.02
Ia_begin  1.624  1.973   0.003    4.626  ...    69.0     115.0      59.0   1.00
E_begin   0.754  0.924   0.010    3.043  ...   107.0      95.0      93.0   1.02

[8 rows x 11 columns]