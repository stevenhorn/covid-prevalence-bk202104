0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1373.17    53.57
p_loo       40.68        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      353   91.9%
 (0.5, 0.7]   (ok)         26    6.8%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.250  0.050   0.164    0.333  ...    92.0      82.0      20.0   1.06
pu        0.759  0.026   0.715    0.810  ...    43.0      42.0      37.0   1.04
mu        0.111  0.022   0.075    0.149  ...    23.0      26.0      59.0   1.06
mus       0.230  0.038   0.155    0.288  ...    38.0      34.0      58.0   1.03
gamma     0.257  0.045   0.167    0.331  ...   114.0     106.0      69.0   1.00
Is_begin  0.638  0.770   0.003    1.913  ...    67.0      49.0      44.0   1.04
Ia_begin  1.355  1.552   0.016    3.335  ...    19.0      35.0      48.0   1.06
E_begin   0.697  0.792   0.004    2.347  ...    50.0      31.0      22.0   1.07

[8 rows x 11 columns]