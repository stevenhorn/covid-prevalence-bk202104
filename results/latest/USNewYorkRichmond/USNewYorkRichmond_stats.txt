0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -2032.50    63.29
p_loo       42.46        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      338   88.0%
 (0.5, 0.7]   (ok)         39   10.2%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    1    0.3%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.180   0.027   0.150    0.230  ...    85.0      65.0      43.0   1.15
pu         0.710   0.009   0.700    0.728  ...   122.0      97.0      57.0   0.99
mu         0.092   0.010   0.073    0.113  ...    32.0      30.0      22.0   1.02
mus        0.258   0.046   0.185    0.325  ...   152.0     152.0      57.0   1.10
gamma      0.668   0.074   0.568    0.826  ...    93.0      91.0      95.0   1.00
Is_begin   3.485   1.916   0.770    6.974  ...    98.0      82.0      58.0   1.02
Ia_begin  17.321   3.582  10.661   22.687  ...    84.0      86.0      81.0   1.00
E_begin   78.539  22.135  38.293  119.230  ...    40.0      40.0      39.0   1.04

[8 rows x 11 columns]