0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1715.19    70.08
p_loo       43.66        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      354   92.2%
 (0.5, 0.7]   (ok)         25    6.5%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.253  0.055   0.165    0.349  ...    39.0      37.0      14.0   1.07
pu        0.748  0.024   0.702    0.783  ...    29.0      26.0      20.0   1.07
mu        0.124  0.015   0.097    0.147  ...    37.0      37.0      77.0   1.01
mus       0.264  0.050   0.180    0.333  ...   152.0     152.0      63.0   1.01
gamma     0.361  0.061   0.274    0.508  ...    89.0      93.0     100.0   1.00
Is_begin  0.639  0.494   0.009    1.692  ...   116.0      89.0      38.0   1.03
Ia_begin  1.311  0.942   0.029    3.003  ...    69.0      54.0      42.0   1.03
E_begin   0.510  0.464   0.003    1.401  ...    67.0      62.0      61.0   1.01

[8 rows x 11 columns]