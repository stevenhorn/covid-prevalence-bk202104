0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1139.84    28.04
p_loo       29.07        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      355   92.4%
 (0.5, 0.7]   (ok)         24    6.2%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.253  0.052   0.159    0.343  ...    51.0      46.0      33.0   1.04
pu        0.817  0.033   0.743    0.864  ...     8.0       7.0      18.0   1.27
mu        0.119  0.024   0.074    0.163  ...     7.0       7.0      24.0   1.25
mus       0.203  0.027   0.154    0.255  ...    43.0      44.0      56.0   1.04
gamma     0.259  0.051   0.186    0.379  ...   152.0     152.0      88.0   1.00
Is_begin  0.808  0.539   0.015    1.671  ...   152.0     138.0      40.0   1.01
Ia_begin  1.616  1.200   0.020    3.861  ...    86.0      59.0      60.0   0.98
E_begin   1.034  1.443   0.007    2.859  ...    90.0      70.0      79.0   0.99

[8 rows x 11 columns]