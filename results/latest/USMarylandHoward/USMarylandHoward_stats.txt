11 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1567.10    24.39
p_loo       28.39        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      345   89.8%
 (0.5, 0.7]   (ok)         29    7.6%
   (0.7, 1]   (bad)         8    2.1%
   (1, Inf)   (very bad)    2    0.5%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.228   0.057   0.151    0.324  ...    90.0      78.0      39.0   1.08
pu         0.765   0.044   0.701    0.838  ...    27.0      27.0      18.0   1.18
mu         0.111   0.019   0.080    0.139  ...    39.0      28.0      37.0   1.07
mus        0.191   0.032   0.137    0.265  ...    31.0      23.0      76.0   1.06
gamma      0.251   0.044   0.188    0.337  ...    89.0      74.0      17.0   1.02
Is_begin   5.709   3.902   0.189   12.686  ...    38.0      35.0      38.0   1.02
Ia_begin   8.824   7.169   0.024   21.614  ...    11.0       8.0      51.0   1.22
E_begin   11.942  10.879   0.079   31.574  ...    28.0      23.0      60.0   1.08

[8 rows x 11 columns]