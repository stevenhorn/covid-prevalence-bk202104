0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -930.62    23.84
p_loo       25.34        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      356   92.7%
 (0.5, 0.7]   (ok)         24    6.2%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.241  0.053   0.154    0.328  ...   152.0     152.0      83.0   1.08
pu        0.757  0.040   0.701    0.818  ...   132.0      98.0      42.0   1.00
mu        0.140  0.025   0.095    0.181  ...    59.0      59.0      59.0   1.01
mus       0.223  0.035   0.162    0.284  ...    98.0      90.0      60.0   1.00
gamma     0.327  0.045   0.242    0.399  ...   147.0     151.0      60.0   0.99
Is_begin  0.878  0.626   0.006    1.983  ...    70.0      59.0      60.0   1.02
Ia_begin  3.832  2.485   0.278    8.401  ...    96.0      89.0      80.0   1.01
E_begin   4.258  3.957   0.036   13.159  ...    84.0      74.0      60.0   1.01

[8 rows x 11 columns]