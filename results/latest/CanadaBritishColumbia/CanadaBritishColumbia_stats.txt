0 Divergences 
Failed validation 
Computed from 80 by 390 log-likelihood matrix

         Estimate       SE
elpd_loo -2423.01    45.43
p_loo       13.57        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      379   97.2%
 (0.5, 0.7]   (ok)          8    2.1%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    0    0.0%

             mean      sd   hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.287   0.013    0.274    0.301  ...     3.0       3.0      21.0   2.13
pu          0.858   0.004    0.853    0.862  ...     3.0       3.0      18.0   2.02
mu          0.231   0.002    0.229    0.233  ...     3.0       3.0      14.0   2.85
mus         0.124   0.002    0.121    0.127  ...     3.0       3.0      14.0   2.77
gamma       0.052   0.000    0.052    0.053  ...     3.0       3.0      17.0   2.06
Is_begin   63.123  53.265   10.025  122.173  ...     3.0       3.0      22.0   2.23
Ia_begin  152.290  13.666  135.342  167.532  ...     3.0       4.0      32.0   1.84
E_begin   112.450  52.228   59.079  168.351  ...     3.0       3.0      33.0   2.32

[8 rows x 11 columns]