0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1413.43    34.17
p_loo       28.65        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      333   86.7%
 (0.5, 0.7]   (ok)         42   10.9%
   (0.7, 1]   (bad)         9    2.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.249  0.055   0.152    0.330  ...   147.0     139.0      95.0   1.12
pu        0.748  0.032   0.702    0.797  ...    75.0      75.0      88.0   1.03
mu        0.127  0.026   0.079    0.172  ...    34.0      37.0      65.0   1.05
mus       0.169  0.038   0.118    0.238  ...   107.0     152.0      75.0   1.08
gamma     0.204  0.040   0.127    0.274  ...   119.0     131.0     100.0   1.00
Is_begin  0.913  0.974   0.004    2.709  ...   100.0      97.0      91.0   1.00
Ia_begin  2.282  3.101   0.005    8.139  ...   116.0     152.0      54.0   1.00
E_begin   0.878  1.005   0.012    3.246  ...    83.0      55.0     100.0   1.02

[8 rows x 11 columns]