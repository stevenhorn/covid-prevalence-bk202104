0 Divergences 
Passed validation 
Computed from 80 by 385 log-likelihood matrix

         Estimate       SE
elpd_loo -2868.42    33.55
p_loo       50.19        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      326   84.7%
 (0.5, 0.7]   (ok)         47   12.2%
   (0.7, 1]   (bad)         9    2.3%
   (1, Inf)   (very bad)    3    0.8%

              mean       sd   hdi_3%  ...  ess_bulk  ess_tail  r_hat
pa           0.247    0.053    0.161  ...     100.0      38.0   1.11
pu           0.755    0.026    0.709  ...      37.0      31.0   1.07
mu           0.243    0.049    0.167  ...       4.0      55.0   1.71
mus          0.266    0.035    0.207  ...      16.0      95.0   1.11
gamma        0.362    0.054    0.276  ...      11.0      69.0   1.15
Is_begin   193.860   90.497   59.384  ...     113.0      40.0   1.05
Ia_begin   479.289  179.809  208.454  ...      76.0      60.0   1.04
E_begin   1110.963  619.030  237.037  ...       6.0      33.0   1.33

[8 rows x 11 columns]