0 Divergences 
Passed validation 
Computed from 80 by 385 log-likelihood matrix

         Estimate       SE
elpd_loo -1310.06    38.70
p_loo       38.95        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      355   92.2%
 (0.5, 0.7]   (ok)         19    4.9%
   (0.7, 1]   (bad)         9    2.3%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.273  0.051   0.184    0.349  ...   152.0     152.0      48.0   0.99
pu        0.873  0.027   0.823    0.899  ...    81.0     132.0      59.0   1.03
mu        0.143  0.022   0.104    0.184  ...     6.0       6.0      20.0   1.34
mus       0.183  0.033   0.122    0.242  ...   150.0     152.0      38.0   1.00
gamma     0.233  0.039   0.166    0.312  ...    71.0      95.0      60.0   1.00
Is_begin  0.766  0.733   0.016    2.554  ...    90.0     149.0      86.0   1.00
Ia_begin  1.420  1.324   0.007    3.944  ...    98.0      68.0      60.0   1.04
E_begin   0.720  0.805   0.014    1.921  ...    96.0     129.0      75.0   0.99

[8 rows x 11 columns]