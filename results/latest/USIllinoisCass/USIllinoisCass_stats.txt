0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1022.79    31.10
p_loo       27.68        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      360   93.8%
 (0.5, 0.7]   (ok)         19    4.9%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.173  0.021   0.151    0.215  ...   152.0     152.0      62.0   1.05
pu        0.707  0.007   0.700    0.721  ...   121.0      83.0     100.0   1.00
mu        0.115  0.020   0.083    0.148  ...    38.0      37.0      88.0   1.05
mus       0.185  0.033   0.128    0.237  ...   120.0     119.0      60.0   0.99
gamma     0.222  0.049   0.130    0.306  ...   125.0     139.0      88.0   0.99
Is_begin  0.408  0.468   0.000    1.482  ...    82.0      73.0      59.0   0.99
Ia_begin  0.845  0.800   0.020    1.910  ...    77.0      84.0      86.0   0.98
E_begin   0.404  0.407   0.017    1.276  ...    53.0      47.0      40.0   1.01

[8 rows x 11 columns]