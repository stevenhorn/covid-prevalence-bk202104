0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -741.76    33.19
p_loo       31.81        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      360   93.8%
 (0.5, 0.7]   (ok)         17    4.4%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.268  0.051   0.184    0.347  ...    36.0      35.0      56.0   1.05
pu        0.874  0.026   0.827    0.900  ...     6.0       5.0      24.0   1.41
mu        0.138  0.025   0.109    0.205  ...    46.0      46.0      29.0   1.01
mus       0.185  0.039   0.119    0.266  ...    63.0      72.0      95.0   1.02
gamma     0.232  0.046   0.142    0.322  ...   115.0     116.0      60.0   0.99
Is_begin  0.763  0.708   0.006    2.052  ...    20.0      16.0      18.0   1.10
Ia_begin  1.298  1.403   0.026    4.145  ...    89.0      79.0      58.0   1.00
E_begin   0.616  0.784   0.021    1.706  ...    56.0      85.0      60.0   1.02

[8 rows x 11 columns]