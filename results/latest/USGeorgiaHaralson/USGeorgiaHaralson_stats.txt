0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1119.57    68.78
p_loo       35.72        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      348   90.6%
 (0.5, 0.7]   (ok)         32    8.3%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.262  0.051   0.176    0.350  ...    71.0      68.0      59.0   1.05
pu        0.812  0.028   0.755    0.851  ...     6.0       6.0      24.0   1.32
mu        0.140  0.021   0.103    0.174  ...    51.0      51.0      40.0   1.00
mus       0.240  0.043   0.165    0.307  ...   152.0     152.0      88.0   0.98
gamma     0.326  0.056   0.246    0.442  ...    71.0      69.0      69.0   1.03
Is_begin  1.023  0.913   0.066    2.349  ...   101.0      84.0      53.0   1.02
Ia_begin  1.981  2.064   0.009    6.685  ...    48.0      22.0      47.0   1.08
E_begin   0.810  0.728   0.015    2.289  ...    26.0      21.0      70.0   1.08

[8 rows x 11 columns]