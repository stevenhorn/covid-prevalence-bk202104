0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1060.60    27.09
p_loo       26.66        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      343   89.3%
 (0.5, 0.7]   (ok)         36    9.4%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.258  0.048   0.175    0.346  ...    60.0      61.0      38.0   1.06
pu        0.818  0.027   0.761    0.857  ...    58.0      62.0      72.0   1.02
mu        0.122  0.027   0.082    0.168  ...    68.0      52.0      73.0   1.03
mus       0.168  0.029   0.119    0.221  ...   100.0     112.0     100.0   0.99
gamma     0.200  0.033   0.155    0.272  ...   100.0     126.0      97.0   1.02
Is_begin  1.320  1.118   0.030    3.881  ...   131.0     109.0      58.0   1.01
Ia_begin  2.608  2.456   0.028    7.220  ...   107.0      70.0      43.0   1.01
E_begin   1.758  1.664   0.022    5.531  ...   112.0      68.0      42.0   0.99

[8 rows x 11 columns]