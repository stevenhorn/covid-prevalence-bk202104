0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1120.10    51.40
p_loo       33.82        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      353   91.9%
 (0.5, 0.7]   (ok)         26    6.8%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.252  0.055   0.157    0.337  ...    66.0      69.0      59.0   1.01
pu        0.781  0.021   0.739    0.814  ...    62.0      62.0      59.0   1.01
mu        0.121  0.017   0.090    0.147  ...    39.0      37.0      59.0   0.99
mus       0.191  0.030   0.139    0.246  ...   104.0      97.0      59.0   1.00
gamma     0.215  0.038   0.158    0.278  ...    78.0     108.0      60.0   1.01
Is_begin  0.748  0.660   0.012    2.037  ...    99.0      73.0      80.0   1.00
Ia_begin  1.240  1.258   0.013    3.123  ...    87.0      71.0      60.0   1.01
E_begin   0.645  0.780   0.009    1.875  ...    78.0      90.0      60.0   1.01

[8 rows x 11 columns]