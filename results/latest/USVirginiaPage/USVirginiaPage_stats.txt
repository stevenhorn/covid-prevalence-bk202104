0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1058.83    47.99
p_loo       38.25        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      361   94.0%
 (0.5, 0.7]   (ok)         18    4.7%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.264  0.055   0.179    0.344  ...    89.0      94.0      95.0   1.00
pu        0.851  0.025   0.796    0.884  ...    45.0      52.0      42.0   1.02
mu        0.128  0.023   0.083    0.168  ...    30.0      30.0      40.0   1.06
mus       0.196  0.034   0.138    0.254  ...    48.0      59.0      48.0   1.04
gamma     0.252  0.042   0.180    0.327  ...   152.0     152.0     100.0   1.02
Is_begin  0.620  0.647   0.019    1.423  ...   107.0     100.0      93.0   1.01
Ia_begin  1.187  1.331   0.015    4.411  ...   125.0      95.0      58.0   1.01
E_begin   0.803  0.975   0.001    3.336  ...    45.0      12.0      14.0   1.13

[8 rows x 11 columns]