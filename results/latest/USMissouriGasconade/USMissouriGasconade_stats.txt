0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1231.21    74.56
p_loo       44.53        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      368   95.8%
 (0.5, 0.7]   (ok)          9    2.3%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.188  0.032   0.150    0.243  ...   126.0      69.0      38.0   1.03
pu        0.711  0.008   0.700    0.726  ...    77.0      54.0      33.0   1.03
mu        0.085  0.014   0.058    0.108  ...    12.0      13.0      59.0   1.14
mus       0.218  0.033   0.174    0.279  ...   112.0     109.0      93.0   1.03
gamma     0.515  0.084   0.394    0.679  ...    91.0     150.0      38.0   1.02
Is_begin  0.589  0.532   0.003    1.560  ...    71.0      59.0      38.0   0.99
Ia_begin  0.897  0.972   0.005    2.907  ...   121.0      74.0      48.0   0.99
E_begin   0.426  0.454   0.002    1.348  ...    93.0      90.0      48.0   1.01

[8 rows x 11 columns]