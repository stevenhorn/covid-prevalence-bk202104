0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1351.54    35.53
p_loo       27.42        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      341   88.8%
 (0.5, 0.7]   (ok)         40   10.4%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.250  0.049   0.162    0.333  ...    53.0      54.0      40.0   1.00
pu        0.758  0.027   0.702    0.796  ...    64.0      63.0      40.0   1.07
mu        0.116  0.021   0.077    0.158  ...    68.0      68.0      48.0   0.98
mus       0.174  0.034   0.116    0.226  ...   152.0     152.0      39.0   1.00
gamma     0.180  0.037   0.106    0.239  ...   152.0     140.0      61.0   1.06
Is_begin  0.726  0.600   0.031    1.937  ...    99.0     106.0      83.0   1.02
Ia_begin  1.538  1.756   0.021    5.294  ...   138.0      40.0      88.0   1.07
E_begin   0.755  0.963   0.016    3.132  ...   121.0      72.0      81.0   1.02

[8 rows x 11 columns]