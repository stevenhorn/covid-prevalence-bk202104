0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -2377.13    28.28
p_loo       24.52        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      354   92.2%
 (0.5, 0.7]   (ok)         23    6.0%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    0    0.0%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.220   0.038   0.166    0.293  ...   152.0     152.0      93.0   0.99
pu         0.725   0.022   0.700    0.765  ...   147.0     121.0      22.0   1.03
mu         0.119   0.019   0.083    0.153  ...    47.0      49.0      51.0   1.01
mus        0.166   0.027   0.122    0.217  ...    96.0     100.0      60.0   1.03
gamma      0.312   0.036   0.253    0.381  ...   152.0     152.0      96.0   0.99
Is_begin   6.185   2.990   0.978   11.649  ...   105.0      88.0      68.0   1.02
Ia_begin  13.685   7.433   0.943   26.331  ...    53.0      44.0      15.0   1.04
E_begin   22.067  14.074   0.842   47.114  ...    86.0      89.0      37.0   0.99

[8 rows x 11 columns]