0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1355.01    39.44
p_loo       27.13        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      350   91.1%
 (0.5, 0.7]   (ok)         26    6.8%
   (0.7, 1]   (bad)         8    2.1%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.261  0.062   0.159    0.349  ...    43.0      40.0      40.0   1.03
pu        0.741  0.022   0.700    0.778  ...    74.0      67.0      38.0   1.02
mu        0.111  0.017   0.085    0.145  ...    15.0      12.0      57.0   1.15
mus       0.191  0.033   0.140    0.248  ...    13.0      23.0      26.0   1.09
gamma     0.231  0.049   0.146    0.318  ...   130.0      83.0      46.0   1.00
Is_begin  0.667  0.549   0.029    1.954  ...    88.0      71.0      34.0   1.00
Ia_begin  0.577  0.446   0.065    1.584  ...   141.0     152.0      33.0   0.98
E_begin   0.372  0.306   0.000    1.025  ...    62.0      63.0      21.0   1.03

[8 rows x 11 columns]