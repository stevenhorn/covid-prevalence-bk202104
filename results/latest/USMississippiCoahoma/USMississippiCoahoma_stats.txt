0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1205.04    55.87
p_loo       44.79        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      364   94.8%
 (0.5, 0.7]   (ok)         17    4.4%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.251  0.051   0.175    0.342  ...    78.0      66.0      32.0   1.04
pu        0.757  0.027   0.710    0.802  ...    11.0      10.0      35.0   1.17
mu        0.131  0.028   0.086    0.181  ...     6.0       5.0      37.0   1.42
mus       0.231  0.043   0.171    0.309  ...    53.0      48.0      40.0   1.00
gamma     0.299  0.063   0.219    0.414  ...    88.0      80.0      96.0   0.99
Is_begin  0.675  0.511   0.034    1.649  ...   115.0      79.0      60.0   1.02
Ia_begin  1.240  0.891   0.095    2.427  ...   115.0      84.0      40.0   1.04
E_begin   0.552  0.628   0.002    1.423  ...    67.0      32.0      60.0   1.04

[8 rows x 11 columns]