0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1509.12    28.88
p_loo       26.42        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      351   91.4%
 (0.5, 0.7]   (ok)         26    6.8%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.235  0.050   0.161    0.331  ...   105.0     101.0      40.0   1.01
pu        0.747  0.036   0.702    0.827  ...    85.0      88.0      54.0   1.01
mu        0.158  0.029   0.106    0.211  ...     5.0       5.0      46.0   1.36
mus       0.218  0.041   0.136    0.281  ...    51.0      51.0      61.0   1.05
gamma     0.304  0.044   0.224    0.382  ...   152.0     152.0      86.0   1.01
Is_begin  3.194  1.995   0.295    6.825  ...   109.0      90.0      40.0   1.04
Ia_begin  7.600  4.292   0.591   14.147  ...   148.0     152.0      88.0   1.03
E_begin   9.282  8.290   0.088   25.770  ...    86.0      54.0      60.0   1.01

[8 rows x 11 columns]