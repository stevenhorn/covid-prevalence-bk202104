0 Divergences 
Passed validation 
Computed from 80 by 385 log-likelihood matrix

         Estimate       SE
elpd_loo -1720.93    33.96
p_loo       91.12        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      363   94.3%
 (0.5, 0.7]   (ok)         16    4.2%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.307  0.014   0.280    0.332  ...    11.0      11.0      14.0   1.13
pu        0.861  0.010   0.845    0.874  ...     4.0       4.0      17.0   1.69
mu        0.181  0.002   0.177    0.185  ...     6.0       7.0      19.0   1.26
mus       0.151  0.013   0.127    0.169  ...     3.0       3.0      19.0   2.29
gamma     0.117  0.004   0.111    0.124  ...     3.0       3.0      23.0   2.08
Is_begin  1.973  0.935   0.700    3.525  ...     3.0       4.0      17.0   1.81
Ia_begin  2.597  1.290   0.784    4.687  ...     3.0       3.0      14.0   1.98
E_begin   2.069  0.653   1.180    3.367  ...    13.0      12.0      51.0   1.14

[8 rows x 11 columns]