0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1524.21    63.95
p_loo       36.86        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      363   94.5%
 (0.5, 0.7]   (ok)         18    4.7%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.163  0.014   0.151    0.203  ...     5.0       4.0      17.0   1.86
pu        0.704  0.002   0.702    0.707  ...    12.0      10.0      60.0   1.16
mu        0.168  0.010   0.153    0.185  ...     4.0       4.0      32.0   1.53
mus       0.235  0.024   0.198    0.281  ...     3.0       3.0      22.0   2.05
gamma     0.382  0.045   0.292    0.436  ...     3.0       5.0      14.0   1.56
Is_begin  0.518  0.451   0.070    1.469  ...     5.0       4.0      15.0   1.55
Ia_begin  1.062  0.586   0.111    2.156  ...     7.0       6.0      14.0   1.28
E_begin   0.515  0.301   0.102    1.051  ...     8.0       7.0      33.0   1.26

[8 rows x 11 columns]