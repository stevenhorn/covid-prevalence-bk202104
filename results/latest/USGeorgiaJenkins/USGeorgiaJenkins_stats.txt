0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -773.64    32.33
p_loo       27.49        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      353   91.9%
 (0.5, 0.7]   (ok)         22    5.7%
   (0.7, 1]   (bad)         9    2.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.244  0.049   0.168    0.332  ...    59.0      56.0      60.0   1.01
pu        0.793  0.029   0.731    0.833  ...    18.0      17.0      40.0   1.08
mu        0.125  0.027   0.070    0.171  ...    18.0      18.0      22.0   1.25
mus       0.185  0.027   0.143    0.237  ...    58.0      61.0      69.0   1.09
gamma     0.268  0.049   0.178    0.358  ...    77.0      77.0      29.0   1.05
Is_begin  0.680  0.703   0.011    1.989  ...    47.0      75.0      29.0   1.02
Ia_begin  1.230  1.154   0.044    3.444  ...    90.0      57.0      38.0   1.04
E_begin   0.496  0.502   0.015    1.493  ...    27.0      29.0      95.0   1.09

[8 rows x 11 columns]