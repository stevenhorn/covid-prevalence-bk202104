0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1580.55    37.68
p_loo       15.86        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      376   97.9%
 (0.5, 0.7]   (ok)          6    1.6%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.195  0.005   0.188    0.205  ...     6.0       6.0      24.0   1.31
pu        0.743  0.003   0.738    0.747  ...    14.0      14.0      16.0   1.13
mu        0.175  0.013   0.160    0.195  ...     3.0       3.0      16.0   2.13
mus       0.227  0.028   0.197    0.263  ...     3.0       3.0      17.0   2.60
gamma     0.133  0.007   0.124    0.143  ...     3.0       3.0      40.0   1.93
Is_begin  1.248  0.532   0.564    2.014  ...     3.0       3.0      14.0   2.48
Ia_begin  1.385  0.312   0.859    1.865  ...     3.0       4.0      29.0   1.59
E_begin   2.263  0.624   1.528    3.110  ...     3.0       3.0      36.0   2.15

[8 rows x 11 columns]