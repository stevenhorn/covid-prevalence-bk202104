0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1773.51    46.19
p_loo       33.38        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      363   94.5%
 (0.5, 0.7]   (ok)         19    4.9%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.248  0.053   0.173    0.337  ...    82.0      73.0      60.0   0.99
pu        0.773  0.031   0.709    0.815  ...     5.0       5.0      26.0   1.43
mu        0.128  0.019   0.098    0.158  ...    17.0      18.0      81.0   1.11
mus       0.197  0.035   0.122    0.255  ...   152.0     152.0      39.0   1.08
gamma     0.271  0.042   0.204    0.350  ...   152.0     152.0      93.0   0.98
Is_begin  1.476  1.115   0.014    3.171  ...    80.0      97.0      49.0   1.10
Ia_begin  3.532  2.894   0.183    8.034  ...    61.0      39.0      60.0   1.04
E_begin   2.666  3.143   0.018   10.409  ...    83.0      67.0      54.0   1.02

[8 rows x 11 columns]