0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -2094.73    49.34
p_loo       37.63        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      341   88.8%
 (0.5, 0.7]   (ok)         34    8.9%
   (0.7, 1]   (bad)         8    2.1%
   (1, Inf)   (very bad)    1    0.3%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.237   0.049   0.150    0.308  ...    36.0      33.0      22.0   1.07
pu         0.758   0.037   0.706    0.826  ...    35.0      37.0      38.0   1.01
mu         0.124   0.020   0.094    0.162  ...    21.0      31.0      80.0   1.10
mus        0.194   0.030   0.149    0.256  ...   128.0     141.0      81.0   0.99
gamma      0.244   0.033   0.187    0.298  ...   126.0     132.0      96.0   1.00
Is_begin  21.883  14.140   1.134   48.420  ...    95.0      87.0      84.0   1.07
Ia_begin  50.886  27.601   1.971   96.438  ...   152.0     152.0      72.0   0.98
E_begin   54.010  40.381   0.338  124.973  ...    54.0      54.0      69.0   1.03

[8 rows x 11 columns]