0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1197.98    58.35
p_loo       35.07        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      345   89.8%
 (0.5, 0.7]   (ok)         31    8.1%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.243  0.050   0.153    0.320  ...   150.0     152.0      60.0   1.00
pu        0.758  0.026   0.716    0.802  ...    67.0      71.0      86.0   1.03
mu        0.132  0.019   0.095    0.159  ...    39.0      38.0      69.0   1.03
mus       0.183  0.034   0.118    0.243  ...    65.0      64.0      49.0   1.05
gamma     0.300  0.049   0.222    0.402  ...    43.0      56.0     100.0   1.03
Is_begin  0.933  0.654   0.051    2.293  ...    84.0     102.0      69.0   1.02
Ia_begin  1.501  1.436   0.003    4.517  ...   106.0      85.0      42.0   1.02
E_begin   0.800  0.878   0.012    2.223  ...   100.0      82.0      60.0   1.02

[8 rows x 11 columns]