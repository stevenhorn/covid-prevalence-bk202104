1 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1462.49    25.23
p_loo       27.68        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      348   90.6%
 (0.5, 0.7]   (ok)         28    7.3%
   (0.7, 1]   (bad)         8    2.1%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.247  0.051   0.165    0.332  ...    84.0     104.0      39.0   1.09
pu        0.764  0.033   0.703    0.818  ...    24.0      25.0      40.0   1.06
mu        0.119  0.018   0.082    0.146  ...    40.0      37.0      24.0   1.01
mus       0.173  0.036   0.094    0.226  ...    68.0      74.0      80.0   1.00
gamma     0.229  0.042   0.160    0.322  ...    62.0      74.0      53.0   1.05
Is_begin  1.482  1.162   0.013    3.810  ...    67.0      47.0      49.0   1.00
Ia_begin  4.212  2.235   0.624    8.323  ...    40.0      30.0      56.0   1.09
E_begin   2.634  2.743   0.031    8.249  ...    34.0      22.0      74.0   1.07

[8 rows x 11 columns]