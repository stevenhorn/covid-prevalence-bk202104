0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -845.64    27.32
p_loo       26.79        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      348   90.6%
 (0.5, 0.7]   (ok)         29    7.6%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.241  0.042   0.173    0.326  ...    89.0      95.0      75.0   1.01
pu        0.762  0.020   0.722    0.796  ...    11.0      13.0      18.0   1.14
mu        0.126  0.025   0.082    0.171  ...    37.0      38.0      77.0   1.04
mus       0.166  0.032   0.113    0.228  ...   107.0     125.0      59.0   1.04
gamma     0.193  0.036   0.132    0.253  ...   152.0     152.0      93.0   1.00
Is_begin  0.195  0.210   0.000    0.632  ...    49.0      38.0      58.0   1.01
Ia_begin  0.416  0.666   0.003    1.051  ...    28.0      13.0      36.0   1.10
E_begin   0.203  0.247   0.000    0.752  ...    29.0      23.0      39.0   1.06

[8 rows x 11 columns]