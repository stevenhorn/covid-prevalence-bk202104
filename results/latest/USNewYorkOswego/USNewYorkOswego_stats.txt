0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1147.83    29.90
p_loo       32.84        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      361   94.0%
 (0.5, 0.7]   (ok)         20    5.2%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.253  0.057   0.168    0.345  ...    59.0      57.0      31.0   0.99
pu        0.845  0.036   0.782    0.900  ...    26.0      24.0      29.0   1.07
mu        0.175  0.033   0.126    0.241  ...     8.0       8.0      97.0   1.24
mus       0.219  0.034   0.165    0.285  ...    95.0     103.0     100.0   1.00
gamma     0.305  0.046   0.212    0.385  ...    69.0      71.0      60.0   1.01
Is_begin  1.412  1.082   0.053    3.662  ...    96.0      83.0      59.0   1.02
Ia_begin  3.498  2.090   0.155    6.924  ...    36.0      36.0      88.0   1.07
E_begin   2.145  2.295   0.090    6.253  ...    80.0      72.0      59.0   1.01

[8 rows x 11 columns]