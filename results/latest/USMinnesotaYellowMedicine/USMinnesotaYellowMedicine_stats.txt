0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -758.89    33.31
p_loo       33.08        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      346   90.1%
 (0.5, 0.7]   (ok)         34    8.9%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.259  0.050   0.157    0.335  ...    41.0      41.0      38.0   1.03
pu        0.735  0.023   0.701    0.779  ...    22.0      27.0      43.0   1.08
mu        0.137  0.031   0.085    0.193  ...    17.0      20.0      59.0   1.07
mus       0.188  0.037   0.134    0.269  ...    38.0      41.0      43.0   1.04
gamma     0.235  0.051   0.171    0.341  ...   110.0     114.0      97.0   1.00
Is_begin  0.756  0.756   0.022    2.139  ...    75.0      56.0      60.0   0.99
Ia_begin  1.317  1.296   0.031    4.067  ...    52.0      40.0      58.0   1.04
E_begin   0.410  0.403   0.006    0.936  ...    27.0      20.0      96.0   1.10

[8 rows x 11 columns]