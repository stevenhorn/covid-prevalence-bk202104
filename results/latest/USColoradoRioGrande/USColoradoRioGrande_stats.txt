0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -806.00    35.32
p_loo       32.60        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      363   94.5%
 (0.5, 0.7]   (ok)         16    4.2%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.250  0.048   0.161    0.323  ...    75.0      75.0      91.0   1.01
pu        0.822  0.024   0.769    0.853  ...    18.0      27.0      34.0   1.07
mu        0.131  0.026   0.085    0.175  ...    10.0      10.0      57.0   1.14
mus       0.166  0.030   0.125    0.237  ...   152.0     140.0      49.0   1.04
gamma     0.191  0.038   0.130    0.262  ...   106.0     114.0      56.0   1.04
Is_begin  0.699  0.646   0.004    1.908  ...    97.0      83.0      84.0   1.00
Ia_begin  1.910  1.926   0.027    6.076  ...    75.0     124.0      80.0   1.01
E_begin   0.912  1.141   0.001    2.884  ...    68.0      80.0      56.0   1.00

[8 rows x 11 columns]