0 Divergences 
Passed validation 
Computed from 80 by 385 log-likelihood matrix

         Estimate       SE
elpd_loo  -511.31    46.16
p_loo       47.34        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      368   95.6%
 (0.5, 0.7]   (ok)          8    2.1%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.177  0.026   0.150    0.214  ...   100.0      65.0      43.0   1.01
pu        0.711  0.008   0.700    0.726  ...   143.0     105.0      37.0   1.02
mu        0.208  0.015   0.185    0.236  ...    38.0      37.0      75.0   1.08
mus       0.353  0.039   0.283    0.413  ...    60.0      54.0      44.0   0.99
gamma     0.910  0.055   0.829    1.026  ...   101.0     102.0      93.0   0.98
Is_begin  0.387  0.442   0.003    1.036  ...    22.0      23.0      40.0   1.08
Ia_begin  0.501  0.803   0.000    2.181  ...    72.0      31.0      42.0   1.07
E_begin   0.187  0.236   0.001    0.632  ...    13.0      14.0      34.0   1.14

[8 rows x 11 columns]