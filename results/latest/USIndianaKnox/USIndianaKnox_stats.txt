0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1100.47    32.55
p_loo       29.85        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      359   93.5%
 (0.5, 0.7]   (ok)         19    4.9%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.239  0.053   0.153    0.332  ...    31.0      37.0      20.0   1.07
pu        0.813  0.017   0.782    0.842  ...    21.0      22.0      42.0   1.07
mu        0.126  0.020   0.097    0.173  ...    37.0      38.0      59.0   1.04
mus       0.172  0.031   0.118    0.224  ...    54.0      60.0      60.0   1.00
gamma     0.196  0.033   0.146    0.270  ...   100.0      99.0      88.0   1.00
Is_begin  0.726  0.629   0.034    1.881  ...    89.0      87.0      88.0   1.03
Ia_begin  1.443  1.537   0.024    4.202  ...   103.0      55.0      60.0   1.01
E_begin   0.669  0.996   0.026    2.677  ...    99.0      55.0      96.0   1.02

[8 rows x 11 columns]