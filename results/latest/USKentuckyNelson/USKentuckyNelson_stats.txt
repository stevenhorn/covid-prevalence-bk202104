0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1181.47    32.22
p_loo       26.32        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      361   94.0%
 (0.5, 0.7]   (ok)         18    4.7%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.259  0.048   0.180    0.350  ...    37.0      38.0      24.0   1.01
pu        0.777  0.021   0.735    0.811  ...    20.0      18.0      40.0   1.09
mu        0.141  0.028   0.096    0.194  ...     6.0       7.0      60.0   1.25
mus       0.172  0.037   0.105    0.239  ...   104.0      92.0     100.0   1.01
gamma     0.197  0.026   0.149    0.250  ...    88.0      92.0      97.0   1.01
Is_begin  0.695  0.759   0.004    2.227  ...    90.0      74.0      45.0   1.01
Ia_begin  1.601  1.431   0.083    4.218  ...    50.0      47.0      60.0   1.06
E_begin   0.795  0.802   0.038    2.180  ...    77.0      58.0      60.0   0.98

[8 rows x 11 columns]