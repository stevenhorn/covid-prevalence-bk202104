0 Divergences 
Passed validation 
Computed from 80 by 390 log-likelihood matrix

         Estimate       SE
elpd_loo  -643.97    39.71
p_loo       40.46        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      369   94.6%
 (0.5, 0.7]   (ok)         13    3.3%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    3    0.8%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.266   0.052   0.177    0.347  ...    74.0      72.0      68.0   1.03
pu         0.868   0.027   0.808    0.899  ...    48.0      46.0      33.0   1.02
mu         0.269   0.038   0.206    0.340  ...     7.0       7.0      53.0   1.29
mus        0.289   0.042   0.220    0.360  ...    82.0      81.0      23.0   1.03
gamma      0.441   0.070   0.304    0.575  ...    53.0      77.0      93.0   1.04
Is_begin   4.537   3.596   0.103   10.628  ...    93.0      74.0      84.0   1.01
Ia_begin  11.674  10.910   0.283   29.133  ...    64.0      50.0      77.0   1.03
E_begin    5.367   5.385   0.551   13.821  ...    41.0      33.0      84.0   1.05

[8 rows x 11 columns]