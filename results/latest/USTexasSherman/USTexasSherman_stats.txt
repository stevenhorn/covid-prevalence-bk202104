0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -664.68    51.87
p_loo       35.01        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      367   95.6%
 (0.5, 0.7]   (ok)         13    3.4%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.258  0.054   0.176    0.348  ...    44.0      43.0      33.0   1.05
pu        0.813  0.023   0.771    0.851  ...    41.0      42.0      40.0   1.05
mu        0.112  0.020   0.075    0.149  ...    15.0      14.0      60.0   1.12
mus       0.189  0.038   0.118    0.258  ...   152.0     152.0      57.0   0.98
gamma     0.277  0.047   0.207    0.361  ...    63.0      57.0      60.0   1.04
Is_begin  0.832  0.858   0.001    2.751  ...   143.0     152.0      60.0   0.99
Ia_begin  1.520  1.891   0.001    5.781  ...    96.0      79.0      60.0   1.02
E_begin   0.774  0.984   0.004    2.748  ...    95.0      33.0      85.0   1.07

[8 rows x 11 columns]