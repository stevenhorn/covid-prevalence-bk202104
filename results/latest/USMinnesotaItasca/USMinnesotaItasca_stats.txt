0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1023.45    28.61
p_loo       25.45        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      355   92.4%
 (0.5, 0.7]   (ok)         23    6.0%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.240  0.050   0.161    0.327  ...    57.0      56.0      60.0   1.01
pu        0.809  0.029   0.759    0.857  ...    43.0      43.0      40.0   1.04
mu        0.129  0.027   0.081    0.176  ...    23.0      21.0      49.0   1.07
mus       0.192  0.041   0.111    0.246  ...    74.0      81.0      53.0   1.02
gamma     0.230  0.040   0.155    0.293  ...    96.0      98.0      60.0   1.01
Is_begin  0.594  0.559   0.017    1.731  ...    68.0      58.0      43.0   1.01
Ia_begin  1.224  1.507   0.000    3.406  ...    85.0     113.0      24.0   1.02
E_begin   0.453  0.504   0.009    1.622  ...    87.0      52.0      81.0   1.04

[8 rows x 11 columns]