0 Divergences 
Passed validation 
Computed from 80 by 390 log-likelihood matrix

         Estimate       SE
elpd_loo  -880.88    33.67
p_loo       37.66        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      348   89.2%
 (0.5, 0.7]   (ok)         34    8.7%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    3    0.8%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.247   0.043   0.164    0.331  ...    63.0      78.0      39.0   1.02
pu         0.781   0.049   0.711    0.877  ...    26.0      32.0      43.0   1.07
mu         0.180   0.021   0.148    0.218  ...    44.0      45.0      69.0   1.04
mus        0.256   0.040   0.181    0.323  ...    63.0      60.0      86.0   0.99
gamma      0.318   0.057   0.234    0.435  ...    94.0      97.0      80.0   1.03
Is_begin   5.767   3.668   0.238   11.723  ...    66.0      64.0      60.0   1.01
Ia_begin  21.814  19.056   1.138   52.028  ...    33.0      25.0      40.0   1.05
E_begin    8.817   8.210   0.176   21.212  ...    37.0      44.0      42.0   1.05

[8 rows x 11 columns]