0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -2957.57    59.84
p_loo       43.76        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      340   88.5%
 (0.5, 0.7]   (ok)         35    9.1%
   (0.7, 1]   (bad)         8    2.1%
   (1, Inf)   (very bad)    1    0.3%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.249   0.058   0.157    0.346  ...   143.0     152.0      42.0   1.00
pu         0.802   0.045   0.718    0.863  ...    24.0      18.0      36.0   1.09
mu         0.128   0.022   0.089    0.171  ...     5.0       6.0      19.0   1.32
mus        0.249   0.042   0.181    0.339  ...   100.0      96.0      99.0   1.00
gamma      0.354   0.060   0.243    0.457  ...    78.0      81.0      80.0   1.00
Is_begin  13.751   7.888   0.693   29.122  ...    66.0      65.0      97.0   1.04
Ia_begin  26.117  18.844   1.246   61.156  ...    20.0      13.0      58.0   1.11
E_begin   23.934  21.556   0.099   67.753  ...    43.0      80.0      40.0   1.03

[8 rows x 11 columns]