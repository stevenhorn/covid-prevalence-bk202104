0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -970.86    26.89
p_loo       28.65        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      363   94.5%
 (0.5, 0.7]   (ok)         16    4.2%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.266  0.060   0.166    0.344  ...    14.0      23.0      53.0   1.08
pu        0.813  0.024   0.768    0.851  ...    18.0      19.0      40.0   1.09
mu        0.152  0.024   0.113    0.196  ...    38.0      41.0      60.0   1.03
mus       0.185  0.027   0.146    0.233  ...   132.0     152.0      99.0   1.02
gamma     0.265  0.037   0.189    0.324  ...    63.0      61.0      18.0   1.10
Is_begin  1.044  0.811   0.097    2.557  ...   115.0     110.0      60.0   1.01
Ia_begin  0.733  0.578   0.014    1.796  ...    92.0      85.0      32.0   1.01
E_begin   0.903  1.017   0.001    3.057  ...    81.0      37.0      59.0   1.06

[8 rows x 11 columns]