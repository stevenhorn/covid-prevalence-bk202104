0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1369.62    71.08
p_loo       37.46        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      365   95.1%
 (0.5, 0.7]   (ok)         15    3.9%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.214  0.042   0.154    0.295  ...    18.0      65.0      18.0   1.09
pu        0.725  0.017   0.700    0.752  ...    77.0      77.0     100.0   1.00
mu        0.101  0.017   0.070    0.126  ...    35.0      35.0      17.0   1.04
mus       0.244  0.029   0.201    0.309  ...    89.0      93.0      72.0   1.00
gamma     0.323  0.058   0.226    0.428  ...    83.0      85.0      91.0   0.99
Is_begin  0.783  0.721   0.002    1.856  ...    81.0      53.0      39.0   1.01
Ia_begin  1.331  1.319   0.016    3.731  ...   104.0      96.0      95.0   1.04
E_begin   0.476  0.410   0.014    1.265  ...    32.0      33.0      88.0   1.05

[8 rows x 11 columns]