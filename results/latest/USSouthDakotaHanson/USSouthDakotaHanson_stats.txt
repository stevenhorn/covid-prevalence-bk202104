0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -511.52    33.96
p_loo       27.07        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      359   93.5%
 (0.5, 0.7]   (ok)         20    5.2%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.255  0.047   0.176    0.338  ...    91.0      98.0      58.0   1.05
pu        0.814  0.019   0.775    0.842  ...    58.0      54.0      49.0   1.04
mu        0.128  0.021   0.093    0.164  ...    66.0      75.0      56.0   1.03
mus       0.168  0.030   0.117    0.220  ...   152.0     152.0      69.0   1.03
gamma     0.205  0.043   0.135    0.288  ...   146.0     152.0      91.0   0.99
Is_begin  0.257  0.305   0.001    0.769  ...    83.0      66.0      59.0   1.02
Ia_begin  0.626  0.845   0.002    2.205  ...    96.0      70.0      39.0   1.02
E_begin   0.336  0.426   0.000    0.996  ...   108.0      93.0      58.0   1.00

[8 rows x 11 columns]