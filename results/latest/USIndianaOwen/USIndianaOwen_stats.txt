0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -899.67    33.77
p_loo       33.27        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      358   93.2%
 (0.5, 0.7]   (ok)         20    5.2%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.253  0.056   0.164    0.344  ...   132.0     129.0      48.0   1.03
pu        0.834  0.018   0.811    0.868  ...    70.0      74.0      72.0   1.03
mu        0.147  0.026   0.100    0.195  ...    47.0      45.0      60.0   1.00
mus       0.188  0.042   0.131    0.284  ...    39.0      52.0      60.0   1.08
gamma     0.228  0.043   0.138    0.287  ...    96.0      93.0      83.0   1.02
Is_begin  0.924  0.741   0.032    2.304  ...    97.0      68.0      60.0   1.07
Ia_begin  2.211  1.906   0.062    5.331  ...    69.0      53.0      60.0   1.03
E_begin   1.236  1.267   0.005    3.568  ...    74.0      44.0      40.0   1.04

[8 rows x 11 columns]