0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1056.45    30.37
p_loo       29.07        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      364   94.8%
 (0.5, 0.7]   (ok)         17    4.4%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.261  0.048   0.171    0.342  ...    63.0      60.0      60.0   1.00
pu        0.745  0.022   0.709    0.783  ...    50.0      50.0      58.0   1.00
mu        0.124  0.017   0.095    0.155  ...     8.0       9.0      56.0   1.22
mus       0.183  0.023   0.140    0.222  ...    37.0      55.0      87.0   1.04
gamma     0.234  0.046   0.162    0.307  ...    81.0     131.0      40.0   0.98
Is_begin  0.721  0.766   0.023    2.428  ...   101.0      76.0      87.0   0.98
Ia_begin  1.408  1.467   0.007    3.709  ...    53.0      43.0      31.0   1.06
E_begin   0.675  0.840   0.004    2.172  ...    61.0      56.0      60.0   1.01

[8 rows x 11 columns]