1 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -900.15    47.99
p_loo       44.87        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      358   93.2%
 (0.5, 0.7]   (ok)         16    4.2%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.198  0.033   0.152    0.263  ...   119.0     108.0      86.0   0.98
pu        0.715  0.013   0.700    0.741  ...    93.0      31.0      30.0   1.04
mu        0.146  0.020   0.114    0.188  ...    44.0      42.0      56.0   1.12
mus       0.274  0.049   0.195    0.362  ...   152.0     152.0      76.0   0.99
gamma     0.425  0.078   0.284    0.542  ...    96.0     126.0      59.0   1.04
Is_begin  0.331  0.389   0.004    1.058  ...    78.0      65.0      97.0   1.01
Ia_begin  0.494  0.713   0.001    1.452  ...    95.0      65.0      88.0   1.04
E_begin   0.264  0.334   0.003    0.813  ...    81.0      52.0      30.0   1.00

[8 rows x 11 columns]