0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1223.83    33.72
p_loo       29.89        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      359   93.5%
 (0.5, 0.7]   (ok)         16    4.2%
   (0.7, 1]   (bad)         8    2.1%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.255  0.057   0.169    0.350  ...   149.0     145.0      60.0   0.99
pu        0.770  0.028   0.721    0.811  ...    75.0      75.0      60.0   1.01
mu        0.138  0.030   0.093    0.196  ...    26.0      23.0      18.0   1.11
mus       0.200  0.036   0.134    0.241  ...    91.0      79.0      40.0   1.02
gamma     0.230  0.041   0.156    0.320  ...    41.0      54.0      87.0   1.05
Is_begin  0.788  0.854   0.001    2.216  ...   104.0      97.0      60.0   1.04
Ia_begin  1.283  1.350   0.018    3.876  ...    76.0     137.0      76.0   1.02
E_begin   0.667  0.609   0.009    1.967  ...   115.0     137.0     100.0   0.98

[8 rows x 11 columns]