0 Divergences 
Passed validation 
Computed from 80 by 385 log-likelihood matrix

         Estimate       SE
elpd_loo  -979.65    46.25
p_loo       37.55        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      355   92.2%
 (0.5, 0.7]   (ok)         18    4.7%
   (0.7, 1]   (bad)        11    2.9%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.295  0.039   0.232    0.349  ...   152.0     152.0      61.0   1.02
pu        0.891  0.009   0.875    0.900  ...   152.0     152.0      93.0   0.98
mu        0.189  0.027   0.130    0.230  ...    28.0      30.0      60.0   1.05
mus       0.203  0.036   0.147    0.267  ...   152.0     152.0      81.0   1.03
gamma     0.283  0.055   0.202    0.378  ...   152.0     152.0      59.0   1.01
Is_begin  0.976  0.783   0.014    2.686  ...    85.0      74.0     100.0   1.02
Ia_begin  2.288  2.287   0.066    7.168  ...   114.0      67.0      60.0   1.01
E_begin   1.245  1.725   0.016    4.494  ...    96.0      81.0      56.0   0.99

[8 rows x 11 columns]