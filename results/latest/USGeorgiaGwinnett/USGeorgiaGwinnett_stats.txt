0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -2265.88    67.50
p_loo       41.43        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      340   88.5%
 (0.5, 0.7]   (ok)         34    8.9%
   (0.7, 1]   (bad)         8    2.1%
   (1, Inf)   (very bad)    2    0.5%

            mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.243  0.052   0.168    0.337  ...    65.0      67.0      59.0   1.03
pu         0.763  0.032   0.708    0.806  ...    18.0      22.0      54.0   1.13
mu         0.112  0.015   0.088    0.140  ...    24.0      25.0      54.0   1.09
mus        0.246  0.042   0.173    0.314  ...   152.0     152.0      95.0   1.00
gamma      0.324  0.054   0.212    0.405  ...   152.0     152.0      58.0   0.99
Is_begin   4.639  2.980   0.082   10.217  ...    73.0      53.0      38.0   1.00
Ia_begin  10.536  7.530   0.346   26.097  ...    82.0      73.0      43.0   1.00
E_begin    9.330  9.480   0.041   28.130  ...    53.0      27.0      16.0   1.02

[8 rows x 11 columns]