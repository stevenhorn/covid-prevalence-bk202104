13 Divergences 
Passed validation 
Computed from 80 by 385 log-likelihood matrix

         Estimate       SE
elpd_loo -2208.82    26.70
p_loo       31.99        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      339   88.1%
 (0.5, 0.7]   (ok)         39   10.1%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    0    0.0%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.256   0.054   0.151    0.332  ...    14.0      21.0      33.0   1.17
pu         0.775   0.054   0.701    0.864  ...    38.0      39.0      29.0   1.01
mu         0.172   0.020   0.138    0.201  ...     6.0       7.0      33.0   1.23
mus        0.258   0.027   0.210    0.310  ...    69.0      65.0      75.0   1.02
gamma      0.351   0.059   0.263    0.450  ...    22.0      17.0      30.0   1.08
Is_begin  10.068   6.574   0.849   20.999  ...     8.0       9.0      24.0   1.20
Ia_begin  20.483  11.958   3.026   41.844  ...    60.0      48.0      38.0   0.99
E_begin   29.577  19.157   2.247   66.977  ...    26.0      16.0      45.0   1.09

[8 rows x 11 columns]