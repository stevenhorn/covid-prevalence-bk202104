0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -833.12    26.95
p_loo       26.66        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      353   91.9%
 (0.5, 0.7]   (ok)         28    7.3%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.220  0.047   0.153    0.301  ...    78.0      70.0      59.0   1.04
pu        0.737  0.019   0.704    0.766  ...    65.0      72.0      60.0   1.03
mu        0.139  0.025   0.099    0.182  ...    14.0      11.0      35.0   1.12
mus       0.192  0.030   0.141    0.242  ...    39.0      46.0      54.0   1.00
gamma     0.251  0.042   0.171    0.319  ...    26.0      23.0      32.0   1.08
Is_begin  0.542  0.564   0.006    1.797  ...    60.0      53.0      43.0   1.01
Ia_begin  1.098  1.227   0.012    3.103  ...    64.0      57.0      58.0   1.01
E_begin   0.476  0.468   0.003    1.443  ...    81.0      21.0      59.0   1.08

[8 rows x 11 columns]