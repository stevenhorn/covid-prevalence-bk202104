0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -2155.04    33.20
p_loo       30.20        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      345   89.8%
 (0.5, 0.7]   (ok)         30    7.8%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.238  0.051   0.155    0.315  ...    31.0      29.0      42.0   1.06
pu        0.742  0.025   0.700    0.784  ...    42.0      43.0      22.0   1.05
mu        0.121  0.021   0.090    0.162  ...     7.0       7.0      36.0   1.28
mus       0.172  0.028   0.125    0.220  ...   152.0     152.0      88.0   1.01
gamma     0.219  0.038   0.151    0.285  ...   152.0     152.0      60.0   1.04
Is_begin  1.767  1.205   0.021    3.781  ...    51.0      20.0      29.0   1.06
Ia_begin  0.910  0.513   0.054    1.694  ...    91.0      81.0      60.0   1.02
E_begin   1.658  1.436   0.122    4.505  ...    75.0      53.0      59.0   1.01

[8 rows x 11 columns]