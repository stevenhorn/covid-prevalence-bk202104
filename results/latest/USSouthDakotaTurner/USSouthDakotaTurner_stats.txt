0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -804.14    42.52
p_loo       29.96        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      367   95.6%
 (0.5, 0.7]   (ok)         14    3.6%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.213  0.044   0.150    0.301  ...   106.0      85.0      59.0   1.01
pu        0.730  0.017   0.702    0.760  ...    28.0      31.0      40.0   1.07
mu        0.119  0.021   0.080    0.155  ...    33.0      35.0      42.0   1.06
mus       0.181  0.035   0.102    0.222  ...    72.0      72.0      60.0   1.02
gamma     0.195  0.039   0.139    0.276  ...   152.0     152.0      96.0   0.99
Is_begin  0.822  0.892   0.016    2.653  ...   147.0     152.0      88.0   0.99
Ia_begin  1.217  1.093   0.001    3.356  ...    65.0      46.0      54.0   1.08
E_begin   0.623  0.694   0.004    2.001  ...   108.0      92.0      39.0   1.00

[8 rows x 11 columns]