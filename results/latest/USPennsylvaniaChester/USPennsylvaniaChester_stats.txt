0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -2164.19    29.97
p_loo       95.90        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      331   86.2%
 (0.5, 0.7]   (ok)         33    8.6%
   (0.7, 1]   (bad)        14    3.6%
   (1, Inf)   (very bad)    6    1.6%

            mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.291  0.060   0.183    0.340  ...     3.0       4.0      29.0   1.84
pu         0.806  0.048   0.725    0.851  ...     3.0       3.0      26.0   2.00
mu         0.150  0.043   0.087    0.195  ...     3.0       3.0      15.0   2.08
mus        0.225  0.071   0.114    0.308  ...     3.0       4.0      20.0   1.89
gamma      0.141  0.079   0.065    0.255  ...     3.0       4.0      15.0   1.82
Is_begin   3.156  1.805   0.548    6.430  ...    17.0      44.0      48.0   1.37
Ia_begin  11.852  5.342   0.118   19.282  ...    23.0      23.0      30.0   1.64
E_begin    4.856  7.022   0.165   19.483  ...     7.0       4.0      80.0   1.51

[8 rows x 11 columns]