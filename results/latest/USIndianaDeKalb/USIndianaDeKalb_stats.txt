0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1174.46    50.35
p_loo       42.00        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      346   90.1%
 (0.5, 0.7]   (ok)         34    8.9%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.226  0.052   0.150    0.322  ...   106.0      89.0      31.0   1.01
pu        0.749  0.037   0.701    0.817  ...    38.0      37.0      15.0   1.06
mu        0.142  0.025   0.094    0.186  ...    28.0      36.0      69.0   1.05
mus       0.243  0.040   0.169    0.313  ...    41.0      50.0      48.0   1.02
gamma     0.325  0.056   0.241    0.439  ...   124.0     130.0      88.0   1.00
Is_begin  1.019  0.981   0.027    2.672  ...   135.0     122.0      48.0   1.00
Ia_begin  1.891  1.778   0.042    5.037  ...    75.0      60.0      75.0   1.01
E_begin   1.137  1.250   0.004    3.466  ...    78.0      45.0      40.0   1.04

[8 rows x 11 columns]