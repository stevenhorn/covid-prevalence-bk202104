0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -984.17    29.53
p_loo       24.63        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      356   92.7%
 (0.5, 0.7]   (ok)         20    5.2%
   (0.7, 1]   (bad)         8    2.1%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.252  0.053   0.167    0.345  ...   116.0     104.0      54.0   1.00
pu        0.761  0.022   0.726    0.802  ...    65.0      63.0      57.0   1.13
mu        0.112  0.020   0.074    0.144  ...    43.0      40.0      88.0   1.04
mus       0.155  0.033   0.095    0.212  ...   152.0     152.0      97.0   1.00
gamma     0.200  0.039   0.131    0.272  ...    44.0      57.0      34.0   1.01
Is_begin  0.596  0.608   0.001    1.567  ...   124.0     137.0      53.0   1.01
Ia_begin  0.946  0.900   0.012    2.756  ...    69.0      62.0      56.0   1.03
E_begin   0.495  0.592   0.001    1.443  ...   105.0      86.0      97.0   1.00

[8 rows x 11 columns]