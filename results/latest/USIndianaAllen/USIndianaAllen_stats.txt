15 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1716.28    34.64
p_loo       32.94        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      348   90.6%
 (0.5, 0.7]   (ok)         28    7.3%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.259  0.053   0.161    0.328  ...    12.0      11.0      56.0   1.14
pu        0.754  0.028   0.707    0.792  ...    24.0      27.0      59.0   1.27
mu        0.102  0.019   0.073    0.134  ...     6.0       6.0      43.0   1.31
mus       0.201  0.033   0.138    0.256  ...    40.0      40.0      93.0   1.05
gamma     0.260  0.048   0.170    0.325  ...    28.0      50.0      56.0   1.05
Is_begin  0.766  0.597   0.041    1.912  ...    44.0      43.0      53.0   1.05
Ia_begin  1.702  1.113   0.211    3.740  ...    61.0      44.0      40.0   1.09
E_begin   1.388  1.192   0.015    3.732  ...    70.0      42.0      60.0   1.01

[8 rows x 11 columns]