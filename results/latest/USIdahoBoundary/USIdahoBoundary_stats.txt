0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -748.26    28.42
p_loo       29.02        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      343   89.3%
 (0.5, 0.7]   (ok)         31    8.1%
   (0.7, 1]   (bad)        10    2.6%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.257  0.052   0.168    0.337  ...    70.0      69.0      44.0   1.01
pu        0.812  0.026   0.753    0.849  ...     4.0       5.0      24.0   1.49
mu        0.135  0.022   0.097    0.176  ...    28.0      40.0      20.0   1.06
mus       0.182  0.032   0.115    0.232  ...   100.0      94.0      58.0   1.01
gamma     0.222  0.043   0.141    0.287  ...    81.0      80.0      59.0   1.02
Is_begin  0.423  0.560   0.001    1.392  ...    85.0      36.0      53.0   1.04
Ia_begin  0.841  1.039   0.026    2.207  ...    78.0      64.0      96.0   1.03
E_begin   0.414  0.509   0.003    1.656  ...    88.0      61.0      93.0   1.00

[8 rows x 11 columns]