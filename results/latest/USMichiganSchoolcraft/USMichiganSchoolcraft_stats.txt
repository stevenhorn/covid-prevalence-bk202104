0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -523.08    29.32
p_loo       24.50        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      364   94.8%
 (0.5, 0.7]   (ok)         15    3.9%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.259  0.044   0.187    0.344  ...   123.0     146.0      53.0   1.00
pu        0.844  0.018   0.808    0.870  ...    83.0      82.0      81.0   1.00
mu        0.137  0.026   0.092    0.177  ...    36.0      36.0      48.0   1.04
mus       0.184  0.039   0.118    0.258  ...   130.0     146.0      54.0   0.99
gamma     0.240  0.035   0.165    0.298  ...   152.0     152.0      80.0   1.05
Is_begin  0.821  0.673   0.001    2.295  ...    67.0      27.0      24.0   1.08
Ia_begin  1.491  1.411   0.036    4.348  ...    75.0      49.0      60.0   1.02
E_begin   0.664  0.705   0.002    1.826  ...    91.0      66.0      59.0   1.01

[8 rows x 11 columns]