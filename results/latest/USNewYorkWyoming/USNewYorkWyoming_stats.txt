0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -938.99    38.75
p_loo       33.88        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      359   93.5%
 (0.5, 0.7]   (ok)         16    4.2%
   (0.7, 1]   (bad)         8    2.1%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.260  0.056   0.171    0.348  ...    77.0      80.0      59.0   1.12
pu        0.821  0.023   0.775    0.852  ...    22.0      29.0      84.0   1.08
mu        0.127  0.024   0.091    0.175  ...    63.0      55.0      80.0   1.06
mus       0.181  0.041   0.111    0.242  ...   152.0     152.0      72.0   1.04
gamma     0.237  0.035   0.170    0.306  ...   152.0     152.0      96.0   1.01
Is_begin  1.188  0.944   0.021    2.959  ...    91.0      71.0      60.0   0.99
Ia_begin  0.701  0.523   0.042    1.870  ...    88.0      56.0      22.0   1.03
E_begin   0.943  1.148   0.002    2.919  ...    71.0      48.0      59.0   0.99

[8 rows x 11 columns]