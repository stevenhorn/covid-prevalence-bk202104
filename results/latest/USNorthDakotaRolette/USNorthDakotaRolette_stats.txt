0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -971.09    42.87
p_loo       32.07        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      338   88.0%
 (0.5, 0.7]   (ok)         33    8.6%
   (0.7, 1]   (bad)        12    3.1%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.199  0.033   0.155    0.261  ...    25.0      23.0      95.0   1.08
pu        0.716  0.012   0.700    0.736  ...    59.0      51.0      43.0   1.03
mu        0.117  0.023   0.083    0.155  ...    48.0      54.0      97.0   1.05
mus       0.197  0.040   0.125    0.267  ...   129.0     114.0      88.0   1.11
gamma     0.230  0.041   0.175    0.321  ...    81.0      89.0      60.0   1.07
Is_begin  0.667  0.703   0.004    2.215  ...    74.0      72.0      60.0   1.01
Ia_begin  0.880  0.975   0.005    2.429  ...    68.0      42.0      40.0   1.05
E_begin   0.435  0.648   0.002    2.023  ...    44.0      35.0      58.0   1.02

[8 rows x 11 columns]