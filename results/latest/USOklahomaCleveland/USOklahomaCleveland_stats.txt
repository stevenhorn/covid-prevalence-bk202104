0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1888.07    34.59
p_loo       32.80        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      355   92.4%
 (0.5, 0.7]   (ok)         22    5.7%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.247  0.053   0.166    0.345  ...    45.0      65.0      34.0   1.05
pu        0.726  0.020   0.700    0.763  ...    89.0      25.0      95.0   1.08
mu        0.133  0.021   0.092    0.171  ...    20.0      22.0      34.0   1.07
mus       0.186  0.031   0.135    0.242  ...    45.0      64.0      93.0   1.04
gamma     0.258  0.042   0.192    0.335  ...    25.0      26.0      69.0   1.06
Is_begin  2.445  2.209   0.149    7.038  ...    96.0     139.0      65.0   1.00
Ia_begin  4.713  3.264   0.444   10.046  ...    97.0     112.0      60.0   1.01
E_begin   3.547  3.435   0.198    8.243  ...    60.0      60.0      91.0   1.01

[8 rows x 11 columns]