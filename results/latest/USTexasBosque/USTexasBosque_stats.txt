1 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1249.96    60.28
p_loo       47.26        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      371   96.6%
 (0.5, 0.7]   (ok)          8    2.1%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.241  0.056   0.158    0.333  ...    28.0      32.0      40.0   1.05
pu        0.743  0.026   0.703    0.787  ...    33.0      37.0      45.0   1.01
mu        0.104  0.022   0.062    0.140  ...    13.0      12.0      47.0   1.13
mus       0.225  0.031   0.170    0.281  ...   105.0     103.0      88.0   0.99
gamma     0.342  0.048   0.244    0.430  ...    75.0      83.0      84.0   1.01
Is_begin  0.598  0.562   0.015    1.756  ...   112.0      91.0     100.0   1.01
Ia_begin  1.025  1.193   0.001    2.937  ...   118.0      85.0      60.0   1.04
E_begin   0.344  0.366   0.002    0.931  ...    41.0      18.0      84.0   1.09

[8 rows x 11 columns]