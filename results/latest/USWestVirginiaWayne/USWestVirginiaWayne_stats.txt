0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1057.81    40.07
p_loo       37.70        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      346   90.1%
 (0.5, 0.7]   (ok)         32    8.3%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.247  0.048   0.176    0.324  ...    62.0      52.0      72.0   1.05
pu        0.775  0.044   0.701    0.834  ...    12.0      14.0      39.0   1.12
mu        0.151  0.022   0.112    0.186  ...    34.0      35.0      57.0   1.05
mus       0.239  0.039   0.158    0.298  ...    84.0      96.0      59.0   1.04
gamma     0.329  0.055   0.250    0.458  ...   119.0     104.0      59.0   1.03
Is_begin  1.097  1.082   0.010    3.174  ...    72.0      57.0      27.0   1.07
Ia_begin  2.212  1.474   0.081    5.397  ...    62.0      29.0      40.0   1.06
E_begin   1.459  1.398   0.018    3.999  ...    17.0      10.0      66.0   1.17

[8 rows x 11 columns]