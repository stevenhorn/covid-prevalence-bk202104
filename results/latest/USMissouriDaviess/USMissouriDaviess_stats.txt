0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -964.26    72.05
p_loo       46.79        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      359   93.5%
 (0.5, 0.7]   (ok)         17    4.4%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.202  0.041   0.152    0.279  ...    59.0      39.0      59.0   1.06
pu        0.717  0.012   0.701    0.738  ...   152.0     152.0      91.0   1.14
mu        0.109  0.018   0.073    0.138  ...    28.0      29.0      60.0   1.06
mus       0.233  0.046   0.163    0.310  ...    72.0      87.0      60.0   1.07
gamma     0.410  0.066   0.310    0.518  ...    95.0      92.0      54.0   1.00
Is_begin  0.639  0.666   0.004    1.890  ...    75.0      33.0      58.0   1.05
Ia_begin  0.765  0.840   0.018    2.870  ...    58.0      56.0      43.0   1.00
E_begin   0.346  0.433   0.004    1.034  ...    46.0      50.0      64.0   1.01

[8 rows x 11 columns]