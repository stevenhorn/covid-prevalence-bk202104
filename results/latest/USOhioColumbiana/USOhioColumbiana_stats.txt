0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1503.74    36.10
p_loo       33.99        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      367   95.6%
 (0.5, 0.7]   (ok)         13    3.4%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.241  0.052   0.175    0.350  ...    77.0      75.0      60.0   0.99
pu        0.835  0.034   0.755    0.878  ...    16.0      18.0      41.0   1.06
mu        0.125  0.024   0.091    0.177  ...    27.0      27.0      33.0   1.02
mus       0.177  0.036   0.121    0.244  ...   134.0     152.0      38.0   1.04
gamma     0.222  0.041   0.143    0.298  ...   152.0     152.0      96.0   1.05
Is_begin  1.275  1.001   0.069    3.262  ...   151.0     152.0      69.0   1.01
Ia_begin  3.292  2.637   0.038    8.605  ...   143.0     139.0      91.0   1.02
E_begin   2.219  2.189   0.037    5.016  ...   103.0      37.0      59.0   1.06

[8 rows x 11 columns]