0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -844.48    28.34
p_loo       25.56        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      355   92.4%
 (0.5, 0.7]   (ok)         24    6.2%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.267  0.056   0.173    0.348  ...    40.0      42.0      60.0   1.00
pu        0.780  0.024   0.740    0.823  ...    17.0      20.0      39.0   1.09
mu        0.118  0.022   0.075    0.155  ...    11.0      11.0      25.0   1.16
mus       0.185  0.033   0.118    0.247  ...    95.0     116.0      60.0   1.01
gamma     0.208  0.038   0.154    0.278  ...   118.0     152.0      60.0   1.01
Is_begin  0.590  0.529   0.003    1.718  ...   103.0     152.0      57.0   1.01
Ia_begin  0.877  0.791   0.060    2.305  ...    40.0      71.0      43.0   1.07
E_begin   0.418  0.473   0.004    1.480  ...    30.0      16.0      45.0   1.12

[8 rows x 11 columns]