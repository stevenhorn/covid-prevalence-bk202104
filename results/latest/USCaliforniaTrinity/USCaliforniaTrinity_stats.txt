0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -709.12    42.95
p_loo       44.01        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      367   95.6%
 (0.5, 0.7]   (ok)         10    2.6%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    4    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.264  0.049   0.175    0.342  ...   119.0     114.0      43.0   1.06
pu        0.888  0.012   0.864    0.900  ...    43.0      24.0      43.0   1.09
mu        0.144  0.025   0.101    0.193  ...    25.0      23.0      59.0   1.07
mus       0.175  0.032   0.122    0.241  ...   148.0     152.0      80.0   1.00
gamma     0.239  0.046   0.158    0.330  ...    32.0     100.0      21.0   1.07
Is_begin  0.623  0.647   0.003    1.943  ...    38.0      30.0      30.0   1.05
Ia_begin  1.547  1.950   0.012    4.967  ...    78.0      49.0      39.0   1.02
E_begin   0.756  1.162   0.001    2.369  ...    93.0      30.0      58.0   1.06

[8 rows x 11 columns]