0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1119.53    28.97
p_loo       26.61        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      363   94.5%
 (0.5, 0.7]   (ok)         14    3.6%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.233  0.046   0.157    0.321  ...    63.0      61.0      60.0   1.14
pu        0.745  0.025   0.702    0.778  ...    43.0      46.0      20.0   1.03
mu        0.139  0.026   0.098    0.188  ...    16.0      15.0      60.0   1.10
mus       0.189  0.030   0.134    0.245  ...    30.0      20.0      38.0   1.08
gamma     0.238  0.047   0.166    0.324  ...   131.0     121.0      95.0   1.02
Is_begin  0.525  0.493   0.004    1.596  ...   107.0     152.0      59.0   0.99
Ia_begin  1.220  1.471   0.010    4.597  ...    64.0     134.0      59.0   1.00
E_begin   0.537  0.566   0.011    1.414  ...    73.0      68.0      56.0   1.01

[8 rows x 11 columns]