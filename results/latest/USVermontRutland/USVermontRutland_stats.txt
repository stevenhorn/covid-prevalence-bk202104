0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -906.37    47.33
p_loo       38.74        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      347   90.4%
 (0.5, 0.7]   (ok)         30    7.8%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.255  0.050   0.166    0.329  ...    75.0      83.0      60.0   1.03
pu        0.867  0.025   0.816    0.900  ...    32.0      43.0      95.0   1.07
mu        0.176  0.024   0.142    0.225  ...    13.0      14.0      19.0   1.14
mus       0.246  0.038   0.179    0.307  ...    92.0     112.0      88.0   1.01
gamma     0.343  0.050   0.243    0.436  ...   111.0     105.0      74.0   1.00
Is_begin  1.322  0.935   0.038    2.979  ...    56.0      37.0      34.0   1.06
Ia_begin  0.726  0.543   0.024    1.687  ...    78.0      66.0      60.0   1.01
E_begin   0.956  0.907   0.018    2.564  ...   102.0      77.0      88.0   1.00

[8 rows x 11 columns]