0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1942.54    40.12
p_loo       36.71        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      340   88.5%
 (0.5, 0.7]   (ok)         36    9.4%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    1    0.3%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.248   0.060   0.166    0.343  ...    10.0      14.0      53.0   1.17
pu         0.741   0.029   0.704    0.794  ...    88.0      59.0      54.0   1.01
mu         0.088   0.020   0.056    0.121  ...     6.0       7.0      33.0   1.29
mus        0.183   0.040   0.114    0.253  ...    36.0      25.0      77.0   1.07
gamma      0.287   0.046   0.192    0.357  ...   111.0     117.0      58.0   1.01
Is_begin  18.992  10.349   2.873   39.466  ...    79.0      71.0      40.0   1.01
Ia_begin  57.333  22.427  16.488   98.123  ...   116.0     106.0      38.0   1.03
E_begin   80.840  53.986   3.455  168.687  ...   118.0     131.0      87.0   1.00

[8 rows x 11 columns]