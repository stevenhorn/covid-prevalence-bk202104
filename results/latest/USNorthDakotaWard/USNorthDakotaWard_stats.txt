0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1298.15    38.05
p_loo       30.56        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      356   92.7%
 (0.5, 0.7]   (ok)         20    5.2%
   (0.7, 1]   (bad)         8    2.1%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.209  0.046   0.150    0.288  ...    88.0      85.0      31.0   1.07
pu        0.723  0.016   0.700    0.751  ...    73.0      78.0      58.0   1.05
mu        0.122  0.020   0.093    0.167  ...    26.0      22.0      72.0   1.08
mus       0.188  0.032   0.137    0.254  ...   152.0     152.0      76.0   0.98
gamma     0.215  0.038   0.151    0.280  ...    87.0      83.0      56.0   1.01
Is_begin  0.719  0.697   0.013    2.308  ...   141.0     103.0      20.0   1.01
Ia_begin  0.508  0.433   0.003    1.348  ...    37.0      15.0      22.0   1.11
E_begin   0.438  0.477   0.006    1.327  ...   112.0      85.0      59.0   0.99

[8 rows x 11 columns]