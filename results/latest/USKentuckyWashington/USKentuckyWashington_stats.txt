0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -827.31    25.98
p_loo       24.59        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      360   93.8%
 (0.5, 0.7]   (ok)         19    4.9%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.241  0.056   0.156    0.339  ...   136.0     152.0      59.0   1.10
pu        0.766  0.021   0.718    0.798  ...    70.0      73.0      84.0   1.18
mu        0.142  0.022   0.096    0.178  ...    67.0      66.0      80.0   1.02
mus       0.189  0.029   0.140    0.243  ...    89.0     106.0      55.0   1.02
gamma     0.205  0.034   0.150    0.261  ...   101.0      93.0      80.0   0.99
Is_begin  0.722  0.733   0.009    1.961  ...   109.0     152.0      81.0   0.99
Ia_begin  1.379  1.393   0.009    3.775  ...    50.0      37.0      43.0   1.01
E_begin   0.638  0.786   0.001    1.949  ...    49.0      36.0      49.0   1.05

[8 rows x 11 columns]