0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -861.97    27.49
p_loo       25.96        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      355   92.4%
 (0.5, 0.7]   (ok)         18    4.7%
   (0.7, 1]   (bad)         9    2.3%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.256  0.052   0.177    0.348  ...    71.0      72.0      59.0   1.00
pu        0.816  0.035   0.744    0.876  ...     7.0       7.0      22.0   1.30
mu        0.122  0.018   0.095    0.161  ...    29.0      29.0      56.0   1.05
mus       0.155  0.028   0.106    0.207  ...    93.0      93.0      73.0   0.99
gamma     0.189  0.044   0.125    0.279  ...   152.0     152.0      45.0   0.99
Is_begin  0.466  0.491   0.014    1.115  ...    37.0      26.0      60.0   1.08
Ia_begin  0.936  0.966   0.006    3.003  ...    36.0      60.0      17.0   1.03
E_begin   0.524  0.604   0.004    2.115  ...    26.0      48.0      95.0   1.06

[8 rows x 11 columns]