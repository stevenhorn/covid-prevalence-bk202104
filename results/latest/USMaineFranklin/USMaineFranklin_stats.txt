0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -812.76    45.87
p_loo       41.33        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      359   93.5%
 (0.5, 0.7]   (ok)         19    4.9%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.291  0.044   0.207    0.347  ...   152.0     152.0     100.0   0.99
pu        0.889  0.012   0.869    0.900  ...    69.0      99.0      60.0   1.07
mu        0.152  0.032   0.103    0.204  ...     6.0       7.0      40.0   1.27
mus       0.201  0.033   0.147    0.260  ...   152.0     152.0      88.0   0.99
gamma     0.271  0.043   0.208    0.355  ...   104.0     133.0      93.0   1.00
Is_begin  1.119  1.020   0.055    2.690  ...   132.0      67.0      40.0   1.01
Ia_begin  2.109  1.690   0.006    5.304  ...    51.0      46.0      60.0   1.00
E_begin   1.188  1.015   0.035    2.791  ...    59.0      65.0      59.0   0.99

[8 rows x 11 columns]