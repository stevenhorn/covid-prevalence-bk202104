0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1028.71    31.97
p_loo       28.92        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      361   94.0%
 (0.5, 0.7]   (ok)         22    5.7%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.243  0.056   0.154    0.338  ...    20.0      26.0      39.0   1.07
pu        0.806  0.030   0.742    0.845  ...    16.0      14.0      60.0   1.11
mu        0.133  0.024   0.090    0.179  ...    45.0      44.0      46.0   1.02
mus       0.171  0.031   0.110    0.226  ...    86.0      93.0      58.0   1.00
gamma     0.202  0.039   0.124    0.256  ...    61.0      56.0      59.0   1.07
Is_begin  0.879  0.709   0.021    1.972  ...    74.0      90.0      59.0   1.03
Ia_begin  1.631  1.437   0.007    4.431  ...    45.0      41.0      58.0   1.07
E_begin   0.780  0.681   0.004    2.232  ...    64.0      63.0      58.0   1.00

[8 rows x 11 columns]