0 Divergences 
Passed validation 
Computed from 80 by 385 log-likelihood matrix

         Estimate       SE
elpd_loo -1830.12    31.87
p_loo       35.68        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      359   93.2%
 (0.5, 0.7]   (ok)         22    5.7%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.268  0.048   0.185    0.347  ...   152.0     152.0      44.0   1.02
pu        0.862  0.040   0.774    0.900  ...    69.0      88.0      46.0   1.01
mu        0.135  0.021   0.099    0.183  ...    24.0      29.0      59.0   1.05
mus       0.157  0.026   0.112    0.209  ...   152.0     152.0      96.0   1.00
gamma     0.204  0.044   0.140    0.293  ...   152.0     133.0     100.0   1.00
Is_begin  0.868  0.737   0.029    2.167  ...   152.0     123.0      60.0   0.99
Ia_begin  0.650  0.468   0.005    1.396  ...   104.0     101.0      92.0   1.03
E_begin   0.628  0.595   0.003    1.613  ...   105.0      76.0      57.0   0.99

[8 rows x 11 columns]