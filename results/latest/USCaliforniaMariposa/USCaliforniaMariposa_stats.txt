0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -645.92    39.61
p_loo       34.71        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      357   93.0%
 (0.5, 0.7]   (ok)         21    5.5%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.251  0.052   0.151    0.324  ...    65.0      57.0      40.0   1.01
pu        0.853  0.035   0.795    0.899  ...    35.0      24.0      60.0   1.08
mu        0.158  0.033   0.106    0.225  ...    79.0      70.0      31.0   1.02
mus       0.213  0.035   0.144    0.271  ...    75.0      86.0      56.0   1.01
gamma     0.265  0.045   0.179    0.342  ...    81.0     124.0      60.0   1.01
Is_begin  0.742  0.680   0.029    1.968  ...    99.0     121.0      62.0   1.00
Ia_begin  1.521  1.363   0.004    3.927  ...    83.0      59.0      38.0   1.00
E_begin   0.614  0.675   0.002    1.821  ...    95.0      55.0      40.0   1.00

[8 rows x 11 columns]