0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1092.43    54.37
p_loo       32.67        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      346   90.1%
 (0.5, 0.7]   (ok)         30    7.8%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.265  0.058   0.172    0.350  ...    29.0      24.0      20.0   1.07
pu        0.841  0.021   0.808    0.882  ...    29.0      36.0      39.0   1.05
mu        0.139  0.021   0.106    0.175  ...    48.0      49.0      57.0   1.00
mus       0.175  0.037   0.122    0.248  ...    60.0      77.0      60.0   0.99
gamma     0.188  0.035   0.131    0.258  ...    47.0      66.0      35.0   1.02
Is_begin  0.765  0.655   0.010    1.939  ...    62.0      50.0      99.0   0.99
Ia_begin  1.670  1.639   0.090    4.908  ...    99.0      91.0      97.0   1.00
E_begin   0.784  0.787   0.002    2.671  ...    52.0      49.0      59.0   1.04

[8 rows x 11 columns]