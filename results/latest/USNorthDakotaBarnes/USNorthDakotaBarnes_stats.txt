0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -868.52    32.54
p_loo       29.18        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      364   94.8%
 (0.5, 0.7]   (ok)         13    3.4%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.228  0.052   0.154    0.326  ...    57.0      50.0      54.0   1.04
pu        0.750  0.022   0.707    0.784  ...    39.0      36.0      22.0   1.04
mu        0.147  0.022   0.106    0.189  ...    54.0      57.0      69.0   1.03
mus       0.192  0.035   0.129    0.256  ...    36.0      38.0      17.0   1.04
gamma     0.255  0.049   0.174    0.343  ...   141.0     152.0      47.0   1.06
Is_begin  0.857  0.657   0.000    2.216  ...    61.0      60.0      56.0   1.03
Ia_begin  1.507  1.900   0.020    5.602  ...    96.0      76.0      60.0   1.00
E_begin   0.613  0.645   0.011    1.804  ...    80.0      90.0      60.0   1.00

[8 rows x 11 columns]