0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -916.19    22.44
p_loo       24.10        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      357   93.0%
 (0.5, 0.7]   (ok)         20    5.2%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.266  0.056   0.168    0.349  ...   131.0     152.0      28.0   1.05
pu        0.826  0.056   0.723    0.897  ...     5.0       6.0      59.0   1.31
mu        0.122  0.025   0.081    0.169  ...     9.0       8.0      48.0   1.22
mus       0.173  0.027   0.120    0.212  ...   106.0     118.0      59.0   1.00
gamma     0.203  0.037   0.148    0.276  ...   152.0     152.0      37.0   1.04
Is_begin  0.958  0.952   0.001    2.819  ...    85.0      37.0      22.0   1.05
Ia_begin  2.192  1.868   0.015    6.034  ...    52.0      46.0      57.0   1.04
E_begin   1.072  1.174   0.004    3.534  ...    52.0      37.0      60.0   1.00

[8 rows x 11 columns]