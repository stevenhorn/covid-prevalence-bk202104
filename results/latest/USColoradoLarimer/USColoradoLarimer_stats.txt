0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1529.13    24.60
p_loo       26.58        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      352   91.7%
 (0.5, 0.7]   (ok)         25    6.5%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    0    0.0%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.241   0.054   0.157    0.332  ...   114.0     117.0      86.0   0.99
pu         0.768   0.042   0.700    0.833  ...    35.0      36.0      40.0   1.04
mu         0.130   0.024   0.092    0.174  ...     5.0       4.0      27.0   1.53
mus        0.199   0.034   0.139    0.260  ...    83.0      81.0      58.0   1.01
gamma      0.243   0.046   0.164    0.310  ...   119.0     100.0      60.0   1.01
Is_begin   5.984   3.870   0.160   13.037  ...    94.0     117.0      56.0   1.03
Ia_begin  14.053   7.482   2.244   28.315  ...   152.0     152.0      77.0   0.99
E_begin   14.444  12.187   0.368   32.873  ...   109.0      83.0     100.0   1.03

[8 rows x 11 columns]