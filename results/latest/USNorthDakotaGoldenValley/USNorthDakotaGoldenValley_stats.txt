0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -542.77    43.34
p_loo       42.08        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      364   94.8%
 (0.5, 0.7]   (ok)         16    4.2%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.209  0.041   0.151    0.282  ...    35.0      36.0      60.0   1.05
pu        0.723  0.020   0.700    0.764  ...    47.0      53.0      79.0   1.04
mu        0.162  0.033   0.104    0.217  ...    75.0      74.0      57.0   1.02
mus       0.253  0.033   0.207    0.312  ...    87.0      98.0      38.0   1.01
gamma     0.315  0.053   0.222    0.408  ...   122.0     146.0      38.0   1.08
Is_begin  0.411  0.505   0.005    1.385  ...   100.0      77.0      60.0   1.01
Ia_begin  0.649  0.743   0.008    1.923  ...    73.0      68.0      96.0   1.04
E_begin   0.251  0.267   0.002    0.734  ...    86.0      69.0     100.0   1.00

[8 rows x 11 columns]