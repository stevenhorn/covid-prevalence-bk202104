0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1535.17    30.09
p_loo       26.92        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      355   92.4%
 (0.5, 0.7]   (ok)         24    6.2%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.246  0.054   0.161    0.339  ...    79.0      85.0      60.0   1.00
pu        0.748  0.025   0.703    0.786  ...    55.0      53.0      44.0   1.05
mu        0.108  0.023   0.070    0.143  ...    18.0      18.0      53.0   1.09
mus       0.170  0.032   0.122    0.228  ...   105.0     123.0      59.0   1.01
gamma     0.187  0.032   0.120    0.239  ...   140.0     132.0      59.0   1.00
Is_begin  1.060  1.029   0.020    2.627  ...   114.0      83.0     100.0   0.99
Ia_begin  0.730  0.558   0.024    1.780  ...   150.0      63.0      61.0   1.03
E_begin   0.826  0.799   0.013    2.541  ...   102.0      72.0     100.0   1.04

[8 rows x 11 columns]