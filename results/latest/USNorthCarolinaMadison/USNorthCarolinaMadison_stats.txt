0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -925.63    31.84
p_loo       26.87        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      360   93.8%
 (0.5, 0.7]   (ok)         18    4.7%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.252  0.051   0.168    0.338  ...    61.0      76.0      81.0   1.02
pu        0.854  0.020   0.817    0.889  ...    17.0      13.0      56.0   1.14
mu        0.129  0.030   0.088    0.185  ...     8.0       8.0      17.0   1.20
mus       0.164  0.035   0.102    0.223  ...   152.0     152.0      87.0   1.03
gamma     0.192  0.046   0.128    0.297  ...   152.0     152.0      88.0   0.98
Is_begin  0.515  0.602   0.000    1.410  ...   108.0      97.0      65.0   1.00
Ia_begin  1.077  1.628   0.018    3.466  ...    90.0      92.0      74.0   1.00
E_begin   0.511  0.737   0.002    2.239  ...   107.0      82.0      60.0   0.98

[8 rows x 11 columns]