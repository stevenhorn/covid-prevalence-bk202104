0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -874.66    28.44
p_loo       19.71        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      346   90.1%
 (0.5, 0.7]   (ok)         29    7.6%
   (0.7, 1]   (bad)         9    2.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.249  0.044   0.176    0.328  ...    41.0      40.0      56.0   1.08
pu        0.794  0.017   0.756    0.820  ...    26.0      27.0      76.0   1.09
mu        0.123  0.020   0.089    0.168  ...    57.0      53.0      79.0   1.01
mus       0.160  0.030   0.112    0.210  ...    70.0      68.0      56.0   1.04
gamma     0.172  0.036   0.118    0.242  ...    86.0      94.0      60.0   1.00
Is_begin  1.057  1.004   0.045    3.013  ...    96.0      75.0      40.0   1.03
Ia_begin  1.906  1.962   0.009    5.728  ...    12.0      13.0      16.0   1.12
E_begin   0.936  0.889   0.040    2.584  ...    15.0      19.0      59.0   1.10

[8 rows x 11 columns]