0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -990.77    25.49
p_loo       26.07        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      355   92.4%
 (0.5, 0.7]   (ok)         22    5.7%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.250  0.053   0.164    0.336  ...    62.0      67.0      35.0   1.03
pu        0.751  0.025   0.705    0.787  ...    10.0      10.0      57.0   1.16
mu        0.129  0.027   0.089    0.183  ...    23.0      20.0      40.0   1.06
mus       0.193  0.037   0.133    0.264  ...    47.0      52.0      60.0   1.01
gamma     0.233  0.043   0.165    0.317  ...    74.0      75.0      59.0   1.02
Is_begin  0.550  0.549   0.009    1.701  ...    98.0      82.0      22.0   1.01
Ia_begin  0.967  0.961   0.001    2.778  ...    94.0      69.0      31.0   1.04
E_begin   0.472  0.399   0.002    1.215  ...    66.0      54.0      58.0   1.01

[8 rows x 11 columns]