0 Divergences 
Passed validation 
Computed from 80 by 375 log-likelihood matrix

         Estimate       SE
elpd_loo  -735.06    49.61
p_loo       40.81        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      357   95.2%
 (0.5, 0.7]   (ok)         10    2.7%
   (0.7, 1]   (bad)         7    1.9%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.257  0.063   0.154    0.350  ...    81.0      79.0      43.0   1.00
pu        0.867  0.021   0.831    0.897  ...     9.0       9.0      78.0   1.21
mu        0.146  0.023   0.106    0.186  ...    42.0      42.0      83.0   1.01
mus       0.182  0.030   0.135    0.238  ...    37.0      40.0      86.0   1.04
gamma     0.244  0.039   0.169    0.307  ...    97.0      95.0      81.0   0.98
Is_begin  0.451  0.417   0.009    1.283  ...     9.0       8.0      61.0   1.21
Ia_begin  0.916  1.228   0.000    2.578  ...    48.0       8.0      17.0   1.21
E_begin   0.442  0.530   0.012    1.460  ...    28.0      14.0      91.0   1.12

[8 rows x 11 columns]