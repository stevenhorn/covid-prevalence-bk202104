0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1685.06    76.24
p_loo       43.83        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      365   95.1%
 (0.5, 0.7]   (ok)         13    3.4%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.174  0.021   0.150    0.215  ...    58.0      59.0      40.0   1.05
pu        0.708  0.007   0.700    0.720  ...   152.0     152.0      46.0   1.01
mu        0.094  0.015   0.067    0.119  ...    25.0      27.0      59.0   1.06
mus       0.219  0.036   0.162    0.306  ...    72.0      73.0      56.0   1.03
gamma     0.455  0.071   0.307    0.580  ...    28.0      36.0      95.0   1.06
Is_begin  0.670  0.645   0.000    1.596  ...   123.0      98.0      39.0   1.00
Ia_begin  0.966  1.056   0.008    3.449  ...   101.0      69.0      91.0   1.00
E_begin   0.441  0.402   0.013    1.170  ...    73.0      54.0      59.0   1.02

[8 rows x 11 columns]