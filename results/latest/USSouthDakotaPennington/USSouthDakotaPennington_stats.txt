0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1491.28    38.55
p_loo       32.88        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      362   94.3%
 (0.5, 0.7]   (ok)         16    4.2%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.244  0.056   0.170    0.335  ...    39.0      36.0      15.0   1.08
pu        0.753  0.022   0.706    0.790  ...    23.0      25.0      22.0   1.07
mu        0.106  0.018   0.069    0.137  ...    32.0      32.0      40.0   1.04
mus       0.173  0.031   0.135    0.249  ...    71.0      63.0      86.0   1.00
gamma     0.188  0.033   0.132    0.243  ...    75.0      81.0      59.0   1.03
Is_begin  0.509  0.413   0.011    1.330  ...   129.0     110.0      64.0   1.04
Ia_begin  0.790  0.829   0.006    2.765  ...    69.0      55.0      57.0   1.00
E_begin   0.392  0.359   0.005    1.054  ...    75.0      72.0      74.0   1.01

[8 rows x 11 columns]