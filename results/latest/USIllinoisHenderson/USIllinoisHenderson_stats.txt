0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -636.80    28.16
p_loo       26.03        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      368   95.8%
 (0.5, 0.7]   (ok)         11    2.9%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.257  0.052   0.161    0.334  ...    93.0      99.0      88.0   1.00
pu        0.844  0.028   0.791    0.885  ...     5.0       4.0      38.0   1.58
mu        0.137  0.025   0.094    0.175  ...    30.0      29.0      38.0   1.09
mus       0.172  0.033   0.116    0.240  ...    75.0      80.0      74.0   1.01
gamma     0.203  0.029   0.149    0.259  ...   140.0     128.0      88.0   1.01
Is_begin  0.637  0.608   0.014    1.879  ...    50.0      33.0      38.0   1.05
Ia_begin  0.885  1.126   0.003    3.174  ...    65.0      41.0      40.0   1.05
E_begin   0.470  0.491   0.009    1.603  ...    48.0      24.0      40.0   1.07

[8 rows x 11 columns]