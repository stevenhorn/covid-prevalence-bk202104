0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -343.82    32.78
p_loo       27.94        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      358   93.2%
 (0.5, 0.7]   (ok)         22    5.7%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.288  0.039   0.221    0.349  ...    85.0      83.0      60.0   1.01
pu        0.889  0.008   0.874    0.900  ...   142.0     128.0      60.0   1.00
mu        0.133  0.024   0.090    0.175  ...    86.0      87.0      60.0   1.02
mus       0.182  0.038   0.125    0.253  ...   152.0     152.0      60.0   1.00
gamma     0.233  0.039   0.171    0.315  ...    91.0      85.0      93.0   1.03
Is_begin  0.419  0.411   0.000    1.246  ...    38.0      36.0      31.0   1.06
Ia_begin  0.835  1.026   0.007    3.011  ...    99.0      81.0     100.0   1.01
E_begin   0.379  0.410   0.003    1.160  ...    80.0      51.0      40.0   1.02

[8 rows x 11 columns]