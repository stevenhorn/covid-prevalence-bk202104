0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1134.13    27.77
p_loo       23.16        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      360   93.8%
 (0.5, 0.7]   (ok)         18    4.7%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.251  0.049   0.175    0.335  ...   150.0     152.0      72.0   1.01
pu        0.806  0.022   0.771    0.845  ...    35.0      28.0      92.0   1.05
mu        0.130  0.023   0.094    0.171  ...    41.0      36.0      38.0   1.00
mus       0.179  0.030   0.127    0.233  ...   152.0     152.0      48.0   1.07
gamma     0.226  0.045   0.140    0.297  ...   132.0     130.0      65.0   0.99
Is_begin  0.791  0.696   0.017    2.249  ...   108.0     149.0      60.0   0.99
Ia_begin  1.637  1.551   0.004    4.728  ...   122.0     100.0      99.0   0.99
E_begin   0.645  0.636   0.004    1.727  ...   111.0      99.0      59.0   1.09

[8 rows x 11 columns]