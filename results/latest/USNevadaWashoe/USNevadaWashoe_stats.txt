0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1971.62    27.23
p_loo       22.10        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      353   91.9%
 (0.5, 0.7]   (ok)         21    5.5%
   (0.7, 1]   (bad)        10    2.6%
   (1, Inf)   (very bad)    0    0.0%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.238   0.055   0.151    0.328  ...   152.0     152.0      39.0   0.99
pu         0.749   0.031   0.707    0.810  ...    63.0      61.0      38.0   1.03
mu         0.102   0.020   0.059    0.135  ...    38.0      43.0      40.0   1.00
mus        0.159   0.027   0.120    0.219  ...   152.0     152.0      61.0   1.03
gamma      0.217   0.040   0.149    0.297  ...    72.0      71.0      76.0   1.02
Is_begin   4.669   3.151   0.030    9.330  ...    77.0      50.0      37.0   1.05
Ia_begin  11.798   6.012   1.733   22.438  ...   120.0     149.0      93.0   1.01
E_begin   11.637  11.000   0.450   36.958  ...    86.0     152.0      55.0   1.01

[8 rows x 11 columns]