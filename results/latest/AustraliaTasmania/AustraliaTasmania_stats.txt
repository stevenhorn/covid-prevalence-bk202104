0 Divergences 
Passed validation 
Computed from 80 by 385 log-likelihood matrix

         Estimate       SE
elpd_loo  -407.40    47.97
p_loo       40.65        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      358   93.0%
 (0.5, 0.7]   (ok)         15    3.9%
   (0.7, 1]   (bad)         9    2.3%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.260  0.052   0.174    0.340  ...    47.0      38.0      40.0   1.09
pu        0.810  0.047   0.728    0.888  ...    35.0      39.0      58.0   0.99
mu        0.142  0.022   0.098    0.176  ...     9.0       9.0      23.0   1.22
mus       0.268  0.039   0.193    0.326  ...    59.0      55.0      40.0   1.02
gamma     0.391  0.050   0.313    0.489  ...    76.0      82.0      57.0   1.00
Is_begin  1.096  0.653   0.198    2.283  ...    99.0      86.0      59.0   1.02
Ia_begin  2.566  1.250   1.013    5.606  ...    89.0      94.0      59.0   1.03
E_begin   3.518  2.767   0.015    8.301  ...    32.0      32.0      17.0   1.07

[8 rows x 11 columns]