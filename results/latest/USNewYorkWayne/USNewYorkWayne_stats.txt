0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1035.35    27.47
p_loo       26.14        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      354   92.2%
 (0.5, 0.7]   (ok)         25    6.5%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.247  0.048   0.165    0.329  ...    78.0      83.0      79.0   1.02
pu        0.827  0.040   0.750    0.884  ...    16.0      19.0      36.0   1.08
mu        0.141  0.024   0.107    0.182  ...    14.0      15.0     100.0   1.13
mus       0.183  0.028   0.146    0.238  ...   132.0     148.0      48.0   0.98
gamma     0.235  0.042   0.162    0.294  ...   152.0     152.0      17.0   0.99
Is_begin  1.650  1.273   0.083    4.221  ...    92.0     104.0      88.0   1.00
Ia_begin  0.675  0.626   0.008    1.754  ...    89.0      55.0      36.0   1.00
E_begin   1.028  1.259   0.005    3.548  ...    54.0      33.0      57.0   1.02

[8 rows x 11 columns]