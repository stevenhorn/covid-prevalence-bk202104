0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -948.85    47.80
p_loo       33.49        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      346   90.1%
 (0.5, 0.7]   (ok)         32    8.3%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.237  0.059   0.157    0.343  ...    99.0     104.0      60.0   1.12
pu        0.781  0.024   0.740    0.822  ...    58.0      57.0      44.0   1.03
mu        0.122  0.019   0.088    0.153  ...    18.0      20.0      54.0   1.10
mus       0.184  0.038   0.116    0.256  ...   152.0     152.0      64.0   1.11
gamma     0.211  0.046   0.144    0.289  ...   118.0     121.0     100.0   0.99
Is_begin  0.709  0.687   0.020    2.098  ...    55.0      42.0      60.0   1.05
Ia_begin  1.375  1.210   0.115    3.852  ...    62.0      60.0      53.0   1.01
E_begin   0.630  0.618   0.019    1.950  ...    68.0      78.0      40.0   1.02

[8 rows x 11 columns]