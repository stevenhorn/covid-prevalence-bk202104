0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1117.15    68.38
p_loo       46.50        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      359   93.5%
 (0.5, 0.7]   (ok)         18    4.7%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.224  0.049   0.151    0.298  ...    42.0      49.0      57.0   1.15
pu        0.730  0.027   0.700    0.774  ...     4.0       4.0      17.0   1.60
mu        0.099  0.021   0.067    0.134  ...     4.0       4.0      49.0   1.63
mus       0.239  0.037   0.183    0.316  ...     6.0       6.0      60.0   1.39
gamma     0.487  0.072   0.367    0.602  ...    59.0      61.0      55.0   1.08
Is_begin  0.618  0.671   0.007    2.062  ...    59.0      48.0      59.0   1.04
Ia_begin  1.096  1.192   0.009    2.818  ...   107.0      52.0      40.0   1.00
E_begin   0.508  0.727   0.004    1.465  ...    87.0      34.0      22.0   1.07

[8 rows x 11 columns]