0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1551.39    64.24
p_loo       48.85        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      354   92.2%
 (0.5, 0.7]   (ok)         24    6.2%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.203  0.038   0.150    0.276  ...    85.0      62.0      40.0   1.06
pu        0.717  0.014   0.700    0.742  ...    76.0      57.0      42.0   1.02
mu        0.113  0.019   0.088    0.151  ...    47.0      45.0      59.0   1.01
mus       0.197  0.032   0.148    0.267  ...    51.0      50.0      93.0   1.03
gamma     0.256  0.060   0.166    0.377  ...    59.0      61.0      97.0   1.01
Is_begin  0.487  0.500   0.005    1.715  ...    86.0      59.0      72.0   0.99
Ia_begin  0.873  0.896   0.008    2.851  ...    57.0      44.0      34.0   1.04
E_begin   0.416  0.479   0.000    1.117  ...    93.0      43.0      22.0   1.04

[8 rows x 11 columns]