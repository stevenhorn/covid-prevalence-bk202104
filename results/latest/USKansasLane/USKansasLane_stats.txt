0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -518.08    52.68
p_loo       39.67        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      360   93.8%
 (0.5, 0.7]   (ok)         17    4.4%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.253  0.045   0.161    0.320  ...   100.0      95.0      64.0   1.05
pu        0.785  0.023   0.743    0.822  ...    60.0      62.0      60.0   0.99
mu        0.129  0.029   0.085    0.186  ...    47.0      52.0      23.0   1.02
mus       0.159  0.028   0.114    0.217  ...    93.0      87.0      99.0   1.00
gamma     0.172  0.036   0.112    0.228  ...   105.0     152.0      55.0   1.01
Is_begin  0.359  0.360   0.004    1.002  ...    51.0      47.0      58.0   1.00
Ia_begin  0.685  0.840   0.002    2.596  ...    58.0      22.0      15.0   1.05
E_begin   0.309  0.387   0.000    1.017  ...    48.0      25.0      29.0   1.08

[8 rows x 11 columns]