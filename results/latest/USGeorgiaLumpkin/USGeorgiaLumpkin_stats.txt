0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1050.19    28.85
p_loo       27.30        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      350   91.1%
 (0.5, 0.7]   (ok)         29    7.6%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.246  0.058   0.151    0.335  ...    82.0      77.0      81.0   1.00
pu        0.774  0.029   0.717    0.815  ...    25.0      29.0      65.0   1.06
mu        0.125  0.023   0.089    0.169  ...    12.0      15.0      19.0   1.10
mus       0.174  0.032   0.108    0.222  ...    76.0      70.0      59.0   1.03
gamma     0.211  0.040   0.158    0.313  ...   148.0     152.0      69.0   1.02
Is_begin  0.996  1.045   0.029    2.982  ...   121.0     117.0      56.0   1.00
Ia_begin  2.353  2.281   0.002    6.403  ...   121.0     152.0      60.0   0.98
E_begin   1.248  1.421   0.035    3.753  ...   123.0     126.0     100.0   0.99

[8 rows x 11 columns]