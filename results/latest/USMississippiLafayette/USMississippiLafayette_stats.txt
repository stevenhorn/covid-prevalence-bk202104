0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1315.69    36.29
p_loo       34.27        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      356   92.7%
 (0.5, 0.7]   (ok)         22    5.7%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.236  0.052   0.152    0.329  ...    85.0      82.0      59.0   1.05
pu        0.748  0.028   0.701    0.788  ...    21.0      23.0      39.0   1.08
mu        0.147  0.019   0.107    0.181  ...    30.0      30.0      33.0   1.05
mus       0.213  0.035   0.159    0.266  ...    54.0      88.0      40.0   1.02
gamma     0.256  0.051   0.185    0.343  ...    74.0      62.0     100.0   1.02
Is_begin  1.075  0.945   0.033    2.688  ...   134.0     100.0      53.0   1.00
Ia_begin  2.546  2.162   0.030    6.448  ...    33.0      15.0      34.0   1.10
E_begin   1.276  1.222   0.092    2.640  ...    87.0      71.0      60.0   1.04

[8 rows x 11 columns]