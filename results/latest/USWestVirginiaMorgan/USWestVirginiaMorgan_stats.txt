0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -749.93    27.07
p_loo       25.40        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      360   93.8%
 (0.5, 0.7]   (ok)         16    4.2%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.243  0.052   0.167    0.342  ...    77.0      79.0      88.0   1.07
pu        0.837  0.020   0.800    0.873  ...    72.0      82.0      60.0   1.02
mu        0.126  0.019   0.093    0.160  ...    69.0      74.0      65.0   1.00
mus       0.169  0.026   0.123    0.214  ...   152.0     152.0      81.0   1.00
gamma     0.226  0.040   0.151    0.289  ...   137.0     152.0      76.0   0.98
Is_begin  0.824  0.855   0.021    2.447  ...   108.0     138.0      86.0   1.03
Ia_begin  1.438  1.308   0.009    4.218  ...   123.0     152.0      88.0   0.98
E_begin   0.808  0.993   0.001    2.901  ...    87.0      36.0      40.0   1.06

[8 rows x 11 columns]