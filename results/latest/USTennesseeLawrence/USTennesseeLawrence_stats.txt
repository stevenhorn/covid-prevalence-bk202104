0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1289.26    28.81
p_loo       26.54        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      357   93.0%
 (0.5, 0.7]   (ok)         21    5.5%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.252  0.056   0.157    0.339  ...    30.0      29.0      15.0   1.07
pu        0.773  0.025   0.738    0.817  ...    19.0      21.0      38.0   1.07
mu        0.137  0.033   0.085    0.194  ...     5.0       5.0      22.0   1.38
mus       0.164  0.031   0.111    0.220  ...    53.0      56.0      59.0   1.03
gamma     0.173  0.041   0.106    0.258  ...   108.0     105.0      57.0   1.09
Is_begin  0.716  0.689   0.001    2.031  ...   124.0     123.0      60.0   0.99
Ia_begin  1.363  1.263   0.046    3.503  ...    80.0      52.0      37.0   1.05
E_begin   0.706  0.769   0.007    2.253  ...    85.0      68.0      39.0   1.02

[8 rows x 11 columns]