0 Divergences 
Passed validation 
Computed from 80 by 385 log-likelihood matrix

         Estimate       SE
elpd_loo -1404.19    35.03
p_loo       26.17        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      354   91.9%
 (0.5, 0.7]   (ok)         25    6.5%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.289  0.052   0.193    0.350  ...   130.0     152.0      35.0   1.02
pu        0.888  0.014   0.858    0.900  ...   150.0     141.0      60.0   1.02
mu        0.175  0.024   0.133    0.216  ...     6.0       6.0      33.0   1.31
mus       0.184  0.029   0.143    0.249  ...    79.0     106.0      74.0   1.01
gamma     0.318  0.050   0.225    0.406  ...    97.0     102.0      60.0   0.99
Is_begin  0.957  0.875   0.014    2.218  ...   110.0      85.0      18.0   1.00
Ia_begin  2.718  2.097   0.041    7.306  ...    89.0      71.0      88.0   1.01
E_begin   1.090  0.986   0.041    2.541  ...    89.0      62.0      87.0   1.01

[8 rows x 11 columns]