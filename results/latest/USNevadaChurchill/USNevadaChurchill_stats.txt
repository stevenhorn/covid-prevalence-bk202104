0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1184.96    49.05
p_loo       38.17        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      365   95.1%
 (0.5, 0.7]   (ok)         12    3.1%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.250  0.057   0.170    0.348  ...    99.0     105.0      96.0   0.98
pu        0.834  0.019   0.805    0.872  ...    82.0      80.0     100.0   1.00
mu        0.134  0.026   0.086    0.174  ...    58.0      58.0      43.0   0.99
mus       0.199  0.046   0.130    0.283  ...    64.0      67.0      59.0   1.03
gamma     0.252  0.043   0.160    0.332  ...    65.0      65.0      57.0   1.05
Is_begin  0.630  0.671   0.017    1.997  ...    53.0      76.0      59.0   1.01
Ia_begin  1.178  1.316   0.003    3.762  ...    49.0      59.0      38.0   0.99
E_begin   0.585  0.746   0.009    1.958  ...    65.0      45.0      83.0   1.04

[8 rows x 11 columns]