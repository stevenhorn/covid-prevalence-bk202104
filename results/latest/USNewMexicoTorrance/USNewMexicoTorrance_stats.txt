0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -745.93    41.58
p_loo       31.29        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      368   95.8%
 (0.5, 0.7]   (ok)         13    3.4%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.282  0.046   0.190    0.349  ...    21.0      33.0      62.0   1.06
pu        0.882  0.017   0.852    0.900  ...    13.0       9.0      59.0   1.19
mu        0.134  0.025   0.092    0.178  ...     9.0      10.0      33.0   1.15
mus       0.171  0.030   0.120    0.223  ...   152.0     152.0      79.0   0.99
gamma     0.217  0.042   0.144    0.289  ...    79.0      78.0      59.0   1.01
Is_begin  0.879  0.892   0.008    2.339  ...    65.0      39.0      59.0   1.09
Ia_begin  1.707  1.444   0.032    4.549  ...    31.0      32.0      60.0   1.04
E_begin   0.788  0.762   0.002    1.944  ...    64.0      38.0      38.0   1.05

[8 rows x 11 columns]