0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -937.75    56.63
p_loo       36.16        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      362   94.3%
 (0.5, 0.7]   (ok)         18    4.7%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.254  0.051   0.158    0.333  ...    53.0      57.0      44.0   1.02
pu        0.869  0.014   0.838    0.888  ...    49.0      42.0      34.0   1.05
mu        0.137  0.019   0.108    0.179  ...    61.0      62.0     100.0   1.03
mus       0.187  0.035   0.137    0.270  ...    96.0     115.0      60.0   0.98
gamma     0.259  0.044   0.181    0.339  ...   152.0     152.0      95.0   1.02
Is_begin  0.843  0.740   0.022    2.173  ...   111.0     115.0      76.0   1.00
Ia_begin  1.582  1.640   0.090    5.042  ...    61.0      81.0      59.0   1.07
E_begin   0.747  0.884   0.036    2.858  ...    68.0     111.0      35.0   1.00

[8 rows x 11 columns]