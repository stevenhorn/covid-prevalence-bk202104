0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -773.47    26.09
p_loo       21.48        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      354   92.2%
 (0.5, 0.7]   (ok)         26    6.8%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.259  0.054   0.167    0.336  ...    88.0      88.0      74.0   1.03
pu        0.853  0.017   0.824    0.882  ...    52.0      52.0      60.0   0.99
mu        0.144  0.021   0.111    0.189  ...    72.0      69.0      52.0   1.02
mus       0.175  0.036   0.109    0.230  ...   108.0     115.0      56.0   1.01
gamma     0.193  0.036   0.138    0.253  ...   105.0     117.0      96.0   1.00
Is_begin  0.842  0.731   0.046    2.199  ...   152.0     118.0      75.0   1.00
Ia_begin  1.600  1.457   0.016    4.438  ...    72.0      53.0      81.0   1.01
E_begin   0.935  0.918   0.016    2.610  ...   116.0     147.0      60.0   0.98

[8 rows x 11 columns]