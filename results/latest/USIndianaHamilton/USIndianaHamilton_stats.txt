0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1710.21    41.91
p_loo       41.57        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      349   90.9%
 (0.5, 0.7]   (ok)         30    7.8%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.229  0.057   0.151    0.325  ...   129.0     110.0      51.0   1.01
pu        0.742  0.028   0.700    0.795  ...    94.0      90.0      60.0   0.99
mu        0.114  0.019   0.079    0.141  ...    29.0      31.0      57.0   1.02
mus       0.206  0.038   0.146    0.288  ...   152.0     151.0      87.0   1.01
gamma     0.298  0.048   0.214    0.373  ...    99.0      89.0      60.0   1.02
Is_begin  2.043  1.418   0.014    4.539  ...   107.0      78.0      31.0   1.01
Ia_begin  7.158  3.302   0.943   12.417  ...   131.0     139.0      96.0   1.02
E_begin   7.288  6.556   0.156   17.873  ...   113.0      84.0     100.0   0.98

[8 rows x 11 columns]