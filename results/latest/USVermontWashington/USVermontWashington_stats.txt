0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -886.49    31.51
p_loo       31.33        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      357   93.0%
 (0.5, 0.7]   (ok)         22    5.7%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.295  0.042   0.228    0.349  ...   152.0     152.0      24.0   1.00
pu        0.892  0.007   0.880    0.900  ...   115.0      38.0      18.0   1.03
mu        0.173  0.022   0.138    0.215  ...    30.0      31.0      22.0   1.02
mus       0.216  0.033   0.157    0.266  ...   100.0     128.0     100.0   1.05
gamma     0.336  0.066   0.229    0.440  ...   152.0     145.0      59.0   1.05
Is_begin  1.303  1.160   0.034    3.734  ...   123.0     141.0      60.0   1.00
Ia_begin  3.576  3.127   0.079   10.025  ...   117.0     148.0      65.0   0.98
E_begin   1.758  1.833   0.013    5.359  ...   107.0      96.0      57.0   1.02

[8 rows x 11 columns]