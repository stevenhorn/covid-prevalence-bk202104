0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1331.54    39.58
p_loo       28.84        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      361   94.0%
 (0.5, 0.7]   (ok)         18    4.7%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.257  0.048   0.182    0.341  ...    50.0      58.0      99.0   1.02
pu        0.824  0.029   0.772    0.867  ...     5.0       5.0      24.0   1.43
mu        0.112  0.026   0.069    0.158  ...    38.0      37.0      43.0   1.00
mus       0.156  0.034   0.109    0.233  ...   152.0     152.0      61.0   1.03
gamma     0.175  0.036   0.115    0.248  ...   118.0     108.0      76.0   1.02
Is_begin  0.603  0.611   0.002    1.844  ...   149.0     109.0      49.0   0.98
Ia_begin  1.026  1.479   0.014    2.995  ...    93.0      60.0      31.0   1.00
E_begin   0.529  0.789   0.001    2.596  ...   100.0      56.0      43.0   0.99

[8 rows x 11 columns]