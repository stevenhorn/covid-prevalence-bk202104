0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1788.76    41.58
p_loo       38.24        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      353   91.9%
 (0.5, 0.7]   (ok)         24    6.2%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.238  0.051   0.168    0.331  ...   110.0     111.0     100.0   1.00
pu        0.755  0.035   0.705    0.810  ...     6.0       8.0      59.0   1.21
mu        0.127  0.027   0.080    0.173  ...     7.0       7.0      15.0   1.28
mus       0.205  0.036   0.131    0.257  ...   110.0     111.0      56.0   1.03
gamma     0.315  0.051   0.228    0.410  ...    65.0      62.0      72.0   1.04
Is_begin  1.614  1.332   0.121    4.196  ...    82.0      77.0      81.0   0.99
Ia_begin  0.799  0.589   0.001    1.840  ...   134.0      77.0      22.0   1.04
E_begin   2.046  2.010   0.084    5.947  ...    52.0      33.0      88.0   1.06

[8 rows x 11 columns]