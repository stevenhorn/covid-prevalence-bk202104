0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -941.87    40.70
p_loo       30.14        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      358   93.2%
 (0.5, 0.7]   (ok)         15    3.9%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    4    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.247  0.053   0.151    0.338  ...   152.0     152.0      49.0   1.09
pu        0.782  0.026   0.731    0.823  ...    48.0      64.0      20.0   1.02
mu        0.114  0.023   0.080    0.152  ...    59.0      64.0      88.0   1.03
mus       0.165  0.027   0.117    0.221  ...   152.0     152.0      63.0   1.05
gamma     0.227  0.041   0.153    0.284  ...   152.0     134.0      51.0   1.00
Is_begin  0.575  0.638   0.001    1.946  ...   101.0     105.0      60.0   1.00
Ia_begin  1.050  1.167   0.002    3.224  ...   106.0     152.0      81.0   1.01
E_begin   0.394  0.418   0.006    1.085  ...    70.0      64.0      59.0   1.01

[8 rows x 11 columns]