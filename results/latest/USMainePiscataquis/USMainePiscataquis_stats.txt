0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -543.00    31.67
p_loo       27.60        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      354   92.2%
 (0.5, 0.7]   (ok)         27    7.0%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.280  0.046   0.212    0.348  ...    68.0      58.0      59.0   1.04
pu        0.884  0.016   0.852    0.900  ...    34.0      17.0      29.0   1.09
mu        0.144  0.024   0.114    0.196  ...    17.0      22.0      96.0   1.08
mus       0.204  0.037   0.146    0.266  ...   114.0     121.0      84.0   1.05
gamma     0.258  0.042   0.191    0.343  ...   152.0     152.0      74.0   1.02
Is_begin  0.598  0.604   0.007    1.682  ...    86.0      90.0      39.0   1.00
Ia_begin  1.229  1.427   0.015    2.909  ...    59.0      59.0      57.0   1.00
E_begin   0.557  0.653   0.005    1.581  ...    81.0      73.0      59.0   0.99

[8 rows x 11 columns]