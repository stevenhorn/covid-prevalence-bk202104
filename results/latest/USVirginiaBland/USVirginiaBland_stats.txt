0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -802.97    43.00
p_loo       40.74        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      370   96.4%
 (0.5, 0.7]   (ok)         11    2.9%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.262  0.055   0.171    0.347  ...    39.0      41.0      59.0   1.03
pu        0.794  0.030   0.730    0.839  ...    11.0      15.0      20.0   1.12
mu        0.138  0.028   0.096    0.187  ...    16.0      17.0      60.0   1.11
mus       0.182  0.040   0.115    0.251  ...   103.0      98.0      95.0   1.00
gamma     0.222  0.049   0.156    0.303  ...    56.0      51.0      32.0   1.03
Is_begin  0.342  0.368   0.000    0.991  ...    72.0      63.0      91.0   1.00
Ia_begin  0.515  0.474   0.017    1.525  ...    60.0      50.0     100.0   0.99
E_begin   0.191  0.179   0.000    0.542  ...    44.0      35.0      60.0   1.01

[8 rows x 11 columns]