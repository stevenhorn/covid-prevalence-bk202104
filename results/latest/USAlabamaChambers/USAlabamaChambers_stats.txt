0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1200.99    24.86
p_loo       27.99        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      342   89.1%
 (0.5, 0.7]   (ok)         30    7.8%
   (0.7, 1]   (bad)        11    2.9%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.239  0.055   0.157    0.323  ...    85.0      85.0      18.0   1.11
pu        0.764  0.037   0.708    0.822  ...     3.0       4.0      23.0   1.86
mu        0.127  0.021   0.091    0.176  ...    45.0      44.0      42.0   1.03
mus       0.190  0.030   0.134    0.249  ...    96.0     102.0      59.0   1.00
gamma     0.293  0.045   0.215    0.373  ...   152.0     152.0      68.0   1.05
Is_begin  1.762  1.476   0.056    4.831  ...    60.0      43.0      60.0   1.02
Ia_begin  4.767  3.508   0.283   11.062  ...    51.0      36.0      22.0   1.03
E_begin   4.693  4.761   0.007   14.268  ...    42.0      25.0      34.0   1.05

[8 rows x 11 columns]