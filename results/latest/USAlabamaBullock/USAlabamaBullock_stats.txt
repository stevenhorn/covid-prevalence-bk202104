0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -991.90    40.70
p_loo       37.49        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      353   91.9%
 (0.5, 0.7]   (ok)         25    6.5%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.254  0.051   0.165    0.336  ...    82.0      88.0      60.0   1.04
pu        0.815  0.015   0.791    0.844  ...    43.0      42.0      60.0   1.02
mu        0.119  0.020   0.088    0.150  ...    31.0      31.0      43.0   1.03
mus       0.171  0.031   0.119    0.220  ...   132.0     152.0      79.0   1.01
gamma     0.250  0.040   0.185    0.329  ...    28.0      25.0      14.0   1.06
Is_begin  0.473  0.439   0.003    1.398  ...    83.0      49.0      39.0   1.04
Ia_begin  0.791  0.948   0.005    3.030  ...    67.0      57.0      15.0   1.00
E_begin   0.411  0.489   0.000    1.475  ...    61.0      65.0      59.0   1.00

[8 rows x 11 columns]