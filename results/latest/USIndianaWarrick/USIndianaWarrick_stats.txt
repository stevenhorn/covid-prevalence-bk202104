0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1212.38    25.20
p_loo       26.54        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      358   93.2%
 (0.5, 0.7]   (ok)         22    5.7%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.249  0.056   0.168    0.347  ...    17.0      17.0      21.0   1.11
pu        0.741  0.024   0.701    0.782  ...     9.0       9.0      24.0   1.19
mu        0.129  0.026   0.081    0.183  ...     8.0       7.0      22.0   1.26
mus       0.180  0.034   0.136    0.257  ...    84.0      87.0      37.0   1.01
gamma     0.237  0.042   0.166    0.310  ...    43.0      66.0      56.0   1.02
Is_begin  0.940  1.009   0.008    3.031  ...    64.0      30.0      37.0   1.06
Ia_begin  2.124  2.045   0.004    6.616  ...   101.0     123.0      80.0   0.98
E_begin   1.072  1.216   0.025    3.159  ...    96.0      81.0      93.0   1.00

[8 rows x 11 columns]