0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -749.22    54.13
p_loo       41.73        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      368   95.8%
 (0.5, 0.7]   (ok)         11    2.9%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.199  0.032   0.157    0.272  ...    40.0      37.0      39.0   1.06
pu        0.714  0.012   0.700    0.739  ...     8.0       6.0      38.0   1.28
mu        0.189  0.044   0.118    0.258  ...     3.0       3.0      17.0   2.53
mus       0.240  0.044   0.170    0.319  ...     3.0       4.0      14.0   1.76
gamma     0.293  0.057   0.210    0.409  ...     4.0       4.0      15.0   1.50
Is_begin  0.428  0.332   0.010    1.180  ...    15.0      12.0      40.0   1.15
Ia_begin  0.673  0.567   0.043    1.889  ...    11.0       8.0      31.0   1.18
E_begin   0.244  0.268   0.029    0.918  ...    40.0      30.0      32.0   1.04

[8 rows x 11 columns]