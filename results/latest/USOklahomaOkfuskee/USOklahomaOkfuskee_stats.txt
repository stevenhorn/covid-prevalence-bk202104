0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1144.35    73.95
p_loo       50.09        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      362   94.3%
 (0.5, 0.7]   (ok)         18    4.7%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.217  0.047   0.153    0.307  ...    66.0      63.0      88.0   1.00
pu        0.722  0.017   0.700    0.756  ...    73.0      45.0      26.0   1.06
mu        0.115  0.022   0.074    0.158  ...    25.0      18.0      68.0   1.10
mus       0.230  0.054   0.141    0.326  ...    99.0     140.0      68.0   1.02
gamma     0.274  0.051   0.189    0.359  ...    74.0      76.0      59.0   1.01
Is_begin  0.506  0.507   0.008    1.404  ...    77.0      73.0      60.0   1.01
Ia_begin  0.774  1.030   0.002    2.685  ...    97.0     108.0      81.0   0.98
E_begin   0.374  0.660   0.004    1.182  ...    79.0      65.0      96.0   0.98

[8 rows x 11 columns]