0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1002.65    31.38
p_loo       28.95        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      347   90.4%
 (0.5, 0.7]   (ok)         32    8.3%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.258  0.056   0.174    0.347  ...    33.0      31.0      33.0   1.11
pu        0.837  0.019   0.794    0.866  ...    33.0      35.0      56.0   1.06
mu        0.149  0.022   0.114    0.190  ...    41.0      40.0      88.0   1.00
mus       0.180  0.030   0.125    0.230  ...    38.0      56.0      95.0   1.07
gamma     0.217  0.036   0.143    0.268  ...   108.0     111.0      81.0   1.00
Is_begin  0.982  0.945   0.004    3.084  ...    81.0      38.0      96.0   1.06
Ia_begin  0.725  0.541   0.004    1.647  ...   111.0      84.0      91.0   1.00
E_begin   0.586  0.857   0.002    1.812  ...   101.0      20.0      59.0   1.09

[8 rows x 11 columns]