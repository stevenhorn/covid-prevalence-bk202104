0 Divergences 
Passed validation 
Computed from 80 by 385 log-likelihood matrix

         Estimate       SE
elpd_loo   454.19    86.91
p_loo       71.59        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      362   94.0%
 (0.5, 0.7]   (ok)         13    3.4%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.268  0.049   0.187    0.348  ...   129.0     121.0      58.0   1.06
pu        0.845  0.049   0.749    0.899  ...    19.0      70.0      36.0   1.20
mu        0.146  0.027   0.111    0.200  ...   103.0     135.0      91.0   1.02
mus       0.242  0.037   0.187    0.322  ...   128.0     152.0      59.0   0.99
gamma     0.270  0.040   0.206    0.338  ...   100.0     146.0      58.0   0.99
Is_begin  0.179  0.230   0.002    0.461  ...    70.0      98.0      93.0   1.01
Ia_begin  0.347  0.573   0.003    0.953  ...    73.0      43.0      31.0   1.02
E_begin   0.166  0.312   0.001    0.468  ...    91.0      67.0      43.0   0.99

[8 rows x 11 columns]