0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1132.56    47.42
p_loo       44.09        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      358   93.2%
 (0.5, 0.7]   (ok)         21    5.5%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.239  0.058   0.155    0.335  ...    85.0      78.0      56.0   1.03
pu        0.751  0.030   0.700    0.801  ...    29.0      29.0      60.0   1.08
mu        0.129  0.025   0.094    0.180  ...    25.0      23.0      16.0   1.03
mus       0.212  0.038   0.130    0.285  ...    51.0      46.0      40.0   1.04
gamma     0.268  0.047   0.194    0.340  ...   152.0     152.0      72.0   1.04
Is_begin  0.654  0.836   0.003    1.884  ...   108.0      43.0      42.0   1.05
Ia_begin  1.231  1.588   0.003    3.393  ...   108.0      23.0      24.0   1.07
E_begin   0.747  1.483   0.000    3.019  ...    97.0      14.0      59.0   1.13

[8 rows x 11 columns]