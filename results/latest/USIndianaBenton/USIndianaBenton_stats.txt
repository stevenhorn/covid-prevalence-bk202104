0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -737.32    26.46
p_loo       22.91        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      356   92.7%
 (0.5, 0.7]   (ok)         24    6.2%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.253  0.052   0.152    0.332  ...    41.0      45.0      60.0   1.00
pu        0.808  0.020   0.779    0.844  ...    33.0      34.0      58.0   1.01
mu        0.129  0.028   0.091    0.188  ...    30.0      34.0      60.0   1.02
mus       0.152  0.038   0.098    0.229  ...    90.0      90.0      59.0   1.01
gamma     0.173  0.035   0.112    0.236  ...    20.0      19.0      59.0   1.09
Is_begin  0.669  0.643   0.044    1.924  ...    98.0     136.0      55.0   0.98
Ia_begin  1.286  1.084   0.056    3.183  ...    44.0      60.0      60.0   1.01
E_begin   0.505  0.512   0.013    1.432  ...    92.0      79.0      91.0   1.02

[8 rows x 11 columns]