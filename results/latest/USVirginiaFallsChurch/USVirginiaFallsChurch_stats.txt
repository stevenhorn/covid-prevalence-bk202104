0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -733.79    47.59
p_loo       36.50        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      354   92.2%
 (0.5, 0.7]   (ok)         25    6.5%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.285  0.044   0.209    0.347  ...   152.0     152.0      88.0   0.99
pu        0.880  0.024   0.840    0.900  ...    25.0      15.0      22.0   1.12
mu        0.145  0.030   0.078    0.184  ...     8.0       8.0      40.0   1.22
mus       0.198  0.037   0.147    0.285  ...   120.0     152.0      59.0   1.07
gamma     0.279  0.047   0.210    0.370  ...   152.0     152.0      61.0   1.03
Is_begin  1.116  0.940   0.038    2.890  ...   117.0     106.0      30.0   0.99
Ia_begin  2.039  1.739   0.044    5.490  ...    82.0      60.0      48.0   0.99
E_begin   1.457  1.712   0.004    4.623  ...    67.0      60.0      60.0   1.01

[8 rows x 11 columns]