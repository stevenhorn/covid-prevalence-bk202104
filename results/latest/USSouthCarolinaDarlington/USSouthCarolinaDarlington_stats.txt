0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1379.65    27.14
p_loo       25.14        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      348   90.6%
 (0.5, 0.7]   (ok)         32    8.3%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.255  0.051   0.169    0.332  ...    66.0      61.0      33.0   1.09
pu        0.767  0.028   0.711    0.806  ...     5.0       5.0      27.0   1.37
mu        0.119  0.021   0.092    0.166  ...     9.0       9.0      19.0   1.21
mus       0.161  0.035   0.113    0.226  ...    94.0      72.0      46.0   1.03
gamma     0.182  0.035   0.130    0.257  ...    91.0      82.0      86.0   0.99
Is_begin  0.895  0.815   0.006    2.927  ...    89.0      44.0      40.0   1.01
Ia_begin  0.575  0.476   0.003    1.525  ...    28.0      42.0      43.0   1.07
E_begin   0.695  0.838   0.000    2.169  ...   117.0      72.0      96.0   1.03

[8 rows x 11 columns]