0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1585.92    55.62
p_loo       40.30        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      365   95.1%
 (0.5, 0.7]   (ok)         16    4.2%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.229  0.044   0.151    0.302  ...    37.0      37.0      60.0   1.05
pu        0.722  0.015   0.701    0.748  ...    58.0      61.0      59.0   1.04
mu        0.111  0.017   0.087    0.147  ...    25.0      23.0      20.0   1.06
mus       0.175  0.031   0.123    0.229  ...    47.0      32.0      80.0   1.06
gamma     0.229  0.043   0.163    0.320  ...    24.0      32.0      16.0   1.05
Is_begin  0.738  0.749   0.014    2.033  ...    74.0      60.0      93.0   0.99
Ia_begin  1.304  1.279   0.001    3.696  ...    84.0      57.0      40.0   1.02
E_begin   0.596  0.628   0.014    1.736  ...   108.0     123.0     100.0   1.00

[8 rows x 11 columns]