0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -915.24    27.85
p_loo       26.04        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      366   95.3%
 (0.5, 0.7]   (ok)         15    3.9%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.251  0.057   0.157    0.335  ...    80.0      81.0      86.0   1.01
pu        0.770  0.029   0.719    0.831  ...    14.0      14.0      43.0   1.11
mu        0.138  0.027   0.096    0.192  ...    24.0      24.0      46.0   1.08
mus       0.177  0.026   0.126    0.215  ...   129.0     124.0      37.0   1.00
gamma     0.224  0.053   0.134    0.309  ...   152.0     152.0      86.0   0.99
Is_begin  0.870  0.783   0.008    2.757  ...    98.0     139.0      38.0   1.03
Ia_begin  1.623  1.785   0.019    4.503  ...   120.0     147.0      58.0   1.00
E_begin   0.735  0.801   0.015    2.608  ...   115.0     118.0      59.0   0.99

[8 rows x 11 columns]