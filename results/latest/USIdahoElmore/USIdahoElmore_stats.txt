0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1021.67    37.24
p_loo       33.85        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      357   93.0%
 (0.5, 0.7]   (ok)         23    6.0%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.253  0.061   0.150    0.339  ...   152.0     152.0      45.0   1.12
pu        0.793  0.032   0.734    0.839  ...    68.0      71.0      86.0   1.03
mu        0.136  0.023   0.093    0.169  ...    62.0      62.0      57.0   1.00
mus       0.178  0.023   0.146    0.220  ...   143.0     142.0      68.0   1.00
gamma     0.229  0.037   0.149    0.291  ...    73.0      77.0      53.0   1.03
Is_begin  0.889  0.718   0.006    2.384  ...   124.0      96.0      57.0   1.00
Ia_begin  2.075  1.681   0.017    5.308  ...   104.0     104.0      59.0   1.01
E_begin   1.000  1.421   0.000    3.212  ...    85.0      29.0      57.0   1.06

[8 rows x 11 columns]