0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1299.42    22.43
p_loo       26.32        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      364   94.8%
 (0.5, 0.7]   (ok)         17    4.4%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.231  0.051   0.161    0.321  ...    69.0      63.0      43.0   1.06
pu        0.748  0.034   0.702    0.804  ...    39.0      44.0      57.0   1.01
mu        0.108  0.019   0.077    0.144  ...    19.0      23.0      54.0   1.09
mus       0.170  0.026   0.124    0.219  ...   146.0     133.0      60.0   1.00
gamma     0.225  0.034   0.172    0.300  ...   117.0     120.0     100.0   0.99
Is_begin  1.518  1.307   0.068    3.698  ...    61.0      42.0      93.0   1.02
Ia_begin  0.758  0.563   0.025    1.772  ...   133.0     114.0      55.0   1.01
E_begin   1.334  1.748   0.024    3.499  ...    78.0      54.0      60.0   1.03

[8 rows x 11 columns]