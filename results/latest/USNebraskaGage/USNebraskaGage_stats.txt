0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1041.71    31.01
p_loo       31.25        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      364   94.8%
 (0.5, 0.7]   (ok)         12    3.1%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.263  0.045   0.195    0.339  ...    69.0      72.0      99.0   0.99
pu        0.785  0.023   0.738    0.828  ...    24.0      28.0      22.0   1.05
mu        0.134  0.025   0.092    0.169  ...    22.0      23.0      59.0   1.10
mus       0.170  0.030   0.115    0.220  ...   100.0     100.0      58.0   1.00
gamma     0.197  0.036   0.142    0.268  ...    86.0      99.0      88.0   0.99
Is_begin  1.150  1.030   0.057    2.923  ...    62.0      52.0      59.0   1.00
Ia_begin  2.774  2.847   0.037    8.672  ...   114.0      63.0      47.0   0.99
E_begin   1.392  1.691   0.008    4.035  ...    84.0      55.0      60.0   1.01

[8 rows x 11 columns]