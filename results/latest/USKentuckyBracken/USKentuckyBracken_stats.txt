0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -702.73    35.11
p_loo       27.83        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      355   92.4%
 (0.5, 0.7]   (ok)         26    6.8%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.252  0.059   0.166    0.341  ...   152.0     152.0      86.0   1.03
pu        0.866  0.016   0.837    0.891  ...   152.0     152.0      93.0   1.01
mu        0.134  0.030   0.086    0.179  ...    17.0      16.0      60.0   1.13
mus       0.157  0.028   0.103    0.205  ...   152.0     152.0      53.0   1.08
gamma     0.180  0.032   0.128    0.239  ...   100.0      97.0      60.0   1.03
Is_begin  0.973  0.826   0.009    2.336  ...   147.0     121.0      93.0   0.99
Ia_begin  1.961  1.739   0.059    5.530  ...    95.0      73.0      79.0   1.02
E_begin   0.977  1.234   0.027    3.212  ...   111.0      97.0      76.0   1.01

[8 rows x 11 columns]