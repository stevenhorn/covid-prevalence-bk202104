0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -945.48    67.86
p_loo       56.50        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      362   94.3%
 (0.5, 0.7]   (ok)         16    4.2%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.200  0.040   0.150    0.281  ...    66.0      65.0      38.0   1.05
pu        0.717  0.016   0.700    0.749  ...   103.0      77.0      39.0   1.07
mu        0.120  0.020   0.086    0.160  ...    13.0      15.0      17.0   1.10
mus       0.261  0.038   0.202    0.350  ...    41.0      44.0      38.0   1.04
gamma     0.501  0.078   0.357    0.609  ...   109.0     100.0      53.0   1.01
Is_begin  0.241  0.365   0.000    1.010  ...    49.0      18.0      15.0   1.08
Ia_begin  0.318  0.405   0.003    1.127  ...    18.0      22.0      30.0   1.07
E_begin   0.183  0.246   0.000    0.819  ...    49.0      20.0      16.0   1.07

[8 rows x 11 columns]