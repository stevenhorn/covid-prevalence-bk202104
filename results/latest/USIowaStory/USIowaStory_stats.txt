0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1475.96    41.55
p_loo       39.23        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      353   91.9%
 (0.5, 0.7]   (ok)         25    6.5%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.237  0.044   0.168    0.331  ...    61.0      59.0      40.0   0.99
pu        0.734  0.018   0.704    0.764  ...    90.0      94.0      87.0   1.01
mu        0.167  0.021   0.135    0.204  ...    48.0      52.0      60.0   1.05
mus       0.243  0.041   0.171    0.314  ...   152.0     152.0      96.0   0.99
gamma     0.346  0.048   0.279    0.433  ...   108.0     117.0      87.0   1.01
Is_begin  0.910  0.642   0.060    1.979  ...    86.0      74.0      56.0   1.01
Ia_begin  1.659  1.658   0.086    4.059  ...   104.0      95.0      66.0   0.99
E_begin   0.703  0.664   0.000    1.842  ...    97.0      58.0      43.0   0.99

[8 rows x 11 columns]