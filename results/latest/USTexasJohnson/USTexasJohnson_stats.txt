0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1893.16    44.93
p_loo       98.95        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      200   52.1%
 (0.5, 0.7]   (ok)         16    4.2%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)  162   42.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.259  0.034   0.196    0.310  ...     8.0      16.0      84.0   1.83
pu        0.734  0.022   0.716    0.775  ...     3.0       4.0       3.0   2.14
mu        0.144  0.023   0.101    0.162  ...     3.0       4.0      83.0   2.14
mus       0.163  0.033   0.102    0.216  ...   107.0     150.0      84.0   2.13
gamma     0.179  0.027   0.136    0.230  ...    73.0      92.0      61.0   2.28
Is_begin  0.559  0.440   0.022    1.587  ...    40.0      79.0      37.0   2.00
Ia_begin  1.001  0.490   0.057    1.553  ...     9.0      13.0      60.0   2.04
E_begin   0.317  0.382   0.014    1.022  ...    10.0       9.0      18.0   1.95

[8 rows x 11 columns]