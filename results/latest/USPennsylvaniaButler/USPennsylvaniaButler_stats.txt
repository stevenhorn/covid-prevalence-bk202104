0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1418.06    30.97
p_loo       29.63        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      340   88.5%
 (0.5, 0.7]   (ok)         39   10.2%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.249  0.054   0.167    0.337  ...   152.0     152.0      19.0   1.14
pu        0.762  0.032   0.708    0.813  ...    57.0      62.0      91.0   1.05
mu        0.140  0.021   0.102    0.167  ...    67.0      71.0      60.0   1.01
mus       0.192  0.040   0.126    0.263  ...    82.0      76.0      31.0   1.09
gamma     0.236  0.041   0.176    0.323  ...    48.0      50.0      50.0   1.04
Is_begin  1.843  1.452   0.040    4.671  ...   152.0     152.0      59.0   1.02
Ia_begin  5.236  3.171   0.067   10.772  ...    81.0      84.0      60.0   1.00
E_begin   3.333  3.015   0.018    9.385  ...    57.0      60.0      60.0   1.02

[8 rows x 11 columns]