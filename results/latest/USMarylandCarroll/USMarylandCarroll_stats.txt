0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1419.64    40.28
p_loo       38.54        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      344   89.6%
 (0.5, 0.7]   (ok)         32    8.3%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.225  0.046   0.155    0.310  ...   113.0     123.0      58.0   0.98
pu        0.732  0.029   0.701    0.793  ...   152.0     152.0     100.0   1.03
mu        0.109  0.021   0.072    0.150  ...    20.0      19.0      35.0   1.04
mus       0.206  0.036   0.140    0.274  ...   152.0     152.0      87.0   1.04
gamma     0.312  0.050   0.241    0.411  ...    73.0      87.0      76.0   1.03
Is_begin  2.262  1.644   0.133    4.934  ...    90.0      69.0      60.0   1.01
Ia_begin  1.103  0.560   0.287    2.223  ...   152.0     152.0      87.0   0.99
E_begin   2.619  2.560   0.038    7.527  ...    23.0      22.0      95.0   1.07

[8 rows x 11 columns]