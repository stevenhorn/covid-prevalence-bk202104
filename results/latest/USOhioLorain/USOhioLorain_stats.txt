0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1701.37    62.06
p_loo       39.37        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      361   94.0%
 (0.5, 0.7]   (ok)         21    5.5%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.250  0.048   0.150    0.332  ...   141.0     152.0      24.0   1.04
pu        0.797  0.039   0.734    0.873  ...    30.0      30.0      40.0   1.06
mu        0.139  0.026   0.107    0.191  ...     8.0       7.0      22.0   1.24
mus       0.198  0.039   0.130    0.269  ...   114.0      99.0      97.0   1.01
gamma     0.254  0.046   0.183    0.359  ...   124.0     135.0      40.0   1.07
Is_begin  2.080  1.491   0.192    5.315  ...    65.0      73.0      59.0   1.01
Ia_begin  4.997  3.861   0.062   11.225  ...    69.0      51.0      43.0   1.02
E_begin   3.723  3.269   0.177    9.680  ...    50.0      65.0      31.0   1.03

[8 rows x 11 columns]