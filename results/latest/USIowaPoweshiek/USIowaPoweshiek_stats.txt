0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -912.85    32.36
p_loo       32.96        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      353   91.9%
 (0.5, 0.7]   (ok)         26    6.8%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.256  0.055   0.166    0.340  ...   152.0     152.0      60.0   1.00
pu        0.836  0.027   0.776    0.874  ...    17.0      26.0      31.0   1.06
mu        0.152  0.027   0.106    0.206  ...    34.0      34.0      40.0   1.05
mus       0.208  0.029   0.152    0.251  ...    79.0      78.0      73.0   1.00
gamma     0.293  0.049   0.209    0.377  ...   152.0     152.0      54.0   0.99
Is_begin  0.881  0.964   0.007    2.708  ...    82.0     113.0      59.0   1.00
Ia_begin  1.570  1.537   0.021    4.583  ...   106.0     116.0      83.0   0.98
E_begin   0.796  0.887   0.000    2.802  ...    56.0      60.0      58.0   1.01

[8 rows x 11 columns]