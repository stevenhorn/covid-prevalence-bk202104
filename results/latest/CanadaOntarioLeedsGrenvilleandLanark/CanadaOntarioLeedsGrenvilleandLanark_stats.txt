0 Divergences 
Passed validation 
Computed from 80 by 390 log-likelihood matrix

         Estimate       SE
elpd_loo -1033.69    37.08
p_loo       37.18        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      361   92.6%
 (0.5, 0.7]   (ok)         23    5.9%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    1    0.3%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.269   0.046   0.206    0.349  ...    32.0      33.0      54.0   1.06
pu         0.873   0.028   0.828    0.900  ...    37.0      19.0      39.0   1.09
mu         0.177   0.019   0.145    0.216  ...    42.0      40.0      95.0   1.01
mus        0.219   0.035   0.151    0.281  ...   107.0      95.0      59.0   1.00
gamma      0.349   0.045   0.281    0.422  ...   115.0     110.0      76.0   1.00
Is_begin   5.223   5.715   0.093   17.984  ...    56.0      43.0      59.0   1.08
Ia_begin  25.651  17.011   2.111   52.667  ...    24.0      16.0      40.0   1.10
E_begin   15.499  14.344   0.210   43.803  ...    32.0      14.0      17.0   1.11

[8 rows x 11 columns]