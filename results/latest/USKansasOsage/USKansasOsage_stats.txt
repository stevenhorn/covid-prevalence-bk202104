7 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1039.84    33.77
p_loo       28.38        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      347   90.4%
 (0.5, 0.7]   (ok)         23    6.0%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    7    1.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.254  0.030   0.203    0.303  ...     9.0      10.0      29.0   1.16
pu        0.788  0.017   0.760    0.820  ...     4.0       5.0      19.0   1.68
mu        0.164  0.017   0.139    0.197  ...     5.0       8.0      34.0   1.30
mus       0.163  0.036   0.106    0.225  ...     3.0       3.0      18.0   2.39
gamma     0.176  0.038   0.129    0.238  ...     5.0       5.0      30.0   1.73
Is_begin  1.028  0.734   0.087    1.997  ...     3.0       4.0      38.0   1.54
Ia_begin  2.907  1.100   1.165    4.529  ...     4.0       5.0      21.0   1.55
E_begin   0.577  0.355   0.152    1.255  ...     6.0       6.0      17.0   1.45

[8 rows x 11 columns]