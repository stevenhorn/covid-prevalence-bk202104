0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -925.74    29.03
p_loo       28.05        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      350   91.1%
 (0.5, 0.7]   (ok)         22    5.7%
   (0.7, 1]   (bad)        12    3.1%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.260  0.048   0.174    0.333  ...    34.0      42.0      60.0   0.99
pu        0.800  0.028   0.732    0.840  ...    25.0      33.0      30.0   1.10
mu        0.134  0.015   0.100    0.156  ...    35.0      39.0      38.0   1.04
mus       0.156  0.028   0.109    0.202  ...    20.0      18.0      60.0   1.10
gamma     0.220  0.038   0.148    0.290  ...    11.0      10.0      38.0   1.20
Is_begin  0.497  0.597   0.005    1.351  ...    64.0      23.0      61.0   1.10
Ia_begin  1.178  0.957   0.016    2.999  ...    59.0      61.0      60.0   1.01
E_begin   0.350  0.421   0.002    1.185  ...    15.0       6.0      40.0   1.31

[8 rows x 11 columns]