0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1095.13    27.54
p_loo       26.06        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      353   91.9%
 (0.5, 0.7]   (ok)         26    6.8%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.261  0.049   0.176    0.338  ...   152.0     152.0      56.0   0.99
pu        0.853  0.016   0.829    0.879  ...   141.0     138.0      96.0   1.00
mu        0.121  0.023   0.091    0.178  ...    63.0      56.0      60.0   1.00
mus       0.192  0.029   0.127    0.239  ...   152.0     151.0      23.0   0.99
gamma     0.219  0.044   0.150    0.314  ...   105.0     109.0      88.0   1.00
Is_begin  0.804  0.796   0.001    2.400  ...   118.0      53.0      14.0   1.02
Ia_begin  1.319  1.307   0.008    3.356  ...   112.0      86.0      96.0   1.00
E_begin   0.556  0.565   0.004    1.525  ...   110.0      80.0      33.0   1.00

[8 rows x 11 columns]