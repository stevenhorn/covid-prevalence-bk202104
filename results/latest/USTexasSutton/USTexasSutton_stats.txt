0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -741.05    35.12
p_loo       30.59        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      359   93.5%
 (0.5, 0.7]   (ok)         21    5.5%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.236  0.050   0.150    0.317  ...    80.0      89.0      97.0   0.99
pu        0.761  0.024   0.724    0.805  ...    83.0      82.0      60.0   1.00
mu        0.143  0.029   0.092    0.188  ...    62.0      60.0      60.0   1.03
mus       0.177  0.031   0.126    0.229  ...   152.0     152.0      91.0   1.04
gamma     0.222  0.038   0.154    0.271  ...   152.0     152.0      79.0   1.05
Is_begin  0.151  0.192   0.000    0.441  ...   102.0     104.0      37.0   0.99
Ia_begin  0.233  0.287   0.000    0.879  ...    58.0      59.0      60.0   1.00
E_begin   0.108  0.135   0.001    0.303  ...    89.0      57.0      24.0   0.98

[8 rows x 11 columns]