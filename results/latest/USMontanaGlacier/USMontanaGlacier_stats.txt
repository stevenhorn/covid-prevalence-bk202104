0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1033.67    31.14
p_loo       28.58        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      363   94.5%
 (0.5, 0.7]   (ok)         15    3.9%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.241  0.045   0.150    0.309  ...    73.0      73.0      36.0   1.06
pu        0.736  0.020   0.702    0.771  ...    57.0      61.0      60.0   1.03
mu        0.130  0.022   0.102    0.178  ...    43.0      37.0      40.0   1.00
mus       0.184  0.029   0.133    0.229  ...    84.0      86.0      60.0   1.01
gamma     0.214  0.031   0.165    0.264  ...    98.0      96.0      24.0   1.14
Is_begin  0.746  0.783   0.000    2.412  ...   113.0      66.0      15.0   1.00
Ia_begin  1.325  1.500   0.008    4.558  ...    46.0      88.0      32.0   1.00
E_begin   0.598  0.827   0.004    1.979  ...    31.0      48.0      38.0   1.04

[8 rows x 11 columns]