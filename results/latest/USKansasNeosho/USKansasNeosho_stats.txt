0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1141.66    36.86
p_loo       25.04        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      374   97.4%
 (0.5, 0.7]   (ok)          8    2.1%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.207  0.022   0.174    0.247  ...     4.0       4.0      21.0   1.82
pu        0.719  0.007   0.708    0.729  ...     5.0       5.0      31.0   1.39
mu        0.224  0.020   0.194    0.259  ...     3.0       3.0      22.0   2.02
mus       0.219  0.023   0.171    0.253  ...     4.0       4.0      14.0   1.80
gamma     0.085  0.018   0.068    0.126  ...     3.0       4.0      17.0   1.83
Is_begin  0.983  0.601   0.156    2.085  ...     4.0       4.0      14.0   1.57
Ia_begin  4.358  3.033   0.276   10.657  ...     3.0       3.0      24.0   2.12
E_begin   1.458  0.630   0.508    2.472  ...     6.0       6.0      24.0   1.31

[8 rows x 11 columns]