0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1069.28    45.37
p_loo       41.43        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      361   94.0%
 (0.5, 0.7]   (ok)         18    4.7%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.266  0.051   0.176    0.345  ...    90.0      81.0      60.0   1.06
pu        0.785  0.024   0.733    0.825  ...    22.0      24.0      25.0   1.08
mu        0.129  0.023   0.093    0.171  ...    21.0      22.0      39.0   1.09
mus       0.186  0.036   0.120    0.247  ...    90.0      89.0      40.0   0.98
gamma     0.201  0.046   0.136    0.280  ...   152.0     120.0      40.0   0.98
Is_begin  0.457  0.479   0.008    1.385  ...    97.0      79.0      59.0   1.02
Ia_begin  1.063  1.370   0.020    4.346  ...   117.0      85.0      39.0   1.02
E_begin   0.557  0.617   0.000    1.793  ...   110.0     147.0      80.0   1.00

[8 rows x 11 columns]