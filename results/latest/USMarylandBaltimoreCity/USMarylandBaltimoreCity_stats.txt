0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1905.30    23.39
p_loo       29.17        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      362   94.3%
 (0.5, 0.7]   (ok)         17    4.4%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    0    0.0%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.242   0.049   0.172    0.334  ...    61.0      49.0      59.0   1.03
pu         0.754   0.036   0.701    0.817  ...    68.0      80.0      51.0   1.08
mu         0.113   0.026   0.062    0.150  ...    30.0      24.0      49.0   1.05
mus        0.195   0.043   0.127    0.290  ...    21.0      49.0      15.0   1.08
gamma      0.285   0.040   0.212    0.349  ...    97.0     103.0      46.0   1.03
Is_begin   4.159   3.082   0.041    8.399  ...    62.0      26.0      17.0   1.07
Ia_begin  11.580   5.415   2.460   20.949  ...   118.0      94.0      37.0   1.03
E_begin   14.492  11.121   0.094   33.920  ...    50.0      41.0      38.0   1.04

[8 rows x 11 columns]