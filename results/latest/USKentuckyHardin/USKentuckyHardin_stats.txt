0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1438.09    35.01
p_loo       23.44        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      352   91.7%
 (0.5, 0.7]   (ok)         30    7.8%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.253  0.050   0.160    0.322  ...    69.0      74.0      83.0   1.03
pu        0.796  0.023   0.752    0.828  ...    38.0      39.0      43.0   1.05
mu        0.113  0.021   0.073    0.150  ...    26.0      23.0      17.0   1.04
mus       0.162  0.032   0.107    0.222  ...    88.0      75.0      40.0   0.99
gamma     0.171  0.029   0.126    0.229  ...   106.0     113.0      60.0   1.01
Is_begin  0.921  0.846   0.003    2.819  ...    48.0     114.0      37.0   1.06
Ia_begin  1.761  1.919   0.016    5.601  ...   105.0      49.0      38.0   1.06
E_begin   0.888  1.294   0.004    2.925  ...   100.0     111.0      99.0   0.99

[8 rows x 11 columns]