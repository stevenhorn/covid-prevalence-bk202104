0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1366.97    30.50
p_loo       27.29        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      347   90.4%
 (0.5, 0.7]   (ok)         31    8.1%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.247  0.045   0.163    0.322  ...    80.0      88.0      59.0   1.13
pu        0.761  0.019   0.727    0.795  ...    12.0      13.0      60.0   1.13
mu        0.135  0.022   0.099    0.177  ...    45.0      50.0      59.0   1.02
mus       0.171  0.032   0.119    0.251  ...    30.0      41.0      15.0   1.04
gamma     0.190  0.033   0.136    0.259  ...    96.0      97.0      96.0   1.01
Is_begin  0.574  0.725   0.002    1.478  ...    80.0      42.0      14.0   1.01
Ia_begin  0.960  1.105   0.001    3.711  ...    96.0      68.0      55.0   0.99
E_begin   0.470  0.705   0.000    1.575  ...    99.0      28.0      38.0   1.06

[8 rows x 11 columns]