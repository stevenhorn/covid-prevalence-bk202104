0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -615.69    23.69
p_loo       19.10        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      362   94.3%
 (0.5, 0.7]   (ok)         17    4.4%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.253  0.053   0.176    0.344  ...   152.0     152.0      84.0   1.03
pu        0.795  0.020   0.764    0.838  ...   100.0      98.0      54.0   0.98
mu        0.133  0.026   0.097    0.181  ...    75.0      82.0      99.0   1.01
mus       0.175  0.034   0.110    0.235  ...   152.0     152.0      70.0   1.00
gamma     0.193  0.040   0.134    0.281  ...   152.0     152.0      80.0   0.99
Is_begin  0.560  0.572   0.000    1.654  ...   114.0     114.0      93.0   1.00
Ia_begin  1.111  1.064   0.069    3.557  ...   107.0     120.0      95.0   1.00
E_begin   0.481  0.502   0.003    1.478  ...    66.0      93.0      57.0   0.99

[8 rows x 11 columns]