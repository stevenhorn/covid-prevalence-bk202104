0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -904.99    34.76
p_loo       28.63        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      356   92.7%
 (0.5, 0.7]   (ok)         23    6.0%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.259  0.038   0.198    0.336  ...    97.0     100.0      96.0   1.06
pu        0.828  0.020   0.783    0.857  ...    10.0      11.0      53.0   1.13
mu        0.120  0.023   0.079    0.165  ...    36.0      34.0      54.0   1.06
mus       0.183  0.046   0.112    0.273  ...   102.0     121.0      33.0   1.01
gamma     0.197  0.044   0.122    0.277  ...    66.0      68.0      74.0   1.06
Is_begin  0.826  0.759   0.002    2.340  ...    78.0      55.0      63.0   1.03
Ia_begin  1.633  1.762   0.007    4.869  ...   127.0     120.0      60.0   1.01
E_begin   0.693  0.756   0.016    1.810  ...    91.0      71.0      60.0   1.01

[8 rows x 11 columns]