0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1092.27    40.03
p_loo       34.85        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      355   92.4%
 (0.5, 0.7]   (ok)         26    6.8%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.256  0.050   0.182    0.339  ...    20.0      18.0      38.0   1.08
pu        0.732  0.021   0.701    0.767  ...    43.0      49.0      24.0   1.04
mu        0.132  0.022   0.101    0.182  ...    15.0      13.0      14.0   1.11
mus       0.174  0.029   0.123    0.218  ...    42.0      40.0      88.0   1.04
gamma     0.199  0.042   0.137    0.280  ...     8.0       7.0      24.0   1.29
Is_begin  0.708  0.894   0.002    1.743  ...   100.0      58.0      20.0   1.10
Ia_begin  1.553  1.298   0.024    3.650  ...    32.0      48.0      40.0   1.03
E_begin   0.675  0.465   0.021    1.502  ...    46.0      66.0      40.0   1.07

[8 rows x 11 columns]