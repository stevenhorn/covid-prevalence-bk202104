0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1595.99    31.33
p_loo       21.57        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      367   95.6%
 (0.5, 0.7]   (ok)         14    3.6%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.209  0.042   0.151    0.285  ...   120.0     109.0      43.0   1.03
pu        0.717  0.014   0.700    0.742  ...    77.0      58.0      66.0   1.02
mu        0.126  0.023   0.081    0.162  ...    44.0      38.0      53.0   1.04
mus       0.170  0.036   0.106    0.236  ...    73.0      79.0      60.0   1.05
gamma     0.178  0.031   0.123    0.230  ...    54.0      61.0      61.0   1.02
Is_begin  0.799  0.642   0.002    1.941  ...    87.0      66.0      40.0   1.03
Ia_begin  1.525  1.426   0.043    4.263  ...    98.0      68.0      56.0   1.02
E_begin   0.617  0.538   0.030    1.757  ...    97.0      90.0      76.0   1.01

[8 rows x 11 columns]