0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1068.57    39.72
p_loo       24.97        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      358   93.2%
 (0.5, 0.7]   (ok)         20    5.2%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.246  0.054   0.151    0.337  ...    58.0      55.0      40.0   1.00
pu        0.808  0.021   0.775    0.846  ...    26.0      26.0      74.0   1.09
mu        0.127  0.025   0.092    0.177  ...    15.0      30.0      59.0   1.14
mus       0.175  0.038   0.107    0.247  ...    64.0      63.0      58.0   1.02
gamma     0.211  0.046   0.134    0.291  ...   152.0     152.0      58.0   1.00
Is_begin  0.700  0.583   0.017    1.690  ...    53.0      43.0      34.0   1.05
Ia_begin  1.061  1.164   0.015    3.206  ...    16.0      12.0      54.0   1.14
E_begin   0.549  0.583   0.003    1.858  ...    28.0      16.0      76.0   1.13

[8 rows x 11 columns]