0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -801.18    30.19
p_loo       26.33        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      367   95.6%
 (0.5, 0.7]   (ok)         14    3.6%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.251  0.053   0.159    0.341  ...    32.0      31.0      91.0   1.06
pu        0.849  0.014   0.822    0.872  ...    43.0      44.0      26.0   1.04
mu        0.127  0.020   0.089    0.160  ...   101.0     103.0      93.0   1.00
mus       0.159  0.033   0.095    0.210  ...   152.0     152.0      88.0   0.99
gamma     0.180  0.031   0.123    0.231  ...   115.0     128.0      83.0   1.03
Is_begin  0.658  0.568   0.010    1.747  ...   152.0     152.0      88.0   1.00
Ia_begin  1.224  1.380   0.004    3.659  ...   135.0      96.0      38.0   1.00
E_begin   0.608  0.886   0.007    1.659  ...   108.0     152.0     100.0   1.00

[8 rows x 11 columns]