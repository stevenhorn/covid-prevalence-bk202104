0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -898.94    25.52
p_loo       23.19        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      356   92.7%
 (0.5, 0.7]   (ok)         26    6.8%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.255  0.055   0.168    0.349  ...   152.0     152.0      59.0   1.07
pu        0.798  0.020   0.766    0.838  ...    67.0      66.0      80.0   1.02
mu        0.128  0.022   0.091    0.156  ...    37.0      28.0      40.0   1.04
mus       0.161  0.030   0.106    0.210  ...   152.0     152.0      49.0   0.99
gamma     0.188  0.032   0.137    0.253  ...   152.0     152.0      93.0   1.02
Is_begin  0.799  0.792   0.004    2.733  ...   115.0      74.0      77.0   0.98
Ia_begin  1.981  1.721   0.075    5.384  ...    98.0      93.0      49.0   1.00
E_begin   0.994  1.072   0.006    2.863  ...    78.0      56.0      81.0   1.02

[8 rows x 11 columns]