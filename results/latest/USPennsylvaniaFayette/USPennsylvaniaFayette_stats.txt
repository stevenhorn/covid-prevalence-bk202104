0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1372.34    40.83
p_loo       33.14        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      355   92.4%
 (0.5, 0.7]   (ok)         22    5.7%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.240  0.052   0.175    0.339  ...    91.0      85.0      59.0   1.06
pu        0.776  0.028   0.712    0.818  ...    66.0      65.0      60.0   1.00
mu        0.143  0.027   0.102    0.192  ...    43.0      36.0      46.0   1.03
mus       0.195  0.039   0.136    0.261  ...    42.0      55.0      83.0   1.03
gamma     0.262  0.046   0.187    0.345  ...   100.0     103.0      96.0   1.00
Is_begin  1.016  0.807   0.047    2.900  ...   115.0      71.0      53.0   1.01
Ia_begin  2.079  1.728   0.052    5.819  ...    52.0      39.0      87.0   1.04
E_begin   1.085  1.257   0.005    3.114  ...    82.0      32.0      20.0   1.06

[8 rows x 11 columns]