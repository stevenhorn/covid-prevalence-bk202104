0 Divergences 
Passed validation 
Computed from 80 by 390 log-likelihood matrix

         Estimate       SE
elpd_loo  -255.19    54.23
p_loo       47.68        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      374   95.9%
 (0.5, 0.7]   (ok)         11    2.8%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.242  0.058   0.163    0.346  ...   126.0     120.0      60.0   1.05
pu        0.772  0.028   0.711    0.820  ...    63.0      56.0      60.0   1.02
mu        0.145  0.030   0.103    0.212  ...    80.0     109.0      53.0   1.00
mus       0.240  0.038   0.181    0.314  ...    98.0     107.0      57.0   1.01
gamma     0.248  0.038   0.182    0.316  ...   100.0      95.0      76.0   1.00
Is_begin  0.804  0.805   0.001    1.924  ...   121.0     152.0     100.0   1.00
Ia_begin  1.530  1.496   0.027    4.307  ...    31.0      86.0      59.0   1.02
E_begin   0.657  0.697   0.002    2.100  ...   123.0      86.0      43.0   0.99

[8 rows x 11 columns]