0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1247.11    32.42
p_loo       26.32        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      367   95.6%
 (0.5, 0.7]   (ok)         11    2.9%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.262  0.053   0.173    0.346  ...     9.0      11.0      14.0   1.16
pu        0.758  0.027   0.704    0.793  ...     9.0      10.0      20.0   1.18
mu        0.142  0.019   0.111    0.174  ...     8.0       9.0      37.0   1.24
mus       0.188  0.036   0.128    0.257  ...    17.0      20.0      22.0   1.09
gamma     0.235  0.044   0.163    0.306  ...    51.0      51.0      50.0   1.01
Is_begin  0.885  0.688   0.044    2.210  ...    26.0      17.0      81.0   1.09
Ia_begin  1.730  1.232   0.369    3.992  ...    68.0      40.0      87.0   1.05
E_begin   0.843  0.739   0.070    2.239  ...    12.0      13.0      33.0   1.13

[8 rows x 11 columns]