0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo   130.76    50.31
p_loo       63.86        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      353   91.9%
 (0.5, 0.7]   (ok)         21    5.5%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    4    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.257  0.049   0.182    0.349  ...   152.0     152.0      38.0   1.02
pu        0.825  0.042   0.742    0.894  ...    83.0      83.0      60.0   1.02
mu        0.127  0.024   0.092    0.174  ...    80.0      76.0     100.0   0.98
mus       0.208  0.038   0.135    0.271  ...   152.0     152.0      44.0   1.01
gamma     0.223  0.033   0.170    0.281  ...   152.0     152.0      87.0   1.06
Is_begin  0.305  0.405   0.001    1.389  ...   119.0      69.0      81.0   0.98
Ia_begin  0.419  0.521   0.003    1.570  ...    99.0      94.0      43.0   0.98
E_begin   0.234  0.376   0.001    1.109  ...    99.0      82.0      52.0   1.00

[8 rows x 11 columns]