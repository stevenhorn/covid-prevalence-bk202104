0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -205.12    44.60
p_loo       39.65        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      355   92.4%
 (0.5, 0.7]   (ok)         23    6.0%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.271  0.048   0.197    0.349  ...    84.0      90.0      38.0   0.98
pu        0.881  0.015   0.858    0.900  ...    21.0      27.0      24.0   1.02
mu        0.131  0.025   0.090    0.177  ...    15.0      16.0      59.0   1.10
mus       0.216  0.033   0.165    0.281  ...   103.0     111.0      75.0   1.01
gamma     0.240  0.040   0.174    0.315  ...    61.0      61.0      73.0   1.01
Is_begin  0.506  0.619   0.009    1.736  ...    72.0      50.0     100.0   1.00
Ia_begin  0.952  1.118   0.004    3.322  ...    78.0      54.0      93.0   0.99
E_begin   0.417  0.472   0.001    1.304  ...    54.0      41.0      46.0   1.03

[8 rows x 11 columns]