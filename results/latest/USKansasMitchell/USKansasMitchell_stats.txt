0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -897.38    49.06
p_loo       35.18        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      364   94.8%
 (0.5, 0.7]   (ok)         16    4.2%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.254  0.051   0.178    0.350  ...   128.0     137.0      59.0   1.05
pu        0.759  0.026   0.710    0.799  ...    83.0      80.0      37.0   1.00
mu        0.114  0.021   0.074    0.152  ...    29.0      29.0      68.0   1.03
mus       0.197  0.031   0.143    0.249  ...   152.0     152.0      97.0   1.02
gamma     0.254  0.038   0.178    0.317  ...   106.0     116.0      59.0   0.99
Is_begin  0.675  0.776   0.000    2.339  ...   130.0      66.0      38.0   1.00
Ia_begin  1.570  1.754   0.003    4.720  ...    49.0      58.0      60.0   1.01
E_begin   0.544  0.578   0.009    1.587  ...    94.0      62.0      60.0   1.03

[8 rows x 11 columns]