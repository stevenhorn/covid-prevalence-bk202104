0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -803.78    68.66
p_loo       48.26        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      362   94.3%
 (0.5, 0.7]   (ok)         16    4.2%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.228  0.054   0.152    0.320  ...   152.0     152.0      48.0   0.99
pu        0.730  0.022   0.700    0.767  ...   152.0     121.0      60.0   0.99
mu        0.122  0.017   0.092    0.155  ...    37.0      35.0      96.0   1.03
mus       0.236  0.038   0.162    0.286  ...    71.0      57.0      93.0   1.04
gamma     0.377  0.065   0.260    0.481  ...   114.0     119.0     100.0   1.01
Is_begin  0.719  0.730   0.016    2.298  ...   147.0     118.0      60.0   0.99
Ia_begin  1.066  1.056   0.008    3.076  ...   125.0     138.0      40.0   1.03
E_begin   0.480  0.539   0.010    1.724  ...   104.0     113.0      59.0   0.99

[8 rows x 11 columns]