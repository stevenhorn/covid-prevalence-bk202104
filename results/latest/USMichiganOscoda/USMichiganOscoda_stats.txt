0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -710.56    42.97
p_loo       43.42        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      360   93.8%
 (0.5, 0.7]   (ok)         16    4.2%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.258  0.060   0.160    0.346  ...    68.0      65.0      40.0   1.05
pu        0.796  0.027   0.752    0.844  ...    81.0      87.0      56.0   1.00
mu        0.156  0.029   0.095    0.198  ...    17.0      14.0      42.0   1.12
mus       0.233  0.044   0.167    0.326  ...    62.0      71.0      33.0   1.03
gamma     0.294  0.055   0.206    0.398  ...   133.0     133.0      74.0   1.06
Is_begin  0.843  0.811   0.011    2.290  ...   121.0      93.0      38.0   0.99
Ia_begin  1.681  1.530   0.014    3.815  ...   100.0      67.0      43.0   1.00
E_begin   0.837  0.921   0.010    2.624  ...   105.0      63.0      33.0   1.00

[8 rows x 11 columns]