0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1894.69    64.76
p_loo       38.25        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      350   91.1%
 (0.5, 0.7]   (ok)         27    7.0%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    1    0.3%

             mean       sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.227    0.058   0.150    0.343  ...     9.0       9.0      40.0   1.19
pu          0.725    0.018   0.701    0.761  ...    39.0      28.0      59.0   1.08
mu          0.100    0.013   0.081    0.126  ...    24.0      23.0      40.0   1.02
mus         0.257    0.044   0.190    0.334  ...    18.0      19.0      59.0   1.10
gamma       0.377    0.072   0.262    0.486  ...    67.0     107.0      49.0   1.03
Is_begin   65.898   48.735   1.150  164.888  ...    36.0      23.0      48.0   1.07
Ia_begin  217.623   85.407  76.658  380.912  ...    24.0      18.0      59.0   1.09
E_begin   290.322  177.117  12.288  577.822  ...    51.0      43.0      40.0   1.04

[8 rows x 11 columns]