0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -808.99    37.51
p_loo       35.78        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      353   91.9%
 (0.5, 0.7]   (ok)         25    6.5%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.254  0.053   0.165    0.345  ...   112.0     106.0      59.0   1.00
pu        0.760  0.034   0.703    0.813  ...    33.0      25.0      40.0   1.06
mu        0.160  0.027   0.111    0.205  ...    36.0      37.0      91.0   1.05
mus       0.217  0.045   0.143    0.300  ...    43.0      45.0      40.0   1.05
gamma     0.263  0.055   0.169    0.359  ...    41.0      86.0      59.0   1.17
Is_begin  0.797  0.712   0.011    2.050  ...   122.0      99.0      60.0   1.00
Ia_begin  1.429  1.494   0.018    3.775  ...    77.0      52.0      80.0   1.06
E_begin   0.573  0.514   0.013    1.509  ...    65.0      49.0      37.0   1.02

[8 rows x 11 columns]