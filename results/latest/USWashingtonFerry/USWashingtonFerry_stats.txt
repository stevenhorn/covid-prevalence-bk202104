0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -481.70    31.27
p_loo       30.06        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      361   94.0%
 (0.5, 0.7]   (ok)         21    5.5%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.277  0.054   0.180    0.350  ...    77.0      86.0      73.0   1.01
pu        0.887  0.014   0.861    0.900  ...    87.0      83.0      39.0   1.01
mu        0.166  0.026   0.123    0.219  ...    65.0      61.0      49.0   1.03
mus       0.213  0.040   0.149    0.297  ...    90.0     113.0      54.0   1.01
gamma     0.284  0.046   0.194    0.367  ...    82.0      81.0      58.0   1.00
Is_begin  0.647  0.730   0.001    1.886  ...    67.0      52.0      58.0   0.99
Ia_begin  1.211  1.286   0.014    3.957  ...    82.0      71.0      59.0   1.10
E_begin   0.502  0.655   0.003    1.474  ...   102.0      51.0      59.0   1.02

[8 rows x 11 columns]