0 Divergences 
Passed validation 
Computed from 80 by 385 log-likelihood matrix

         Estimate       SE
elpd_loo -1345.48    29.33
p_loo       26.67        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      359   93.2%
 (0.5, 0.7]   (ok)         22    5.7%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    1    0.3%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.178   0.024   0.150    0.220  ...   148.0      83.0      60.0   1.02
pu         0.710   0.007   0.700    0.722  ...   151.0     137.0      72.0   1.03
mu         0.133   0.018   0.104    0.170  ...    45.0      44.0      56.0   1.04
mus        0.192   0.030   0.149    0.259  ...   112.0     109.0      87.0   1.02
gamma      0.252   0.037   0.206    0.321  ...   152.0     152.0      95.0   1.00
Is_begin  27.843  14.556   0.229   50.757  ...    69.0      61.0      30.0   1.05
Ia_begin  55.605  29.635   5.994  103.496  ...   129.0     134.0      61.0   1.05
E_begin   47.515  34.516   1.212  116.047  ...    76.0      33.0      40.0   1.07

[8 rows x 11 columns]