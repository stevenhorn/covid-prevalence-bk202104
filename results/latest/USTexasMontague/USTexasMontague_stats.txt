0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1113.40    46.60
p_loo       41.76        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      348   90.6%
 (0.5, 0.7]   (ok)         25    6.5%
   (0.7, 1]   (bad)         8    2.1%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.243  0.047   0.151    0.311  ...    18.0      19.0      15.0   1.09
pu        0.724  0.022   0.705    0.781  ...    36.0      14.0      40.0   1.15
mu        0.135  0.030   0.093    0.186  ...     5.0       4.0      22.0   1.51
mus       0.151  0.031   0.108    0.214  ...     8.0       8.0      36.0   1.22
gamma     0.154  0.027   0.109    0.214  ...     6.0       6.0      21.0   1.36
Is_begin  0.879  0.902   0.041    2.933  ...    32.0      39.0      43.0   1.03
Ia_begin  1.844  1.681   0.059    5.442  ...    10.0      10.0      40.0   1.18
E_begin   0.581  0.583   0.022    1.774  ...    15.0       9.0      83.0   1.19

[8 rows x 11 columns]