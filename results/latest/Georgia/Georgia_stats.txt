0 Divergences 
Failed validation 
Computed from 80 by 385 log-likelihood matrix

         Estimate       SE
elpd_loo -2125.81    39.87
p_loo       21.89        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      335   87.0%
 (0.5, 0.7]   (ok)         41   10.6%
   (0.7, 1]   (bad)         9    2.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.257  0.051   0.158    0.330  ...   110.0     105.0      39.0   1.05
pu        0.831  0.028   0.784    0.875  ...    82.0      85.0      83.0   1.06
mu        0.124  0.023   0.089    0.171  ...    24.0      27.0      97.0   1.09
mus       0.182  0.027   0.136    0.228  ...    57.0      57.0      91.0   1.03
gamma     0.233  0.037   0.164    0.292  ...   109.0     100.0      38.0   1.00
Is_begin  2.720  2.760   0.037    7.338  ...   141.0     152.0      49.0   1.00
Ia_begin  5.555  3.971   0.072   12.984  ...    91.0      82.0      59.0   1.01
E_begin   3.047  3.159   0.021    8.341  ...    86.0      59.0      36.0   1.01

[8 rows x 11 columns]