0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1003.91    62.89
p_loo       41.68        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      368   95.8%
 (0.5, 0.7]   (ok)         12    3.1%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.195  0.033   0.151    0.260  ...   107.0     106.0      43.0   1.01
pu        0.713  0.011   0.700    0.737  ...   128.0      97.0      60.0   1.00
mu        0.111  0.019   0.079    0.146  ...    60.0      57.0      60.0   1.01
mus       0.216  0.038   0.160    0.293  ...    67.0      95.0      39.0   1.04
gamma     0.328  0.062   0.225    0.461  ...   152.0     131.0      84.0   0.99
Is_begin  0.657  0.798   0.001    2.040  ...    95.0     152.0      64.0   1.03
Ia_begin  1.090  1.143   0.008    3.591  ...   120.0     122.0      61.0   1.00
E_begin   0.430  0.536   0.005    1.311  ...   112.0      47.0      61.0   1.02

[8 rows x 11 columns]