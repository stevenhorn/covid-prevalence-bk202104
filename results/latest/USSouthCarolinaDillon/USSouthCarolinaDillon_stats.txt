0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1184.24    26.27
p_loo       23.47        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      362   94.3%
 (0.5, 0.7]   (ok)         16    4.2%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.220  0.042   0.153    0.281  ...    57.0      50.0      24.0   1.04
pu        0.724  0.016   0.700    0.749  ...    79.0      76.0      55.0   1.01
mu        0.107  0.020   0.077    0.146  ...    24.0      20.0      20.0   1.09
mus       0.160  0.033   0.106    0.217  ...   152.0     152.0      80.0   1.01
gamma     0.174  0.030   0.110    0.222  ...   149.0     152.0      59.0   1.01
Is_begin  0.712  0.634   0.006    1.847  ...   102.0      69.0      63.0   1.01
Ia_begin  1.393  1.330   0.005    3.898  ...    84.0      59.0      58.0   0.99
E_begin   0.720  0.915   0.003    2.251  ...    46.0      39.0      41.0   1.03

[8 rows x 11 columns]