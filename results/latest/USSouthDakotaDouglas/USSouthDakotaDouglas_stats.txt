0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -580.90    24.57
p_loo       26.11        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      366   95.3%
 (0.5, 0.7]   (ok)         14    3.6%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.267  0.055   0.167    0.343  ...    82.0      87.0      63.0   1.01
pu        0.745  0.026   0.700    0.788  ...    47.0      38.0      22.0   1.10
mu        0.121  0.024   0.083    0.165  ...    58.0      61.0      60.0   1.01
mus       0.179  0.039   0.119    0.252  ...    74.0      95.0      59.0   1.00
gamma     0.198  0.037   0.140    0.275  ...    73.0      88.0      72.0   1.01
Is_begin  0.344  0.366   0.008    0.884  ...    91.0      82.0     100.0   1.00
Ia_begin  0.724  0.856   0.002    2.439  ...    90.0      50.0      38.0   1.00
E_begin   0.287  0.338   0.005    1.003  ...    63.0      55.0      60.0   0.98

[8 rows x 11 columns]