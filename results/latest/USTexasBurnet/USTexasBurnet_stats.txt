0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1565.33    64.24
p_loo       50.47        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      364   94.8%
 (0.5, 0.7]   (ok)         14    3.6%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.254  0.060   0.165    0.348  ...    23.0      33.0      20.0   1.07
pu        0.739  0.025   0.702    0.778  ...    57.0      39.0      60.0   1.05
mu        0.107  0.014   0.083    0.131  ...    13.0      13.0      39.0   1.13
mus       0.220  0.036   0.157    0.283  ...   150.0     146.0      88.0   0.99
gamma     0.454  0.086   0.320    0.629  ...    48.0      43.0      88.0   1.04
Is_begin  0.799  0.894   0.004    2.528  ...    83.0     134.0      40.0   1.06
Ia_begin  1.071  1.302   0.030    3.938  ...    97.0      78.0      57.0   1.02
E_begin   0.527  0.671   0.011    1.883  ...   115.0      89.0      81.0   1.04

[8 rows x 11 columns]