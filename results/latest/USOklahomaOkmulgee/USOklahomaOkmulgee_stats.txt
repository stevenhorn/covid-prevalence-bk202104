0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1181.78    38.10
p_loo       33.43        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      355   92.4%
 (0.5, 0.7]   (ok)         23    6.0%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.254  0.049   0.166    0.325  ...   130.0     130.0      88.0   0.99
pu        0.816  0.017   0.789    0.849  ...    33.0      31.0      18.0   1.06
mu        0.127  0.021   0.086    0.169  ...    56.0      58.0      60.0   0.99
mus       0.170  0.027   0.120    0.222  ...   152.0     152.0      91.0   0.99
gamma     0.201  0.044   0.121    0.267  ...   117.0     132.0      79.0   1.00
Is_begin  0.639  0.644   0.003    1.959  ...   120.0      93.0      24.0   1.00
Ia_begin  1.464  1.612   0.021    5.120  ...   123.0     125.0      99.0   1.00
E_begin   0.622  0.711   0.015    1.665  ...    95.0      83.0     100.0   1.02

[8 rows x 11 columns]