0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1299.56    38.96
p_loo       33.00        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      350   91.1%
 (0.5, 0.7]   (ok)         28    7.3%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.214  0.044   0.150    0.306  ...    92.0      63.0      22.0   1.03
pu        0.726  0.015   0.703    0.753  ...    85.0     125.0      72.0   1.04
mu        0.125  0.022   0.083    0.158  ...    16.0      14.0      60.0   1.11
mus       0.177  0.030   0.126    0.227  ...   114.0     116.0      37.0   1.02
gamma     0.224  0.041   0.156    0.304  ...   110.0     112.0      86.0   0.99
Is_begin  0.970  1.020   0.016    2.776  ...    88.0      65.0      40.0   1.00
Ia_begin  0.694  0.520   0.029    1.549  ...    86.0      62.0      60.0   1.02
E_begin   0.522  0.602   0.004    1.395  ...    99.0      73.0      60.0   1.00

[8 rows x 11 columns]