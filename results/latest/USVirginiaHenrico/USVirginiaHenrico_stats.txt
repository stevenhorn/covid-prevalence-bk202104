23 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1662.88    21.33
p_loo       29.88        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      331   86.2%
 (0.5, 0.7]   (ok)         45   11.7%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.212  0.059   0.151    0.323  ...     7.0       6.0      57.0   1.26
pu        0.766  0.049   0.710    0.876  ...    10.0      12.0      54.0   1.14
mu        0.102  0.018   0.071    0.131  ...     8.0       8.0      15.0   1.46
mus       0.181  0.029   0.124    0.228  ...    87.0      91.0      60.0   1.23
gamma     0.273  0.033   0.220    0.328  ...    57.0      56.0      56.0   1.14
Is_begin  2.100  1.167   0.186    4.283  ...   129.0     152.0      38.0   1.52
Ia_begin  4.443  2.112   0.994    8.631  ...    28.0      26.0      31.0   1.28
E_begin   6.220  4.256   0.290   12.850  ...    50.0      37.0      56.0   1.21

[8 rows x 11 columns]