0 Divergences 
Passed validation 
Computed from 80 by 385 log-likelihood matrix

         Estimate       SE
elpd_loo -1872.14    50.51
p_loo       44.09        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      338   87.8%
 (0.5, 0.7]   (ok)         39   10.1%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    4    1.0%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.239   0.051   0.161    0.329  ...   152.0     152.0      39.0   1.01
pu         0.751   0.040   0.702    0.838  ...    64.0      75.0      81.0   1.01
mu         0.168   0.026   0.120    0.207  ...     3.0       3.0      14.0   2.60
mus        0.243   0.032   0.187    0.289  ...    10.0      12.0      20.0   1.17
gamma      0.353   0.041   0.287    0.426  ...   152.0     152.0      41.0   1.00
Is_begin  21.569  11.724   0.949   42.630  ...    21.0      19.0      20.0   1.10
Ia_begin  57.137  20.497  27.950   87.212  ...    63.0      63.0      57.0   1.00
E_begin   87.816  46.937  14.331  160.764  ...    80.0      47.0      80.0   1.05

[8 rows x 11 columns]