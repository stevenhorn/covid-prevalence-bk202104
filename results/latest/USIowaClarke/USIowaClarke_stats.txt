0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -809.61    24.29
p_loo       24.10        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      362   94.3%
 (0.5, 0.7]   (ok)         20    5.2%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.248  0.057   0.162    0.341  ...    79.0      82.0      58.0   1.01
pu        0.817  0.020   0.778    0.852  ...    10.0      12.0      56.0   1.16
mu        0.109  0.021   0.074    0.149  ...    31.0      24.0      22.0   1.08
mus       0.151  0.032   0.092    0.206  ...    73.0      96.0      45.0   1.05
gamma     0.166  0.033   0.109    0.228  ...    58.0      57.0      93.0   1.01
Is_begin  0.475  0.455   0.001    1.347  ...    80.0      83.0      91.0   0.99
Ia_begin  0.879  1.313   0.000    2.110  ...    88.0     152.0      60.0   1.04
E_begin   0.430  0.569   0.007    1.141  ...    81.0      55.0      58.0   1.02

[8 rows x 11 columns]