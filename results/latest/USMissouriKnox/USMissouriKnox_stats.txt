0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -833.95    81.36
p_loo       33.62        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      366   95.3%
 (0.5, 0.7]   (ok)         14    3.6%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.192  0.028   0.151    0.236  ...   152.0     119.0      20.0   1.02
pu        0.712  0.009   0.702    0.730  ...    80.0      69.0     100.0   0.98
mu        0.082  0.015   0.062    0.115  ...    69.0      78.0      88.0   1.00
mus       0.244  0.031   0.188    0.294  ...    88.0      92.0      60.0   1.03
gamma     0.554  0.076   0.415    0.691  ...    94.0     105.0      53.0   1.18
Is_begin  0.201  0.202   0.000    0.597  ...    60.0      55.0      93.0   1.02
Ia_begin  0.234  0.264   0.002    0.743  ...    52.0      51.0      59.0   1.00
E_begin   0.139  0.157   0.002    0.420  ...    56.0      49.0      91.0   1.01

[8 rows x 11 columns]