0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -413.25    41.13
p_loo       38.71        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      363   94.5%
 (0.5, 0.7]   (ok)         17    4.4%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.196  0.032   0.153    0.260  ...    87.0      85.0      39.0   1.00
pu        0.716  0.013   0.700    0.741  ...   105.0      81.0      60.0   1.00
mu        0.136  0.027   0.093    0.192  ...    86.0      84.0      81.0   1.03
mus       0.256  0.040   0.195    0.329  ...    21.0      22.0      68.0   1.09
gamma     0.277  0.040   0.205    0.341  ...   131.0     121.0      55.0   1.01
Is_begin  0.469  0.453   0.002    1.415  ...    56.0      25.0      59.0   1.06
Ia_begin  0.655  0.677   0.001    2.052  ...    78.0      49.0      22.0   1.02
E_begin   0.257  0.390   0.000    0.695  ...    95.0      12.0      40.0   1.12

[8 rows x 11 columns]