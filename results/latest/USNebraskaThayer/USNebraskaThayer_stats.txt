0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -601.79    31.33
p_loo       28.99        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      355   92.4%
 (0.5, 0.7]   (ok)         26    6.8%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.247  0.057   0.154    0.336  ...   152.0     152.0      65.0   1.04
pu        0.791  0.024   0.742    0.830  ...    54.0      58.0      15.0   1.05
mu        0.132  0.026   0.089    0.178  ...    20.0      16.0      16.0   1.11
mus       0.200  0.036   0.136    0.265  ...   141.0     141.0     100.0   1.05
gamma     0.241  0.050   0.157    0.325  ...   138.0     126.0      88.0   0.99
Is_begin  0.442  0.560   0.002    1.592  ...   112.0      94.0      91.0   1.05
Ia_begin  0.593  0.881   0.008    2.245  ...    99.0     102.0      99.0   0.99
E_begin   0.369  0.636   0.002    1.132  ...    72.0      65.0      49.0   0.99

[8 rows x 11 columns]