0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1402.18    30.42
p_loo       36.18        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      337   87.8%
 (0.5, 0.7]   (ok)         37    9.6%
   (0.7, 1]   (bad)         9    2.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.254  0.055   0.165    0.345  ...    66.0      76.0      38.0   1.05
pu        0.764  0.031   0.709    0.812  ...    27.0      27.0      33.0   1.06
mu        0.123  0.020   0.088    0.156  ...    10.0      11.0      33.0   1.13
mus       0.212  0.035   0.153    0.269  ...   101.0     106.0      93.0   1.04
gamma     0.267  0.046   0.179    0.349  ...    99.0     116.0     100.0   1.00
Is_begin  1.057  1.048   0.018    3.489  ...   146.0     108.0      96.0   0.99
Ia_begin  2.410  2.306   0.049    6.583  ...    83.0      70.0      60.0   1.00
E_begin   1.162  1.176   0.004    3.447  ...    70.0      56.0      36.0   1.05

[8 rows x 11 columns]