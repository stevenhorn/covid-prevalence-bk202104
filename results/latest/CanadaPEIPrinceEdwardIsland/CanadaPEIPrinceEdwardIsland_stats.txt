0 Divergences 
Passed validation 
Computed from 80 by 390 log-likelihood matrix

         Estimate       SE
elpd_loo  -496.47    32.21
p_loo       40.45        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      360   92.3%
 (0.5, 0.7]   (ok)         22    5.6%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.238  0.053   0.154    0.323  ...    81.0      74.0      59.0   1.03
pu        0.735  0.027   0.702    0.783  ...    79.0      58.0      22.0   1.00
mu        0.165  0.034   0.106    0.231  ...    64.0      75.0      60.0   1.05
mus       0.283  0.046   0.204    0.374  ...    19.0      39.0      60.0   1.06
gamma     0.301  0.041   0.218    0.360  ...    62.0      61.0      60.0   1.02
Is_begin  2.777  2.618   0.025    7.233  ...    60.0      59.0      14.0   1.11
Ia_begin  5.876  5.473   0.052   17.630  ...   118.0     152.0      73.0   1.04
E_begin   2.119  1.891   0.002    4.770  ...    68.0      61.0      34.0   1.04

[8 rows x 11 columns]