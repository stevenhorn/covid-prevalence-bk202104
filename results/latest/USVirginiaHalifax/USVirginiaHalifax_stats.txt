0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1103.49    36.02
p_loo       32.11        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      360   93.8%
 (0.5, 0.7]   (ok)         18    4.7%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.255  0.060   0.169    0.346  ...    86.0      93.0     100.0   1.01
pu        0.843  0.023   0.794    0.875  ...    17.0      34.0      36.0   1.07
mu        0.123  0.028   0.075    0.179  ...    34.0      50.0      22.0   1.05
mus       0.176  0.035   0.114    0.235  ...    76.0      81.0     100.0   1.02
gamma     0.202  0.045   0.132    0.285  ...   124.0     146.0      68.0   0.99
Is_begin  0.822  0.768   0.030    2.742  ...   102.0      96.0      91.0   1.01
Ia_begin  1.627  1.965   0.005    6.376  ...    60.0      87.0      38.0   1.00
E_begin   0.774  0.946   0.009    2.627  ...    56.0      82.0      40.0   1.02

[8 rows x 11 columns]