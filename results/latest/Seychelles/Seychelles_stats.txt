0 Divergences 
Failed validation 
Computed from 80 by 385 log-likelihood matrix

         Estimate       SE
elpd_loo -1312.77    49.11
p_loo       41.43        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      354   91.9%
 (0.5, 0.7]   (ok)         21    5.5%
   (0.7, 1]   (bad)         8    2.1%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.273  0.048   0.182    0.348  ...    61.0      62.0      54.0   1.08
pu        0.885  0.012   0.864    0.899  ...    49.0      49.0      59.0   1.04
mu        0.153  0.026   0.110    0.205  ...    44.0      45.0      86.0   1.06
mus       0.184  0.041   0.122    0.261  ...   152.0     142.0     100.0   1.00
gamma     0.242  0.048   0.158    0.334  ...   143.0     139.0      59.0   1.01
Is_begin  0.789  0.718   0.004    2.025  ...   129.0     102.0      60.0   1.01
Ia_begin  1.622  1.513   0.043    3.902  ...   132.0     141.0      49.0   1.06
E_begin   0.795  0.717   0.033    2.029  ...    55.0      66.0      86.0   1.10

[8 rows x 11 columns]