0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -890.97    30.93
p_loo       30.22        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      355   92.4%
 (0.5, 0.7]   (ok)         25    6.5%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.256  0.049   0.171    0.343  ...    71.0      69.0      56.0   1.00
pu        0.848  0.019   0.805    0.876  ...    22.0      18.0      20.0   1.11
mu        0.150  0.025   0.114    0.187  ...    51.0      47.0      96.0   1.02
mus       0.179  0.035   0.129    0.244  ...   152.0     152.0     100.0   1.00
gamma     0.235  0.041   0.163    0.303  ...   116.0      96.0      93.0   1.03
Is_begin  1.033  1.027   0.017    3.009  ...    62.0      34.0      16.0   1.02
Ia_begin  2.582  2.158   0.023    6.566  ...   108.0     152.0      88.0   1.03
E_begin   1.556  1.660   0.040    5.345  ...   111.0      45.0      55.0   1.04

[8 rows x 11 columns]