0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1185.81    28.65
p_loo       11.46        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      361   94.0%
 (0.5, 0.7]   (ok)         17    4.4%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.172  0.015   0.153    0.195  ...     3.0       3.0      15.0   1.99
pu        0.820  0.020   0.790    0.855  ...     3.0       3.0      14.0   2.59
mu        0.240  0.015   0.211    0.257  ...     3.0       3.0      14.0   2.41
mus       0.143  0.006   0.130    0.152  ...    10.0      11.0      40.0   1.16
gamma     0.120  0.009   0.108    0.132  ...     3.0       3.0      22.0   2.05
Is_begin  1.874  0.806   0.949    3.447  ...     3.0       4.0      18.0   1.82
Ia_begin  4.353  1.919   1.892    7.695  ...     3.0       3.0      17.0   2.47
E_begin   1.072  1.063   0.209    3.508  ...     3.0       3.0      14.0   2.15

[8 rows x 11 columns]