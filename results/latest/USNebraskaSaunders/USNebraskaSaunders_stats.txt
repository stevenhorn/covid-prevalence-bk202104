0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1045.78    39.72
p_loo       34.87        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      356   92.7%
 (0.5, 0.7]   (ok)         20    5.2%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.219  0.033   0.162    0.273  ...   152.0     135.0      91.0   1.01
pu        0.726  0.018   0.700    0.762  ...   111.0      98.0      31.0   0.99
mu        0.121  0.020   0.088    0.152  ...    24.0      26.0      60.0   1.03
mus       0.184  0.033   0.133    0.255  ...   152.0     152.0      88.0   0.98
gamma     0.235  0.042   0.177    0.346  ...    76.0     124.0      57.0   0.98
Is_begin  0.579  0.617   0.006    1.770  ...    77.0      97.0      40.0   1.01
Ia_begin  0.989  0.998   0.004    3.022  ...    28.0      23.0      45.0   1.09
E_begin   0.444  0.466   0.002    1.504  ...   107.0      33.0      87.0   1.05

[8 rows x 11 columns]