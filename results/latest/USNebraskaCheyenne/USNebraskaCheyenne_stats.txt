0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -804.19    42.31
p_loo       37.46        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      350   91.1%
 (0.5, 0.7]   (ok)         27    7.0%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.243  0.046   0.154    0.314  ...    58.0      53.0      88.0   1.05
pu        0.756  0.020   0.725    0.797  ...    64.0      61.0      93.0   1.03
mu        0.117  0.022   0.082    0.150  ...    28.0      21.0      36.0   1.07
mus       0.179  0.049   0.110    0.260  ...    41.0      57.0      40.0   1.04
gamma     0.213  0.043   0.145    0.304  ...   152.0     138.0      60.0   0.99
Is_begin  0.833  0.887   0.015    2.570  ...   108.0      99.0      65.0   1.03
Ia_begin  1.342  1.317   0.005    3.882  ...    84.0      83.0      59.0   1.00
E_begin   0.621  0.765   0.001    2.333  ...   110.0      75.0      60.0   1.01

[8 rows x 11 columns]