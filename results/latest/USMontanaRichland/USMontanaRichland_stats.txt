0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1054.74    51.88
p_loo       40.52        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      351   91.4%
 (0.5, 0.7]   (ok)         23    6.0%
   (0.7, 1]   (bad)         8    2.1%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.244  0.051   0.161    0.323  ...    47.0      70.0      65.0   1.03
pu        0.783  0.028   0.721    0.821  ...    17.0      18.0      20.0   1.11
mu        0.132  0.025   0.092    0.181  ...    47.0      45.0      81.0   1.03
mus       0.187  0.032   0.130    0.229  ...    74.0      63.0      74.0   1.04
gamma     0.241  0.042   0.168    0.311  ...    68.0      70.0      55.0   1.08
Is_begin  0.514  0.560   0.021    1.378  ...    66.0      91.0      44.0   1.00
Ia_begin  1.028  1.383   0.002    3.672  ...   104.0     152.0      60.0   1.02
E_begin   0.405  0.439   0.006    1.333  ...   108.0      48.0      76.0   1.03

[8 rows x 11 columns]