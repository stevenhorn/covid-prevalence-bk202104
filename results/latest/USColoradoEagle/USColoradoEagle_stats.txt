0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1268.31    26.38
p_loo       29.03        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      339   88.3%
 (0.5, 0.7]   (ok)         35    9.1%
   (0.7, 1]   (bad)         8    2.1%
   (1, Inf)   (very bad)    2    0.5%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.246   0.051   0.172    0.335  ...    97.0     115.0      60.0   1.02
pu         0.744   0.024   0.704    0.786  ...    24.0      22.0      30.0   1.09
mu         0.163   0.026   0.118    0.205  ...    30.0      33.0      38.0   1.02
mus        0.217   0.036   0.160    0.274  ...    60.0      75.0      19.0   1.02
gamma      0.309   0.053   0.236    0.421  ...   152.0     152.0     100.0   0.99
Is_begin  18.766   9.724   2.743   33.990  ...    87.0      69.0      48.0   1.03
Ia_begin  44.093  23.167   4.436   86.792  ...    45.0      46.0      59.0   1.05
E_begin   50.512  33.176   3.447  107.887  ...    49.0      26.0      16.0   1.06

[8 rows x 11 columns]