0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -525.13    41.70
p_loo       33.06        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      354   92.2%
 (0.5, 0.7]   (ok)         24    6.2%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.255  0.052   0.177    0.349  ...   119.0      99.0      53.0   1.03
pu        0.784  0.022   0.745    0.819  ...    11.0      12.0      18.0   1.13
mu        0.115  0.023   0.069    0.158  ...    77.0      76.0      91.0   0.99
mus       0.166  0.037   0.114    0.238  ...   152.0     152.0      83.0   0.99
gamma     0.190  0.044   0.115    0.263  ...   152.0     152.0      96.0   1.04
Is_begin  0.255  0.392   0.005    0.949  ...    87.0      67.0      40.0   1.02
Ia_begin  0.457  0.743   0.003    2.046  ...    69.0      66.0      60.0   0.99
E_begin   0.198  0.275   0.001    0.630  ...    76.0      56.0      93.0   1.00

[8 rows x 11 columns]