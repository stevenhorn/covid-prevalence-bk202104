0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -523.46    70.83
p_loo       52.35        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      360   93.8%
 (0.5, 0.7]   (ok)         19    4.9%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.242  0.053   0.159    0.337  ...    28.0      38.0      54.0   1.09
pu        0.750  0.035   0.705    0.816  ...     8.0       8.0      59.0   1.27
mu        0.128  0.023   0.094    0.170  ...   110.0     107.0      60.0   1.02
mus       0.214  0.045   0.153    0.309  ...    63.0      74.0      49.0   1.00
gamma     0.242  0.039   0.165    0.314  ...   109.0     126.0      65.0   1.00
Is_begin  0.439  0.385   0.006    1.196  ...    95.0      73.0      59.0   1.04
Ia_begin  0.725  0.923   0.006    2.465  ...    78.0      81.0      91.0   1.01
E_begin   0.217  0.190   0.002    0.537  ...    67.0      62.0      59.0   1.01

[8 rows x 11 columns]