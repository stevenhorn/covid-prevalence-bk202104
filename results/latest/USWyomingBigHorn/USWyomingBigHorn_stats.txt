0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -819.58    26.51
p_loo       19.77        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      353   91.9%
 (0.5, 0.7]   (ok)         28    7.3%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.257  0.052   0.169    0.333  ...   101.0     108.0      93.0   1.00
pu        0.834  0.019   0.802    0.871  ...    17.0      17.0      61.0   1.11
mu        0.120  0.023   0.081    0.161  ...    28.0      37.0      55.0   1.05
mus       0.163  0.028   0.112    0.215  ...   114.0     140.0      59.0   1.00
gamma     0.190  0.033   0.127    0.253  ...    75.0      71.0      58.0   1.02
Is_begin  0.488  0.516   0.003    1.827  ...    65.0      55.0      75.0   1.01
Ia_begin  0.853  0.923   0.015    2.500  ...   102.0     101.0      84.0   0.99
E_begin   0.446  0.546   0.001    1.445  ...   113.0      96.0      96.0   0.98

[8 rows x 11 columns]