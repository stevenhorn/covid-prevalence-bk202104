0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -2163.91    24.58
p_loo       20.17        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      358   93.2%
 (0.5, 0.7]   (ok)         20    5.2%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.271  0.058   0.169    0.349  ...    25.0      29.0      50.0   1.06
pu        0.777  0.024   0.739    0.825  ...    58.0      55.0      40.0   1.04
mu        0.123  0.026   0.087    0.178  ...    13.0      19.0      59.0   1.07
mus       0.147  0.028   0.100    0.199  ...    32.0      30.0      43.0   1.05
gamma     0.177  0.031   0.117    0.229  ...    62.0      63.0      88.0   1.01
Is_begin  1.309  1.046   0.113    3.240  ...    18.0      24.0      59.0   1.07
Ia_begin  2.785  2.100   0.014    6.409  ...    30.0      18.0      18.0   1.09
E_begin   2.072  2.110   0.105    5.997  ...    44.0      23.0      60.0   1.06

[8 rows x 11 columns]