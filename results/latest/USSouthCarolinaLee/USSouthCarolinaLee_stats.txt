0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1035.67    30.26
p_loo       27.41        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      359   93.5%
 (0.5, 0.7]   (ok)         20    5.2%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.271  0.055   0.178    0.348  ...    49.0      69.0      59.0   1.01
pu        0.804  0.021   0.768    0.840  ...    55.0      58.0      93.0   1.08
mu        0.133  0.023   0.095    0.171  ...     8.0       8.0      59.0   1.24
mus       0.172  0.035   0.120    0.253  ...   109.0      90.0      62.0   1.01
gamma     0.189  0.033   0.136    0.244  ...    42.0      58.0      96.0   1.06
Is_begin  1.004  0.811   0.058    2.587  ...   152.0      56.0      80.0   1.04
Ia_begin  2.518  2.265   0.027    6.332  ...   128.0     112.0      46.0   1.00
E_begin   1.210  1.121   0.010    3.528  ...    59.0      52.0      61.0   1.02

[8 rows x 11 columns]