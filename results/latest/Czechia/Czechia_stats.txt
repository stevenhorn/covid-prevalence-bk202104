3 Divergences 
Passed validation 
Computed from 80 by 385 log-likelihood matrix

         Estimate       SE
elpd_loo -2914.08    36.60
p_loo       25.68        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      333   86.5%
 (0.5, 0.7]   (ok)         39   10.1%
   (0.7, 1]   (bad)        12    3.1%
   (1, Inf)   (very bad)    1    0.3%

             mean       sd   hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.179    0.023    0.150    0.215  ...    61.0      56.0      40.0   0.98
pu          0.710    0.008    0.700    0.725  ...    40.0      21.0      40.0   1.08
mu          0.136    0.022    0.097    0.171  ...     6.0       6.0      39.0   1.34
mus         0.205    0.029    0.158    0.259  ...    61.0      61.0      60.0   1.00
gamma       0.294    0.040    0.222    0.368  ...    85.0      85.0      86.0   0.99
Is_begin  159.285  128.334    6.126  386.664  ...    61.0      51.0      60.0   1.02
Ia_begin  507.712  242.825  163.446  944.740  ...    55.0      60.0      60.0   1.01
E_begin   348.459  250.364   16.370  805.659  ...    39.0      47.0      47.0   1.04

[8 rows x 11 columns]