0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -315.67    36.40
p_loo       30.55        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      359   93.5%
 (0.5, 0.7]   (ok)         18    4.7%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.271  0.051   0.187    0.342  ...   152.0     152.0      86.0   0.99
pu        0.877  0.019   0.840    0.900  ...    22.0      26.0      27.0   1.04
mu        0.127  0.026   0.082    0.167  ...    43.0      41.0      60.0   1.07
mus       0.181  0.037   0.122    0.247  ...    61.0      78.0     100.0   1.02
gamma     0.207  0.041   0.121    0.281  ...    83.0      89.0      60.0   0.98
Is_begin  0.480  0.515   0.003    1.369  ...    85.0      84.0      62.0   1.00
Ia_begin  0.831  1.052   0.011    3.078  ...    73.0      60.0      60.0   0.99
E_begin   0.359  0.546   0.003    1.127  ...    53.0      22.0      81.0   1.12

[8 rows x 11 columns]