0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1136.58    30.68
p_loo       27.93        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      342   89.1%
 (0.5, 0.7]   (ok)         35    9.1%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.227  0.043   0.152    0.298  ...    76.0      64.0      37.0   1.01
pu        0.723  0.015   0.701    0.748  ...    30.0      26.0      49.0   1.06
mu        0.141  0.027   0.097    0.194  ...    21.0      19.0      32.0   1.08
mus       0.205  0.041   0.129    0.290  ...    76.0      85.0      84.0   1.01
gamma     0.240  0.046   0.171    0.323  ...   141.0     122.0      54.0   1.00
Is_begin  0.434  0.488   0.009    1.362  ...    86.0     102.0      16.0   1.00
Ia_begin  0.612  0.534   0.008    1.541  ...    98.0      74.0      38.0   1.02
E_begin   0.347  0.390   0.005    1.102  ...   109.0      89.0      49.0   1.00

[8 rows x 11 columns]