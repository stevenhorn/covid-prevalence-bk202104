0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -580.09    40.07
p_loo       40.54        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      350   91.1%
 (0.5, 0.7]   (ok)         26    6.8%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.254  0.054   0.152    0.332  ...    80.0      73.0      57.0   1.04
pu        0.800  0.020   0.763    0.827  ...    41.0      67.0      57.0   1.05
mu        0.146  0.027   0.101    0.191  ...    87.0      81.0     100.0   1.00
mus       0.232  0.033   0.183    0.304  ...   133.0     152.0      56.0   1.00
gamma     0.313  0.048   0.242    0.431  ...   152.0     152.0     100.0   1.00
Is_begin  1.156  1.075   0.016    3.479  ...    83.0      88.0      60.0   1.00
Ia_begin  2.470  1.765   0.225    6.494  ...    60.0      55.0      58.0   1.02
E_begin   1.162  1.109   0.029    3.503  ...    78.0      62.0      48.0   1.03

[8 rows x 11 columns]