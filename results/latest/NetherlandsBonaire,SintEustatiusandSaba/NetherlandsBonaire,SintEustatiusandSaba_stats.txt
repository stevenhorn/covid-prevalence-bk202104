0 Divergences 
Passed validation 
Computed from 80 by 385 log-likelihood matrix

         Estimate       SE
elpd_loo  -509.41    42.31
p_loo       40.25        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      364   94.5%
 (0.5, 0.7]   (ok)         17    4.4%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.288  0.042   0.217    0.348  ...   137.0     132.0      38.0   0.99
pu        0.885  0.014   0.866    0.899  ...    29.0      34.0      74.0   1.06
mu        0.208  0.025   0.153    0.245  ...    52.0      51.0      49.0   1.02
mus       0.246  0.039   0.177    0.322  ...   107.0     152.0      76.0   1.06
gamma     0.380  0.042   0.297    0.445  ...    79.0      77.0     100.0   0.99
Is_begin  1.153  0.895   0.009    2.928  ...    94.0      74.0      43.0   0.99
Ia_begin  2.191  1.891   0.049    5.509  ...    98.0      98.0      60.0   0.99
E_begin   1.055  0.976   0.002    3.247  ...    35.0      83.0      35.0   1.00

[8 rows x 11 columns]