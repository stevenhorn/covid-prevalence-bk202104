0 Divergences 
Passed validation 
Computed from 80 by 385 log-likelihood matrix

         Estimate       SE
elpd_loo -2910.85    42.71
p_loo       31.24        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      361   93.8%
 (0.5, 0.7]   (ok)         17    4.4%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    0    0.0%

             mean       sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.244    0.058   0.152    0.335  ...    82.0      63.0      40.0   1.03
pu          0.805    0.052   0.704    0.878  ...    58.0      56.0      60.0   1.03
mu          0.146    0.033   0.097    0.198  ...     3.0       3.0      49.0   2.21
mus         0.196    0.031   0.139    0.254  ...    18.0      24.0      59.0   1.09
gamma       0.223    0.037   0.148    0.283  ...    25.0      33.0      95.0   1.07
Is_begin  168.146  135.604   6.432  445.945  ...    42.0      19.0      40.0   1.13
Ia_begin  447.407  225.999  56.301  842.584  ...    43.0      40.0      25.0   1.06
E_begin   379.566  399.230   2.162  891.947  ...    12.0      10.0      33.0   1.19

[8 rows x 11 columns]