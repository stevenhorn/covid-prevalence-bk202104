0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1125.51    28.04
p_loo       28.27        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      361   94.0%
 (0.5, 0.7]   (ok)         19    4.9%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.256  0.060   0.157    0.349  ...    54.0      49.0      29.0   0.99
pu        0.819  0.019   0.783    0.850  ...    19.0      19.0      36.0   1.10
mu        0.153  0.029   0.109    0.219  ...     6.0       7.0      18.0   1.30
mus       0.199  0.042   0.139    0.296  ...    21.0      20.0      37.0   1.13
gamma     0.238  0.044   0.169    0.326  ...   100.0      98.0      60.0   1.01
Is_begin  1.049  1.022   0.004    2.837  ...   108.0      67.0      38.0   1.01
Ia_begin  2.294  2.217   0.048    6.345  ...    28.0      10.0      25.0   1.19
E_begin   1.248  1.574   0.026    4.911  ...    87.0      68.0      93.0   1.04

[8 rows x 11 columns]