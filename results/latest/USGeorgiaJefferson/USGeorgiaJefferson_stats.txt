0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -967.53    41.86
p_loo       30.93        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      349   90.9%
 (0.5, 0.7]   (ok)         32    8.3%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.240  0.057   0.163    0.343  ...    61.0      59.0      40.0   1.01
pu        0.793  0.020   0.757    0.823  ...    60.0      62.0      36.0   1.02
mu        0.129  0.022   0.094    0.169  ...    28.0      26.0      60.0   1.01
mus       0.168  0.026   0.113    0.208  ...    17.0      16.0      40.0   1.09
gamma     0.216  0.052   0.120    0.300  ...   120.0     130.0     100.0   1.00
Is_begin  0.666  0.773   0.001    2.268  ...    69.0      27.0      22.0   1.06
Ia_begin  1.540  1.597   0.019    4.327  ...    30.0     127.0      19.0   1.08
E_begin   0.699  0.945   0.005    2.212  ...    63.0      41.0      44.0   1.03

[8 rows x 11 columns]