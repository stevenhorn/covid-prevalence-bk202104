3 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1950.79    29.07
p_loo       33.27        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      344   89.6%
 (0.5, 0.7]   (ok)         28    7.3%
   (0.7, 1]   (bad)        11    2.9%
   (1, Inf)   (very bad)    1    0.3%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.229   0.051   0.153    0.320  ...   119.0     111.0      86.0   1.01
pu         0.748   0.038   0.700    0.817  ...    41.0      41.0      36.0   1.03
mu         0.112   0.024   0.068    0.158  ...    11.0      14.0      40.0   1.13
mus        0.200   0.039   0.139    0.268  ...    22.0      29.0      40.0   1.06
gamma      0.321   0.045   0.236    0.391  ...   152.0     152.0      87.0   1.01
Is_begin   4.743   2.550   0.531    9.743  ...   113.0     112.0      93.0   1.01
Ia_begin  13.428   6.813   2.285   26.912  ...   121.0     107.0      59.0   0.99
E_begin   21.142  11.420   1.310   37.093  ...    91.0      80.0      35.0   1.05

[8 rows x 11 columns]