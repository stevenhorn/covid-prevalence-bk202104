0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1270.68    66.28
p_loo       39.62        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      369   96.1%
 (0.5, 0.7]   (ok)         10    2.6%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.177  0.027   0.150    0.223  ...   147.0     152.0      36.0   0.99
pu        0.708  0.007   0.700    0.719  ...   114.0      82.0      60.0   1.00
mu        0.097  0.015   0.071    0.123  ...    18.0      20.0      59.0   1.10
mus       0.200  0.036   0.131    0.265  ...    33.0      29.0      37.0   1.06
gamma     0.401  0.061   0.293    0.505  ...    86.0      94.0      79.0   1.01
Is_begin  0.571  0.465   0.020    1.417  ...   152.0     131.0      97.0   1.00
Ia_begin  0.856  0.826   0.050    2.510  ...    84.0      73.0      96.0   1.01
E_begin   0.400  0.485   0.003    1.501  ...    94.0     100.0      86.0   1.02

[8 rows x 11 columns]