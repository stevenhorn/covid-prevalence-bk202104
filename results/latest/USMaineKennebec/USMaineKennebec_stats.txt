0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1161.82    46.35
p_loo       39.93        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      358   93.2%
 (0.5, 0.7]   (ok)         20    5.2%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.265  0.058   0.176    0.349  ...   136.0     130.0      59.0   1.07
pu        0.875  0.029   0.829    0.900  ...    20.0      23.0      59.0   1.07
mu        0.159  0.029   0.095    0.201  ...    10.0      10.0      24.0   1.16
mus       0.217  0.034   0.170    0.293  ...    26.0      52.0      61.0   1.08
gamma     0.316  0.050   0.234    0.418  ...    68.0      63.0      77.0   1.04
Is_begin  0.901  0.778   0.006    2.261  ...   151.0      82.0      57.0   1.04
Ia_begin  1.408  1.253   0.056    3.608  ...   102.0      59.0      38.0   1.02
E_begin   1.344  1.190   0.017    3.604  ...    72.0      25.0      91.0   1.06

[8 rows x 11 columns]