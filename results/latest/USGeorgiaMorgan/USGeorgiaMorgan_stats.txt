0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1127.77    68.45
p_loo       45.05        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      369   96.1%
 (0.5, 0.7]   (ok)         13    3.4%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.240  0.041   0.180    0.324  ...    48.0      49.0      59.0   1.10
pu        0.732  0.021   0.701    0.765  ...    49.0      47.0      59.0   1.04
mu        0.157  0.019   0.122    0.188  ...    10.0       9.0      36.0   1.23
mus       0.283  0.040   0.204    0.356  ...    91.0      89.0      60.0   1.02
gamma     0.439  0.076   0.310    0.572  ...   114.0     111.0      97.0   1.00
Is_begin  0.950  0.901   0.037    2.796  ...    91.0      73.0      76.0   1.02
Ia_begin  1.598  1.497   0.041    4.275  ...    63.0      39.0      22.0   1.02
E_begin   0.560  0.566   0.004    1.500  ...    44.0      29.0      60.0   1.02

[8 rows x 11 columns]