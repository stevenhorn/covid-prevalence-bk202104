0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1054.15    33.14
p_loo       23.90        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      359   93.5%
 (0.5, 0.7]   (ok)         19    4.9%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.248  0.047   0.166    0.329  ...    19.0      16.0      32.0   1.10
pu        0.815  0.024   0.761    0.855  ...     6.0       5.0      40.0   1.43
mu        0.109  0.016   0.084    0.138  ...    38.0      34.0      38.0   1.06
mus       0.147  0.024   0.099    0.195  ...   134.0     152.0      60.0   1.08
gamma     0.163  0.031   0.109    0.218  ...   102.0      80.0      66.0   1.02
Is_begin  0.802  0.723   0.015    2.143  ...    48.0      32.0      62.0   1.06
Ia_begin  1.682  1.836   0.034    4.463  ...   107.0      76.0     100.0   1.01
E_begin   0.729  0.825   0.003    2.068  ...    74.0      66.0      60.0   1.02

[8 rows x 11 columns]