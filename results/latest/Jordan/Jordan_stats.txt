1 Divergences 
Passed validation 
Computed from 80 by 385 log-likelihood matrix

         Estimate       SE
elpd_loo -2248.66    42.42
p_loo       27.46        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      344   89.4%
 (0.5, 0.7]   (ok)         36    9.4%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    1    0.3%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.273   0.051   0.181    0.348  ...    28.0      27.0      15.0   1.06
pu         0.853   0.017   0.813    0.875  ...    39.0      44.0      40.0   1.00
mu         0.154   0.021   0.123    0.192  ...    14.0      16.0      22.0   1.08
mus        0.176   0.031   0.129    0.239  ...    84.0      79.0      60.0   0.99
gamma      0.230   0.041   0.152    0.309  ...    68.0      74.0      59.0   1.05
Is_begin  19.399  14.242   0.120   42.471  ...    97.0      77.0      38.0   1.09
Ia_begin  37.868  24.591   2.325   78.976  ...    93.0      81.0      57.0   0.99
E_begin   21.665  18.194   0.411   52.072  ...    77.0      41.0      53.0   1.05

[8 rows x 11 columns]