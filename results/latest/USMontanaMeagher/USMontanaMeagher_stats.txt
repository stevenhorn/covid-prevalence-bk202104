0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -432.68    57.29
p_loo       46.60        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      363   94.5%
 (0.5, 0.7]   (ok)         16    4.2%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.267  0.057   0.164    0.348  ...    55.0      48.0      22.0   1.03
pu        0.835  0.024   0.789    0.870  ...    20.0      26.0      33.0   1.05
mu        0.142  0.024   0.101    0.186  ...    46.0      45.0      60.0   1.01
mus       0.205  0.037   0.146    0.274  ...   128.0     144.0     100.0   1.00
gamma     0.257  0.052   0.203    0.403  ...   122.0     105.0      59.0   1.06
Is_begin  0.798  0.692   0.018    2.478  ...    82.0      73.0      17.0   1.03
Ia_begin  1.424  1.770   0.007    4.373  ...   111.0      71.0      91.0   1.03
E_begin   0.676  0.747   0.003    1.936  ...   103.0      43.0      59.0   1.05

[8 rows x 11 columns]