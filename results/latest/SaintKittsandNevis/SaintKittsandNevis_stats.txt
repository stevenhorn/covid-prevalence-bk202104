1 Divergences 
Passed validation 
Computed from 80 by 385 log-likelihood matrix

         Estimate       SE
elpd_loo  -198.95    52.29
p_loo       74.79        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      352   91.4%
 (0.5, 0.7]   (ok)         26    6.8%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.252  0.054   0.170    0.350  ...    90.0     121.0      76.0   1.06
pu        0.828  0.034   0.747    0.872  ...    15.0      15.0      25.0   1.11
mu        0.156  0.030   0.113    0.209  ...    25.0      25.0      60.0   1.08
mus       0.236  0.042   0.182    0.319  ...    73.0      81.0      96.0   1.00
gamma     0.283  0.042   0.207    0.360  ...    68.0      84.0      72.0   1.08
Is_begin  1.248  0.914   0.044    3.055  ...   125.0      76.0      43.0   1.02
Ia_begin  3.411  2.371   0.308    7.008  ...    82.0      70.0      56.0   1.03
E_begin   1.576  1.505   0.076    4.369  ...    74.0      50.0      77.0   1.01

[8 rows x 11 columns]