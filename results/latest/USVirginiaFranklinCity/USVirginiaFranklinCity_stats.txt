0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -861.54    27.21
p_loo       25.88        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      355   92.4%
 (0.5, 0.7]   (ok)         23    6.0%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.255  0.050   0.172    0.342  ...    83.0      70.0      59.0   1.02
pu        0.771  0.020   0.735    0.805  ...    55.0      54.0      43.0   1.04
mu        0.136  0.025   0.090    0.181  ...    10.0      10.0      15.0   1.16
mus       0.166  0.038   0.101    0.232  ...   152.0     152.0      69.0   1.02
gamma     0.176  0.032   0.115    0.235  ...    79.0      79.0      40.0   1.06
Is_begin  0.721  0.715   0.010    1.943  ...   121.0      83.0      95.0   1.01
Ia_begin  1.459  1.293   0.003    3.681  ...   113.0      77.0      60.0   1.00
E_begin   0.634  0.644   0.002    2.120  ...   102.0      39.0      22.0   1.04

[8 rows x 11 columns]