0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -284.52    34.89
p_loo       52.70        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      349   90.9%
 (0.5, 0.7]   (ok)         22    5.7%
   (0.7, 1]   (bad)        11    2.9%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.244  0.043   0.165    0.305  ...   149.0     152.0      86.0   1.01
pu        0.779  0.049   0.701    0.860  ...     6.0       7.0      46.0   1.25
mu        0.173  0.032   0.108    0.219  ...    22.0      30.0      80.0   1.06
mus       0.270  0.060   0.163    0.365  ...    17.0      16.0      60.0   1.12
gamma     0.307  0.043   0.233    0.392  ...    42.0      45.0      61.0   1.04
Is_begin  0.586  0.657   0.004    1.897  ...    63.0      44.0      30.0   1.02
Ia_begin  0.973  0.965   0.014    3.047  ...    62.0      46.0      19.0   1.05
E_begin   0.444  0.509   0.004    1.157  ...    79.0      50.0      58.0   1.01

[8 rows x 11 columns]