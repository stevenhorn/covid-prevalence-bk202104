0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1074.50    24.44
p_loo       20.00        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      345   89.8%
 (0.5, 0.7]   (ok)         29    7.6%
   (0.7, 1]   (bad)        10    2.6%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.275  0.044   0.200    0.348  ...   152.0     151.0      87.0   1.01
pu        0.884  0.013   0.865    0.900  ...    66.0      30.0      43.0   1.08
mu        0.136  0.025   0.094    0.178  ...    20.0      20.0      43.0   1.07
mus       0.166  0.033   0.109    0.217  ...   125.0     152.0      49.0   1.05
gamma     0.198  0.040   0.138    0.281  ...   113.0     102.0      20.0   1.00
Is_begin  1.262  0.944   0.043    3.068  ...   152.0      97.0      58.0   1.01
Ia_begin  0.675  0.547   0.023    1.687  ...   133.0     105.0      44.0   0.99
E_begin   0.809  0.847   0.020    2.495  ...   101.0      98.0      60.0   1.01

[8 rows x 11 columns]