0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -905.57    31.24
p_loo       31.33        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      358   93.2%
 (0.5, 0.7]   (ok)         18    4.7%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.253  0.049   0.170    0.334  ...    37.0      61.0      60.0   1.06
pu        0.806  0.018   0.772    0.834  ...    15.0      18.0      56.0   1.08
mu        0.141  0.023   0.105    0.186  ...    16.0      13.0      38.0   1.14
mus       0.176  0.029   0.122    0.227  ...    77.0      74.0      93.0   1.03
gamma     0.198  0.035   0.137    0.257  ...    96.0     102.0      65.0   1.00
Is_begin  0.815  0.750   0.005    2.387  ...   129.0     123.0      80.0   0.99
Ia_begin  1.587  1.700   0.034    4.795  ...   113.0     108.0      64.0   1.05
E_begin   0.826  1.118   0.006    3.345  ...   123.0     107.0      37.0   1.02

[8 rows x 11 columns]