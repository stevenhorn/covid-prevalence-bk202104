0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -2408.71    29.73
p_loo       30.17        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      356   92.7%
 (0.5, 0.7]   (ok)         20    5.2%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    3    0.8%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.237   0.051   0.159    0.333  ...   108.0     108.0      57.0   0.98
pu         0.735   0.030   0.700    0.795  ...    67.0      46.0      40.0   1.02
mu         0.113   0.019   0.083    0.146  ...    34.0      34.0      58.0   1.02
mus        0.164   0.026   0.120    0.205  ...    97.0     110.0      74.0   1.05
gamma      0.261   0.042   0.201    0.338  ...   102.0     118.0      95.0   1.04
Is_begin   7.210   4.048   0.400   14.149  ...    92.0      86.0      97.0   1.03
Ia_begin  19.406   9.801   2.868   36.201  ...   138.0     114.0      54.0   1.09
E_begin   18.600  14.302   0.218   40.912  ...    77.0      63.0      30.0   1.01

[8 rows x 11 columns]