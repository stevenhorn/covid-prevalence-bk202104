7 Divergences 
Passed validation 
Computed from 80 by 385 log-likelihood matrix

         Estimate       SE
elpd_loo -1415.48    39.02
p_loo       39.78        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      359   93.2%
 (0.5, 0.7]   (ok)         14    3.6%
   (0.7, 1]   (bad)        10    2.6%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.256  0.054   0.167    0.342  ...    78.0     103.0      80.0   1.03
pu        0.874  0.027   0.805    0.899  ...    93.0      99.0      24.0   1.03
mu        0.156  0.040   0.097    0.240  ...     4.0       4.0      15.0   1.71
mus       0.166  0.029   0.115    0.219  ...   107.0     106.0      32.0   1.01
gamma     0.200  0.041   0.140    0.273  ...    83.0      93.0      80.0   1.02
Is_begin  0.827  0.842   0.020    2.419  ...    66.0      88.0      49.0   1.02
Ia_begin  1.938  1.656   0.003    5.406  ...    70.0      60.0      60.0   1.05
E_begin   0.921  1.051   0.003    3.106  ...    74.0      63.0      58.0   1.00

[8 rows x 11 columns]