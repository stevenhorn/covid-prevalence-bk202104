0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -346.53    55.32
p_loo       37.30        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      358   93.2%
 (0.5, 0.7]   (ok)         21    5.5%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.292  0.048   0.201    0.345  ...    43.0      44.0      69.0   1.01
pu        0.888  0.010   0.870    0.900  ...    67.0      70.0      50.0   1.00
mu        0.169  0.023   0.135    0.210  ...    90.0      85.0     100.0   0.99
mus       0.254  0.041   0.184    0.329  ...    80.0      73.0      42.0   1.05
gamma     0.361  0.049   0.277    0.464  ...   152.0     152.0      91.0   0.99
Is_begin  0.843  0.693   0.043    2.182  ...    73.0      45.0      37.0   1.02
Ia_begin  1.543  1.369   0.029    3.804  ...    79.0      63.0      26.0   1.06
E_begin   0.693  0.637   0.008    1.912  ...    59.0      58.0      58.0   0.99

[8 rows x 11 columns]