0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1291.71    29.00
p_loo       22.94        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      337   87.8%
 (0.5, 0.7]   (ok)         40   10.4%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.257  0.058   0.179    0.346  ...    33.0      37.0      57.0   1.03
pu        0.789  0.035   0.716    0.844  ...     5.0       7.0      15.0   1.23
mu        0.136  0.024   0.103    0.197  ...    16.0      16.0      16.0   1.08
mus       0.188  0.031   0.128    0.247  ...    72.0      75.0      57.0   1.02
gamma     0.225  0.036   0.178    0.291  ...   152.0     151.0      87.0   1.02
Is_begin  0.716  0.579   0.032    1.783  ...    43.0      16.0      18.0   1.09
Ia_begin  0.512  0.497   0.003    1.535  ...    62.0      19.0      43.0   1.09
E_begin   0.408  0.453   0.002    1.431  ...    16.0      10.0      22.0   1.16

[8 rows x 11 columns]