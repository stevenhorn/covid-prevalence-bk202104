0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -856.44    44.19
p_loo       33.06        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      362   94.3%
 (0.5, 0.7]   (ok)         17    4.4%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.262  0.055   0.168    0.340  ...    28.0      29.0      97.0   1.04
pu        0.788  0.024   0.737    0.825  ...    30.0      31.0      58.0   1.06
mu        0.140  0.026   0.096    0.185  ...    38.0      44.0      58.0   1.04
mus       0.208  0.033   0.141    0.264  ...   152.0     152.0      76.0   1.01
gamma     0.267  0.035   0.201    0.321  ...   129.0     110.0      69.0   1.01
Is_begin  0.751  0.595   0.016    1.844  ...    59.0      62.0      37.0   1.04
Ia_begin  1.495  1.425   0.002    3.976  ...    99.0      82.0      96.0   0.99
E_begin   0.569  0.800   0.003    1.562  ...    96.0      72.0      39.0   0.98

[8 rows x 11 columns]