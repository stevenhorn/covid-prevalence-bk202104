0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -869.15    19.90
p_loo       18.93        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      369   96.1%
 (0.5, 0.7]   (ok)         12    3.1%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.241  0.056   0.158    0.335  ...    91.0      85.0      57.0   0.98
pu        0.802  0.019   0.766    0.833  ...    79.0      76.0      59.0   1.01
mu        0.131  0.021   0.097    0.175  ...     8.0       7.0      24.0   1.30
mus       0.170  0.029   0.124    0.236  ...    47.0      55.0      24.0   1.03
gamma     0.224  0.034   0.164    0.282  ...    70.0      63.0      60.0   1.02
Is_begin  0.579  0.537   0.029    1.644  ...    61.0      62.0      57.0   1.02
Ia_begin  1.089  1.163   0.037    3.329  ...    74.0      64.0      58.0   1.04
E_begin   0.593  0.619   0.010    1.628  ...   127.0     139.0     100.0   1.01

[8 rows x 11 columns]