0 Divergences 
Passed validation 
Computed from 80 by 385 log-likelihood matrix

         Estimate       SE
elpd_loo -1870.44    25.69
p_loo       39.30        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      335   87.0%
 (0.5, 0.7]   (ok)         41   10.6%
   (0.7, 1]   (bad)         9    2.3%
   (1, Inf)   (very bad)    0    0.0%

             mean       sd   hdi_3%  ...  ess_bulk  ess_tail  r_hat
pa          0.227    0.054    0.150  ...      46.0      19.0   1.07
pu          0.736    0.030    0.703  ...      76.0      95.0   1.01
mu          0.194    0.016    0.169  ...      23.0      40.0   1.05
mus         0.264    0.033    0.201  ...      69.0      26.0   1.03
gamma       0.413    0.065    0.290  ...     152.0      74.0   1.00
Is_begin  317.211  136.127   20.269  ...      58.0      32.0   1.03
Ia_begin  674.973  281.445  170.041  ...      52.0      38.0   1.02
E_begin   764.710  264.508  307.506  ...      79.0      46.0   1.04

[8 rows x 11 columns]