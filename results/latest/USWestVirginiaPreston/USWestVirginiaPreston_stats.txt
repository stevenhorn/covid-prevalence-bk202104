0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -919.82    28.35
p_loo       27.49        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      353   91.9%
 (0.5, 0.7]   (ok)         22    5.7%
   (0.7, 1]   (bad)         9    2.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.246  0.053   0.156    0.322  ...    84.0      80.0      97.0   1.02
pu        0.820  0.018   0.790    0.852  ...    63.0      60.0      37.0   1.12
mu        0.143  0.023   0.100    0.188  ...    28.0      28.0      27.0   1.02
mus       0.190  0.028   0.133    0.236  ...    61.0      47.0      59.0   1.06
gamma     0.238  0.043   0.168    0.328  ...    97.0     101.0      76.0   1.00
Is_begin  0.786  0.689   0.001    2.205  ...   123.0      81.0      22.0   1.01
Ia_begin  1.456  1.725   0.002    4.418  ...    83.0      83.0      82.0   1.00
E_begin   0.729  1.011   0.025    2.396  ...    87.0      80.0      42.0   1.01

[8 rows x 11 columns]