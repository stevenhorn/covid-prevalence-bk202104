0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1565.80    43.18
p_loo       38.35        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      356   92.7%
 (0.5, 0.7]   (ok)         17    4.4%
   (0.7, 1]   (bad)        11    2.9%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.173  0.015   0.150    0.199  ...    83.0      76.0      40.0   0.98
pu        0.707  0.007   0.700    0.721  ...    84.0      42.0      15.0   1.04
mu        0.125  0.020   0.091    0.157  ...    67.0      68.0      58.0   0.98
mus       0.148  0.029   0.100    0.207  ...    26.0      18.0      34.0   1.12
gamma     0.213  0.031   0.158    0.271  ...    36.0      68.0      91.0   1.03
Is_begin  0.089  0.190   0.000    0.294  ...    66.0      21.0      15.0   1.06
Ia_begin  0.146  0.268   0.002    0.635  ...    40.0      36.0      88.0   1.01
E_begin   0.062  0.096   0.000    0.251  ...    62.0      30.0      40.0   1.01

[8 rows x 11 columns]