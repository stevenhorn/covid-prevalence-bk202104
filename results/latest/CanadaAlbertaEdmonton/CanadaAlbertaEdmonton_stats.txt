0 Divergences 
Passed validation 
Computed from 80 by 390 log-likelihood matrix

         Estimate       SE
elpd_loo -2156.53    37.29
p_loo       25.47        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      359   92.1%
 (0.5, 0.7]   (ok)         26    6.7%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.256  0.054   0.165    0.339  ...   152.0     152.0      62.0   1.07
pu        0.831  0.041   0.753    0.888  ...    86.0      82.0      77.0   0.98
mu        0.132  0.020   0.097    0.177  ...    24.0      22.0      57.0   1.07
mus       0.174  0.033   0.121    0.243  ...    67.0      64.0      93.0   0.99
gamma     0.225  0.042   0.157    0.294  ...   132.0     121.0      96.0   1.00
Is_begin  3.051  2.474   0.038    7.601  ...   113.0     104.0      59.0   1.00
Ia_begin  3.158  2.475   0.093    8.186  ...    70.0      59.0      59.0   1.02
E_begin   2.473  2.338   0.094    7.624  ...    64.0      56.0      42.0   1.00

[8 rows x 11 columns]