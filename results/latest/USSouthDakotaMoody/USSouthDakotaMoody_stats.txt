0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -636.68    31.43
p_loo       28.87        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      354   92.2%
 (0.5, 0.7]   (ok)         24    6.2%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.255  0.054   0.172    0.349  ...    70.0      67.0      20.0   1.03
pu        0.822  0.019   0.792    0.852  ...    22.0      21.0      58.0   1.08
mu        0.120  0.025   0.064    0.159  ...   126.0     152.0      92.0   1.09
mus       0.166  0.032   0.113    0.221  ...   152.0     152.0      52.0   1.03
gamma     0.187  0.040   0.119    0.276  ...   152.0     152.0      79.0   1.01
Is_begin  0.518  0.458   0.019    1.266  ...    54.0      55.0      31.0   1.02
Ia_begin  0.958  0.822   0.004    2.222  ...    85.0      60.0      49.0   1.02
E_begin   0.467  0.450   0.006    1.308  ...    70.0      63.0      59.0   1.02

[8 rows x 11 columns]