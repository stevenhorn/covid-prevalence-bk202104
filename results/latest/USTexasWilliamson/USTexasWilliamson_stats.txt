2 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -2215.93    48.13
p_loo       42.60        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      355   92.4%
 (0.5, 0.7]   (ok)         16    4.2%
   (0.7, 1]   (bad)         9    2.3%
   (1, Inf)   (very bad)    4    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.261  0.047   0.183    0.340  ...    52.0      57.0      59.0   1.04
pu        0.783  0.027   0.738    0.838  ...    11.0      11.0      59.0   1.18
mu        0.157  0.022   0.119    0.198  ...    11.0      12.0      59.0   1.15
mus       0.180  0.029   0.127    0.239  ...    42.0      40.0      32.0   1.06
gamma     0.246  0.042   0.169    0.314  ...    56.0      47.0      56.0   1.03
Is_begin  1.075  0.798   0.060    2.347  ...    55.0      63.0      49.0   1.01
Ia_begin  1.985  2.041   0.054    6.729  ...   107.0      34.0      14.0   1.04
E_begin   0.944  1.103   0.000    3.338  ...    60.0      40.0      39.0   1.02

[8 rows x 11 columns]