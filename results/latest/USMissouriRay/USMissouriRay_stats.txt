0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1194.50    74.88
p_loo       47.39        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      354   92.2%
 (0.5, 0.7]   (ok)         27    7.0%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.213  0.051   0.152    0.333  ...    22.0      42.0      22.0   1.04
pu        0.720  0.018   0.700    0.755  ...   152.0     145.0      95.0   1.01
mu        0.123  0.016   0.091    0.151  ...    35.0      35.0      40.0   1.05
mus       0.283  0.044   0.195    0.354  ...   152.0     152.0      83.0   1.00
gamma     0.475  0.073   0.336    0.582  ...    77.0      68.0      59.0   1.00
Is_begin  0.861  0.798   0.044    2.312  ...   102.0     101.0     100.0   1.00
Ia_begin  1.200  1.019   0.006    3.117  ...    77.0      41.0      33.0   1.02
E_begin   0.606  0.819   0.011    1.650  ...    97.0      27.0      40.0   1.06

[8 rows x 11 columns]