0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -561.48    70.89
p_loo       73.82        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      363   94.5%
 (0.5, 0.7]   (ok)         14    3.6%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    5    1.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.224  0.049   0.150    0.315  ...    15.0      13.0      20.0   1.14
pu        0.758  0.044   0.700    0.829  ...     3.0       4.0      40.0   1.92
mu        0.156  0.036   0.095    0.225  ...    48.0      43.0      76.0   1.03
mus       0.270  0.064   0.172    0.387  ...     6.0       6.0      96.0   1.34
gamma     0.313  0.073   0.178    0.443  ...     8.0       7.0      63.0   1.24
Is_begin  0.470  0.710   0.003    1.994  ...    92.0      63.0      60.0   1.03
Ia_begin  0.586  0.648   0.004    2.232  ...    60.0      34.0      39.0   1.07
E_begin   0.376  0.505   0.005    1.306  ...    67.0      53.0      40.0   1.03

[8 rows x 11 columns]