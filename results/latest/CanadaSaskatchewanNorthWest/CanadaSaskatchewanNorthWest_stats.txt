0 Divergences 
Passed validation 
Computed from 80 by 247 log-likelihood matrix

         Estimate       SE
elpd_loo  -770.67    33.93
p_loo       27.14        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      234   94.7%
 (0.5, 0.7]   (ok)         10    4.0%
   (0.7, 1]   (bad)         2    0.8%
   (1, Inf)   (very bad)    1    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.259  0.049   0.174    0.345  ...   152.0     152.0      60.0   1.01
pu        0.833  0.048   0.745    0.897  ...    27.0      32.0     100.0   1.06
mu        0.124  0.030   0.075    0.176  ...     9.0      11.0      33.0   1.16
mus       0.178  0.031   0.131    0.232  ...   120.0     126.0      59.0   0.98
gamma     0.220  0.044   0.145    0.293  ...   126.0     127.0      88.0   1.02
Is_begin  5.176  3.181   0.315   10.493  ...   107.0      93.0      60.0   1.00
Ia_begin  2.484  1.744   0.013    5.610  ...    88.0      72.0      46.0   1.01
E_begin   2.531  2.795   0.041    9.709  ...    97.0      69.0     100.0   1.03

[8 rows x 11 columns]