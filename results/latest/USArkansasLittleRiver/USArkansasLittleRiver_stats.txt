0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -957.55    53.83
p_loo       39.33        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      355   92.4%
 (0.5, 0.7]   (ok)         21    5.5%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.260  0.043   0.189    0.334  ...    49.0      35.0      38.0   1.05
pu        0.761  0.031   0.710    0.811  ...    51.0      48.0      22.0   1.07
mu        0.119  0.020   0.081    0.150  ...    18.0      19.0      60.0   1.08
mus       0.195  0.040   0.142    0.282  ...    73.0      75.0      91.0   1.07
gamma     0.245  0.045   0.172    0.328  ...   127.0     125.0     100.0   0.99
Is_begin  0.251  0.343   0.001    0.906  ...    58.0      52.0      40.0   1.04
Ia_begin  0.459  0.697   0.000    2.061  ...    68.0      51.0      65.0   1.04
E_begin   0.187  0.235   0.000    0.622  ...   118.0      69.0      60.0   1.00

[8 rows x 11 columns]