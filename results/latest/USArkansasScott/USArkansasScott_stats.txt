0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -736.23    31.19
p_loo       31.82        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      369   96.1%
 (0.5, 0.7]   (ok)         10    2.6%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.251  0.051   0.164    0.339  ...   102.0      94.0      57.0   1.05
pu        0.787  0.020   0.751    0.825  ...    65.0      64.0      60.0   1.03
mu        0.141  0.022   0.103    0.186  ...    38.0      37.0      93.0   1.02
mus       0.195  0.029   0.143    0.244  ...    12.0      11.0      23.0   1.16
gamma     0.253  0.037   0.171    0.310  ...    83.0      81.0      49.0   1.02
Is_begin  0.619  0.587   0.007    1.658  ...   109.0      68.0      80.0   1.02
Ia_begin  0.866  0.844   0.001    2.388  ...    81.0      69.0      57.0   1.01
E_begin   0.399  0.330   0.024    1.067  ...   102.0      57.0      72.0   1.03

[8 rows x 11 columns]