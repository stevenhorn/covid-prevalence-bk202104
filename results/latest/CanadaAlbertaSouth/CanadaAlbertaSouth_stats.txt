0 Divergences 
Passed validation 
Computed from 80 by 390 log-likelihood matrix

         Estimate       SE
elpd_loo -1573.97    29.52
p_loo       34.57        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      350   89.7%
 (0.5, 0.7]   (ok)         30    7.7%
   (0.7, 1]   (bad)         9    2.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.275  0.058   0.163    0.349  ...    92.0      93.0      47.0   1.05
pu        0.865  0.033   0.791    0.898  ...    32.0      34.0      60.0   1.00
mu        0.179  0.020   0.145    0.211  ...    16.0      15.0      32.0   1.09
mus       0.241  0.031   0.178    0.298  ...   108.0     106.0      59.0   1.00
gamma     0.373  0.055   0.287    0.472  ...    73.0      73.0      37.0   1.04
Is_begin  2.088  2.505   0.047    6.095  ...    71.0      61.0      54.0   1.02
Ia_begin  5.053  5.275   0.049   14.831  ...   126.0     137.0     100.0   1.00
E_begin   2.546  2.771   0.041    6.458  ...    57.0      55.0      54.0   0.99

[8 rows x 11 columns]