0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -948.40    22.10
p_loo       25.29        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      350   91.1%
 (0.5, 0.7]   (ok)         28    7.3%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.255  0.050   0.169    0.331  ...    45.0      57.0      60.0   1.03
pu        0.789  0.021   0.747    0.821  ...     9.0      10.0      22.0   1.17
mu        0.121  0.026   0.078    0.168  ...    15.0      14.0      26.0   1.13
mus       0.173  0.027   0.128    0.220  ...    82.0      80.0      59.0   1.01
gamma     0.202  0.034   0.150    0.260  ...    96.0     103.0      96.0   0.99
Is_begin  0.897  0.847   0.014    2.185  ...   133.0      62.0      59.0   0.99
Ia_begin  1.813  1.785   0.028    4.966  ...   116.0      93.0      60.0   1.00
E_begin   0.881  0.887   0.003    2.657  ...   100.0      86.0      38.0   1.02

[8 rows x 11 columns]