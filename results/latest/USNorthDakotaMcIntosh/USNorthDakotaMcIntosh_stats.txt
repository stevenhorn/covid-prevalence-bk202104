0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -697.00    69.92
p_loo       42.99        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      357   93.0%
 (0.5, 0.7]   (ok)         17    4.4%
   (0.7, 1]   (bad)         8    2.1%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.233  0.041   0.168    0.317  ...    83.0      87.0      32.0   1.04
pu        0.727  0.019   0.701    0.760  ...    37.0      35.0      40.0   1.02
mu        0.120  0.025   0.077    0.163  ...    52.0      50.0     100.0   0.99
mus       0.242  0.038   0.169    0.303  ...   100.0     110.0     100.0   1.00
gamma     0.312  0.052   0.213    0.401  ...   152.0     152.0      95.0   1.00
Is_begin  0.609  0.534   0.002    1.482  ...   101.0     152.0      65.0   0.99
Ia_begin  0.899  0.742   0.016    2.267  ...    57.0      39.0      24.0   1.06
E_begin   0.377  0.403   0.002    1.076  ...    73.0      37.0      40.0   1.05

[8 rows x 11 columns]