0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1145.65    24.26
p_loo       22.61        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      360   93.8%
 (0.5, 0.7]   (ok)         20    5.2%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.246  0.053   0.170    0.346  ...    69.0      68.0      54.0   1.09
pu        0.842  0.017   0.811    0.867  ...    52.0      57.0      55.0   1.02
mu        0.146  0.031   0.098    0.200  ...     4.0       5.0      41.0   1.46
mus       0.160  0.035   0.099    0.218  ...    84.0      84.0      60.0   1.02
gamma     0.151  0.032   0.103    0.216  ...    22.0      17.0      59.0   1.09
Is_begin  0.859  0.627   0.016    1.966  ...   152.0     152.0     100.0   0.99
Ia_begin  2.105  1.940   0.051    6.900  ...    86.0      88.0      62.0   1.01
E_begin   1.044  1.171   0.005    3.263  ...    83.0      59.0      60.0   0.99

[8 rows x 11 columns]