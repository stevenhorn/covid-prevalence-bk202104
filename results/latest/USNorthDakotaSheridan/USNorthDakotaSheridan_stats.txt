0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -378.77    52.52
p_loo       49.14        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      346   90.1%
 (0.5, 0.7]   (ok)         29    7.6%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.246  0.048   0.151    0.320  ...    81.0      84.0      25.0   1.01
pu        0.839  0.038   0.751    0.892  ...     5.0       6.0      38.0   1.38
mu        0.150  0.036   0.088    0.221  ...    28.0      30.0      60.0   1.05
mus       0.216  0.043   0.142    0.305  ...    90.0     114.0      93.0   1.13
gamma     0.258  0.037   0.192    0.325  ...    89.0     100.0      59.0   1.01
Is_begin  0.511  0.713   0.000    1.962  ...   106.0      90.0      96.0   0.99
Ia_begin  0.838  1.013   0.004    3.471  ...    66.0      69.0      43.0   0.99
E_begin   0.391  0.533   0.000    1.492  ...    97.0      35.0      19.0   1.06

[8 rows x 11 columns]