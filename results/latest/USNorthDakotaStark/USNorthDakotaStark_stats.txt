0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1258.08    33.34
p_loo       30.53        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      367   95.6%
 (0.5, 0.7]   (ok)         10    2.6%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.186  0.028   0.150    0.231  ...   110.0      81.0      43.0   0.98
pu        0.712  0.010   0.700    0.731  ...   113.0     140.0     100.0   1.01
mu        0.122  0.021   0.086    0.153  ...    44.0      52.0      88.0   1.03
mus       0.168  0.027   0.122    0.218  ...   104.0      99.0      96.0   0.99
gamma     0.203  0.037   0.139    0.277  ...   152.0     152.0     100.0   0.99
Is_begin  0.855  0.845   0.007    2.596  ...   128.0     116.0      40.0   1.04
Ia_begin  1.852  2.230   0.005    5.739  ...    70.0      71.0      34.0   1.03
E_begin   0.587  0.616   0.003    1.954  ...    85.0      70.0      56.0   1.05

[8 rows x 11 columns]