0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -814.93    26.13
p_loo       22.24        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      349   90.9%
 (0.5, 0.7]   (ok)         29    7.6%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.257  0.057   0.166    0.342  ...    77.0      81.0     100.0   1.10
pu        0.878  0.017   0.846    0.899  ...    40.0      46.0      51.0   1.00
mu        0.137  0.027   0.089    0.178  ...     8.0       9.0      59.0   1.19
mus       0.166  0.038   0.099    0.243  ...   152.0     152.0      49.0   1.00
gamma     0.205  0.036   0.147    0.258  ...   152.0     152.0      88.0   0.99
Is_begin  1.596  1.366   0.004    4.023  ...    75.0      51.0      78.0   1.00
Ia_begin  0.764  0.544   0.023    1.670  ...    71.0      63.0      24.0   1.01
E_begin   1.183  1.111   0.021    3.548  ...    23.0      13.0      60.0   1.12

[8 rows x 11 columns]