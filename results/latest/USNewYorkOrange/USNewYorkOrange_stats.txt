0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1982.22    37.99
p_loo       30.29        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      343   89.3%
 (0.5, 0.7]   (ok)         38    9.9%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    1    0.3%

             mean       sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.235    0.054   0.153    0.334  ...    62.0      62.0      39.0   1.05
pu          0.738    0.025   0.701    0.784  ...    30.0      34.0      47.0   1.01
mu          0.143    0.026   0.108    0.192  ...     6.0       5.0      22.0   1.38
mus         0.194    0.026   0.142    0.232  ...    46.0      52.0      59.0   1.04
gamma       0.291    0.041   0.216    0.371  ...   152.0     152.0      81.0   1.00
Is_begin   55.265   42.206   0.380  142.816  ...    35.0      14.0      17.0   1.12
Ia_begin  162.963   51.478  76.881  257.491  ...    74.0      80.0      99.0   1.02
E_begin   248.343  110.075  62.248  439.916  ...    31.0      46.0      30.0   1.05

[8 rows x 11 columns]