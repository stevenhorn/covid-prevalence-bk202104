0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1913.21    44.35
p_loo       36.53        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      357   93.0%
 (0.5, 0.7]   (ok)         20    5.2%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.250  0.049   0.168    0.343  ...   145.0     145.0      79.0   1.01
pu        0.769  0.026   0.726    0.813  ...    63.0      69.0      30.0   0.99
mu        0.130  0.025   0.078    0.173  ...    49.0      50.0      38.0   1.04
mus       0.189  0.033   0.124    0.235  ...   109.0     113.0      88.0   0.99
gamma     0.241  0.042   0.176    0.327  ...   126.0     142.0      97.0   1.02
Is_begin  1.369  1.162   0.068    3.706  ...   152.0     152.0      53.0   1.00
Ia_begin  2.097  2.176   0.009    6.971  ...   102.0      86.0      36.0   1.00
E_begin   0.920  0.929   0.013    2.804  ...   131.0      96.0      19.0   1.01

[8 rows x 11 columns]