0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -768.79    28.21
p_loo       27.67        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      356   92.7%
 (0.5, 0.7]   (ok)         18    4.7%
   (0.7, 1]   (bad)         8    2.1%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.254  0.064   0.151    0.338  ...    28.0      33.0      22.0   1.11
pu        0.802  0.021   0.763    0.838  ...    31.0      32.0      56.0   1.07
mu        0.118  0.022   0.087    0.163  ...    37.0      36.0      53.0   1.04
mus       0.185  0.038   0.135    0.254  ...    60.0      83.0      36.0   0.98
gamma     0.201  0.039   0.147    0.283  ...    63.0      78.0      60.0   1.00
Is_begin  0.626  0.612   0.001    1.700  ...    88.0      64.0      25.0   1.06
Ia_begin  0.811  1.007   0.007    2.899  ...    79.0      22.0      31.0   1.06
E_begin   0.416  0.546   0.000    1.612  ...    21.0       7.0      22.0   1.28

[8 rows x 11 columns]