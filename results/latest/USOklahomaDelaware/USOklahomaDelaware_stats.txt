0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1270.89    30.81
p_loo       35.20        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      361   94.0%
 (0.5, 0.7]   (ok)         20    5.2%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.248  0.051   0.152    0.326  ...    60.0      59.0      60.0   1.06
pu        0.756  0.026   0.711    0.802  ...    19.0      19.0      47.0   1.08
mu        0.142  0.026   0.102    0.184  ...    15.0      16.0      91.0   1.12
mus       0.202  0.033   0.137    0.250  ...   122.0     152.0      67.0   1.02
gamma     0.268  0.036   0.199    0.354  ...    71.0      66.0      61.0   1.03
Is_begin  1.128  0.924   0.007    2.604  ...   128.0      86.0      25.0   1.03
Ia_begin  3.402  2.472   0.239    8.304  ...    67.0      89.0      59.0   0.99
E_begin   2.085  1.746   0.064    5.298  ...    95.0      75.0      60.0   1.00

[8 rows x 11 columns]