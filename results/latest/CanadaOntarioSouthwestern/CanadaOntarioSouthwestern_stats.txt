0 Divergences 
Passed validation 
Computed from 80 by 390 log-likelihood matrix

         Estimate       SE
elpd_loo -1079.56    33.83
p_loo       35.36        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      366   93.8%
 (0.5, 0.7]   (ok)         19    4.9%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    1    0.3%

            mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.290  0.048   0.193    0.349  ...    29.0      17.0      15.0   1.08
pu         0.886  0.012   0.859    0.900  ...    69.0      56.0      93.0   1.02
mu         0.178  0.018   0.148    0.214  ...    13.0      15.0      38.0   1.10
mus        0.203  0.026   0.161    0.255  ...     8.0       8.0      58.0   1.22
gamma      0.288  0.044   0.219    0.363  ...   118.0     114.0      96.0   1.04
Is_begin   3.244  3.608   0.002   11.684  ...   121.0      61.0      39.0   1.00
Ia_begin  10.086  8.994   0.112   25.918  ...    71.0      39.0      36.0   1.04
E_begin    3.526  3.986   0.028    8.084  ...    75.0      59.0      40.0   1.00

[8 rows x 11 columns]