0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1988.10    31.04
p_loo       31.21        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      342   89.1%
 (0.5, 0.7]   (ok)         36    9.4%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    0    0.0%

             mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.217   0.046   0.153    0.285  ...   152.0     152.0      25.0   1.04
pu          0.724   0.020   0.701    0.758  ...    35.0      26.0      40.0   1.08
mu          0.103   0.018   0.066    0.129  ...     5.0       6.0      20.0   1.35
mus         0.185   0.029   0.140    0.229  ...    67.0     117.0      39.0   1.01
gamma       0.354   0.059   0.246    0.463  ...   152.0     152.0      60.0   1.00
Is_begin   22.803  14.504   0.384   46.794  ...   113.0      94.0      59.0   1.02
Ia_begin   67.909  24.891  19.716  111.317  ...    95.0      99.0      49.0   1.02
E_begin   117.434  65.650  21.183  248.130  ...   135.0     152.0      60.0   1.03

[8 rows x 11 columns]