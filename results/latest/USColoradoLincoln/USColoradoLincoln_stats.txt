0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1038.67    83.93
p_loo       50.80        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      360   93.8%
 (0.5, 0.7]   (ok)         17    4.4%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.159  0.009   0.150    0.176  ...   132.0      89.0      59.0   1.00
pu        0.703  0.003   0.700    0.709  ...    82.0      65.0      56.0   1.03
mu        0.072  0.011   0.050    0.090  ...    28.0      28.0      40.0   1.06
mus       0.288  0.038   0.228    0.351  ...    39.0      50.0      58.0   1.03
gamma     0.450  0.057   0.361    0.564  ...   107.0     100.0      91.0   1.01
Is_begin  0.688  0.645   0.008    2.247  ...    52.0      50.0      42.0   1.03
Ia_begin  0.680  0.693   0.006    1.609  ...    68.0      24.0      30.0   1.10
E_begin   0.367  0.379   0.024    1.135  ...    87.0      54.0      60.0   1.00

[8 rows x 11 columns]