0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1040.48    35.01
p_loo       33.56        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      358   93.2%
 (0.5, 0.7]   (ok)         22    5.7%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.207  0.034   0.153    0.263  ...    79.0      70.0      42.0   1.02
pu        0.716  0.013   0.700    0.739  ...    52.0      45.0      43.0   1.01
mu        0.150  0.031   0.111    0.221  ...    27.0      27.0      75.0   1.05
mus       0.222  0.039   0.168    0.300  ...    62.0      50.0      56.0   1.06
gamma     0.287  0.045   0.197    0.372  ...   109.0     123.0      88.0   1.03
Is_begin  0.676  0.677   0.011    1.832  ...    84.0      71.0      80.0   1.04
Ia_begin  1.080  1.092   0.033    3.172  ...    95.0      86.0      50.0   1.05
E_begin   0.447  0.565   0.013    1.375  ...   102.0      85.0      83.0   0.99

[8 rows x 11 columns]