0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -591.94    25.98
p_loo       22.65        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      354   92.2%
 (0.5, 0.7]   (ok)         25    6.5%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.261  0.051   0.167    0.339  ...    63.0      69.0      57.0   1.00
pu        0.799  0.032   0.738    0.854  ...    15.0      16.0      22.0   1.12
mu        0.121  0.025   0.080    0.164  ...     5.0       7.0      57.0   1.26
mus       0.157  0.038   0.100    0.222  ...    91.0     107.0      60.0   1.01
gamma     0.164  0.032   0.114    0.236  ...   152.0     152.0      59.0   0.99
Is_begin  0.443  0.481   0.006    1.325  ...    56.0      48.0      79.0   1.01
Ia_begin  0.801  1.000   0.007    2.518  ...    62.0      55.0      56.0   1.01
E_begin   0.369  0.372   0.002    1.173  ...    54.0      40.0      58.0   1.05

[8 rows x 11 columns]