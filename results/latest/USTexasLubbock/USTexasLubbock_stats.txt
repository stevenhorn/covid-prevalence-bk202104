0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1963.36    28.06
p_loo       25.55        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      354   92.2%
 (0.5, 0.7]   (ok)         25    6.5%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.233  0.045   0.158    0.304  ...    13.0      17.0      18.0   1.11
pu        0.721  0.016   0.700    0.749  ...    76.0      59.0      60.0   0.99
mu        0.140  0.028   0.085    0.182  ...     4.0       4.0      57.0   1.58
mus       0.192  0.034   0.120    0.243  ...    59.0      49.0      45.0   1.02
gamma     0.260  0.045   0.184    0.352  ...   152.0     152.0      93.0   1.00
Is_begin  1.600  1.028   0.432    3.590  ...    88.0      83.0      69.0   1.00
Ia_begin  3.945  2.730   0.301    9.117  ...    81.0      65.0      97.0   1.03
E_begin   2.795  2.670   0.050    7.979  ...    75.0      21.0      69.0   1.08

[8 rows x 11 columns]