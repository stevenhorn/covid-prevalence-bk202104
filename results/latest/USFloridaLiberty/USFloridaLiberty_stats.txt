0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -987.19    41.79
p_loo       41.98        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      364   94.8%
 (0.5, 0.7]   (ok)         15    3.9%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.191  0.035   0.151    0.251  ...    92.0     102.0      57.0   0.98
pu        0.714  0.013   0.700    0.733  ...    67.0      67.0      46.0   1.03
mu        0.156  0.022   0.112    0.191  ...    47.0      46.0      57.0   1.04
mus       0.272  0.043   0.195    0.349  ...   131.0     112.0      59.0   1.10
gamma     0.438  0.062   0.352    0.563  ...    58.0      55.0      74.0   1.00
Is_begin  0.370  0.351   0.001    1.060  ...   116.0      95.0      81.0   0.99
Ia_begin  0.488  0.679   0.004    1.562  ...    64.0      86.0      60.0   0.99
E_begin   0.252  0.299   0.004    0.881  ...    53.0      52.0      38.0   1.02

[8 rows x 11 columns]