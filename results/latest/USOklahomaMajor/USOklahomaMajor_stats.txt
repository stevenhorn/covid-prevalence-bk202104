0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -792.13    28.79
p_loo       30.76        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      355   92.4%
 (0.5, 0.7]   (ok)         19    4.9%
   (0.7, 1]   (bad)        10    2.6%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.251  0.050   0.170    0.335  ...    55.0      60.0     100.0   1.02
pu        0.765  0.026   0.722    0.812  ...    50.0      51.0      56.0   1.01
mu        0.129  0.023   0.086    0.159  ...    77.0      69.0      58.0   1.01
mus       0.181  0.029   0.120    0.237  ...   152.0     152.0      59.0   1.01
gamma     0.197  0.040   0.121    0.259  ...    99.0      92.0      58.0   0.98
Is_begin  0.692  0.525   0.003    1.688  ...    99.0      78.0      59.0   0.99
Ia_begin  1.266  0.884   0.111    3.027  ...    89.0      79.0      60.0   1.04
E_begin   0.571  0.471   0.014    1.537  ...    56.0      59.0      56.0   0.99

[8 rows x 11 columns]