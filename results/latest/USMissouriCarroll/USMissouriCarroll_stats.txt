0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1091.37    73.47
p_loo       37.37        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      354   92.2%
 (0.5, 0.7]   (ok)         27    7.0%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.213  0.041   0.153    0.297  ...    12.0      12.0      40.0   1.15
pu        0.715  0.013   0.700    0.739  ...    57.0      52.0      59.0   1.03
mu        0.095  0.017   0.074    0.124  ...    35.0      39.0      56.0   1.07
mus       0.203  0.038   0.134    0.254  ...     7.0       7.0      24.0   1.31
gamma     0.405  0.072   0.248    0.525  ...    22.0      22.0      87.0   1.08
Is_begin  0.401  0.475   0.001    1.230  ...    93.0     132.0      59.0   1.01
Ia_begin  0.530  0.561   0.005    1.525  ...   105.0     109.0      49.0   0.99
E_begin   0.273  0.377   0.006    0.913  ...    98.0      92.0     100.0   1.02

[8 rows x 11 columns]