0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -506.29    27.96
p_loo       25.21        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      351   91.4%
 (0.5, 0.7]   (ok)         27    7.0%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.259  0.053   0.161    0.337  ...    26.0      27.0      55.0   1.07
pu        0.847  0.018   0.814    0.878  ...    47.0      48.0      65.0   1.02
mu        0.157  0.023   0.112    0.195  ...    49.0      50.0      57.0   1.02
mus       0.193  0.037   0.121    0.255  ...   130.0     102.0      66.0   1.02
gamma     0.252  0.043   0.176    0.338  ...   152.0     152.0      60.0   0.99
Is_begin  0.736  0.659   0.033    1.761  ...   130.0     129.0      76.0   1.00
Ia_begin  1.250  1.219   0.015    4.237  ...   100.0     106.0      85.0   1.01
E_begin   0.514  0.556   0.006    1.340  ...    81.0      69.0      59.0   1.02

[8 rows x 11 columns]