0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -856.34    31.51
p_loo       28.98        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      351   91.4%
 (0.5, 0.7]   (ok)         23    6.0%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.254  0.041   0.200    0.330  ...    78.0      95.0     100.0   1.01
pu        0.816  0.021   0.769    0.844  ...     6.0       7.0      25.0   1.24
mu        0.131  0.026   0.084    0.180  ...    24.0      22.0      86.0   1.08
mus       0.170  0.034   0.123    0.241  ...   152.0     152.0      91.0   0.98
gamma     0.178  0.035   0.128    0.254  ...    83.0      66.0      60.0   1.03
Is_begin  0.600  0.511   0.020    1.601  ...   122.0     106.0      53.0   1.00
Ia_begin  1.069  1.208   0.029    3.366  ...   110.0     125.0      24.0   1.00
E_begin   0.498  0.597   0.004    1.988  ...    29.0      89.0      16.0   1.00

[8 rows x 11 columns]