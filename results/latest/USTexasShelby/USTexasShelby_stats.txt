0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1179.02    59.75
p_loo       38.07        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      359   93.5%
 (0.5, 0.7]   (ok)         20    5.2%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.256  0.051   0.171    0.337  ...   142.0     144.0      59.0   1.02
pu        0.856  0.022   0.813    0.893  ...    21.0      20.0      40.0   1.09
mu        0.152  0.029   0.102    0.199  ...     6.0       6.0      18.0   1.29
mus       0.239  0.040   0.173    0.304  ...    97.0      94.0      57.0   1.01
gamma     0.333  0.060   0.222    0.432  ...    86.0      74.0      68.0   1.04
Is_begin  1.024  0.931   0.035    2.593  ...   121.0      98.0      79.0   1.01
Ia_begin  2.188  1.917   0.011    5.388  ...    82.0     113.0      40.0   1.05
E_begin   1.286  1.651   0.017    3.629  ...    90.0      86.0     100.0   1.06

[8 rows x 11 columns]