0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1293.52    33.26
p_loo       31.23        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      351   91.4%
 (0.5, 0.7]   (ok)         25    6.5%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.260  0.053   0.171    0.339  ...   152.0     126.0      56.0   0.99
pu        0.817  0.025   0.764    0.859  ...    41.0      45.0      40.0   1.04
mu        0.127  0.026   0.083    0.178  ...    25.0      24.0      46.0   1.08
mus       0.160  0.033   0.107    0.225  ...   127.0     127.0      26.0   1.12
gamma     0.183  0.032   0.136    0.250  ...   111.0     110.0      88.0   0.99
Is_begin  0.539  0.404   0.005    1.345  ...   135.0      96.0      32.0   1.01
Ia_begin  1.036  0.978   0.013    2.875  ...   106.0      66.0      36.0   1.04
E_begin   0.436  0.461   0.007    1.279  ...    72.0      43.0      59.0   1.06

[8 rows x 11 columns]