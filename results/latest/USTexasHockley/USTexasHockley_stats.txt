0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1244.18    41.09
p_loo       37.57        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      369   96.1%
 (0.5, 0.7]   (ok)          9    2.3%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    4    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.199  0.040   0.151    0.277  ...    59.0      53.0      60.0   1.00
pu        0.718  0.013   0.701    0.741  ...    81.0      66.0      53.0   1.00
mu        0.130  0.021   0.094    0.162  ...    25.0      28.0      67.0   1.06
mus       0.190  0.028   0.147    0.240  ...    74.0      80.0      93.0   1.04
gamma     0.211  0.044   0.120    0.273  ...   152.0     148.0      81.0   0.99
Is_begin  0.833  0.678   0.020    2.001  ...    16.0      15.0      53.0   1.09
Ia_begin  0.635  0.520   0.010    1.708  ...    76.0      70.0      84.0   1.04
E_begin   0.511  0.507   0.006    1.584  ...    63.0      57.0      69.0   1.01

[8 rows x 11 columns]