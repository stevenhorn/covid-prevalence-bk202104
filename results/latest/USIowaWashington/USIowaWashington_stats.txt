0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1035.72    27.59
p_loo       27.53        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      353   91.9%
 (0.5, 0.7]   (ok)         24    6.2%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.249  0.054   0.166    0.339  ...    66.0      76.0      20.0   1.06
pu        0.768  0.029   0.719    0.814  ...     8.0       9.0      40.0   1.20
mu        0.141  0.024   0.093    0.180  ...    12.0      12.0      37.0   1.14
mus       0.189  0.033   0.140    0.248  ...   152.0     152.0      49.0   1.04
gamma     0.249  0.041   0.191    0.345  ...    86.0      93.0      81.0   1.02
Is_begin  1.853  1.271   0.143    4.581  ...   139.0     152.0      60.0   0.99
Ia_begin  0.754  0.561   0.020    1.734  ...    72.0      55.0      60.0   0.99
E_begin   1.465  1.191   0.013    4.095  ...    55.0      44.0      53.0   1.04

[8 rows x 11 columns]