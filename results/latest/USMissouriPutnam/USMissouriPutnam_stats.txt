0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -727.68    70.91
p_loo       60.84        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      356   92.7%
 (0.5, 0.7]   (ok)         22    5.7%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.230  0.047   0.161    0.312  ...    97.0      94.0      24.0   1.06
pu        0.727  0.020   0.700    0.763  ...   101.0      94.0      83.0   1.04
mu        0.145  0.024   0.109    0.194  ...    59.0      59.0      38.0   1.03
mus       0.273  0.042   0.196    0.336  ...   106.0      94.0      59.0   1.04
gamma     0.473  0.068   0.363    0.605  ...   152.0     152.0      46.0   0.98
Is_begin  0.590  0.685   0.007    2.468  ...    89.0      76.0     100.0   1.01
Ia_begin  0.911  1.374   0.007    3.896  ...   101.0      58.0      93.0   1.03
E_begin   0.375  0.481   0.003    1.174  ...    53.0      52.0      40.0   1.03

[8 rows x 11 columns]