0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -603.62    31.33
p_loo       28.46        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      364   94.8%
 (0.5, 0.7]   (ok)         15    3.9%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.265  0.049   0.185    0.339  ...    63.0      67.0      47.0   0.99
pu        0.810  0.019   0.777    0.847  ...    51.0      51.0      58.0   1.00
mu        0.126  0.028   0.090    0.195  ...     8.0       6.0      40.0   1.29
mus       0.193  0.037   0.134    0.266  ...    62.0      67.0      59.0   1.02
gamma     0.220  0.040   0.146    0.287  ...    70.0      71.0      59.0   1.00
Is_begin  0.557  0.600   0.008    1.847  ...    41.0      33.0      59.0   1.05
Ia_begin  0.985  1.015   0.022    2.852  ...    31.0      23.0      43.0   1.10
E_begin   0.492  0.535   0.001    1.663  ...    75.0      28.0      36.0   1.06

[8 rows x 11 columns]