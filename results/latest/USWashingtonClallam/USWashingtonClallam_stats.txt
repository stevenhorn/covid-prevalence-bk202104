0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -864.44    26.83
p_loo       30.67        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      367   95.6%
 (0.5, 0.7]   (ok)         14    3.6%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.288  0.040   0.204    0.348  ...   139.0     152.0      39.0   1.02
pu        0.888  0.013   0.859    0.900  ...    82.0     143.0      60.0   1.06
mu        0.162  0.026   0.121    0.202  ...    16.0      19.0      32.0   1.03
mus       0.189  0.036   0.137    0.267  ...   102.0     111.0      60.0   0.99
gamma     0.254  0.042   0.170    0.332  ...   147.0     122.0      60.0   0.99
Is_begin  1.204  0.944   0.023    3.001  ...    63.0      75.0      43.0   1.05
Ia_begin  0.799  0.534   0.003    1.801  ...    65.0      79.0      40.0   1.07
E_begin   0.755  0.743   0.006    2.338  ...    54.0      33.0      40.0   1.06

[8 rows x 11 columns]