0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -873.62    33.56
p_loo       32.17        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      361   94.0%
 (0.5, 0.7]   (ok)         12    3.1%
   (0.7, 1]   (bad)         9    2.3%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.240  0.063   0.151    0.334  ...    54.0      53.0      56.0   1.01
pu        0.769  0.033   0.706    0.819  ...    22.0      27.0      24.0   1.08
mu        0.114  0.021   0.087    0.156  ...    11.0      12.0      60.0   1.15
mus       0.190  0.037   0.125    0.257  ...    62.0      63.0      39.0   1.04
gamma     0.224  0.041   0.159    0.304  ...   102.0     101.0      47.0   1.07
Is_begin  0.575  0.573   0.001    1.812  ...    10.0       7.0      22.0   1.27
Ia_begin  0.948  0.788   0.029    2.482  ...    69.0      45.0      80.0   1.06
E_begin   0.415  0.348   0.007    1.099  ...    17.0      14.0      34.0   1.15

[8 rows x 11 columns]