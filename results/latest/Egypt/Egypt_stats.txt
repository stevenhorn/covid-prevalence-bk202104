0 Divergences 
Passed validation 
Computed from 80 by 385 log-likelihood matrix

         Estimate       SE
elpd_loo -2012.91    22.23
p_loo       42.74        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      316   82.1%
 (0.5, 0.7]   (ok)         54   14.0%
   (0.7, 1]   (bad)        13    3.4%
   (1, Inf)   (very bad)    2    0.5%

             mean       sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.244    0.054   0.162    0.330  ...   114.0      86.0      38.0   1.03
pu          0.776    0.056   0.701    0.862  ...     4.0       4.0      14.0   1.66
mu          0.161    0.027   0.126    0.205  ...     3.0       4.0      31.0   1.66
mus         0.307    0.058   0.224    0.419  ...    39.0      40.0      40.0   1.03
gamma       0.349    0.059   0.271    0.494  ...    51.0      52.0      39.0   1.01
Is_begin   78.500   45.355   7.691  157.951  ...    25.0      25.0      37.0   1.09
Ia_begin  178.425   65.114  47.031  289.997  ...    28.0      35.0      22.0   1.06
E_begin   175.432  124.809   5.091  411.317  ...     7.0       6.0      24.0   1.35

[8 rows x 11 columns]