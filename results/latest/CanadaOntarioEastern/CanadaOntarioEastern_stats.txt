0 Divergences 
Passed validation 
Computed from 80 by 390 log-likelihood matrix

         Estimate       SE
elpd_loo -1299.39    46.49
p_loo       43.58        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      373   95.6%
 (0.5, 0.7]   (ok)         11    2.8%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    1    0.3%

            mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.271  0.007   0.260    0.280  ...     3.0       3.0      20.0   2.15
pu         0.834  0.011   0.822    0.846  ...     3.0       4.0      38.0   1.88
mu         0.300  0.015   0.282    0.318  ...     3.0       3.0      15.0   2.17
mus        0.254  0.006   0.245    0.265  ...     3.0       3.0      22.0   2.09
gamma      0.162  0.010   0.149    0.179  ...     3.0       3.0      14.0   2.54
Is_begin   2.412  0.707   1.378    3.545  ...     3.0       3.0      14.0   1.99
Ia_begin  10.799  2.385   7.204   14.616  ...     4.0       5.0      37.0   1.47
E_begin    1.874  0.818   0.752    3.089  ...     3.0       3.0      19.0   2.29

[8 rows x 11 columns]