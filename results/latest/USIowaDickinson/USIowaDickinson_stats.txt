0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1163.59    57.15
p_loo       38.12        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      360   93.8%
 (0.5, 0.7]   (ok)         17    4.4%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.192  0.032   0.150    0.250  ...    86.0      65.0      39.0   1.00
pu        0.711  0.012   0.700    0.735  ...    35.0      26.0      59.0   1.03
mu        0.126  0.021   0.083    0.162  ...    12.0      10.0      36.0   1.15
mus       0.210  0.040   0.130    0.278  ...    19.0      17.0      17.0   1.10
gamma     0.297  0.052   0.225    0.407  ...    20.0      40.0      23.0   1.03
Is_begin  0.457  0.471   0.001    1.567  ...    33.0      12.0      25.0   1.15
Ia_begin  0.743  0.760   0.010    1.894  ...    98.0      58.0      51.0   1.01
E_begin   0.383  0.417   0.004    1.365  ...    91.0      73.0      54.0   1.04

[8 rows x 11 columns]