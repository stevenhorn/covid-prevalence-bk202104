1 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1583.75    25.13
p_loo       28.23        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      355   92.4%
 (0.5, 0.7]   (ok)         24    6.2%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    0    0.0%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.210   0.040   0.155    0.278  ...    36.0      42.0      88.0   1.03
pu         0.723   0.018   0.702    0.758  ...   114.0     103.0     100.0   1.00
mu         0.103   0.017   0.077    0.136  ...    34.0      36.0      49.0   1.02
mus        0.208   0.031   0.161    0.267  ...    67.0      69.0      43.0   1.03
gamma      0.347   0.051   0.274    0.444  ...    82.0      83.0      96.0   1.02
Is_begin   2.605   1.598   0.228    5.389  ...    65.0      57.0      59.0   1.05
Ia_begin  10.242   3.436   3.486   15.343  ...    41.0      36.0      38.0   1.05
E_begin   19.448  11.284   1.224   38.833  ...    36.0      28.0      22.0   1.06

[8 rows x 11 columns]