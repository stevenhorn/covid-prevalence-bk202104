0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1379.37    46.83
p_loo       35.41        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      358   93.2%
 (0.5, 0.7]   (ok)         20    5.2%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.244  0.054   0.151    0.336  ...    57.0      49.0      30.0   1.02
pu        0.758  0.025   0.717    0.794  ...    63.0      65.0      95.0   1.00
mu        0.123  0.022   0.083    0.165  ...    36.0      32.0      60.0   1.03
mus       0.185  0.031   0.140    0.246  ...    62.0      60.0      60.0   0.99
gamma     0.236  0.045   0.150    0.311  ...    55.0      54.0      52.0   1.05
Is_begin  0.716  0.738   0.014    2.088  ...    51.0      26.0      60.0   1.05
Ia_begin  1.488  1.342   0.046    3.997  ...    70.0      59.0      59.0   1.02
E_begin   0.651  0.822   0.006    2.132  ...    50.0      27.0      14.0   1.05

[8 rows x 11 columns]