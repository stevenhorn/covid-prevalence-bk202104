0 Divergences 
Passed validation 
Computed from 80 by 385 log-likelihood matrix

         Estimate       SE
elpd_loo  -823.09    34.91
p_loo       32.03        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      353   91.7%
 (0.5, 0.7]   (ok)         24    6.2%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    1    0.3%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.203   0.035   0.151    0.267  ...   152.0     152.0      86.0   0.99
pu         0.719   0.019   0.700    0.756  ...   152.0      62.0      58.0   1.03
mu         0.148   0.023   0.105    0.184  ...     5.0       5.0      40.0   1.42
mus        0.238   0.032   0.193    0.294  ...   152.0     152.0      52.0   0.99
gamma      0.312   0.050   0.237    0.407  ...   106.0     120.0      57.0   1.02
Is_begin  11.515   8.333   0.862   25.777  ...   123.0      84.0      60.0   1.01
Ia_begin  34.476  14.252   7.391   61.270  ...    86.0      88.0      61.0   1.00
E_begin   26.983  15.107   3.479   50.542  ...    68.0      72.0      55.0   1.04

[8 rows x 11 columns]