0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1241.11    34.65
p_loo       27.25        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      361   94.0%
 (0.5, 0.7]   (ok)         19    4.9%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.248  0.047   0.177    0.346  ...    61.0      62.0      60.0   1.02
pu        0.737  0.023   0.701    0.775  ...    78.0      62.0      57.0   1.01
mu        0.135  0.025   0.092    0.177  ...    35.0      35.0      14.0   1.04
mus       0.199  0.029   0.151    0.260  ...    52.0      58.0      60.0   1.05
gamma     0.218  0.045   0.149    0.316  ...    80.0      97.0      60.0   1.05
Is_begin  0.792  0.686   0.001    1.868  ...   122.0      79.0      42.0   1.01
Ia_begin  1.615  1.242   0.109    3.708  ...    95.0     101.0      65.0   1.01
E_begin   0.694  0.629   0.017    1.799  ...   133.0      96.0      99.0   0.98

[8 rows x 11 columns]