1 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -820.58    54.83
p_loo       46.24        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      356   92.7%
 (0.5, 0.7]   (ok)         22    5.7%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.256  0.053   0.162    0.345  ...    86.0      98.0      43.0   1.02
pu        0.774  0.026   0.717    0.816  ...    44.0      44.0      93.0   1.02
mu        0.170  0.022   0.136    0.219  ...    39.0      58.0      59.0   1.03
mus       0.264  0.035   0.208    0.338  ...    62.0      63.0      60.0   1.01
gamma     0.436  0.061   0.327    0.542  ...    72.0      77.0      60.0   1.04
Is_begin  1.320  1.017   0.105    3.429  ...    91.0     112.0      35.0   1.00
Ia_begin  2.607  2.142   0.040    7.201  ...    97.0      77.0      74.0   1.02
E_begin   1.557  1.349   0.015    4.149  ...    79.0      55.0      40.0   1.04

[8 rows x 11 columns]