0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -796.68    47.97
p_loo       39.72        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      358   93.2%
 (0.5, 0.7]   (ok)         22    5.7%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.266  0.048   0.182    0.348  ...   134.0     129.0      59.0   1.03
pu        0.842  0.045   0.736    0.893  ...     5.0       4.0      32.0   1.50
mu        0.124  0.025   0.075    0.163  ...    11.0      11.0      65.0   1.16
mus       0.217  0.046   0.149    0.295  ...    97.0      91.0      78.0   1.02
gamma     0.240  0.054   0.149    0.348  ...    52.0      49.0      54.0   1.03
Is_begin  0.420  0.525   0.000    1.582  ...   117.0     119.0      60.0   1.00
Ia_begin  0.872  1.460   0.002    3.490  ...    66.0      53.0      57.0   1.00
E_begin   0.371  0.557   0.006    1.049  ...    83.0      49.0      47.0   1.04

[8 rows x 11 columns]