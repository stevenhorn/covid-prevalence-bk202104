1 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1540.10    23.14
p_loo       23.60        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      346   90.1%
 (0.5, 0.7]   (ok)         35    9.1%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.247  0.050   0.151    0.331  ...    73.0      76.0      60.0   1.02
pu        0.773  0.031   0.725    0.831  ...    36.0      41.0      57.0   1.14
mu        0.117  0.021   0.082    0.155  ...    32.0      39.0      46.0   1.02
mus       0.168  0.031   0.115    0.221  ...    93.0      87.0      60.0   1.00
gamma     0.216  0.041   0.160    0.299  ...   152.0     152.0      69.0   1.00
Is_begin  1.781  1.217   0.019    4.003  ...    90.0      82.0      60.0   1.01
Ia_begin  0.709  0.558   0.002    1.662  ...    62.0      19.0      24.0   1.08
E_begin   1.472  1.435   0.106    4.435  ...    79.0      65.0     100.0   1.02

[8 rows x 11 columns]