0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -914.07    39.17
p_loo       31.04        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      355   92.4%
 (0.5, 0.7]   (ok)         23    6.0%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.251  0.057   0.155    0.327  ...   116.0     127.0      38.0   1.04
pu        0.803  0.029   0.734    0.843  ...     5.0       5.0      19.0   1.38
mu        0.116  0.026   0.073    0.160  ...    16.0      15.0      59.0   1.11
mus       0.159  0.031   0.110    0.210  ...    54.0      57.0      60.0   1.01
gamma     0.174  0.042   0.109    0.241  ...   152.0     152.0      48.0   1.13
Is_begin  0.283  0.410   0.001    0.860  ...    77.0      52.0     100.0   1.04
Ia_begin  0.446  0.617   0.006    1.046  ...    74.0      63.0      60.0   1.02
E_begin   0.267  0.383   0.002    0.955  ...    97.0      37.0      25.0   1.03

[8 rows x 11 columns]