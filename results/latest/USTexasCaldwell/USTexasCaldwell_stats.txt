0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1657.28    64.06
p_loo       41.44        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      354   92.2%
 (0.5, 0.7]   (ok)         26    6.8%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.222  0.039   0.161    0.304  ...    54.0      69.0      38.0   1.00
pu        0.721  0.015   0.700    0.751  ...    67.0      57.0      39.0   1.03
mu        0.083  0.021   0.054    0.134  ...     9.0      17.0      14.0   1.09
mus       0.230  0.028   0.179    0.275  ...    81.0      86.0      69.0   1.00
gamma     0.440  0.071   0.330    0.580  ...   103.0     134.0      75.0   1.07
Is_begin  0.704  0.690   0.051    2.057  ...    71.0      56.0      60.0   1.00
Ia_begin  1.061  1.233   0.008    3.630  ...    24.0      20.0      59.0   1.08
E_begin   0.501  0.557   0.026    1.799  ...    75.0      65.0      83.0   1.02

[8 rows x 11 columns]