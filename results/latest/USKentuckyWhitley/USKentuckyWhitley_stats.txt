0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1264.69    40.06
p_loo       33.33        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      354   92.2%
 (0.5, 0.7]   (ok)         28    7.3%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.249  0.051   0.153    0.329  ...    46.0      47.0      43.0   1.07
pu        0.736  0.021   0.703    0.773  ...    51.0      51.0      39.0   1.00
mu        0.127  0.026   0.087    0.179  ...    49.0      49.0      58.0   1.02
mus       0.164  0.033   0.106    0.226  ...    65.0      87.0      51.0   1.03
gamma     0.166  0.034   0.114    0.227  ...    73.0      74.0      93.0   1.07
Is_begin  0.563  0.531   0.005    1.606  ...    78.0      57.0      56.0   1.07
Ia_begin  1.505  2.033   0.001    4.004  ...    45.0      43.0      30.0   1.05
E_begin   0.494  0.576   0.004    1.882  ...    71.0      58.0      57.0   1.01

[8 rows x 11 columns]