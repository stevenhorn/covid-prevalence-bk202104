0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1436.19    28.41
p_loo       30.17        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      355   92.4%
 (0.5, 0.7]   (ok)         23    6.0%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.244  0.045   0.166    0.315  ...   114.0     101.0      77.0   0.99
pu        0.740  0.031   0.700    0.794  ...    62.0      52.0      60.0   0.99
mu        0.116  0.018   0.086    0.146  ...    69.0      67.0      91.0   1.02
mus       0.183  0.029   0.130    0.244  ...   107.0     139.0      75.0   0.99
gamma     0.248  0.034   0.193    0.311  ...    92.0      85.0     100.0   1.00
Is_begin  1.962  1.450   0.088    4.631  ...   152.0      95.0      22.0   1.07
Ia_begin  5.477  2.938   0.408   10.125  ...   113.0     112.0      91.0   1.00
E_begin   5.132  4.089   0.140   13.266  ...    70.0      53.0      69.0   1.02

[8 rows x 11 columns]