0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1057.56    34.75
p_loo       24.59        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      361   94.0%
 (0.5, 0.7]   (ok)         19    4.9%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.244  0.048   0.165    0.329  ...    63.0      61.0     100.0   0.99
pu        0.771  0.021   0.732    0.804  ...    23.0      24.0      53.0   1.06
mu        0.121  0.020   0.086    0.166  ...    29.0      39.0      93.0   1.04
mus       0.186  0.036   0.124    0.243  ...    60.0      70.0      56.0   1.03
gamma     0.206  0.035   0.144    0.266  ...    40.0      45.0      46.0   1.06
Is_begin  0.733  0.739   0.002    2.301  ...    89.0      70.0      40.0   1.01
Ia_begin  1.376  1.324   0.049    3.450  ...   124.0     151.0      79.0   1.02
E_begin   0.515  0.476   0.040    1.575  ...    79.0      64.0      59.0   1.01

[8 rows x 11 columns]