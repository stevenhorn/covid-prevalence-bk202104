0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -568.41    60.39
p_loo       54.65        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      336   87.5%
 (0.5, 0.7]   (ok)         36    9.4%
   (0.7, 1]   (bad)         9    2.3%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.189  0.038   0.150    0.271  ...    48.0      41.0      60.0   1.02
pu        0.714  0.013   0.700    0.736  ...    61.0      67.0      58.0   1.04
mu        0.184  0.041   0.125    0.256  ...     6.0       6.0      25.0   1.34
mus       0.279  0.038   0.210    0.347  ...    29.0      33.0      61.0   1.06
gamma     0.325  0.050   0.256    0.447  ...    30.0      28.0      59.0   1.05
Is_begin  0.616  0.637   0.014    1.827  ...   103.0      64.0      60.0   0.99
Ia_begin  0.936  1.295   0.002    3.626  ...    73.0      54.0      42.0   1.02
E_begin   0.478  0.536   0.002    1.665  ...    57.0      41.0      60.0   1.02

[8 rows x 11 columns]