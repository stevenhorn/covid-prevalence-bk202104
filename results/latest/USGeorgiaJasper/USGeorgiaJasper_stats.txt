0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -997.56    68.79
p_loo       45.22        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      360   93.8%
 (0.5, 0.7]   (ok)         17    4.4%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.242  0.046   0.158    0.316  ...    63.0      63.0      31.0   0.99
pu        0.771  0.026   0.724    0.815  ...    38.0      39.0      96.0   1.03
mu        0.141  0.019   0.108    0.178  ...    44.0      45.0      60.0   1.02
mus       0.276  0.047   0.206    0.387  ...   152.0     140.0      74.0   1.04
gamma     0.407  0.064   0.319    0.533  ...    72.0      80.0      66.0   1.00
Is_begin  0.972  0.858   0.034    2.560  ...   125.0      78.0      40.0   1.00
Ia_begin  1.340  1.165   0.018    3.496  ...    75.0      59.0      72.0   1.01
E_begin   0.621  0.598   0.008    1.850  ...    57.0      30.0      38.0   1.01

[8 rows x 11 columns]