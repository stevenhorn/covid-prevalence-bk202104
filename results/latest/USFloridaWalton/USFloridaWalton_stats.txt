0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1391.65    51.66
p_loo       38.37        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      359   93.5%
 (0.5, 0.7]   (ok)         20    5.2%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.237  0.053   0.151    0.328  ...    29.0      27.0      43.0   1.04
pu        0.783  0.023   0.730    0.811  ...    20.0      37.0      16.0   1.07
mu        0.144  0.024   0.106    0.194  ...     8.0       8.0      15.0   1.24
mus       0.226  0.036   0.168    0.291  ...   106.0     127.0      86.0   0.99
gamma     0.243  0.050   0.148    0.329  ...    41.0      33.0      48.0   1.06
Is_begin  0.644  0.523   0.015    1.692  ...   113.0     100.0      57.0   1.01
Ia_begin  1.131  0.821   0.122    2.573  ...    75.0      65.0      74.0   1.04
E_begin   0.668  0.642   0.003    1.918  ...   101.0      82.0      60.0   0.99

[8 rows x 11 columns]