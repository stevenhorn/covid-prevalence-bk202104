0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1369.85    38.14
p_loo       33.15        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      350   91.1%
 (0.5, 0.7]   (ok)         28    7.3%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.244  0.056   0.156    0.338  ...    68.0      61.0      60.0   1.02
pu        0.792  0.044   0.706    0.846  ...     4.0       5.0      24.0   1.49
mu        0.117  0.022   0.080    0.158  ...    20.0      25.0      34.0   1.06
mus       0.165  0.039   0.100    0.237  ...    60.0     152.0      21.0   1.04
gamma     0.199  0.042   0.120    0.278  ...    86.0      83.0      87.0   1.00
Is_begin  1.371  0.888   0.239    2.693  ...    66.0      75.0      57.0   1.02
Ia_begin  3.429  2.706   0.030    8.428  ...    84.0      71.0      29.0   1.01
E_begin   2.094  1.833   0.231    5.631  ...    80.0      73.0      42.0   1.02

[8 rows x 11 columns]