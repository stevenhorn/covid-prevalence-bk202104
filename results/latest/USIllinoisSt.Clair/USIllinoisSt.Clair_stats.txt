0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1671.38    31.03
p_loo       25.17        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      352   91.7%
 (0.5, 0.7]   (ok)         23    6.0%
   (0.7, 1]   (bad)         9    2.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.238  0.054   0.152    0.322  ...    38.0      34.0      48.0   1.03
pu        0.789  0.030   0.718    0.827  ...     4.0       5.0      24.0   1.51
mu        0.101  0.017   0.068    0.130  ...     7.0       7.0      38.0   1.26
mus       0.171  0.038   0.112    0.245  ...    38.0      56.0      34.0   1.09
gamma     0.216  0.033   0.154    0.273  ...    75.0      76.0      69.0   1.09
Is_begin  1.545  1.268   0.135    3.804  ...    76.0      57.0      77.0   1.00
Ia_begin  4.207  3.287   0.061   10.989  ...    14.0      11.0      32.0   1.13
E_begin   3.387  3.671   0.012    9.152  ...    13.0       7.0      34.0   1.23

[8 rows x 11 columns]