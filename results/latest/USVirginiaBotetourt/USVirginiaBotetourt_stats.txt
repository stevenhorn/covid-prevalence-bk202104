0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1090.31    46.09
p_loo       42.92        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      348   90.6%
 (0.5, 0.7]   (ok)         29    7.6%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.256  0.052   0.175    0.339  ...   108.0      93.0      60.0   1.05
pu        0.834  0.025   0.787    0.876  ...    31.0      31.0      52.0   1.02
mu        0.133  0.024   0.086    0.174  ...    37.0      37.0      42.0   1.02
mus       0.194  0.032   0.143    0.251  ...   152.0     152.0      86.0   0.98
gamma     0.233  0.044   0.154    0.324  ...    65.0      61.0      48.0   1.01
Is_begin  0.923  0.828   0.037    2.687  ...   141.0     105.0      43.0   1.03
Ia_begin  2.005  1.683   0.045    5.587  ...   104.0      77.0      59.0   0.99
E_begin   1.000  0.972   0.016    3.178  ...   125.0      84.0      81.0   0.98

[8 rows x 11 columns]