0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1233.31    31.99
p_loo       30.63        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      353   91.9%
 (0.5, 0.7]   (ok)         20    5.2%
   (0.7, 1]   (bad)        11    2.9%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.222  0.047   0.154    0.321  ...    24.0      26.0      25.0   1.13
pu        0.722  0.017   0.701    0.748  ...   120.0     101.0      59.0   1.04
mu        0.132  0.021   0.102    0.171  ...    27.0      30.0      40.0   1.05
mus       0.173  0.030   0.117    0.223  ...    80.0      79.0      97.0   1.02
gamma     0.211  0.037   0.155    0.271  ...   126.0     135.0     100.0   0.99
Is_begin  0.787  0.618   0.038    1.889  ...    81.0      79.0      58.0   1.01
Ia_begin  1.242  1.268   0.005    3.685  ...    90.0      41.0      17.0   1.04
E_begin   0.504  0.500   0.001    1.630  ...    97.0      71.0      43.0   1.02

[8 rows x 11 columns]