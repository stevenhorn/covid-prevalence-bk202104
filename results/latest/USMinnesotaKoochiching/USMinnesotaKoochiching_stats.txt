0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -671.82    27.93
p_loo       29.28        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      350   91.1%
 (0.5, 0.7]   (ok)         26    6.8%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.257  0.054   0.173    0.349  ...    88.0      84.0      38.0   0.99
pu        0.783  0.038   0.702    0.830  ...    31.0      35.0      49.0   1.02
mu        0.176  0.035   0.119    0.232  ...    51.0      56.0      58.0   1.03
mus       0.221  0.036   0.165    0.287  ...    87.0      90.0      93.0   0.99
gamma     0.296  0.038   0.241    0.372  ...   104.0     105.0      40.0   1.01
Is_begin  0.624  0.645   0.018    1.954  ...    51.0      79.0      72.0   1.02
Ia_begin  1.045  1.062   0.008    3.101  ...   101.0      82.0      87.0   0.98
E_begin   0.467  0.601   0.002    1.493  ...    91.0      86.0      59.0   0.98

[8 rows x 11 columns]