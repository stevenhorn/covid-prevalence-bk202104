0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1679.83    48.97
p_loo       43.54        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      358   93.2%
 (0.5, 0.7]   (ok)         18    4.7%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    4    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.250  0.050   0.153    0.325  ...    46.0      47.0      58.0   1.03
pu        0.781  0.027   0.731    0.830  ...     9.0      10.0      14.0   1.16
mu        0.106  0.020   0.074    0.144  ...    19.0      18.0      40.0   1.08
mus       0.155  0.036   0.098    0.215  ...    26.0      27.0      60.0   1.04
gamma     0.172  0.030   0.115    0.229  ...    59.0      58.0      58.0   1.02
Is_begin  0.755  0.608   0.022    1.732  ...    47.0      38.0      43.0   1.03
Ia_begin  1.219  1.097   0.005    3.472  ...    56.0      44.0      22.0   1.04
E_begin   0.470  0.389   0.011    1.258  ...    51.0      21.0      59.0   1.07

[8 rows x 11 columns]