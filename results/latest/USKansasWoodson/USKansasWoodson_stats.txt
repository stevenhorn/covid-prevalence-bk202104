0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -572.04    40.69
p_loo       26.90        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      360   93.8%
 (0.5, 0.7]   (ok)         19    4.9%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.261  0.058   0.162    0.350  ...    96.0     117.0      38.0   1.06
pu        0.818  0.021   0.778    0.850  ...    73.0      73.0      59.0   1.03
mu        0.126  0.019   0.091    0.155  ...    71.0      74.0      59.0   1.00
mus       0.178  0.031   0.125    0.227  ...   103.0      92.0      88.0   1.01
gamma     0.207  0.037   0.159    0.286  ...   121.0     120.0      72.0   1.01
Is_begin  0.792  0.781   0.021    2.371  ...   113.0     105.0      97.0   1.00
Ia_begin  1.696  1.971   0.078    4.644  ...   100.0     152.0      93.0   0.98
E_begin   0.709  0.737   0.009    1.660  ...    93.0      63.0     100.0   1.00

[8 rows x 11 columns]