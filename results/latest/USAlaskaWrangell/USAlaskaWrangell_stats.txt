0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -102.85    36.02
p_loo       29.78        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      358   93.2%
 (0.5, 0.7]   (ok)         21    5.5%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.265  0.058   0.167    0.347  ...   152.0     152.0      88.0   1.01
pu        0.882  0.014   0.854    0.900  ...   103.0     106.0      88.0   1.04
mu        0.131  0.023   0.091    0.170  ...   152.0     152.0      76.0   0.99
mus       0.181  0.032   0.133    0.241  ...    84.0      70.0      35.0   1.01
gamma     0.211  0.035   0.147    0.268  ...   152.0     152.0      83.0   0.98
Is_begin  0.414  0.414   0.007    1.327  ...    77.0      69.0      19.0   1.09
Ia_begin  0.762  0.761   0.006    2.106  ...    89.0      91.0      93.0   0.99
E_begin   0.392  0.561   0.009    1.835  ...    92.0      96.0      91.0   1.03

[8 rows x 11 columns]