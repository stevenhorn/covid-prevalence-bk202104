0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -955.31    42.88
p_loo       34.59        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      365   95.1%
 (0.5, 0.7]   (ok)         14    3.6%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.261  0.054   0.168    0.334  ...    81.0      71.0      60.0   1.05
pu        0.750  0.031   0.704    0.800  ...    80.0      72.0      43.0   1.03
mu        0.128  0.026   0.089    0.185  ...    45.0      49.0      58.0   1.04
mus       0.192  0.036   0.134    0.252  ...   110.0     105.0      59.0   0.99
gamma     0.248  0.043   0.180    0.320  ...   144.0     144.0      39.0   1.05
Is_begin  0.558  0.559   0.008    1.559  ...    72.0      55.0      59.0   1.03
Ia_begin  1.002  1.135   0.007    3.470  ...    66.0      52.0      60.0   1.00
E_begin   0.420  0.565   0.002    1.197  ...    99.0      84.0      97.0   1.01

[8 rows x 11 columns]