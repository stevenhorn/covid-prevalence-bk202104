0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1125.58    33.46
p_loo       31.97        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      348   90.6%
 (0.5, 0.7]   (ok)         28    7.3%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.262  0.055   0.174    0.348  ...   103.0     116.0      56.0   1.02
pu        0.783  0.030   0.726    0.831  ...    73.0      83.0      56.0   1.00
mu        0.140  0.030   0.089    0.198  ...    47.0      46.0      58.0   1.00
mus       0.176  0.034   0.118    0.234  ...    67.0      76.0      31.0   0.98
gamma     0.223  0.037   0.161    0.284  ...   131.0     122.0      58.0   0.99
Is_begin  0.653  0.566   0.008    1.657  ...   100.0      79.0      66.0   0.99
Ia_begin  1.461  1.549   0.008    4.309  ...   122.0     101.0      60.0   1.01
E_begin   0.608  0.655   0.005    1.861  ...    44.0      31.0      22.0   1.06

[8 rows x 11 columns]