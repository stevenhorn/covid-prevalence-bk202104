0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -2195.61    65.15
p_loo       52.11        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      357   93.0%
 (0.5, 0.7]   (ok)         13    3.4%
   (0.7, 1]   (bad)        12    3.1%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.248  0.057   0.158    0.341  ...     8.0       8.0      32.0   1.26
pu        0.740  0.027   0.703    0.793  ...    33.0      35.0      46.0   1.06
mu        0.129  0.019   0.091    0.158  ...     7.0       7.0      41.0   1.26
mus       0.281  0.047   0.212    0.364  ...    16.0      15.0      80.0   1.11
gamma     0.449  0.072   0.338    0.593  ...    59.0      72.0      72.0   1.03
Is_begin  0.925  0.920   0.006    2.515  ...    42.0      11.0      35.0   1.14
Ia_begin  1.505  1.370   0.049    3.539  ...    83.0      52.0      43.0   1.02
E_begin   0.710  0.815   0.004    2.594  ...    48.0      25.0      57.0   1.08

[8 rows x 11 columns]