0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1640.09    45.69
p_loo       38.43        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      348   90.6%
 (0.5, 0.7]   (ok)         28    7.3%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.237  0.050   0.151    0.323  ...    98.0      98.0      56.0   1.02
pu        0.755  0.032   0.702    0.807  ...    14.0      13.0      38.0   1.14
mu        0.152  0.021   0.119    0.189  ...    19.0      19.0      33.0   1.05
mus       0.247  0.037   0.201    0.322  ...   121.0     120.0      88.0   1.02
gamma     0.372  0.058   0.263    0.483  ...   152.0     152.0     100.0   1.01
Is_begin  2.704  2.200   0.073    6.268  ...    85.0      83.0      79.0   1.00
Ia_begin  5.396  4.028   0.386   13.004  ...    82.0      56.0      59.0   1.02
E_begin   4.585  4.298   0.020   13.248  ...    79.0      59.0      57.0   0.98

[8 rows x 11 columns]