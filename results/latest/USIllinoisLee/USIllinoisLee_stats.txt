0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1172.83    30.38
p_loo       31.22        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      336   87.5%
 (0.5, 0.7]   (ok)         37    9.6%
   (0.7, 1]   (bad)         9    2.3%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.236  0.054   0.153    0.331  ...    27.0      28.0      32.0   1.11
pu        0.746  0.023   0.703    0.783  ...    36.0      37.0      21.0   1.09
mu        0.128  0.023   0.089    0.178  ...    40.0      39.0      20.0   1.06
mus       0.171  0.038   0.104    0.234  ...   123.0     105.0      67.0   1.01
gamma     0.208  0.041   0.136    0.279  ...   152.0     152.0      64.0   1.01
Is_begin  0.635  0.491   0.005    1.500  ...    74.0      59.0      39.0   1.03
Ia_begin  1.320  1.384   0.044    3.365  ...    64.0      59.0      49.0   1.01
E_begin   0.660  0.966   0.006    2.006  ...    89.0      54.0      46.0   1.03

[8 rows x 11 columns]