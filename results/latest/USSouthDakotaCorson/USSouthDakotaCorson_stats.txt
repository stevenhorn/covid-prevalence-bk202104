0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -618.16    38.25
p_loo       29.66        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      369   96.1%
 (0.5, 0.7]   (ok)         12    3.1%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.253  0.055   0.154    0.330  ...   105.0     106.0     100.0   1.02
pu        0.780  0.020   0.743    0.817  ...    68.0      68.0      38.0   1.05
mu        0.117  0.021   0.086    0.157  ...    88.0      89.0      57.0   0.99
mus       0.172  0.029   0.135    0.239  ...    43.0      57.0      45.0   1.04
gamma     0.192  0.038   0.123    0.243  ...    52.0      60.0      59.0   1.00
Is_begin  0.473  0.435   0.001    1.199  ...    97.0      53.0      16.0   1.05
Ia_begin  0.732  0.736   0.000    2.344  ...   123.0     152.0      91.0   1.04
E_begin   0.276  0.218   0.018    0.750  ...    42.0      26.0      56.0   1.06

[8 rows x 11 columns]