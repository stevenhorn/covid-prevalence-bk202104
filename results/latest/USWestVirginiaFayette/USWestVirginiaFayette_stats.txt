0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1100.30    46.71
p_loo       41.54        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      350   91.1%
 (0.5, 0.7]   (ok)         28    7.3%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.245  0.063   0.161    0.350  ...    85.0      79.0      30.0   0.98
pu        0.818  0.024   0.781    0.874  ...    65.0      64.0      54.0   1.00
mu        0.147  0.026   0.111    0.200  ...    57.0      55.0      91.0   1.03
mus       0.198  0.039   0.144    0.283  ...   110.0     152.0      83.0   1.00
gamma     0.258  0.051   0.179    0.348  ...   112.0     103.0      63.0   1.00
Is_begin  0.653  0.575   0.016    1.521  ...   106.0     122.0      74.0   1.02
Ia_begin  1.460  1.494   0.034    4.792  ...   106.0     105.0      49.0   1.00
E_begin   0.666  0.804   0.011    2.518  ...    86.0      43.0      51.0   1.03

[8 rows x 11 columns]