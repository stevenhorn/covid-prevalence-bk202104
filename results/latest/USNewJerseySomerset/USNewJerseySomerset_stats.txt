0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1611.36    22.62
p_loo       25.95        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      337   87.8%
 (0.5, 0.7]   (ok)         43   11.2%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    0    0.0%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.234   0.051   0.165    0.316  ...   152.0     152.0      57.0   1.00
pu         0.742   0.031   0.700    0.796  ...    66.0      56.0      37.0   1.02
mu         0.107   0.019   0.078    0.143  ...    28.0      28.0      59.0   1.04
mus        0.184   0.031   0.134    0.225  ...   127.0     129.0      60.0   1.01
gamma      0.295   0.045   0.228    0.379  ...   152.0     152.0      45.0   1.03
Is_begin   8.505   7.717   0.001   22.320  ...    13.0       7.0      20.0   1.27
Ia_begin  33.511  10.842  12.348   52.207  ...    28.0      39.0      60.0   1.05
E_begin   51.534  22.264  11.229   91.892  ...    98.0      92.0      53.0   0.99

[8 rows x 11 columns]