0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -939.47    31.54
p_loo       25.86        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      359   93.5%
 (0.5, 0.7]   (ok)         16    4.2%
   (0.7, 1]   (bad)         9    2.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.250  0.055   0.165    0.338  ...    61.0      55.0      59.0   1.01
pu        0.831  0.020   0.789    0.857  ...   103.0     106.0      95.0   1.00
mu        0.144  0.016   0.114    0.167  ...    36.0      37.0      40.0   1.05
mus       0.192  0.027   0.141    0.235  ...    90.0     101.0      60.0   1.03
gamma     0.289  0.046   0.218    0.378  ...    54.0      54.0      42.0   1.02
Is_begin  1.454  0.864   0.107    3.103  ...    93.0      93.0      65.0   1.00
Ia_begin  3.632  2.760   0.576    9.138  ...    81.0      78.0      96.0   1.01
E_begin   1.870  1.874   0.003    5.473  ...    58.0      34.0      59.0   1.05

[8 rows x 11 columns]