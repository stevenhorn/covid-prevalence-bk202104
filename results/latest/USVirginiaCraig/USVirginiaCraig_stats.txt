0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -529.63    33.96
p_loo       33.61        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      357   93.0%
 (0.5, 0.7]   (ok)         21    5.5%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.270  0.054   0.175    0.350  ...    80.0      65.0      39.0   1.02
pu        0.879  0.013   0.855    0.900  ...    70.0      83.0      87.0   0.99
mu        0.131  0.023   0.094    0.170  ...    56.0      60.0      45.0   0.99
mus       0.173  0.034   0.119    0.241  ...   108.0     131.0      91.0   0.99
gamma     0.202  0.038   0.136    0.270  ...   112.0     120.0      88.0   1.00
Is_begin  0.738  0.680   0.011    2.005  ...    96.0     109.0      58.0   1.05
Ia_begin  1.512  1.334   0.032    4.110  ...    47.0      74.0      39.0   1.00
E_begin   0.702  0.694   0.043    1.993  ...   100.0      97.0      81.0   0.98

[8 rows x 11 columns]