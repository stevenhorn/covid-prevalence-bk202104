0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -767.26    27.12
p_loo       26.14        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      356   92.7%
 (0.5, 0.7]   (ok)         25    6.5%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.283  0.043   0.211    0.348  ...    94.0      93.0      59.0   1.06
pu        0.884  0.015   0.862    0.899  ...    97.0      69.0      67.0   1.01
mu        0.140  0.025   0.102    0.186  ...    35.0      36.0      43.0   1.04
mus       0.166  0.030   0.109    0.214  ...    86.0     100.0      59.0   0.99
gamma     0.194  0.032   0.145    0.257  ...   152.0     152.0     100.0   0.99
Is_begin  0.806  0.730   0.020    1.995  ...   123.0      94.0      60.0   0.98
Ia_begin  1.828  1.620   0.021    4.778  ...   128.0     107.0      59.0   1.00
E_begin   0.903  0.980   0.001    2.849  ...   116.0      56.0      40.0   1.03

[8 rows x 11 columns]