0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1431.64    27.27
p_loo       26.68        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      351   91.4%
 (0.5, 0.7]   (ok)         26    6.8%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.243  0.061   0.156    0.342  ...    40.0      32.0      35.0   1.01
pu        0.804  0.029   0.753    0.849  ...    45.0      45.0      59.0   1.02
mu        0.113  0.018   0.076    0.139  ...    25.0      31.0      46.0   1.07
mus       0.152  0.029   0.102    0.199  ...    79.0      62.0      22.0   1.04
gamma     0.214  0.052   0.129    0.302  ...    98.0     122.0      59.0   1.08
Is_begin  1.211  1.038   0.014    3.304  ...    84.0      91.0      49.0   1.07
Ia_begin  2.656  2.096   0.032    6.502  ...    49.0      34.0      60.0   1.02
E_begin   1.596  1.751   0.018    4.137  ...    44.0      27.0      59.0   1.05

[8 rows x 11 columns]