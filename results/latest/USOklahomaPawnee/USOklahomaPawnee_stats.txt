0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -995.00    40.14
p_loo       32.80        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      353   91.9%
 (0.5, 0.7]   (ok)         23    6.0%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.250  0.051   0.173    0.349  ...    36.0      34.0      37.0   1.05
pu        0.751  0.021   0.707    0.785  ...    36.0      39.0      19.0   1.06
mu        0.130  0.024   0.088    0.167  ...    27.0      25.0      48.0   1.07
mus       0.208  0.038   0.137    0.286  ...   106.0     112.0      21.0   1.03
gamma     0.251  0.049   0.180    0.347  ...   120.0     109.0      93.0   1.02
Is_begin  1.132  0.940   0.106    3.164  ...    83.0      89.0      60.0   1.02
Ia_begin  0.570  0.501   0.005    1.495  ...    47.0      32.0      37.0   1.03
E_begin   0.635  0.832   0.023    2.004  ...    41.0      20.0      65.0   1.07

[8 rows x 11 columns]