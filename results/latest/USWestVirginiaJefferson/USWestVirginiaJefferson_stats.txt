0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1120.48    25.71
p_loo       30.80        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      353   91.9%
 (0.5, 0.7]   (ok)         27    7.0%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.244  0.052   0.152    0.329  ...   132.0     128.0      31.0   1.00
pu        0.786  0.033   0.728    0.838  ...    40.0      43.0      21.0   1.10
mu        0.135  0.023   0.097    0.185  ...    51.0      52.0      43.0   1.00
mus       0.183  0.034   0.117    0.230  ...    77.0      88.0      60.0   1.01
gamma     0.238  0.048   0.159    0.342  ...   118.0     152.0      75.0   1.02
Is_begin  1.196  0.978   0.032    2.988  ...    27.0      24.0      43.0   1.08
Ia_begin  0.632  0.527   0.017    1.689  ...    78.0      47.0      31.0   0.99
E_begin   0.737  0.656   0.018    1.886  ...    17.0      12.0      57.0   1.14

[8 rows x 11 columns]