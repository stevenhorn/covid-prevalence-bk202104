0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1110.29    70.76
p_loo       54.25        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      365   95.1%
 (0.5, 0.7]   (ok)         15    3.9%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.202  0.038   0.150    0.276  ...    70.0      59.0      30.0   1.01
pu        0.716  0.012   0.701    0.738  ...   129.0     113.0      58.0   1.00
mu        0.126  0.022   0.087    0.173  ...    35.0      31.0      49.0   1.02
mus       0.254  0.049   0.150    0.328  ...   152.0     152.0      88.0   1.03
gamma     0.438  0.074   0.304    0.581  ...    47.0      46.0      53.0   1.05
Is_begin  0.830  0.854   0.041    2.621  ...    93.0      96.0      83.0   1.00
Ia_begin  0.981  0.931   0.029    2.795  ...   121.0     152.0      59.0   1.07
E_begin   0.569  0.612   0.002    1.956  ...   131.0     110.0      59.0   1.00

[8 rows x 11 columns]