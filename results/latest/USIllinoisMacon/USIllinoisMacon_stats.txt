0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1354.30    34.12
p_loo       35.31        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      355   92.4%
 (0.5, 0.7]   (ok)         23    6.0%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.260  0.047   0.180    0.345  ...   152.0     152.0      88.0   1.01
pu        0.782  0.028   0.723    0.824  ...    50.0      55.0      53.0   0.99
mu        0.145  0.028   0.098    0.188  ...     7.0       7.0      19.0   1.30
mus       0.194  0.035   0.131    0.254  ...    34.0      60.0      74.0   1.04
gamma     0.271  0.046   0.203    0.358  ...    79.0      59.0      49.0   1.05
Is_begin  0.821  0.810   0.011    2.246  ...    84.0      70.0      59.0   1.03
Ia_begin  1.801  1.650   0.009    5.188  ...    86.0      85.0      43.0   0.99
E_begin   0.859  0.932   0.006    3.174  ...    66.0      63.0      56.0   1.01

[8 rows x 11 columns]