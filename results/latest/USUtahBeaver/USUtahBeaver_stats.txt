0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo   687.48   130.86
p_loo       86.29        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      370   96.4%
 (0.5, 0.7]   (ok)          5    1.3%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    5    1.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.263  0.053   0.178    0.343  ...   152.0     135.0      29.0   1.01
pu        0.868  0.029   0.808    0.900  ...    47.0      44.0      60.0   1.00
mu        0.181  0.037   0.127    0.258  ...    64.0      77.0      59.0   1.01
mus       0.434  0.061   0.318    0.531  ...   152.0     152.0      60.0   1.02
gamma     0.456  0.071   0.347    0.590  ...   152.0     152.0      59.0   1.04
Is_begin  0.387  0.400   0.004    1.139  ...   113.0     101.0      48.0   1.01
Ia_begin  0.409  0.429   0.001    1.169  ...    95.0      81.0      40.0   1.01
E_begin   0.161  0.180   0.001    0.517  ...    81.0      61.0      60.0   1.03

[8 rows x 11 columns]