1 Divergences 
Passed validation 
Computed from 80 by 382 log-likelihood matrix

         Estimate       SE
elpd_loo  -570.61    37.87
p_loo       27.29        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      355   92.9%
 (0.5, 0.7]   (ok)         16    4.2%
   (0.7, 1]   (bad)         8    2.1%
   (1, Inf)   (very bad)    3    0.8%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.275   0.049   0.188    0.345  ...    54.0      59.0      40.0   1.04
pu         0.884   0.017   0.852    0.900  ...    45.0      46.0      14.0   1.00
mu         0.167   0.032   0.113    0.226  ...    13.0      14.0      58.0   1.15
mus        0.218   0.038   0.155    0.279  ...    68.0      56.0      38.0   1.03
gamma      0.301   0.049   0.229    0.389  ...    80.0      82.0      96.0   1.00
Is_begin   3.856   3.728   0.034   11.510  ...    75.0      39.0      32.0   1.05
Ia_begin  16.486  15.876   0.174   49.650  ...    35.0      24.0      29.0   1.05
E_begin    4.266   3.935   0.008   12.445  ...    53.0      62.0      60.0   1.01

[8 rows x 11 columns]