0 Divergences 
Passed validation 
Computed from 80 by 385 log-likelihood matrix

         Estimate       SE
elpd_loo -2361.66    25.57
p_loo       24.35        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      347   90.1%
 (0.5, 0.7]   (ok)         32    8.3%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.260  0.053   0.165    0.342  ...   152.0     152.0      54.0   1.01
pu        0.831  0.048   0.737    0.897  ...    26.0      25.0      48.0   1.04
mu        0.141  0.041   0.088    0.216  ...     3.0       4.0      22.0   1.69
mus       0.174  0.038   0.099    0.240  ...    31.0      33.0      40.0   1.05
gamma     0.213  0.041   0.138    0.272  ...    57.0      49.0      54.0   1.01
Is_begin  2.213  1.677   0.066    5.060  ...    34.0      19.0      15.0   1.10
Ia_begin  4.958  3.358   0.348   10.346  ...    39.0      31.0      36.0   1.03
E_begin   4.626  4.380   0.171   14.016  ...    34.0      28.0      15.0   1.06

[8 rows x 11 columns]