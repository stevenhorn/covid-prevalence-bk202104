0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1076.58    50.34
p_loo       47.76        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      357   93.0%
 (0.5, 0.7]   (ok)         21    5.5%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    4    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.247  0.060   0.154    0.340  ...    62.0      57.0      40.0   1.02
pu        0.735  0.026   0.700    0.776  ...    35.0      33.0      30.0   1.05
mu        0.145  0.019   0.112    0.182  ...    25.0      26.0      60.0   1.06
mus       0.224  0.036   0.163    0.301  ...    47.0      51.0      60.0   1.03
gamma     0.337  0.049   0.255    0.414  ...    63.0      70.0      60.0   1.01
Is_begin  0.414  0.537   0.004    1.510  ...    24.0      30.0      44.0   1.01
Ia_begin  0.776  0.942   0.006    2.882  ...    25.0      21.0      19.0   1.09
E_begin   0.320  0.334   0.005    0.983  ...    23.0      23.0      56.0   1.06

[8 rows x 11 columns]