0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1054.22    46.47
p_loo       44.43        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      355   92.4%
 (0.5, 0.7]   (ok)         22    5.7%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.263  0.047   0.190    0.345  ...    36.0      39.0      58.0   1.01
pu        0.754  0.028   0.709    0.801  ...    29.0      28.0      15.0   1.01
mu        0.140  0.022   0.103    0.181  ...    21.0      22.0      55.0   1.10
mus       0.201  0.036   0.132    0.259  ...    53.0      60.0      34.0   1.04
gamma     0.241  0.044   0.164    0.319  ...   142.0     152.0      34.0   0.98
Is_begin  0.758  0.782   0.024    2.264  ...    89.0      74.0      60.0   1.05
Ia_begin  1.203  1.157   0.009    2.724  ...    97.0      57.0      59.0   1.01
E_begin   0.607  0.723   0.001    1.783  ...   101.0      92.0      60.0   0.99

[8 rows x 11 columns]