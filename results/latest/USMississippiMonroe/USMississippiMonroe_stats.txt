0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1164.73    24.18
p_loo       24.47        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      365   95.1%
 (0.5, 0.7]   (ok)         16    4.2%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.250  0.052   0.160    0.321  ...    87.0      84.0      59.0   0.99
pu        0.761  0.028   0.704    0.800  ...     8.0       8.0      54.0   1.21
mu        0.111  0.017   0.080    0.139  ...    44.0      45.0      59.0   1.01
mus       0.165  0.025   0.122    0.215  ...   152.0     152.0      88.0   1.02
gamma     0.202  0.034   0.152    0.258  ...   152.0     152.0      74.0   1.11
Is_begin  1.074  0.741   0.002    2.623  ...   152.0     152.0      88.0   1.00
Ia_begin  0.752  0.597   0.039    1.812  ...   152.0     152.0      95.0   1.02
E_begin   0.714  0.719   0.005    2.281  ...    70.0      57.0      81.0   0.99

[8 rows x 11 columns]