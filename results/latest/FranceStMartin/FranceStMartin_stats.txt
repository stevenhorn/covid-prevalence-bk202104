0 Divergences 
Passed validation 
Computed from 80 by 385 log-likelihood matrix

         Estimate       SE
elpd_loo -1356.48    47.33
p_loo       41.61        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      359   93.2%
 (0.5, 0.7]   (ok)         20    5.2%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.236  0.055   0.151    0.323  ...    18.0      16.0      20.0   1.12
pu        0.818  0.023   0.756    0.845  ...    26.0      28.0      60.0   1.04
mu        0.122  0.022   0.088    0.159  ...    10.0      12.0      40.0   1.15
mus       0.166  0.029   0.112    0.216  ...    64.0      60.0      56.0   1.04
gamma     0.208  0.033   0.144    0.262  ...    97.0      93.0      59.0   1.03
Is_begin  1.058  0.977   0.063    2.583  ...   129.0     152.0      77.0   0.98
Ia_begin  0.574  0.466   0.002    1.388  ...    24.0      23.0      40.0   1.10
E_begin   0.578  0.647   0.023    1.882  ...    90.0     108.0     100.0   0.99

[8 rows x 11 columns]