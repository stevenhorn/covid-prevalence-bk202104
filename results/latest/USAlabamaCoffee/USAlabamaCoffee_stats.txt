2 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1315.62    39.33
p_loo       34.92        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      352   91.7%
 (0.5, 0.7]   (ok)         18    4.7%
   (0.7, 1]   (bad)        13    3.4%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.252  0.059   0.163    0.349  ...    48.0      59.0      38.0   1.07
pu        0.798  0.028   0.725    0.835  ...    14.0      18.0      22.0   1.16
mu        0.109  0.018   0.083    0.152  ...    46.0      58.0      22.0   1.08
mus       0.169  0.028   0.132    0.227  ...    58.0      55.0      50.0   1.05
gamma     0.193  0.033   0.133    0.246  ...   107.0     115.0      96.0   1.00
Is_begin  1.043  1.008   0.002    3.227  ...   105.0      76.0      43.0   1.01
Ia_begin  1.996  2.206   0.004    5.594  ...    74.0      37.0      14.0   1.03
E_begin   1.197  1.716   0.008    5.774  ...    52.0      13.0      47.0   1.12

[8 rows x 11 columns]