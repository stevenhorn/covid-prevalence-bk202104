0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1126.36    33.44
p_loo       30.83        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      366   95.3%
 (0.5, 0.7]   (ok)         14    3.6%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.234  0.047   0.150    0.305  ...    72.0      68.0      53.0   1.06
pu        0.738  0.022   0.703    0.775  ...    93.0      86.0      60.0   0.99
mu        0.130  0.026   0.090    0.180  ...    40.0      35.0      59.0   1.06
mus       0.182  0.030   0.136    0.238  ...    92.0      90.0      96.0   1.03
gamma     0.218  0.040   0.142    0.274  ...   111.0     126.0      60.0   1.03
Is_begin  0.731  0.633   0.059    2.134  ...    96.0      90.0     100.0   1.01
Ia_begin  0.596  0.539   0.016    1.682  ...   102.0     110.0      69.0   1.06
E_begin   0.486  0.623   0.010    1.744  ...   106.0      64.0      72.0   1.00

[8 rows x 11 columns]