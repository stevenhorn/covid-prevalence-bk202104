2 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1689.19    58.47
p_loo       36.14        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      341   88.8%
 (0.5, 0.7]   (ok)         36    9.4%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.254  0.050   0.169    0.341  ...    87.0      72.0      35.0   1.01
pu        0.777  0.031   0.719    0.828  ...    39.0      35.0      59.0   1.06
mu        0.113  0.020   0.077    0.150  ...    66.0      68.0      38.0   1.02
mus       0.190  0.039   0.119    0.256  ...   110.0     102.0      38.0   1.11
gamma     0.240  0.041   0.175    0.326  ...    70.0     115.0      96.0   1.00
Is_begin  0.775  0.560   0.039    1.964  ...   145.0     108.0      80.0   1.04
Ia_begin  1.587  1.151   0.034    3.927  ...    96.0      95.0      33.0   0.98
E_begin   1.264  1.042   0.005    3.415  ...   141.0     152.0      57.0   1.05

[8 rows x 11 columns]