0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -700.49    60.18
p_loo       57.35        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      362   94.3%
 (0.5, 0.7]   (ok)         16    4.2%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.250  0.045   0.152    0.320  ...    59.0      55.0      38.0   1.06
pu        0.750  0.027   0.700    0.794  ...    37.0      36.0      38.0   1.03
mu        0.124  0.024   0.081    0.167  ...    50.0      65.0      59.0   1.02
mus       0.190  0.033   0.140    0.261  ...    74.0      77.0      60.0   1.04
gamma     0.227  0.045   0.163    0.309  ...    45.0      77.0      58.0   1.06
Is_begin  0.594  0.638   0.013    1.757  ...    90.0      54.0      43.0   1.01
Ia_begin  0.960  1.026   0.004    3.130  ...    97.0      73.0      24.0   0.99
E_begin   0.562  0.612   0.008    1.477  ...   103.0      91.0      97.0   1.00

[8 rows x 11 columns]