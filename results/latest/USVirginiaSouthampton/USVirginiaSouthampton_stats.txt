0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1223.44    49.37
p_loo       52.36        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      349   90.9%
 (0.5, 0.7]   (ok)         24    6.2%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    4    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.218  0.046   0.152    0.299  ...    27.0      26.0      93.0   1.09
pu        0.722  0.018   0.700    0.752  ...    52.0      31.0      93.0   1.05
mu        0.161  0.020   0.129    0.199  ...     8.0       8.0      17.0   1.23
mus       0.237  0.039   0.163    0.302  ...    55.0      54.0      26.0   1.06
gamma     0.420  0.065   0.300    0.523  ...    87.0      71.0      58.0   1.01
Is_begin  0.607  0.702   0.004    1.607  ...    94.0      87.0      59.0   1.05
Ia_begin  0.855  1.222   0.005    2.876  ...    84.0      62.0      40.0   1.02
E_begin   0.579  0.958   0.000    1.674  ...    88.0      60.0      18.0   1.01

[8 rows x 11 columns]