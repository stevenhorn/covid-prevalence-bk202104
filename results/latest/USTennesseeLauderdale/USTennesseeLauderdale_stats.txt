0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1242.12    51.80
p_loo       31.37        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      354   92.2%
 (0.5, 0.7]   (ok)         22    5.7%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    4    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.251  0.046   0.177    0.343  ...    70.0      68.0      93.0   1.01
pu        0.762  0.029   0.719    0.815  ...    52.0      47.0      34.0   1.06
mu        0.125  0.023   0.085    0.169  ...    26.0      24.0      86.0   1.05
mus       0.184  0.035   0.132    0.258  ...    74.0      61.0      59.0   1.01
gamma     0.218  0.041   0.155    0.293  ...    89.0      85.0      91.0   1.09
Is_begin  0.821  0.843   0.028    2.592  ...    78.0      91.0      24.0   1.00
Ia_begin  1.275  1.230   0.011    3.208  ...    93.0      92.0      40.0   0.99
E_begin   0.610  0.748   0.003    2.316  ...    81.0      65.0      60.0   1.03

[8 rows x 11 columns]