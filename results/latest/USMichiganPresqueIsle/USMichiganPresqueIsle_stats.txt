0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -800.48    28.70
p_loo       26.99        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      356   92.7%
 (0.5, 0.7]   (ok)         23    6.0%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.250  0.056   0.166    0.349  ...    20.0      23.0      31.0   1.08
pu        0.822  0.023   0.773    0.855  ...    17.0      16.0      56.0   1.11
mu        0.127  0.019   0.095    0.158  ...    65.0      63.0      59.0   1.01
mus       0.159  0.030   0.107    0.216  ...   152.0     152.0      47.0   0.99
gamma     0.229  0.044   0.157    0.313  ...   152.0     152.0      95.0   0.99
Is_begin  0.683  0.652   0.005    2.130  ...    99.0      56.0      42.0   1.01
Ia_begin  1.454  1.467   0.008    4.982  ...   152.0     152.0      69.0   1.01
E_begin   0.713  1.213   0.001    2.848  ...   102.0      68.0      88.0   1.00

[8 rows x 11 columns]