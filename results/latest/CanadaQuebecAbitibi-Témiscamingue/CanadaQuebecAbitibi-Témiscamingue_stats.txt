0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -773.56    27.28
p_loo       30.97        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      353   91.9%
 (0.5, 0.7]   (ok)         23    6.0%
   (0.7, 1]   (bad)         8    2.1%
   (1, Inf)   (very bad)    0    0.0%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.239   0.050   0.164    0.317  ...    89.0      75.0      80.0   1.03
pu         0.769   0.042   0.709    0.844  ...     6.0       7.0      23.0   1.24
mu         0.189   0.040   0.125    0.262  ...     4.0       4.0      20.0   1.63
mus        0.305   0.038   0.237    0.366  ...    52.0      53.0      45.0   1.02
gamma      0.464   0.062   0.342    0.553  ...    88.0      91.0      99.0   1.03
Is_begin   7.934   6.692   0.195   22.089  ...   106.0      73.0      58.0   1.10
Ia_begin  40.426  28.075   0.879   95.915  ...    64.0      46.0      35.0   1.01
E_begin   14.055  11.638   0.091   39.329  ...   111.0      69.0      40.0   1.06

[8 rows x 11 columns]