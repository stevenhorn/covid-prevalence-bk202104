0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -382.81    34.41
p_loo       36.24        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      361   94.0%
 (0.5, 0.7]   (ok)         14    3.6%
   (0.7, 1]   (bad)         8    2.1%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.265  0.055   0.171    0.350  ...    19.0      14.0      18.0   1.14
pu        0.865  0.017   0.836    0.894  ...    21.0      20.0      20.0   1.11
mu        0.152  0.027   0.115    0.207  ...     8.0       7.0      69.0   1.23
mus       0.174  0.031   0.124    0.237  ...    49.0      53.0      38.0   1.00
gamma     0.209  0.053   0.115    0.313  ...    21.0      22.0      58.0   1.06
Is_begin  0.711  0.620   0.014    1.785  ...    67.0      77.0      58.0   0.99
Ia_begin  1.274  1.250   0.001    3.835  ...    85.0      47.0      42.0   1.00
E_begin   0.643  0.709   0.003    2.329  ...    70.0      54.0      59.0   1.01

[8 rows x 11 columns]