0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -566.90    32.11
p_loo       28.18        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      357   93.0%
 (0.5, 0.7]   (ok)         23    6.0%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.248  0.050   0.167    0.330  ...    49.0      50.0      37.0   1.03
pu        0.820  0.023   0.780    0.855  ...    11.0      10.0      60.0   1.16
mu        0.133  0.025   0.091    0.178  ...    17.0      18.0      83.0   1.10
mus       0.184  0.034   0.136    0.267  ...    47.0      51.0      56.0   1.05
gamma     0.213  0.049   0.137    0.315  ...    74.0      99.0      39.0   1.03
Is_begin  0.364  0.459   0.000    1.135  ...    71.0      38.0      14.0   1.02
Ia_begin  0.666  0.896   0.006    2.779  ...    76.0      82.0      91.0   0.99
E_begin   0.347  0.461   0.004    1.423  ...    71.0      58.0      91.0   1.00

[8 rows x 11 columns]