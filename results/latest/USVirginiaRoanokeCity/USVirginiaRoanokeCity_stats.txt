0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1494.51    27.95
p_loo       23.63        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      361   94.0%
 (0.5, 0.7]   (ok)         19    4.9%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.259  0.045   0.177    0.328  ...    67.0      69.0      42.0   1.00
pu        0.818  0.026   0.767    0.853  ...    15.0      12.0      54.0   1.14
mu        0.115  0.026   0.076    0.159  ...    19.0      16.0      40.0   1.11
mus       0.149  0.027   0.099    0.196  ...    54.0      52.0      30.0   1.01
gamma     0.161  0.030   0.117    0.216  ...    96.0      96.0     100.0   1.00
Is_begin  0.892  0.814   0.022    2.708  ...   142.0     118.0     100.0   1.02
Ia_begin  2.082  1.927   0.042    5.899  ...    90.0      85.0      59.0   1.01
E_begin   0.999  0.997   0.000    2.767  ...    56.0      52.0      29.0   1.04

[8 rows x 11 columns]