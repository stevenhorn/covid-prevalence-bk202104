0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1175.11    54.92
p_loo       32.29        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      358   93.2%
 (0.5, 0.7]   (ok)         20    5.2%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.253  0.059   0.166    0.350  ...    75.0      65.0      14.0   1.11
pu        0.759  0.027   0.713    0.807  ...    61.0      62.0      45.0   1.06
mu        0.116  0.019   0.078    0.147  ...    23.0      24.0      59.0   1.08
mus       0.253  0.039   0.172    0.302  ...   107.0     101.0      59.0   1.00
gamma     0.330  0.065   0.265    0.517  ...   129.0     130.0      83.0   0.99
Is_begin  0.837  0.661   0.006    1.626  ...   100.0      68.0      60.0   0.98
Ia_begin  1.431  1.449   0.003    3.879  ...    90.0      79.0      60.0   0.99
E_begin   0.620  0.579   0.024    1.784  ...    79.0      67.0      57.0   1.00

[8 rows x 11 columns]