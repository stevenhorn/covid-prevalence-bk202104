0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -2111.26    32.93
p_loo       24.83        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      353   91.9%
 (0.5, 0.7]   (ok)         23    6.0%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    2    0.5%

             mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.221   0.048   0.151    0.318  ...   102.0     152.0      59.0   1.10
pu          0.724   0.022   0.700    0.765  ...    53.0      51.0      49.0   1.04
mu          0.104   0.019   0.080    0.145  ...    29.0      23.0      74.0   1.06
mus         0.167   0.027   0.121    0.215  ...    82.0     100.0      75.0   1.01
gamma       0.333   0.054   0.228    0.409  ...   152.0     152.0      55.0   1.01
Is_begin   33.986  18.066   5.652   61.846  ...    55.0      49.0      73.0   1.05
Ia_begin   93.854  36.735  28.542  154.945  ...   105.0      99.0      59.0   1.02
E_begin   157.485  79.665  21.510  312.122  ...    92.0      93.0      93.0   1.02

[8 rows x 11 columns]