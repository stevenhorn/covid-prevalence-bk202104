0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1280.21    28.47
p_loo       30.09        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      353   91.9%
 (0.5, 0.7]   (ok)         27    7.0%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.257  0.051   0.165    0.338  ...    12.0      14.0      39.0   1.14
pu        0.769  0.036   0.703    0.821  ...     6.0       7.0      22.0   1.26
mu        0.142  0.021   0.101    0.177  ...     7.0       8.0      33.0   1.25
mus       0.184  0.033   0.124    0.241  ...    81.0      99.0      59.0   1.01
gamma     0.268  0.042   0.193    0.342  ...   114.0     110.0      46.0   1.00
Is_begin  1.141  0.967   0.129    3.224  ...    78.0      73.0      60.0   1.01
Ia_begin  2.894  2.296   0.008    6.463  ...   101.0      63.0      22.0   1.04
E_begin   1.529  1.402   0.016    3.897  ...    64.0      51.0      60.0   1.03

[8 rows x 11 columns]