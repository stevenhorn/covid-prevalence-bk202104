0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -844.75    28.66
p_loo       30.05        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      367   95.6%
 (0.5, 0.7]   (ok)         10    2.6%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.254  0.055   0.151    0.338  ...    27.0      27.0      30.0   1.05
pu        0.775  0.022   0.739    0.813  ...    19.0      21.0      60.0   1.12
mu        0.121  0.023   0.088    0.168  ...     8.0       8.0      40.0   1.22
mus       0.178  0.029   0.135    0.250  ...    44.0      47.0      49.0   1.05
gamma     0.200  0.036   0.135    0.259  ...    98.0     114.0      93.0   0.99
Is_begin  0.445  0.522   0.003    1.315  ...    61.0      53.0      56.0   1.04
Ia_begin  0.684  0.837   0.000    2.642  ...    50.0      17.0      33.0   1.08
E_begin   0.301  0.376   0.003    1.269  ...    56.0      31.0      43.0   1.05

[8 rows x 11 columns]