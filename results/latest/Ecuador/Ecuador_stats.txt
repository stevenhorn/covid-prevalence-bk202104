30 Divergences 
Passed validation 
Computed from 80 by 385 log-likelihood matrix

         Estimate       SE
elpd_loo -2996.58    41.98
p_loo       46.11        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      354   91.9%
 (0.5, 0.7]   (ok)         20    5.2%
   (0.7, 1]   (bad)        10    2.6%
   (1, Inf)   (very bad)    1    0.3%

             mean       sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.250    0.052   0.163    0.320  ...    53.0      61.0      87.0   1.11
pu          0.799    0.054   0.713    0.890  ...    49.0      59.0      65.0   1.02
mu          0.100    0.021   0.069    0.137  ...     7.0       8.0      16.0   1.19
mus         0.184    0.031   0.128    0.249  ...    35.0      35.0      43.0   1.02
gamma       0.268    0.035   0.198    0.324  ...    30.0      35.0      51.0   1.07
Is_begin   80.220   55.141   0.358  185.709  ...    18.0      17.0      15.0   1.09
Ia_begin  232.231  106.485  33.147  395.081  ...    35.0      33.0      38.0   1.05
E_begin   221.897  185.533   4.816  606.474  ...    28.0      22.0      38.0   1.21

[8 rows x 11 columns]