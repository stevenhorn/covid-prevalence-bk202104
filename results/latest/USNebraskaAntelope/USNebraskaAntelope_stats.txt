0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -658.07    35.05
p_loo       27.61        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      357   93.0%
 (0.5, 0.7]   (ok)         23    6.0%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.245  0.051   0.170    0.331  ...    14.0      15.0      80.0   1.11
pu        0.851  0.017   0.820    0.880  ...    14.0      15.0      60.0   1.10
mu        0.128  0.027   0.074    0.171  ...     8.0       7.0      14.0   1.26
mus       0.160  0.029   0.112    0.226  ...    51.0      42.0      60.0   1.04
gamma     0.199  0.038   0.138    0.269  ...    57.0     102.0      83.0   1.08
Is_begin  0.603  0.506   0.004    1.568  ...   127.0      76.0      60.0   1.02
Ia_begin  1.408  1.384   0.006    3.997  ...   114.0      87.0      60.0   1.00
E_begin   0.528  0.524   0.016    1.763  ...   135.0      85.0     100.0   0.99

[8 rows x 11 columns]