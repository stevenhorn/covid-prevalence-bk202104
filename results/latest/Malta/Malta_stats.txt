0 Divergences 
Passed validation 
Computed from 80 by 385 log-likelihood matrix

         Estimate       SE
elpd_loo -1618.58    34.01
p_loo       38.17        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      336   87.3%
 (0.5, 0.7]   (ok)         36    9.4%
   (0.7, 1]   (bad)        11    2.9%
   (1, Inf)   (very bad)    2    0.5%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.250   0.053   0.159    0.334  ...    36.0      36.0      35.0   1.01
pu         0.783   0.051   0.707    0.861  ...    41.0      37.0      53.0   1.03
mu         0.174   0.031   0.115    0.224  ...    24.0      27.0      40.0   1.05
mus        0.231   0.036   0.172    0.294  ...   152.0     131.0      58.0   0.98
gamma      0.367   0.054   0.278    0.470  ...   147.0     152.0     100.0   1.00
Is_begin  14.529   9.021   1.185   30.183  ...   152.0     126.0      43.0   0.99
Ia_begin  33.665  18.856   1.098   67.779  ...    46.0      42.0      31.0   1.03
E_begin   30.814  27.862   0.091   77.848  ...    65.0      54.0      31.0   1.05

[8 rows x 11 columns]