1 Divergences 
Passed validation 
Computed from 80 by 385 log-likelihood matrix

         Estimate       SE
elpd_loo -2078.84    45.97
p_loo       42.57        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      360   93.5%
 (0.5, 0.7]   (ok)         18    4.7%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.277  0.041   0.190    0.336  ...    96.0      92.0      43.0   1.01
pu        0.861  0.046   0.746    0.900  ...    27.0      65.0      25.0   1.11
mu        0.178  0.035   0.128    0.248  ...     4.0       4.0      24.0   1.53
mus       0.189  0.029   0.142    0.245  ...    45.0      44.0      93.0   1.03
gamma     0.317  0.054   0.215    0.399  ...    12.0      13.0      37.0   1.12
Is_begin  0.890  0.740   0.014    2.405  ...   152.0     108.0      65.0   1.00
Ia_begin  1.588  1.350   0.014    4.147  ...    53.0      38.0      60.0   1.00
E_begin   0.906  0.897   0.009    2.453  ...    80.0      52.0      60.0   1.04

[8 rows x 11 columns]