0 Divergences 
Passed validation 
Computed from 80 by 390 log-likelihood matrix

         Estimate       SE
elpd_loo -1436.22    48.10
p_loo       41.54        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      355   91.0%
 (0.5, 0.7]   (ok)         31    7.9%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    3    0.8%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.244   0.055   0.160    0.333  ...    33.0      33.0      17.0   1.13
pu         0.777   0.058   0.703    0.889  ...    19.0      20.0      15.0   1.02
mu         0.138   0.015   0.114    0.164  ...    29.0      29.0      58.0   1.04
mus        0.211   0.031   0.165    0.272  ...    44.0      47.0      57.0   1.00
gamma      0.309   0.044   0.246    0.399  ...    89.0      84.0     100.0   1.00
Is_begin   4.658   4.377   0.001   12.437  ...    26.0      17.0      18.0   1.11
Ia_begin  18.098  16.090   0.122   45.180  ...    36.0      25.0      40.0   1.08
E_begin    9.600   8.691   0.249   26.913  ...    32.0      33.0      81.0   1.02

[8 rows x 11 columns]