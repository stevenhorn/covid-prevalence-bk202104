0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -850.02    31.26
p_loo       29.85        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      358   93.2%
 (0.5, 0.7]   (ok)         21    5.5%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.296  0.040   0.216    0.347  ...    68.0      92.0      39.0   1.04
pu        0.892  0.009   0.881    0.900  ...    63.0      74.0     100.0   1.02
mu        0.137  0.021   0.099    0.172  ...    19.0      19.0      61.0   1.07
mus       0.167  0.037   0.094    0.236  ...   152.0     152.0      60.0   1.01
gamma     0.208  0.048   0.129    0.283  ...    92.0      94.0      60.0   0.98
Is_begin  0.698  0.628   0.007    1.686  ...    96.0      74.0      60.0   1.00
Ia_begin  1.595  1.496   0.024    4.497  ...    77.0      62.0      59.0   1.02
E_begin   0.767  0.873   0.009    1.934  ...    28.0      36.0      40.0   1.06

[8 rows x 11 columns]