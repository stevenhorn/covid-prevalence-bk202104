0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -894.02    40.79
p_loo       33.91        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      358   93.2%
 (0.5, 0.7]   (ok)         21    5.5%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.249  0.059   0.170    0.346  ...    70.0      81.0      83.0   1.00
pu        0.831  0.020   0.790    0.858  ...    26.0      26.0      54.0   1.07
mu        0.128  0.031   0.074    0.177  ...    30.0      29.0      59.0   1.09
mus       0.200  0.032   0.154    0.261  ...    85.0      88.0      59.0   1.00
gamma     0.250  0.049   0.177    0.334  ...   134.0     141.0      40.0   1.02
Is_begin  0.819  0.728   0.000    2.330  ...    74.0      54.0      24.0   1.00
Ia_begin  0.574  0.513   0.002    1.499  ...    63.0      47.0      29.0   1.02
E_begin   0.456  0.589   0.005    1.571  ...    83.0      38.0      56.0   1.04

[8 rows x 11 columns]