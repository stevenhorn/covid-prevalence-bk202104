0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1759.36    33.38
p_loo       24.29        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      355   92.4%
 (0.5, 0.7]   (ok)         22    5.7%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.248  0.056   0.153    0.328  ...    56.0      56.0      43.0   1.00
pu        0.785  0.032   0.720    0.828  ...    33.0      36.0      40.0   1.07
mu        0.115  0.021   0.076    0.148  ...    27.0      28.0      33.0   1.02
mus       0.181  0.031   0.124    0.230  ...   152.0     152.0      70.0   1.05
gamma     0.190  0.032   0.140    0.249  ...    72.0      75.0      59.0   0.99
Is_begin  1.538  1.245   0.004    3.985  ...    73.0      63.0      30.0   1.04
Ia_begin  0.707  0.607   0.011    1.938  ...   109.0      83.0      96.0   1.02
E_begin   1.030  1.049   0.001    3.432  ...    57.0      43.0      57.0   1.05

[8 rows x 11 columns]