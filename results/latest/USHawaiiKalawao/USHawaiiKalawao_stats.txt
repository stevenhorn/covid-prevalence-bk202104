0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo   308.74   121.89
p_loo       82.87        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      369   96.1%
 (0.5, 0.7]   (ok)          6    1.6%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.262  0.045   0.169    0.331  ...    91.0      90.0      91.0   1.01
pu        0.875  0.014   0.847    0.896  ...    58.0      72.0      40.0   1.03
mu        0.132  0.024   0.094    0.184  ...     9.0      12.0      49.0   1.14
mus       0.317  0.042   0.230    0.385  ...    79.0      81.0      60.0   0.99
gamma     0.432  0.057   0.343    0.533  ...    85.0      89.0      97.0   1.03
Is_begin  0.378  0.359   0.026    1.082  ...    24.0      93.0      17.0   1.08
Ia_begin  0.738  0.907   0.022    2.696  ...    22.0      35.0      49.0   1.06
E_begin   0.349  0.484   0.001    1.140  ...    16.0      29.0      88.0   1.06

[8 rows x 11 columns]