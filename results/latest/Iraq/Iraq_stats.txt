0 Divergences 
Passed validation 
Computed from 80 by 385 log-likelihood matrix

         Estimate       SE
elpd_loo -2536.69    26.68
p_loo       35.56        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      338   87.8%
 (0.5, 0.7]   (ok)         34    8.8%
   (0.7, 1]   (bad)        13    3.4%
   (1, Inf)   (very bad)    0    0.0%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.244   0.045   0.179    0.339  ...   152.0     152.0      87.0   1.00
pu         0.765   0.045   0.712    0.860  ...   121.0      84.0      59.0   0.99
mu         0.119   0.020   0.091    0.154  ...     5.0       5.0      30.0   1.39
mus        0.218   0.034   0.160    0.278  ...    90.0      89.0      99.0   0.98
gamma      0.267   0.046   0.192    0.342  ...    84.0      84.0      72.0   0.99
Is_begin  34.543  19.100   5.836   70.305  ...    61.0      47.0      59.0   1.02
Ia_begin  79.898  39.900  10.144  134.623  ...    75.0      67.0      40.0   1.05
E_begin   79.289  66.268   2.346  175.963  ...    71.0      71.0      58.0   0.99

[8 rows x 11 columns]