0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1046.16    68.63
p_loo       43.78        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      350   91.1%
 (0.5, 0.7]   (ok)         27    7.0%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.254  0.059   0.155    0.348  ...    43.0      44.0      57.0   1.01
pu        0.784  0.024   0.742    0.831  ...    16.0      18.0      50.0   1.08
mu        0.170  0.046   0.105    0.241  ...     3.0       4.0      31.0   1.81
mus       0.226  0.032   0.177    0.289  ...    34.0      36.0      37.0   1.01
gamma     0.288  0.053   0.195    0.397  ...   107.0     100.0     100.0   1.01
Is_begin  0.907  0.660   0.118    2.352  ...    72.0      64.0      91.0   0.99
Ia_begin  2.358  2.206   0.020    7.331  ...    18.0      17.0      24.0   1.10
E_begin   0.794  0.578   0.046    1.824  ...    34.0      29.0      58.0   1.05

[8 rows x 11 columns]