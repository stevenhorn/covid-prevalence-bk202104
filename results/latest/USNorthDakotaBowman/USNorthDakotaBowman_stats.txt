0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -787.69    70.77
p_loo       53.95        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      352   91.7%
 (0.5, 0.7]   (ok)         24    6.2%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.231  0.047   0.152    0.308  ...    73.0      78.0      60.0   1.07
pu        0.721  0.020   0.700    0.756  ...    11.0       7.0      14.0   1.23
mu        0.110  0.023   0.074    0.155  ...    42.0      42.0      24.0   1.03
mus       0.225  0.036   0.164    0.297  ...    15.0      15.0     100.0   1.13
gamma     0.361  0.056   0.248    0.447  ...    94.0      92.0      93.0   1.00
Is_begin  0.548  0.627   0.010    1.761  ...   103.0      87.0      59.0   0.99
Ia_begin  0.832  0.909   0.045    2.626  ...    44.0      37.0      59.0   1.01
E_begin   0.328  0.393   0.007    1.099  ...    91.0      67.0     100.0   0.99

[8 rows x 11 columns]