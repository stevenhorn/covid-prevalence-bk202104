0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -989.94    32.51
p_loo       29.74        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      358   93.2%
 (0.5, 0.7]   (ok)         21    5.5%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.245  0.051   0.164    0.332  ...    40.0      51.0      79.0   1.07
pu        0.782  0.017   0.744    0.809  ...    59.0      56.0      53.0   1.04
mu        0.138  0.025   0.099    0.188  ...    31.0      32.0      91.0   1.06
mus       0.163  0.036   0.097    0.221  ...   116.0     109.0      93.0   1.01
gamma     0.178  0.037   0.110    0.238  ...   107.0     113.0      93.0   1.02
Is_begin  0.754  0.658   0.007    2.128  ...   132.0     126.0      49.0   1.02
Ia_begin  1.460  1.303   0.006    4.262  ...   102.0      75.0      40.0   0.99
E_begin   0.662  0.585   0.007    1.453  ...    63.0      45.0      40.0   1.00

[8 rows x 11 columns]