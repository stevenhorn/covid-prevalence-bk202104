0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1517.39    43.22
p_loo       30.97        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      354   92.2%
 (0.5, 0.7]   (ok)         26    6.8%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.247  0.052   0.163    0.334  ...   111.0     101.0      59.0   0.99
pu        0.780  0.035   0.727    0.849  ...    47.0      50.0      45.0   0.99
mu        0.126  0.020   0.082    0.159  ...    14.0      16.0      40.0   1.14
mus       0.172  0.033   0.099    0.230  ...   152.0     112.0     100.0   1.03
gamma     0.231  0.040   0.166    0.308  ...   110.0     120.0      93.0   0.99
Is_begin  1.824  1.196   0.235    4.232  ...    98.0      79.0      59.0   1.03
Ia_begin  4.319  2.730   0.033    9.506  ...    68.0      27.0      24.0   1.07
E_begin   3.554  2.684   0.023    8.957  ...    40.0      34.0      39.0   1.07

[8 rows x 11 columns]