0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -914.37    33.50
p_loo       29.29        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      352   91.7%
 (0.5, 0.7]   (ok)         25    6.5%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.255  0.048   0.161    0.340  ...    29.0      35.0      43.0   1.02
pu        0.844  0.017   0.817    0.875  ...    18.0      27.0      60.0   1.06
mu        0.134  0.023   0.096    0.168  ...     5.0       4.0      24.0   1.52
mus       0.174  0.034   0.111    0.233  ...    63.0      60.0      58.0   1.04
gamma     0.214  0.037   0.148    0.286  ...    84.0     112.0      46.0   1.00
Is_begin  0.878  0.768   0.021    2.335  ...   101.0      87.0      60.0   1.01
Ia_begin  2.049  1.831   0.178    5.698  ...    96.0      99.0      86.0   1.02
E_begin   0.753  0.760   0.016    2.150  ...    85.0      65.0      43.0   1.00

[8 rows x 11 columns]