0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -483.88    36.05
p_loo       35.30        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      360   93.8%
 (0.5, 0.7]   (ok)         19    4.9%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.243  0.052   0.151    0.321  ...    48.0      47.0      40.0   1.00
pu        0.762  0.022   0.723    0.810  ...    37.0      39.0      22.0   1.04
mu        0.124  0.020   0.086    0.160  ...    72.0      69.0      96.0   0.98
mus       0.187  0.032   0.130    0.241  ...    80.0      91.0      88.0   0.98
gamma     0.210  0.042   0.142    0.282  ...    69.0      74.0      60.0   1.02
Is_begin  0.405  0.436   0.001    1.422  ...    18.0       9.0      44.0   1.18
Ia_begin  0.845  1.226   0.000    2.902  ...    95.0      58.0      54.0   1.00
E_begin   0.380  0.522   0.002    1.484  ...    62.0      29.0      40.0   1.04

[8 rows x 11 columns]