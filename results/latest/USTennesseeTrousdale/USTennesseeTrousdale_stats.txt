0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1311.61    75.71
p_loo       29.09        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      364   94.8%
 (0.5, 0.7]   (ok)         16    4.2%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.164  0.011   0.152    0.182  ...   121.0      91.0      60.0   1.01
pu        0.704  0.004   0.700    0.712  ...    32.0      18.0      21.0   1.08
mu        0.061  0.011   0.043    0.082  ...   126.0     112.0      73.0   1.04
mus       0.250  0.030   0.213    0.314  ...   117.0     107.0      96.0   1.00
gamma     0.493  0.062   0.380    0.595  ...    74.0      78.0      57.0   1.02
Is_begin  0.297  0.320   0.000    0.939  ...    88.0      68.0      59.0   0.99
Ia_begin  0.342  0.398   0.002    1.290  ...    51.0      63.0      60.0   1.01
E_begin   0.208  0.263   0.004    0.715  ...    53.0      52.0      40.0   1.01

[8 rows x 11 columns]