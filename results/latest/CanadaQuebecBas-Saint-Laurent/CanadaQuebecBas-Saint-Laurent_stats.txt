0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -983.48    39.08
p_loo       32.51        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      351   91.4%
 (0.5, 0.7]   (ok)         25    6.5%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    2    0.5%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.286   0.046   0.199    0.346  ...    67.0      76.0      56.0   1.08
pu         0.887   0.014   0.860    0.900  ...    48.0      34.0      22.0   1.02
mu         0.170   0.018   0.141    0.201  ...     5.0       5.0      25.0   1.47
mus        0.267   0.038   0.196    0.347  ...    75.0      60.0      80.0   1.02
gamma      0.439   0.073   0.302    0.555  ...    53.0      61.0      97.0   1.02
Is_begin   8.843   7.567   0.369   22.409  ...    70.0      66.0      41.0   1.00
Ia_begin  29.451  30.377   0.124  105.854  ...    61.0      19.0      15.0   1.07
E_begin    8.046   7.912   0.073   23.870  ...    47.0      33.0      24.0   1.05

[8 rows x 11 columns]