0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -823.89    40.27
p_loo       39.33        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      356   92.7%
 (0.5, 0.7]   (ok)         19    4.9%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.240  0.054   0.152    0.324  ...   103.0     105.0      93.0   1.01
pu        0.805  0.024   0.768    0.843  ...    33.0      34.0      88.0   1.06
mu        0.118  0.023   0.075    0.157  ...     6.0       6.0      20.0   1.31
mus       0.183  0.028   0.135    0.242  ...    41.0      43.0      84.0   1.06
gamma     0.220  0.050   0.129    0.304  ...   144.0     151.0      88.0   1.00
Is_begin  0.432  0.451   0.006    1.406  ...    60.0      66.0      54.0   1.03
Ia_begin  0.557  0.717   0.006    2.000  ...    28.0      46.0      26.0   1.06
E_begin   0.323  0.399   0.011    1.135  ...    75.0      54.0      86.0   1.04

[8 rows x 11 columns]