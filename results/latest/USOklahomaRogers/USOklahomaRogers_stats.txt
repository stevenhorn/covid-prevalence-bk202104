0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1476.37    33.37
p_loo       31.22        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      364   94.8%
 (0.5, 0.7]   (ok)         14    3.6%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.251  0.051   0.163    0.340  ...    85.0      94.0      96.0   1.04
pu        0.752  0.025   0.712    0.797  ...   140.0     140.0      93.0   1.01
mu        0.128  0.023   0.097    0.174  ...    46.0      38.0      60.0   1.04
mus       0.173  0.028   0.124    0.218  ...   138.0     137.0      56.0   1.07
gamma     0.212  0.046   0.146    0.302  ...    81.0     101.0      93.0   1.00
Is_begin  0.743  0.666   0.029    2.044  ...   152.0     114.0      91.0   1.00
Ia_begin  1.543  1.473   0.014    4.391  ...   104.0      56.0      54.0   1.03
E_begin   0.634  0.639   0.007    1.961  ...    85.0      83.0      49.0   1.01

[8 rows x 11 columns]