0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -687.03    23.39
p_loo       23.01        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      356   92.7%
 (0.5, 0.7]   (ok)         21    5.5%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.259  0.045   0.179    0.330  ...    19.0      19.0      59.0   1.10
pu        0.745  0.021   0.703    0.776  ...    20.0      26.0      17.0   1.08
mu        0.123  0.020   0.087    0.156  ...    59.0      59.0      91.0   1.10
mus       0.179  0.034   0.124    0.238  ...    65.0      72.0      96.0   1.04
gamma     0.196  0.039   0.130    0.274  ...   152.0     152.0      53.0   1.00
Is_begin  0.632  0.665   0.006    1.622  ...    97.0      91.0      40.0   0.99
Ia_begin  1.252  1.170   0.092    3.559  ...    69.0      76.0      60.0   1.02
E_begin   0.552  0.542   0.003    1.639  ...    99.0      76.0      56.0   1.01

[8 rows x 11 columns]