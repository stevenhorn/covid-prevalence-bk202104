0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1073.56    57.82
p_loo       47.91        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      350   91.1%
 (0.5, 0.7]   (ok)         24    6.2%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.259  0.050   0.175    0.337  ...   108.0     110.0      39.0   1.03
pu        0.770  0.025   0.737    0.821  ...    75.0      71.0      56.0   1.00
mu        0.165  0.018   0.131    0.194  ...    48.0      50.0      59.0   1.05
mus       0.256  0.040   0.181    0.323  ...    37.0      37.0      48.0   1.04
gamma     0.414  0.070   0.291    0.532  ...    58.0      58.0      53.0   1.06
Is_begin  0.868  0.829   0.006    2.343  ...   100.0      69.0      42.0   1.03
Ia_begin  1.582  1.707   0.002    5.101  ...    66.0      37.0      31.0   1.03
E_begin   0.716  0.772   0.014    2.569  ...    95.0      65.0      56.0   1.02

[8 rows x 11 columns]