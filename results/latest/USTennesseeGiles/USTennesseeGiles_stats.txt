0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1206.31    29.41
p_loo       30.33        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      350   91.1%
 (0.5, 0.7]   (ok)         29    7.6%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.221  0.064   0.151    0.334  ...     6.0       5.0      40.0   1.42
pu        0.777  0.022   0.738    0.816  ...    32.0      28.0      21.0   1.07
mu        0.133  0.026   0.091    0.172  ...    41.0      27.0      58.0   1.04
mus       0.186  0.029   0.145    0.255  ...    18.0      18.0      57.0   1.10
gamma     0.234  0.046   0.158    0.305  ...    44.0      49.0      65.0   1.04
Is_begin  0.526  0.506   0.022    1.318  ...    81.0      67.0      56.0   1.00
Ia_begin  0.856  1.095   0.004    2.581  ...    48.0       9.0      24.0   1.17
E_begin   0.456  0.573   0.000    1.463  ...    52.0      17.0      38.0   1.09

[8 rows x 11 columns]