0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -639.90    38.21
p_loo       67.99        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      356   92.7%
 (0.5, 0.7]   (ok)         23    6.0%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.255  0.051   0.171    0.344  ...    84.0      75.0      59.0   0.98
pu        0.809  0.031   0.755    0.855  ...     5.0       5.0      46.0   1.46
mu        0.128  0.023   0.093    0.173  ...    45.0      51.0      39.0   0.99
mus       0.164  0.037   0.107    0.247  ...    46.0      52.0      56.0   1.05
gamma     0.169  0.041   0.097    0.239  ...    36.0      37.0      30.0   1.01
Is_begin  0.341  0.394   0.000    1.370  ...    61.0      72.0      30.0   1.01
Ia_begin  0.649  0.789   0.001    2.499  ...    67.0      44.0      29.0   1.04
E_begin   0.325  0.402   0.005    0.860  ...    61.0      45.0      57.0   1.04

[8 rows x 11 columns]