0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -2317.84    67.23
p_loo       35.10        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      336   87.5%
 (0.5, 0.7]   (ok)         38    9.9%
   (0.7, 1]   (bad)         9    2.3%
   (1, Inf)   (very bad)    1    0.3%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.232   0.051   0.152    0.317  ...    57.0      57.0      34.0   1.14
pu         0.732   0.022   0.701    0.772  ...    63.0      56.0      60.0   1.00
mu         0.122   0.024   0.080    0.164  ...     6.0       6.0      49.0   1.30
mus        0.221   0.035   0.160    0.281  ...    89.0     100.0      60.0   1.00
gamma      0.297   0.051   0.212    0.381  ...    84.0      83.0      95.0   1.02
Is_begin  40.973  23.899   4.175   82.267  ...   133.0     129.0      43.0   1.01
Ia_begin  69.453  45.516   4.020  145.915  ...    80.0      62.0      60.0   1.00
E_begin   65.708  47.154   1.235  141.887  ...    98.0      71.0      91.0   1.01

[8 rows x 11 columns]