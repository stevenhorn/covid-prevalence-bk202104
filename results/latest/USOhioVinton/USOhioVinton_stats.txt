0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -659.91    27.79
p_loo       24.65        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      369   96.1%
 (0.5, 0.7]   (ok)         11    2.9%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.260  0.051   0.164    0.332  ...   123.0     131.0      59.0   0.99
pu        0.880  0.020   0.857    0.899  ...    38.0      22.0      85.0   1.08
mu        0.122  0.024   0.077    0.163  ...    92.0      83.0      88.0   1.02
mus       0.188  0.031   0.145    0.239  ...    91.0     152.0      44.0   0.99
gamma     0.228  0.031   0.169    0.275  ...    91.0      85.0     100.0   1.00
Is_begin  0.782  0.675   0.011    2.014  ...   106.0     127.0      59.0   1.01
Ia_begin  1.228  1.225   0.023    3.794  ...   109.0      96.0      91.0   1.02
E_begin   0.493  0.572   0.007    1.310  ...    91.0      35.0      47.0   1.06

[8 rows x 11 columns]