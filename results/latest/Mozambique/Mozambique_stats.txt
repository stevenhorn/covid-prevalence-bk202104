0 Divergences 
Passed validation 
Computed from 80 by 385 log-likelihood matrix

         Estimate       SE
elpd_loo -1935.30    32.28
p_loo       26.89        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      352   91.4%
 (0.5, 0.7]   (ok)         25    6.5%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.271  0.053   0.168    0.347  ...    52.0      49.0      38.0   1.05
pu        0.874  0.025   0.828    0.900  ...    62.0      59.0      59.0   0.99
mu        0.145  0.023   0.106    0.182  ...     4.0       4.0      15.0   1.49
mus       0.167  0.028   0.124    0.219  ...    88.0      84.0      88.0   1.03
gamma     0.202  0.042   0.144    0.301  ...    47.0      46.0      31.0   1.03
Is_begin  0.780  0.802   0.011    2.227  ...    99.0      68.0      58.0   1.02
Ia_begin  1.614  1.400   0.042    4.457  ...    45.0      52.0      42.0   1.04
E_begin   0.814  1.010   0.016    2.288  ...    67.0      66.0      43.0   1.00

[8 rows x 11 columns]