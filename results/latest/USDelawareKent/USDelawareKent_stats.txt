0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1629.33    45.58
p_loo       45.56        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      339   88.3%
 (0.5, 0.7]   (ok)         38    9.9%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.240  0.055   0.151    0.330  ...   127.0     152.0      59.0   1.02
pu        0.750  0.031   0.703    0.807  ...    39.0      46.0      20.0   1.09
mu        0.120  0.029   0.073    0.173  ...    14.0      12.0      40.0   1.15
mus       0.188  0.038   0.135    0.240  ...   100.0     152.0      43.0   1.02
gamma     0.283  0.043   0.206    0.353  ...   152.0     152.0      91.0   0.99
Is_begin  0.776  0.580   0.013    1.808  ...    90.0      94.0      79.0   1.01
Ia_begin  1.713  1.462   0.030    4.054  ...    23.0      11.0      20.0   1.15
E_begin   1.551  1.896   0.004    4.831  ...    66.0      27.0      17.0   1.04

[8 rows x 11 columns]