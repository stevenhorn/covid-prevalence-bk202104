0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1313.80    28.42
p_loo       25.93        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      357   93.0%
 (0.5, 0.7]   (ok)         24    6.2%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.241  0.053   0.157    0.329  ...    65.0      54.0      59.0   1.02
pu        0.777  0.038   0.702    0.828  ...    53.0      57.0      60.0   1.09
mu        0.122  0.022   0.088    0.165  ...    13.0      11.0      57.0   1.16
mus       0.153  0.025   0.110    0.195  ...   117.0     148.0      61.0   1.03
gamma     0.183  0.035   0.121    0.244  ...    87.0     101.0      97.0   1.01
Is_begin  0.793  0.547   0.051    1.656  ...    91.0      72.0      43.0   0.99
Ia_begin  1.416  0.943   0.174    3.248  ...    60.0      54.0      40.0   1.00
E_begin   1.178  1.293   0.006    4.216  ...    54.0      50.0      39.0   1.06

[8 rows x 11 columns]