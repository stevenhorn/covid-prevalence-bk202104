0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -833.47    29.63
p_loo       25.62        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      353   91.9%
 (0.5, 0.7]   (ok)         27    7.0%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.256  0.051   0.154    0.331  ...    66.0      63.0      40.0   0.99
pu        0.786  0.020   0.754    0.825  ...    35.0      35.0      48.0   1.01
mu        0.127  0.022   0.097    0.179  ...    46.0      45.0      60.0   1.00
mus       0.177  0.031   0.122    0.233  ...   152.0     152.0      73.0   1.02
gamma     0.206  0.032   0.137    0.254  ...   141.0     152.0      88.0   1.00
Is_begin  0.600  0.636   0.016    1.949  ...   106.0      84.0      93.0   1.07
Ia_begin  1.256  1.764   0.009    5.251  ...   112.0      89.0      43.0   1.08
E_begin   0.550  0.763   0.004    1.887  ...   107.0      87.0     100.0   1.01

[8 rows x 11 columns]