0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1380.31    54.16
p_loo       37.57        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      348   90.6%
 (0.5, 0.7]   (ok)         28    7.3%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    4    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.197  0.020   0.160    0.230  ...    16.0      16.0      17.0   1.07
pu        0.715  0.009   0.702    0.729  ...     3.0       4.0      22.0   1.64
mu        0.225  0.041   0.170    0.277  ...     3.0       3.0      40.0   2.21
mus       0.217  0.025   0.172    0.257  ...     4.0       4.0      14.0   1.62
gamma     0.207  0.068   0.130    0.323  ...     3.0       3.0      17.0   1.86
Is_begin  1.170  0.950   0.157    3.350  ...     8.0       6.0      15.0   1.33
Ia_begin  1.875  1.018   0.277    3.710  ...    10.0       9.0      14.0   1.23
E_begin   0.517  0.953   0.013    2.726  ...     6.0       4.0      26.0   1.75

[8 rows x 11 columns]