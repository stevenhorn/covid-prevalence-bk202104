0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1424.83    53.08
p_loo       38.97        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      346   90.1%
 (0.5, 0.7]   (ok)         29    7.6%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.261  0.053   0.181    0.349  ...    57.0      58.0      40.0   1.03
pu        0.853  0.026   0.796    0.885  ...    43.0      26.0      59.0   1.12
mu        0.165  0.015   0.138    0.199  ...    57.0      60.0      59.0   1.00
mus       0.218  0.032   0.179    0.290  ...   120.0     152.0      40.0   1.01
gamma     0.340  0.057   0.242    0.438  ...    75.0      67.0      25.0   1.03
Is_begin  0.814  0.732   0.010    2.189  ...    88.0      45.0      22.0   1.03
Ia_begin  1.709  1.916   0.006    5.874  ...    78.0      48.0      40.0   1.02
E_begin   0.775  0.873   0.007    1.891  ...    47.0      37.0      81.0   1.08

[8 rows x 11 columns]