0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -848.83    29.77
p_loo       27.93        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      350   91.1%
 (0.5, 0.7]   (ok)         27    7.0%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.243  0.053   0.164    0.342  ...    26.0      36.0      42.0   1.06
pu        0.741  0.022   0.707    0.785  ...    13.0      14.0      49.0   1.12
mu        0.118  0.022   0.080    0.152  ...    14.0      13.0      63.0   1.13
mus       0.179  0.031   0.127    0.232  ...   152.0     152.0      79.0   0.99
gamma     0.203  0.049   0.115    0.279  ...   152.0     152.0      81.0   0.99
Is_begin  0.300  0.341   0.001    1.049  ...   104.0      63.0      57.0   1.01
Ia_begin  0.507  0.782   0.002    1.745  ...    70.0      67.0      58.0   1.00
E_begin   0.199  0.269   0.000    0.681  ...    95.0      61.0      60.0   1.02

[8 rows x 11 columns]