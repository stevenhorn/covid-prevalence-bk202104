0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -838.07    66.38
p_loo       91.38        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      364   94.8%
 (0.5, 0.7]   (ok)         14    3.6%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    4    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.208  0.037   0.154    0.274  ...    59.0      52.0      60.0   1.05
pu        0.719  0.015   0.702    0.751  ...    56.0      34.0      40.0   1.04
mu        0.153  0.027   0.096    0.194  ...    40.0      40.0      41.0   1.04
mus       0.246  0.034   0.172    0.297  ...    80.0      84.0      59.0   1.03
gamma     0.348  0.057   0.234    0.435  ...    34.0      40.0      43.0   1.02
Is_begin  0.255  0.353   0.007    0.935  ...    82.0      66.0      67.0   1.02
Ia_begin  0.385  0.487   0.003    1.192  ...   106.0      83.0      65.0   1.04
E_begin   0.220  0.290   0.003    0.802  ...    78.0      61.0      59.0   0.99

[8 rows x 11 columns]