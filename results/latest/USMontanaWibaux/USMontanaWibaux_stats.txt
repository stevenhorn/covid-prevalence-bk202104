0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -409.82    47.26
p_loo       50.83        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      353   91.9%
 (0.5, 0.7]   (ok)         25    6.5%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.218  0.053   0.151    0.311  ...    10.0       9.0      80.0   1.20
pu        0.763  0.051   0.700    0.832  ...     3.0       4.0      17.0   1.70
mu        0.136  0.028   0.076    0.182  ...    73.0      68.0      40.0   1.01
mus       0.251  0.055   0.164    0.362  ...     7.0       7.0      40.0   1.27
gamma     0.291  0.065   0.191    0.415  ...    35.0      21.0      97.0   1.09
Is_begin  0.394  0.406   0.001    1.183  ...    78.0     105.0      33.0   1.10
Ia_begin  0.487  0.774   0.001    1.700  ...    32.0      20.0      44.0   1.07
E_begin   0.245  0.284   0.000    0.767  ...    37.0      18.0      57.0   1.07

[8 rows x 11 columns]