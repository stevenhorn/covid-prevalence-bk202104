0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -813.29    25.90
p_loo       24.59        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      362   94.3%
 (0.5, 0.7]   (ok)         15    3.9%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.265  0.055   0.163    0.341  ...    61.0      59.0      59.0   1.02
pu        0.830  0.018   0.799    0.863  ...    10.0      10.0      40.0   1.18
mu        0.125  0.026   0.089    0.174  ...    28.0      27.0      56.0   1.04
mus       0.177  0.032   0.124    0.234  ...   147.0     152.0      65.0   1.01
gamma     0.207  0.036   0.141    0.270  ...    74.0      69.0      61.0   1.02
Is_begin  0.606  0.601   0.006    1.677  ...    67.0      45.0      55.0   1.05
Ia_begin  1.342  1.447   0.017    3.580  ...   101.0      69.0      56.0   1.03
E_begin   0.481  0.603   0.016    1.665  ...    24.0      55.0      87.0   1.03

[8 rows x 11 columns]