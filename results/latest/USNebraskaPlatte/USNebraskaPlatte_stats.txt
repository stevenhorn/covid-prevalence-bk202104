0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1284.34    26.25
p_loo       26.97        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      358   93.2%
 (0.5, 0.7]   (ok)         21    5.5%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.200  0.036   0.154    0.281  ...   152.0     116.0      97.0   1.02
pu        0.722  0.017   0.700    0.755  ...   120.0     121.0      52.0   0.99
mu        0.124  0.025   0.088    0.174  ...    31.0      27.0      56.0   1.05
mus       0.183  0.026   0.142    0.236  ...   152.0     152.0      83.0   1.01
gamma     0.268  0.049   0.200    0.367  ...   149.0     152.0      22.0   1.01
Is_begin  0.467  0.537   0.006    1.344  ...   101.0     152.0      76.0   0.99
Ia_begin  0.720  0.665   0.009    2.081  ...   152.0     152.0      88.0   1.01
E_begin   0.432  0.470   0.006    1.430  ...   122.0     148.0      46.0   0.99

[8 rows x 11 columns]