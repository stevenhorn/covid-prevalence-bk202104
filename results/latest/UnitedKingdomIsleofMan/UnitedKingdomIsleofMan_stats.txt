0 Divergences 
Passed validation 
Computed from 80 by 385 log-likelihood matrix

         Estimate       SE
elpd_loo  -745.95    35.39
p_loo       39.79        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      348   90.4%
 (0.5, 0.7]   (ok)         28    7.3%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.225  0.054   0.151    0.318  ...    40.0      35.0      43.0   1.19
pu        0.746  0.041   0.700    0.818  ...     7.0       5.0      42.0   1.50
mu        0.165  0.016   0.139    0.198  ...    64.0      63.0      93.0   0.99
mus       0.296  0.040   0.236    0.376  ...    16.0      18.0      59.0   1.11
gamma     0.739  0.092   0.555    0.879  ...    72.0      67.0      78.0   1.05
Is_begin  0.984  1.038   0.044    2.995  ...    72.0      79.0     100.0   1.08
Ia_begin  2.539  3.161   0.053    7.116  ...    32.0      12.0      30.0   1.15
E_begin   1.214  1.364   0.003    3.760  ...    56.0      46.0      18.0   1.02

[8 rows x 11 columns]