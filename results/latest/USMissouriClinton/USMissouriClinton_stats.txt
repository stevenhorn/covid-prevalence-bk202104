0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1259.32    76.23
p_loo       38.27        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      365   95.1%
 (0.5, 0.7]   (ok)         14    3.6%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.194  0.038   0.150    0.266  ...   109.0     107.0      58.0   0.99
pu        0.711  0.012   0.700    0.730  ...   107.0      81.0      40.0   1.03
mu        0.101  0.015   0.076    0.130  ...    36.0      35.0      37.0   1.00
mus       0.234  0.041   0.177    0.309  ...   152.0     152.0      95.0   1.02
gamma     0.487  0.081   0.352    0.637  ...   146.0     152.0     100.0   1.00
Is_begin  0.684  0.720   0.010    1.632  ...    96.0      57.0      38.0   1.03
Ia_begin  1.123  1.308   0.018    3.753  ...    89.0     123.0      88.0   1.00
E_begin   0.491  0.661   0.004    1.787  ...    80.0      62.0      87.0   1.04

[8 rows x 11 columns]