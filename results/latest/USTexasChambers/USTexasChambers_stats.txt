0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1497.21    35.74
p_loo       37.69        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      361   94.0%
 (0.5, 0.7]   (ok)         17    4.4%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.202  0.035   0.151    0.263  ...    34.0      32.0      40.0   1.06
pu        0.716  0.014   0.700    0.737  ...    46.0      47.0      24.0   1.03
mu        0.130  0.020   0.092    0.163  ...    10.0      11.0      47.0   1.14
mus       0.179  0.032   0.122    0.235  ...    31.0      33.0      23.0   1.05
gamma     0.198  0.039   0.131    0.268  ...    83.0      82.0      60.0   1.00
Is_begin  0.865  0.752   0.011    2.234  ...    51.0      30.0      42.0   1.02
Ia_begin  1.646  1.912   0.037    4.240  ...    83.0      58.0      60.0   1.00
E_begin   0.768  0.805   0.001    2.110  ...    78.0      58.0      59.0   1.06

[8 rows x 11 columns]