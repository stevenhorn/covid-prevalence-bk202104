0 Divergences 
Failed validation 
Computed from 80 by 390 log-likelihood matrix

         Estimate       SE
elpd_loo  -684.52    30.74
p_loo       30.56        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      363   93.1%
 (0.5, 0.7]   (ok)         23    5.9%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    0    0.0%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.291   0.050   0.201    0.350  ...    94.0      98.0      30.0   1.05
pu         0.885   0.015   0.857    0.900  ...    86.0      83.0      40.0   1.07
mu         0.134   0.020   0.103    0.182  ...     8.0       8.0      60.0   1.23
mus        0.178   0.035   0.130    0.249  ...    68.0      74.0      27.0   1.07
gamma      0.262   0.042   0.181    0.335  ...    71.0      67.0      40.0   0.99
Is_begin   3.684   2.600   0.105    8.741  ...    73.0      61.0      59.0   1.00
Ia_begin  13.682  10.132   2.010   33.478  ...   102.0      87.0      55.0   1.03
E_begin    5.315   4.020   0.480   12.642  ...    70.0      68.0      93.0   1.01

[8 rows x 11 columns]