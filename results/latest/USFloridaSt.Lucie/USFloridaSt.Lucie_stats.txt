6 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1686.00    39.04
p_loo       29.00        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      349   90.9%
 (0.5, 0.7]   (ok)         29    7.6%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.250  0.053   0.173    0.348  ...    57.0      62.0      43.0   1.08
pu        0.790  0.043   0.712    0.849  ...     8.0       8.0      15.0   1.24
mu        0.110  0.019   0.074    0.138  ...    12.0      12.0      42.0   1.14
mus       0.166  0.025   0.123    0.205  ...    68.0      70.0      55.0   1.02
gamma     0.205  0.029   0.148    0.258  ...    44.0      46.0      57.0   1.03
Is_begin  1.617  1.169   0.046    3.687  ...    77.0      60.0      60.0   1.01
Ia_begin  5.124  2.883   0.586    9.946  ...    72.0      71.0      59.0   1.00
E_begin   3.563  2.757   0.358    8.772  ...    65.0      61.0     100.0   0.98

[8 rows x 11 columns]