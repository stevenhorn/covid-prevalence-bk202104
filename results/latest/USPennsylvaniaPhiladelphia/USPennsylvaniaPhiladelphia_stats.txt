0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -2603.62    23.89
p_loo       27.64        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      360   93.8%
 (0.5, 0.7]   (ok)         18    4.7%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    2    0.5%

             mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.222   0.043   0.157    0.298  ...   152.0     146.0      61.0   0.99
pu          0.738   0.028   0.700    0.782  ...    59.0      43.0      38.0   1.03
mu          0.102   0.019   0.071    0.142  ...    68.0      78.0      42.0   1.02
mus         0.162   0.027   0.113    0.213  ...   152.0     152.0      35.0   1.00
gamma       0.295   0.050   0.219    0.398  ...    74.0      75.0      19.0   1.00
Is_begin   28.791  18.157   2.172   63.988  ...   152.0     152.0      59.0   1.04
Ia_begin   83.006  30.455  30.961  141.465  ...   118.0     109.0      65.0   1.00
E_begin   160.000  83.763  40.905  302.943  ...    92.0      89.0      91.0   1.00

[8 rows x 11 columns]