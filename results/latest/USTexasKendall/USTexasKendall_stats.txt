1 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1450.89    56.63
p_loo       44.57        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      369   96.1%
 (0.5, 0.7]   (ok)          8    2.1%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.234  0.056   0.153    0.321  ...    36.0      38.0      40.0   1.04
pu        0.831  0.017   0.805    0.862  ...    29.0      30.0      39.0   1.07
mu        0.131  0.019   0.100    0.164  ...    21.0      20.0      60.0   1.07
mus       0.182  0.033   0.128    0.239  ...    83.0      83.0      75.0   1.01
gamma     0.199  0.037   0.131    0.258  ...    65.0      62.0      46.0   1.08
Is_begin  0.931  0.936   0.036    2.725  ...    66.0      58.0      54.0   1.03
Ia_begin  1.894  1.985   0.025    4.944  ...    73.0      61.0      53.0   1.07
E_begin   0.778  1.103   0.002    2.993  ...    71.0      45.0      39.0   0.99

[8 rows x 11 columns]