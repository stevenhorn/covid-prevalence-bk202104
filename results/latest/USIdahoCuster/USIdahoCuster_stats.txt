0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -504.46    34.07
p_loo       34.45        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      368   95.8%
 (0.5, 0.7]   (ok)         12    3.1%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.255  0.050   0.186    0.341  ...    50.0      60.0      88.0   0.99
pu        0.869  0.018   0.838    0.895  ...     7.0       8.0      36.0   1.20
mu        0.144  0.022   0.104    0.187  ...     9.0       8.0      44.0   1.24
mus       0.163  0.031   0.101    0.212  ...    84.0      90.0      88.0   0.98
gamma     0.188  0.036   0.129    0.249  ...    82.0      87.0      60.0   1.02
Is_begin  0.716  0.859   0.004    2.759  ...   106.0      39.0      55.0   1.03
Ia_begin  1.891  1.793   0.042    5.284  ...   111.0      75.0      59.0   1.00
E_begin   0.837  0.913   0.007    2.611  ...    97.0      52.0      60.0   1.01

[8 rows x 11 columns]