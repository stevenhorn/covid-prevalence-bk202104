0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1056.95    48.44
p_loo       36.65        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      364   94.8%
 (0.5, 0.7]   (ok)         15    3.9%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.268  0.054   0.177    0.346  ...    67.0      68.0     100.0   1.02
pu        0.822  0.019   0.786    0.853  ...    14.0      16.0      49.0   1.12
mu        0.126  0.020   0.095    0.163  ...    50.0      46.0      55.0   1.03
mus       0.209  0.048   0.125    0.315  ...   152.0     152.0      84.0   0.99
gamma     0.241  0.045   0.177    0.339  ...   115.0     113.0      73.0   1.02
Is_begin  0.811  0.662   0.037    2.202  ...    91.0      51.0      74.0   1.04
Ia_begin  1.512  1.586   0.018    4.391  ...    56.0      53.0      89.0   1.05
E_begin   0.795  0.971   0.001    2.283  ...    95.0      35.0      24.0   1.04

[8 rows x 11 columns]