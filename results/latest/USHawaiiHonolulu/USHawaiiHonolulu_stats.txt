2 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1592.46    27.78
p_loo       31.91        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      352   91.7%
 (0.5, 0.7]   (ok)         31    8.1%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    0    0.0%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.246   0.051   0.169    0.342  ...    94.0      99.0      83.0   1.13
pu         0.773   0.046   0.702    0.846  ...    56.0      57.0      60.0   1.02
mu         0.201   0.038   0.139    0.268  ...     5.0       5.0      33.0   1.43
mus        0.239   0.036   0.184    0.310  ...    32.0      32.0      51.0   1.04
gamma      0.371   0.067   0.253    0.499  ...   152.0     152.0      60.0   1.00
Is_begin   9.347   4.827   1.430   17.554  ...    81.0      77.0      56.0   1.04
Ia_begin  22.366   9.883   6.768   39.212  ...    87.0      87.0      96.0   1.04
E_begin   31.612  21.109   0.340   65.043  ...    66.0      61.0      58.0   1.02

[8 rows x 11 columns]