0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -704.74    22.95
p_loo       23.65        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      358   93.2%
 (0.5, 0.7]   (ok)         22    5.7%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.254  0.061   0.157    0.343  ...    79.0      79.0      61.0   1.00
pu        0.845  0.018   0.816    0.875  ...    44.0      40.0      42.0   1.05
mu        0.143  0.022   0.097    0.172  ...    33.0      36.0      47.0   1.05
mus       0.185  0.036   0.130    0.253  ...   128.0     152.0      88.0   0.99
gamma     0.231  0.042   0.164    0.313  ...   152.0     152.0      80.0   1.00
Is_begin  0.799  0.744   0.040    2.547  ...   122.0     152.0      91.0   1.01
Ia_begin  1.640  1.841   0.014    5.589  ...    86.0     109.0     100.0   1.04
E_begin   0.801  0.888   0.018    3.262  ...   126.0     132.0      93.0   0.99

[8 rows x 11 columns]