0 Divergences 
Passed validation 
Computed from 80 by 385 log-likelihood matrix

         Estimate       SE
elpd_loo -2294.15    22.59
p_loo       30.43        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      322   83.6%
 (0.5, 0.7]   (ok)         43   11.2%
   (0.7, 1]   (bad)        20    5.2%
   (1, Inf)   (very bad)    0    0.0%

              mean       sd    hdi_3%  ...  ess_bulk  ess_tail  r_hat
pa           0.346    0.006     0.332  ...       4.0      16.0   1.61
pu           0.899    0.001     0.898  ...       5.0      18.0   1.41
mu           0.378    0.006     0.368  ...      40.0      60.0   1.04
mus          0.365    0.039     0.291  ...      64.0      87.0   1.03
gamma        0.912    0.064     0.786  ...      44.0      30.0   1.03
Is_begin   144.581   47.823    48.420  ...       8.0      15.0   1.22
Ia_begin   420.913   75.622   290.702  ...      11.0      37.0   1.15
E_begin   1937.171  222.093  1528.031  ...      29.0      59.0   1.03

[8 rows x 11 columns]