0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1472.87    30.82
p_loo       31.65        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      358   93.2%
 (0.5, 0.7]   (ok)         19    4.9%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    2    0.5%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.211   0.048   0.153    0.301  ...    61.0      58.0      38.0   0.99
pu         0.721   0.015   0.701    0.748  ...    34.0      72.0      33.0   1.09
mu         0.135   0.024   0.095    0.172  ...    19.0      21.0      57.0   1.06
mus        0.212   0.031   0.157    0.269  ...   152.0     152.0      91.0   0.99
gamma      0.340   0.051   0.250    0.439  ...    76.0      75.0      99.0   1.03
Is_begin   2.558   1.544   0.178    5.528  ...    47.0      47.0     100.0   1.04
Ia_begin  11.068   3.986   3.014   17.978  ...    58.0      60.0      22.0   1.02
E_begin   22.924  12.463   4.076   40.951  ...    74.0      59.0      58.0   1.01

[8 rows x 11 columns]