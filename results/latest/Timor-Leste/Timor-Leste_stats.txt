0 Divergences 
Passed validation 
Computed from 80 by 385 log-likelihood matrix

         Estimate       SE
elpd_loo  -673.94    53.42
p_loo       54.44        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      361   93.8%
 (0.5, 0.7]   (ok)         15    3.9%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.268  0.051   0.179    0.348  ...    87.0      87.0      60.0   1.01
pu        0.874  0.020   0.841    0.900  ...    56.0      44.0      40.0   1.04
mu        0.155  0.025   0.104    0.198  ...    27.0      27.0      59.0   1.00
mus       0.189  0.032   0.139    0.259  ...   152.0     152.0      59.0   1.05
gamma     0.254  0.041   0.183    0.329  ...   152.0     152.0      97.0   0.99
Is_begin  0.919  0.833   0.015    2.753  ...    68.0      57.0     100.0   1.01
Ia_begin  1.791  1.949   0.002    5.800  ...    40.0      15.0      18.0   1.10
E_begin   1.170  1.151   0.002    3.068  ...    69.0      49.0      40.0   1.05

[8 rows x 11 columns]