0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1068.32    31.93
p_loo       27.87        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      352   91.7%
 (0.5, 0.7]   (ok)         27    7.0%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.246  0.047   0.165    0.327  ...    83.0      78.0      40.0   1.01
pu        0.769  0.023   0.726    0.806  ...    10.0      11.0      58.0   1.15
mu        0.141  0.018   0.118    0.182  ...     5.0       6.0      39.0   1.35
mus       0.186  0.029   0.143    0.243  ...    63.0      63.0      60.0   1.00
gamma     0.245  0.048   0.171    0.342  ...   150.0     152.0      57.0   0.99
Is_begin  0.724  0.523   0.064    1.506  ...    79.0      49.0      83.0   1.08
Ia_begin  1.652  1.557   0.014    4.793  ...   114.0      38.0      34.0   1.05
E_begin   0.760  0.893   0.002    2.651  ...    21.0      19.0      54.0   1.10

[8 rows x 11 columns]