0 Divergences 
Passed validation 
Computed from 80 by 385 log-likelihood matrix

         Estimate       SE
elpd_loo -1948.65    32.26
p_loo       28.80        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      352   91.4%
 (0.5, 0.7]   (ok)         25    6.5%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    1    0.3%

            mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.273  0.055   0.162    0.349  ...   106.0     120.0      51.0   1.03
pu         0.879  0.022   0.841    0.900  ...   152.0     152.0      91.0   1.02
mu         0.167  0.027   0.131    0.231  ...    22.0      21.0      80.0   1.09
mus        0.181  0.031   0.118    0.236  ...   144.0     152.0      93.0   1.04
gamma      0.260  0.051   0.185    0.358  ...    46.0      53.0      64.0   1.03
Is_begin   5.848  3.802   0.036   12.391  ...   130.0     103.0      83.0   1.04
Ia_begin  13.454  8.655   0.919   28.565  ...    96.0      82.0      60.0   1.02
E_begin   10.009  7.986   0.343   24.198  ...    77.0      84.0      74.0   1.01

[8 rows x 11 columns]