0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1081.84    37.26
p_loo       36.70        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      347   90.4%
 (0.5, 0.7]   (ok)         29    7.6%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.262  0.055   0.164    0.349  ...    83.0      82.0      59.0   1.01
pu        0.861  0.026   0.837    0.889  ...    29.0      37.0      20.0   1.15
mu        0.125  0.018   0.092    0.159  ...    43.0      41.0      57.0   1.04
mus       0.157  0.034   0.101    0.221  ...   110.0      97.0      51.0   1.05
gamma     0.174  0.036   0.119    0.242  ...    58.0      57.0      60.0   1.00
Is_begin  0.806  0.713   0.026    2.096  ...    95.0      76.0      88.0   1.00
Ia_begin  1.808  1.554   0.002    4.929  ...   111.0      69.0      15.0   0.99
E_begin   0.929  0.869   0.005    2.594  ...   114.0      89.0     100.0   1.00

[8 rows x 11 columns]