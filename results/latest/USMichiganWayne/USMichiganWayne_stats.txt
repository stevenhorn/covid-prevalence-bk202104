0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -2459.30    23.92
p_loo       21.79        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      354   92.2%
 (0.5, 0.7]   (ok)         24    6.2%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    0    0.0%

             mean       sd   hdi_3%  ...  ess_bulk  ess_tail  r_hat
pa          0.285    0.062    0.162  ...       6.0      30.0   1.31
pu          0.721    0.018    0.701  ...       7.0      51.0   1.22
mu          0.176    0.050    0.105  ...       3.0      17.0   2.18
mus         0.233    0.043    0.164  ...       4.0      24.0   1.61
gamma       0.370    0.053    0.286  ...      25.0      91.0   1.09
Is_begin   59.490   23.170    4.905  ...       7.0      40.0   1.32
Ia_begin  152.595   45.274   57.557  ...       5.0      36.0   1.41
E_begin   684.462  274.059  276.235  ...       3.0      15.0   1.91

[8 rows x 11 columns]