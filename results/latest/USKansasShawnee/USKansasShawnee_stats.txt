0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1858.59    31.63
p_loo        9.04        -
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      377   98.2%
 (0.5, 0.7]   (ok)          7    1.8%
   (0.7, 1]   (bad)         0    0.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.194  0.005   0.187    0.203  ...     3.0       4.0      14.0   1.84
pu        0.829  0.006   0.823    0.836  ...     3.0       3.0      21.0   1.98
mu        0.169  0.009   0.160    0.179  ...     3.0       3.0      14.0   2.12
mus       0.137  0.016   0.120    0.155  ...     3.0       3.0      22.0   1.91
gamma     0.069  0.004   0.064    0.073  ...     3.0       3.0      22.0   2.23
Is_begin  0.811  0.404   0.349    1.329  ...     3.0       3.0      14.0   2.35
Ia_begin  4.526  1.624   2.818    6.729  ...     3.0       3.0      24.0   1.99
E_begin   1.109  0.711   0.322    1.871  ...     3.0       3.0      14.0   2.03

[8 rows x 11 columns]