0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1009.34    56.16
p_loo       43.64        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      351   91.4%
 (0.5, 0.7]   (ok)         26    6.8%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.243  0.056   0.156    0.323  ...    44.0      39.0      15.0   1.09
pu        0.785  0.024   0.734    0.820  ...    32.0      34.0      54.0   1.03
mu        0.131  0.024   0.096    0.186  ...     8.0       8.0      59.0   1.22
mus       0.216  0.034   0.145    0.266  ...    56.0      52.0      59.0   1.02
gamma     0.276  0.051   0.190    0.359  ...    73.0      71.0      57.0   1.02
Is_begin  0.699  0.697   0.000    2.231  ...    46.0      32.0      14.0   1.02
Ia_begin  1.367  1.284   0.101    4.323  ...    51.0      42.0      60.0   1.02
E_begin   0.683  0.859   0.020    2.222  ...    46.0      35.0      40.0   1.01

[8 rows x 11 columns]