0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -532.50    31.45
p_loo       36.13        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      353   91.9%
 (0.5, 0.7]   (ok)         26    6.8%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.210  0.045   0.152    0.302  ...    68.0      73.0      58.0   1.05
pu        0.735  0.032   0.702    0.793  ...    24.0      10.0      32.0   1.16
mu        0.155  0.040   0.080    0.214  ...     9.0       9.0      33.0   1.24
mus       0.240  0.043   0.172    0.316  ...    62.0      63.0      33.0   1.05
gamma     0.280  0.043   0.216    0.355  ...    75.0      70.0      97.0   0.98
Is_begin  0.245  0.286   0.003    0.803  ...    53.0      30.0      46.0   1.07
Ia_begin  0.496  0.727   0.001    1.365  ...    19.0      16.0      56.0   1.11
E_begin   0.201  0.305   0.003    0.744  ...    50.0      47.0      39.0   1.01

[8 rows x 11 columns]