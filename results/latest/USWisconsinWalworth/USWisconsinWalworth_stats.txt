0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1620.39    40.44
p_loo       29.53        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      346   90.1%
 (0.5, 0.7]   (ok)         32    8.3%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.241  0.047   0.168    0.322  ...    74.0      87.0      31.0   1.06
pu        0.740  0.023   0.705    0.785  ...    55.0      50.0      56.0   1.03
mu        0.109  0.020   0.072    0.140  ...    45.0      45.0      43.0   1.03
mus       0.163  0.030   0.100    0.212  ...    93.0      97.0      60.0   1.05
gamma     0.179  0.032   0.131    0.245  ...   117.0     121.0     100.0   1.02
Is_begin  0.832  0.759   0.013    2.151  ...    93.0      66.0      99.0   1.01
Ia_begin  0.654  0.610   0.005    1.968  ...   101.0      47.0      45.0   1.01
E_begin   0.463  0.660   0.002    1.575  ...   101.0      50.0      58.0   1.01

[8 rows x 11 columns]