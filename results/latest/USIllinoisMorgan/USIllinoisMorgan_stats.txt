0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1196.86    42.42
p_loo       38.12        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      360   93.8%
 (0.5, 0.7]   (ok)         19    4.9%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.236  0.054   0.151    0.314  ...    21.0      26.0      60.0   1.06
pu        0.759  0.023   0.720    0.802  ...    27.0      27.0      36.0   1.02
mu        0.169  0.037   0.108    0.236  ...     7.0       6.0      16.0   1.27
mus       0.215  0.043   0.141    0.315  ...    19.0      21.0      17.0   1.07
gamma     0.267  0.050   0.191    0.368  ...   105.0     105.0     100.0   1.02
Is_begin  0.819  0.654   0.017    2.039  ...    81.0      99.0      60.0   1.01
Ia_begin  1.062  1.531   0.002    3.387  ...    80.0      29.0      43.0   1.05
E_begin   0.598  0.733   0.020    1.911  ...    87.0      98.0      72.0   0.98

[8 rows x 11 columns]