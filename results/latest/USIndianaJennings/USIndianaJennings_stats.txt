0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -939.17    29.67
p_loo       30.73        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      352   91.7%
 (0.5, 0.7]   (ok)         27    7.0%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.250  0.050   0.173    0.332  ...    84.0      98.0      50.0   1.09
pu        0.800  0.037   0.744    0.863  ...    21.0      23.0      58.0   1.05
mu        0.132  0.023   0.093    0.175  ...    21.0      23.0      35.0   1.08
mus       0.183  0.036   0.118    0.247  ...    59.0      59.0      34.0   1.11
gamma     0.228  0.043   0.157    0.295  ...   152.0     152.0      93.0   1.05
Is_begin  1.390  0.786   0.211    3.063  ...    77.0      65.0      88.0   1.02
Ia_begin  4.077  3.392   0.058    8.716  ...   109.0     126.0      50.0   1.23
E_begin   2.373  2.291   0.114    6.302  ...    78.0      93.0      95.0   1.01

[8 rows x 11 columns]