0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -758.00    32.48
p_loo       29.06        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      365   95.1%
 (0.5, 0.7]   (ok)         17    4.4%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.255  0.048   0.155    0.327  ...   104.0     105.0      55.0   1.00
pu        0.799  0.017   0.770    0.830  ...    61.0      61.0      93.0   0.99
mu        0.145  0.026   0.094    0.187  ...    45.0      45.0      66.0   1.13
mus       0.184  0.033   0.132    0.241  ...    35.0      54.0      57.0   1.03
gamma     0.205  0.031   0.144    0.251  ...    59.0      60.0      65.0   1.02
Is_begin  0.579  0.451   0.007    1.448  ...    94.0      86.0      59.0   1.00
Ia_begin  1.382  1.745   0.005    4.039  ...    98.0      91.0      84.0   1.00
E_begin   0.593  0.652   0.006    2.052  ...    77.0      62.0      93.0   0.99

[8 rows x 11 columns]