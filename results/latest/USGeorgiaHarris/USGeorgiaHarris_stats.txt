0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1056.83    35.03
p_loo       31.02        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      357   93.0%
 (0.5, 0.7]   (ok)         22    5.7%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.253  0.052   0.174    0.347  ...   152.0     152.0      60.0   1.12
pu        0.849  0.033   0.779    0.892  ...    13.0      23.0      22.0   1.08
mu        0.136  0.023   0.097    0.179  ...    17.0      16.0      65.0   1.13
mus       0.176  0.031   0.112    0.226  ...    88.0      90.0      76.0   1.02
gamma     0.222  0.044   0.151    0.314  ...    97.0     115.0      54.0   1.01
Is_begin  1.048  0.990   0.019    2.805  ...   152.0     136.0      60.0   0.98
Ia_begin  2.867  2.249   0.171    6.907  ...   147.0     152.0      93.0   1.00
E_begin   1.451  2.137   0.018    4.127  ...    94.0     109.0      54.0   1.05

[8 rows x 11 columns]