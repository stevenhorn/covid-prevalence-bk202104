0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1262.83    28.60
p_loo       28.47        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      345   89.8%
 (0.5, 0.7]   (ok)         28    7.3%
   (0.7, 1]   (bad)        10    2.6%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.237  0.054   0.151    0.332  ...    47.0      44.0      40.0   1.18
pu        0.787  0.022   0.753    0.835  ...    44.0      47.0      25.0   1.02
mu        0.117  0.022   0.079    0.159  ...    46.0      48.0      60.0   1.03
mus       0.166  0.037   0.102    0.234  ...    28.0      34.0      60.0   1.05
gamma     0.177  0.042   0.125    0.271  ...   118.0     111.0      58.0   1.01
Is_begin  0.853  0.788   0.036    2.187  ...   119.0      99.0      96.0   0.99
Ia_begin  1.592  1.807   0.010    5.086  ...    23.0      17.0      54.0   1.09
E_begin   0.929  1.043   0.014    3.122  ...    21.0      26.0      95.0   1.06

[8 rows x 11 columns]