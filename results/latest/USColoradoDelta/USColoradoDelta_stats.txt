0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -988.69    34.04
p_loo       28.77        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      352   91.7%
 (0.5, 0.7]   (ok)         26    6.8%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.272  0.048   0.197    0.348  ...    55.0      58.0      59.0   0.99
pu        0.827  0.020   0.790    0.857  ...    48.0      43.0      60.0   1.03
mu        0.138  0.025   0.107    0.196  ...    53.0      42.0      93.0   1.00
mus       0.174  0.038   0.118    0.236  ...    63.0      72.0      60.0   1.00
gamma     0.218  0.039   0.153    0.285  ...   127.0     113.0      49.0   1.00
Is_begin  0.777  0.786   0.024    2.351  ...    77.0      58.0      40.0   1.04
Ia_begin  1.979  2.032   0.015    6.001  ...    88.0      70.0      60.0   1.03
E_begin   1.033  1.063   0.023    3.046  ...    76.0      73.0      40.0   0.98

[8 rows x 11 columns]