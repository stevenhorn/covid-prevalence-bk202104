0 Divergences 
Passed validation 
Computed from 80 by 390 log-likelihood matrix

         Estimate       SE
elpd_loo  -899.12    39.75
p_loo       35.50        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      368   94.4%
 (0.5, 0.7]   (ok)         16    4.1%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    2    0.5%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.276   0.048   0.193    0.350  ...   152.0     138.0      38.0   1.05
pu         0.880   0.016   0.850    0.900  ...    77.0     123.0      58.0   1.01
mu         0.172   0.026   0.132    0.215  ...    20.0      19.0      37.0   1.03
mus        0.233   0.033   0.178    0.298  ...   152.0     152.0      52.0   1.09
gamma      0.361   0.045   0.276    0.440  ...   143.0     138.0      60.0   1.00
Is_begin   7.739   5.672   0.073   18.175  ...   123.0     105.0      42.0   1.01
Ia_begin  57.752  26.380  11.808  104.249  ...    66.0      68.0      59.0   1.02
E_begin   38.935  26.508   1.379   94.818  ...    81.0      69.0      24.0   1.01

[8 rows x 11 columns]