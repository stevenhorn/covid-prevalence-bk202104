0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1293.55    70.26
p_loo       31.22        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      360   93.8%
 (0.5, 0.7]   (ok)         17    4.4%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.159  0.007   0.150    0.175  ...    92.0      87.0      60.0   1.02
pu        0.702  0.002   0.700    0.706  ...    85.0      56.0      60.0   1.01
mu        0.109  0.011   0.090    0.134  ...    13.0      13.0      32.0   1.12
mus       0.224  0.041   0.158    0.308  ...     8.0       8.0      31.0   1.27
gamma     0.360  0.050   0.279    0.442  ...    45.0      45.0      55.0   1.02
Is_begin  0.231  0.268   0.000    0.677  ...    71.0      43.0      81.0   1.05
Ia_begin  0.359  0.719   0.006    0.935  ...    92.0      54.0      60.0   1.01
E_begin   0.128  0.162   0.000    0.353  ...    73.0      46.0      29.0   1.01

[8 rows x 11 columns]