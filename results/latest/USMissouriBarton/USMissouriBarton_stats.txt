0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1049.27    63.84
p_loo       36.81        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      360   93.8%
 (0.5, 0.7]   (ok)         18    4.7%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.189  0.033   0.150    0.253  ...    77.0     101.0      45.0   1.00
pu        0.710  0.009   0.700    0.727  ...    46.0      43.0      16.0   1.04
mu        0.104  0.016   0.065    0.131  ...    43.0      44.0      22.0   1.04
mus       0.238  0.035   0.191    0.314  ...   102.0     152.0      96.0   1.03
gamma     0.402  0.068   0.291    0.535  ...   102.0      96.0      60.0   1.02
Is_begin  0.260  0.273   0.001    0.753  ...    78.0      65.0     100.0   1.01
Ia_begin  0.426  0.481   0.001    1.363  ...    69.0      76.0      59.0   0.99
E_begin   0.179  0.206   0.003    0.684  ...    58.0      30.0      53.0   1.09

[8 rows x 11 columns]