0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -821.34    31.45
p_loo       33.15        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      359   93.5%
 (0.5, 0.7]   (ok)         19    4.9%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.255  0.049   0.181    0.344  ...    68.0      62.0      58.0   1.03
pu        0.782  0.022   0.732    0.813  ...    16.0      18.0      16.0   1.08
mu        0.169  0.024   0.121    0.204  ...    10.0      10.0      82.0   1.19
mus       0.204  0.035   0.134    0.263  ...   142.0     145.0      57.0   1.00
gamma     0.263  0.045   0.171    0.334  ...   152.0     152.0      97.0   0.99
Is_begin  0.306  0.445   0.001    1.381  ...    72.0      84.0      76.0   1.01
Ia_begin  0.652  0.880   0.003    3.019  ...    73.0      56.0      93.0   1.01
E_begin   0.201  0.248   0.002    0.654  ...   111.0      67.0      43.0   1.01

[8 rows x 11 columns]