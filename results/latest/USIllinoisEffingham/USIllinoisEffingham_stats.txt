0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1092.88    38.00
p_loo       35.37        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      363   94.5%
 (0.5, 0.7]   (ok)         15    3.9%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.225  0.047   0.152    0.290  ...    23.0      28.0      53.0   1.07
pu        0.724  0.016   0.701    0.751  ...    46.0      55.0      34.0   1.02
mu        0.135  0.021   0.105    0.188  ...    21.0      24.0      15.0   1.04
mus       0.200  0.038   0.152    0.295  ...    61.0      70.0      59.0   1.00
gamma     0.253  0.048   0.183    0.345  ...   133.0     118.0     100.0   0.99
Is_begin  0.755  0.731   0.045    1.963  ...   113.0     134.0      59.0   1.00
Ia_begin  1.321  1.226   0.025    3.813  ...    92.0     145.0      60.0   1.00
E_begin   0.567  0.435   0.049    1.537  ...    89.0      72.0      60.0   1.00

[8 rows x 11 columns]