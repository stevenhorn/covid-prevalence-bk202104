0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1502.84    70.56
p_loo       39.19        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      359   93.5%
 (0.5, 0.7]   (ok)         19    4.9%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.179  0.026   0.150    0.226  ...   152.0     107.0      40.0   1.01
pu        0.711  0.011   0.700    0.736  ...   152.0     152.0      42.0   1.00
mu        0.100  0.014   0.077    0.128  ...    86.0      84.0      91.0   0.99
mus       0.216  0.031   0.162    0.281  ...   152.0     152.0      77.0   1.05
gamma     0.425  0.075   0.302    0.566  ...    98.0     100.0      66.0   1.00
Is_begin  0.942  0.877   0.002    2.673  ...   152.0     128.0      69.0   1.00
Ia_begin  1.730  1.756   0.009    5.290  ...   130.0     140.0      22.0   0.99
E_begin   0.650  0.734   0.001    1.622  ...   103.0      25.0      97.0   1.07

[8 rows x 11 columns]