0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -699.95    29.35
p_loo       30.79        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      356   92.7%
 (0.5, 0.7]   (ok)         26    6.8%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.198  0.035   0.151    0.261  ...   152.0     127.0      53.0   1.00
pu        0.712  0.010   0.700    0.728  ...    88.0      68.0      21.0   1.02
mu        0.142  0.030   0.101    0.200  ...    22.0      25.0      33.0   1.07
mus       0.215  0.035   0.145    0.284  ...    76.0      76.0      43.0   1.02
gamma     0.239  0.053   0.144    0.333  ...    88.0      93.0      60.0   0.99
Is_begin  0.561  0.563   0.007    1.877  ...    42.0      72.0      59.0   1.02
Ia_begin  1.094  1.027   0.022    2.777  ...    77.0      83.0      49.0   0.99
E_begin   0.356  0.395   0.002    1.447  ...    64.0      47.0      40.0   1.02

[8 rows x 11 columns]