0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -875.14    23.80
p_loo       26.31        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      349   90.9%
 (0.5, 0.7]   (ok)         27    7.0%
   (0.7, 1]   (bad)         8    2.1%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.255  0.063   0.156    0.348  ...   152.0     152.0      56.0   1.07
pu        0.842  0.025   0.790    0.880  ...    16.0      19.0      42.0   1.08
mu        0.126  0.026   0.086    0.174  ...    23.0      22.0      47.0   1.07
mus       0.192  0.036   0.125    0.247  ...   152.0     152.0      59.0   1.00
gamma     0.243  0.047   0.147    0.310  ...   152.0     152.0      35.0   1.00
Is_begin  0.973  0.715   0.051    2.358  ...   152.0     152.0      80.0   1.02
Ia_begin  1.949  1.611   0.010    5.174  ...   119.0      85.0      60.0   0.99
E_begin   0.868  1.040   0.008    2.640  ...    79.0      48.0      40.0   0.99

[8 rows x 11 columns]