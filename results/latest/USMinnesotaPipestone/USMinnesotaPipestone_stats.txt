0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -813.26    26.84
p_loo       32.54        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      360   93.8%
 (0.5, 0.7]   (ok)         19    4.9%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.251  0.059   0.162    0.344  ...   152.0     152.0      40.0   1.10
pu        0.764  0.025   0.720    0.805  ...    65.0      69.0      60.0   1.03
mu        0.158  0.029   0.117    0.220  ...    55.0      44.0      61.0   1.06
mus       0.214  0.036   0.165    0.283  ...    82.0      81.0     100.0   0.99
gamma     0.278  0.054   0.196    0.387  ...    68.0      66.0      55.0   1.01
Is_begin  0.753  0.692   0.005    1.973  ...    91.0     116.0      59.0   1.04
Ia_begin  1.036  1.114   0.014    3.227  ...    73.0      63.0      60.0   1.02
E_begin   0.630  0.794   0.002    2.521  ...    72.0      66.0      57.0   1.01

[8 rows x 11 columns]