5 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -2472.14    36.67
p_loo       31.43        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      343   89.3%
 (0.5, 0.7]   (ok)         31    8.1%
   (0.7, 1]   (bad)         8    2.1%
   (1, Inf)   (very bad)    2    0.5%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.253   0.058   0.150    0.330  ...    55.0      57.0      25.0   1.07
pu         0.778   0.048   0.703    0.853  ...     5.0       5.0      22.0   1.51
mu         0.096   0.012   0.075    0.115  ...    23.0      22.0      62.0   1.07
mus        0.198   0.031   0.144    0.262  ...    76.0      85.0      69.0   1.02
gamma      0.272   0.052   0.191    0.386  ...    67.0      92.0      39.0   1.00
Is_begin  15.982  10.527   0.510   35.898  ...    78.0      73.0      96.0   1.00
Ia_begin  43.891  24.004  10.054   92.193  ...    78.0      65.0      54.0   0.99
E_begin   37.040  28.745   0.246   95.546  ...    36.0      31.0      22.0   1.03

[8 rows x 11 columns]