0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -992.31    37.33
p_loo       33.68        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      364   94.8%
 (0.5, 0.7]   (ok)         15    3.9%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.258  0.045   0.164    0.322  ...    97.0      98.0      84.0   0.99
pu        0.795  0.026   0.732    0.827  ...    34.0      38.0      38.0   1.05
mu        0.122  0.022   0.081    0.167  ...    34.0      34.0      29.0   1.01
mus       0.208  0.040   0.149    0.284  ...    60.0      64.0      66.0   1.02
gamma     0.287  0.058   0.174    0.386  ...    87.0      80.0      91.0   0.99
Is_begin  0.455  0.471   0.010    1.453  ...    97.0     119.0      72.0   1.01
Ia_begin  0.948  1.310   0.000    3.149  ...    63.0      70.0      56.0   1.01
E_begin   0.461  0.646   0.004    1.773  ...    90.0      66.0      57.0   0.99

[8 rows x 11 columns]