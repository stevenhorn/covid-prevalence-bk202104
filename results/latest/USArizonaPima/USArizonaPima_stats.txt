0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -2282.59    31.85
p_loo       29.58        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      359   93.5%
 (0.5, 0.7]   (ok)         22    5.7%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.245  0.057   0.163    0.339  ...   152.0     152.0      95.0   1.01
pu        0.762  0.038   0.704    0.826  ...    32.0      32.0      59.0   1.00
mu        0.133  0.027   0.091    0.179  ...    17.0      22.0      19.0   1.04
mus       0.198  0.035   0.150    0.265  ...    54.0      77.0      38.0   1.02
gamma     0.268  0.044   0.195    0.355  ...   134.0     152.0     100.0   1.00
Is_begin  2.068  1.331   0.125    4.529  ...   152.0     152.0      83.0   1.05
Ia_begin  5.489  3.894   0.562   13.696  ...   104.0     109.0      96.0   0.98
E_begin   5.366  4.889   0.248   14.295  ...    97.0     116.0      96.0   0.99

[8 rows x 11 columns]