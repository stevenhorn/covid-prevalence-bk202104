0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1101.37    47.26
p_loo       34.85        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      349   90.9%
 (0.5, 0.7]   (ok)         29    7.6%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.261  0.055   0.160    0.348  ...    50.0      44.0      38.0   1.06
pu        0.847  0.022   0.799    0.881  ...    35.0      39.0      36.0   1.03
mu        0.145  0.024   0.102    0.184  ...    16.0      22.0      59.0   1.11
mus       0.174  0.035   0.116    0.243  ...   152.0     152.0      73.0   1.00
gamma     0.212  0.045   0.124    0.287  ...    71.0      61.0      60.0   1.01
Is_begin  0.866  0.782   0.010    2.428  ...   132.0     107.0      56.0   1.03
Ia_begin  1.984  2.160   0.066    6.136  ...   114.0     152.0     100.0   0.99
E_begin   0.815  0.735   0.009    2.330  ...    77.0     101.0      43.0   1.01

[8 rows x 11 columns]