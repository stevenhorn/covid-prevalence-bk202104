0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -975.96    22.81
p_loo       22.78        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      357   93.0%
 (0.5, 0.7]   (ok)         20    5.2%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.263  0.052   0.179    0.349  ...    56.0      52.0      31.0   1.02
pu        0.835  0.058   0.708    0.899  ...     4.0       5.0      27.0   1.44
mu        0.101  0.021   0.065    0.137  ...     7.0       7.0      60.0   1.27
mus       0.141  0.025   0.100    0.183  ...   127.0     120.0      42.0   1.01
gamma     0.166  0.037   0.102    0.229  ...    75.0      90.0      57.0   1.07
Is_begin  0.905  0.898   0.002    2.392  ...    90.0      70.0      59.0   1.02
Ia_begin  2.254  1.905   0.200    5.854  ...    23.0      54.0      59.0   1.05
E_begin   1.257  1.126   0.029    3.480  ...    16.0      17.0      65.0   1.09

[8 rows x 11 columns]