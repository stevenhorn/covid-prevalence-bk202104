0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1296.94    54.46
p_loo       37.19        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      356   92.7%
 (0.5, 0.7]   (ok)         23    6.0%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.256  0.054   0.169    0.347  ...   152.0     152.0      58.0   1.04
pu        0.767  0.025   0.719    0.805  ...    27.0      24.0      59.0   1.06
mu        0.110  0.021   0.084    0.164  ...    32.0      33.0      54.0   1.05
mus       0.196  0.032   0.142    0.255  ...    86.0     106.0      51.0   1.00
gamma     0.228  0.041   0.163    0.312  ...   122.0     138.0      96.0   1.06
Is_begin  0.705  0.519   0.036    1.740  ...   152.0     152.0      44.0   1.00
Ia_begin  1.654  1.315   0.009    3.936  ...   145.0     101.0      60.0   1.00
E_begin   0.937  0.958   0.000    2.732  ...   109.0      72.0      49.0   0.99

[8 rows x 11 columns]