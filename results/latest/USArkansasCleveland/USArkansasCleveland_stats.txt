0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -793.12    27.60
p_loo       26.86        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      358   93.2%
 (0.5, 0.7]   (ok)         19    4.9%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.260  0.051   0.170    0.342  ...   152.0     145.0      60.0   1.00
pu        0.754  0.024   0.707    0.796  ...    60.0      62.0      60.0   1.00
mu        0.129  0.025   0.083    0.171  ...    38.0      38.0     100.0   1.01
mus       0.200  0.032   0.144    0.259  ...    83.0      82.0      96.0   0.99
gamma     0.232  0.043   0.172    0.318  ...   110.0     113.0      93.0   0.99
Is_begin  0.801  0.777   0.039    2.293  ...   111.0      88.0      66.0   1.00
Ia_begin  1.500  1.545   0.016    4.745  ...    77.0      76.0      61.0   1.01
E_begin   0.697  0.863   0.007    2.485  ...    81.0      87.0      72.0   1.00

[8 rows x 11 columns]