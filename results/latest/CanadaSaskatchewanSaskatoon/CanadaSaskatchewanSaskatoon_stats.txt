0 Divergences 
Passed validation 
Computed from 80 by 247 log-likelihood matrix

         Estimate       SE
elpd_loo  -995.55    23.13
p_loo       17.09        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      233   94.3%
 (0.5, 0.7]   (ok)         12    4.9%
   (0.7, 1]   (bad)         2    0.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.259  0.060   0.158    0.342  ...    37.0      93.0      40.0   1.18
pu        0.860  0.040   0.779    0.900  ...    37.0      12.0      38.0   1.14
mu        0.136  0.035   0.094    0.205  ...     6.0       7.0      20.0   1.29
mus       0.164  0.027   0.122    0.208  ...    59.0      62.0      20.0   1.09
gamma     0.199  0.039   0.140    0.278  ...   152.0     152.0     102.0   0.99
Is_begin  4.987  3.356   0.148   10.717  ...    78.0      68.0      60.0   1.01
Ia_begin  2.196  1.608   0.043    4.856  ...   103.0      46.0      26.0   1.11
E_begin   2.542  2.546   0.040    7.375  ...    73.0      53.0      72.0   1.01

[8 rows x 11 columns]