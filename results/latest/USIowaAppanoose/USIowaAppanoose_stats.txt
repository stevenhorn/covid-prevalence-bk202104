0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -823.34    32.96
p_loo       32.76        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      351   91.4%
 (0.5, 0.7]   (ok)         29    7.6%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.251  0.066   0.153    0.338  ...    97.0      89.0      26.0   1.02
pu        0.830  0.017   0.807    0.865  ...    42.0      57.0      60.0   1.05
mu        0.134  0.021   0.095    0.164  ...    10.0      10.0      35.0   1.19
mus       0.229  0.033   0.169    0.288  ...    40.0      44.0      97.0   1.03
gamma     0.294  0.050   0.198    0.376  ...    86.0      85.0      96.0   0.99
Is_begin  0.775  0.755   0.007    2.107  ...    37.0      18.0      60.0   1.08
Ia_begin  1.333  1.502   0.008    5.141  ...    57.0      31.0      60.0   1.01
E_begin   0.573  0.553   0.009    1.587  ...    56.0      42.0      42.0   1.01

[8 rows x 11 columns]