0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -663.30    34.37
p_loo       30.00        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      361   94.0%
 (0.5, 0.7]   (ok)         19    4.9%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.244  0.052   0.158    0.324  ...    82.0      79.0      46.0   0.99
pu        0.741  0.025   0.705    0.783  ...    44.0      39.0      60.0   1.04
mu        0.134  0.030   0.092    0.204  ...    14.0      12.0      17.0   1.12
mus       0.203  0.037   0.147    0.276  ...    59.0      43.0      59.0   1.04
gamma     0.243  0.040   0.184    0.318  ...    91.0      95.0      60.0   1.04
Is_begin  0.597  0.511   0.007    1.593  ...    78.0      81.0      60.0   1.01
Ia_begin  1.203  1.586   0.003    4.146  ...   107.0      82.0      58.0   1.01
E_begin   0.507  0.683   0.007    1.579  ...    99.0      65.0      76.0   1.01

[8 rows x 11 columns]