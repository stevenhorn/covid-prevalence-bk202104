0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1481.12    28.59
p_loo       23.39        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      351   91.4%
 (0.5, 0.7]   (ok)         27    7.0%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.245  0.048   0.151    0.322  ...   143.0     152.0      40.0   1.04
pu        0.765  0.034   0.703    0.818  ...    56.0      54.0      19.0   1.06
mu        0.120  0.024   0.076    0.156  ...    35.0      62.0      58.0   1.05
mus       0.171  0.030   0.110    0.216  ...    50.0      45.0      60.0   1.02
gamma     0.201  0.040   0.142    0.286  ...    61.0      74.0      40.0   1.00
Is_begin  1.445  1.116   0.060    3.764  ...   102.0      72.0      59.0   1.03
Ia_begin  3.764  2.575   0.251    9.052  ...   139.0     110.0      59.0   1.04
E_begin   3.377  2.939   0.199    8.608  ...   152.0     131.0      60.0   0.98

[8 rows x 11 columns]