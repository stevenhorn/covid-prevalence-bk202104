0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -640.81    78.98
p_loo       56.98        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      365   95.1%
 (0.5, 0.7]   (ok)         17    4.4%
   (0.7, 1]   (bad)         0    0.0%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.236  0.052   0.156    0.322  ...   152.0     103.0      36.0   1.03
pu        0.732  0.020   0.701    0.768  ...    75.0      65.0      81.0   1.04
mu        0.128  0.027   0.089    0.188  ...    76.0      61.0      45.0   1.01
mus       0.236  0.040   0.186    0.327  ...    87.0      81.0      40.0   0.99
gamma     0.312  0.065   0.186    0.413  ...   152.0     140.0      60.0   0.98
Is_begin  0.433  0.381   0.013    1.192  ...    59.0      70.0      60.0   0.99
Ia_begin  0.659  0.760   0.010    1.919  ...   100.0      71.0      44.0   1.01
E_begin   0.262  0.288   0.004    0.745  ...   107.0      76.0      59.0   1.01

[8 rows x 11 columns]