0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1476.29    29.40
p_loo       31.54        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      352   91.7%
 (0.5, 0.7]   (ok)         28    7.3%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.234  0.043   0.153    0.303  ...    59.0     110.0      23.0   1.03
pu        0.744  0.030   0.700    0.800  ...    54.0      49.0      54.0   1.01
mu        0.110  0.024   0.063    0.153  ...    23.0      20.0      24.0   1.10
mus       0.178  0.033   0.132    0.253  ...   152.0     152.0      80.0   0.99
gamma     0.271  0.048   0.177    0.347  ...    75.0      97.0      40.0   0.99
Is_begin  1.727  1.322   0.054    4.037  ...   152.0     149.0      31.0   0.99
Ia_begin  0.777  0.542   0.023    1.692  ...    82.0      80.0      88.0   1.03
E_begin   1.838  2.046   0.001    5.808  ...   125.0     152.0      40.0   1.01

[8 rows x 11 columns]