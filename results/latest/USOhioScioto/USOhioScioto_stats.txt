0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1165.30    33.15
p_loo       27.36        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      352   91.7%
 (0.5, 0.7]   (ok)         24    6.2%
   (0.7, 1]   (bad)         8    2.1%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.264  0.052   0.166    0.337  ...    56.0      58.0      57.0   1.01
pu        0.837  0.019   0.805    0.866  ...    49.0      50.0      49.0   1.01
mu        0.114  0.024   0.071    0.154  ...    13.0      13.0      40.0   1.11
mus       0.199  0.035   0.129    0.256  ...    88.0      85.0      60.0   0.99
gamma     0.231  0.046   0.157    0.315  ...   108.0     107.0      79.0   1.05
Is_begin  0.662  0.587   0.051    1.834  ...    66.0      46.0     100.0   1.01
Ia_begin  0.949  0.912   0.002    2.888  ...    61.0      64.0      79.0   0.99
E_begin   0.496  0.528   0.006    1.672  ...    66.0      85.0      79.0   1.04

[8 rows x 11 columns]