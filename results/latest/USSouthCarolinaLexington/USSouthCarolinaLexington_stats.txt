0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1779.89    31.34
p_loo       29.73        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      346   90.1%
 (0.5, 0.7]   (ok)         34    8.9%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.245  0.058   0.157    0.341  ...   130.0     133.0      60.0   1.04
pu        0.742  0.028   0.704    0.784  ...    38.0      39.0      60.0   1.03
mu        0.110  0.022   0.074    0.149  ...    43.0      37.0      20.0   1.03
mus       0.203  0.031   0.149    0.266  ...   126.0     139.0      97.0   1.08
gamma     0.256  0.039   0.194    0.323  ...    47.0      51.0      88.0   1.05
Is_begin  2.239  1.582   0.002    4.812  ...   152.0     152.0      60.0   1.01
Ia_begin  4.453  3.275   0.013   10.242  ...    67.0      73.0      35.0   1.02
E_begin   3.735  4.187   0.009   11.230  ...    91.0      43.0      43.0   1.04

[8 rows x 11 columns]