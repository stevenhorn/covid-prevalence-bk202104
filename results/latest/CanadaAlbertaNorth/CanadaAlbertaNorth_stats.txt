2 Divergences 
Failed validation 
Computed from 80 by 390 log-likelihood matrix

         Estimate       SE
elpd_loo -1722.17    39.25
p_loo       32.95        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      360   92.3%
 (0.5, 0.7]   (ok)         24    6.2%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.256  0.057   0.159    0.350  ...   146.0     152.0      60.0   1.04
pu        0.859  0.030   0.792    0.897  ...    64.0      78.0      31.0   1.00
mu        0.138  0.023   0.103    0.184  ...    36.0      33.0      41.0   1.07
mus       0.187  0.037   0.127    0.249  ...   133.0     152.0      18.0   1.09
gamma     0.232  0.039   0.146    0.289  ...   100.0     104.0      65.0   1.05
Is_begin  2.419  2.216   0.021    5.680  ...    66.0     102.0      59.0   0.99
Ia_begin  2.813  2.735   0.033    8.547  ...   105.0     130.0      59.0   1.02
E_begin   1.981  2.626   0.010    7.435  ...    68.0     132.0      59.0   1.00

[8 rows x 11 columns]