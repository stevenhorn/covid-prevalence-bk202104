0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1719.30    27.73
p_loo       27.65        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      342   89.1%
 (0.5, 0.7]   (ok)         37    9.6%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.245  0.046   0.158    0.313  ...   126.0     147.0      60.0   1.01
pu        0.771  0.038   0.701    0.825  ...    59.0      54.0      40.0   1.03
mu        0.122  0.024   0.086    0.165  ...    29.0      28.0      60.0   1.05
mus       0.188  0.031   0.132    0.235  ...   152.0     152.0      76.0   0.99
gamma     0.242  0.041   0.170    0.317  ...   111.0     121.0      54.0   0.99
Is_begin  1.861  1.338   0.245    4.741  ...    95.0      66.0      32.0   1.03
Ia_begin  3.866  2.921   0.056    9.727  ...    96.0      89.0      59.0   1.02
E_begin   4.220  4.597   0.049   13.086  ...    91.0      68.0     100.0   1.03

[8 rows x 11 columns]