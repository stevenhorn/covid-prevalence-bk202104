0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1019.89    30.37
p_loo       29.00        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      340   88.5%
 (0.5, 0.7]   (ok)         30    7.8%
   (0.7, 1]   (bad)        14    3.6%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.266  0.055   0.155    0.339  ...    17.0      18.0      16.0   1.08
pu        0.874  0.017   0.851    0.898  ...    13.0      11.0      55.0   1.17
mu        0.187  0.024   0.134    0.220  ...     8.0       8.0      18.0   1.23
mus       0.254  0.036   0.190    0.317  ...    52.0      62.0      92.0   1.03
gamma     0.378  0.062   0.290    0.497  ...   105.0     115.0      67.0   1.00
Is_begin  1.587  1.335   0.175    3.918  ...   137.0     152.0      25.0   1.00
Ia_begin  3.565  2.251   0.352    7.161  ...    78.0      74.0      23.0   1.07
E_begin   2.204  2.273   0.059    6.166  ...    55.0      44.0      75.0   1.03

[8 rows x 11 columns]