0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1306.38    23.36
p_loo       23.38        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      354   92.2%
 (0.5, 0.7]   (ok)         28    7.3%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.247  0.052   0.180    0.345  ...    34.0      34.0      35.0   1.01
pu        0.794  0.026   0.748    0.836  ...    42.0      44.0      88.0   1.04
mu        0.133  0.023   0.095    0.172  ...    11.0      13.0      60.0   1.11
mus       0.182  0.032   0.123    0.230  ...    72.0      75.0      60.0   0.99
gamma     0.221  0.047   0.127    0.291  ...   152.0     152.0      59.0   1.00
Is_begin  1.100  0.843   0.037    2.700  ...   134.0      85.0      40.0   1.06
Ia_begin  2.470  1.898   0.273    6.034  ...    43.0      38.0      56.0   1.04
E_begin   1.433  1.516   0.021    4.189  ...    95.0      77.0      88.0   1.01

[8 rows x 11 columns]