0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1355.86    30.65
p_loo       29.11        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      358   93.2%
 (0.5, 0.7]   (ok)         17    4.4%
   (0.7, 1]   (bad)         9    2.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.261  0.056   0.167    0.349  ...    78.0     152.0      67.0   1.01
pu        0.796  0.025   0.743    0.834  ...    46.0      56.0      57.0   1.10
mu        0.133  0.025   0.099    0.182  ...    33.0      32.0      40.0   1.05
mus       0.175  0.029   0.112    0.214  ...   114.0     112.0      39.0   1.01
gamma     0.203  0.038   0.127    0.277  ...    90.0     111.0      59.0   1.00
Is_begin  1.196  0.886   0.060    2.948  ...   132.0      98.0      60.0   1.00
Ia_begin  2.807  2.077   0.017    6.102  ...    99.0      81.0      60.0   1.02
E_begin   1.905  2.148   0.011    4.626  ...    95.0     118.0      92.0   0.99

[8 rows x 11 columns]