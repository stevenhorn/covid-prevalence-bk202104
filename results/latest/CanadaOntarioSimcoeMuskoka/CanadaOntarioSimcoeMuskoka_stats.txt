0 Divergences 
Passed validation 
Computed from 80 by 390 log-likelihood matrix

         Estimate       SE
elpd_loo -1574.53    31.00
p_loo       67.29        -
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      386   99.0%
 (0.5, 0.7]   (ok)          4    1.0%
   (0.7, 1]   (bad)         0    0.0%
   (1, Inf)   (very bad)    0    0.0%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.282   0.019   0.254    0.308  ...     3.0       3.0      22.0   2.26
pu         0.827   0.006   0.816    0.836  ...     3.0       3.0      14.0   2.74
mu         0.308   0.004   0.302    0.314  ...     3.0       4.0      22.0   1.91
mus        0.241   0.020   0.215    0.268  ...     3.0       3.0      17.0   2.10
gamma      0.110   0.004   0.106    0.117  ...     3.0       3.0      19.0   2.24
Is_begin   7.242   5.913   2.577   21.623  ...     3.0       3.0      16.0   2.56
Ia_begin  15.402  13.670   1.154   35.113  ...     3.0       3.0      14.0   2.77
E_begin   17.816  17.335   3.566   52.625  ...     3.0       3.0      24.0   2.16

[8 rows x 11 columns]