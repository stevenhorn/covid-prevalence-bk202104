0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1724.46    46.12
p_loo       34.56        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      346   90.1%
 (0.5, 0.7]   (ok)         33    8.6%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.249  0.054   0.151    0.329  ...    60.0      61.0      43.0   1.00
pu        0.795  0.027   0.741    0.836  ...     7.0       7.0      48.0   1.23
mu        0.109  0.017   0.078    0.141  ...    21.0      17.0      46.0   1.10
mus       0.151  0.023   0.104    0.183  ...   133.0     135.0      93.0   1.00
gamma     0.168  0.041   0.098    0.242  ...   146.0     152.0      47.0   1.03
Is_begin  1.302  0.894   0.025    2.505  ...    72.0      69.0      48.0   1.04
Ia_begin  2.836  2.239   0.375    7.848  ...    96.0      88.0      34.0   1.02
E_begin   1.481  1.408   0.039    4.878  ...    74.0      72.0      74.0   1.05

[8 rows x 11 columns]