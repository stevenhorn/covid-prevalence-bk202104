9 Divergences 
Passed validation 
Computed from 80 by 385 log-likelihood matrix

         Estimate       SE
elpd_loo -3191.48    27.60
p_loo      174.54        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      352   91.4%
 (0.5, 0.7]   (ok)         25    6.5%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    1    0.3%

             mean       sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.310    0.038   0.232    0.350  ...     3.0       3.0      19.0   2.01
pu          0.889    0.014   0.859    0.900  ...     4.0       4.0      53.0   1.64
mu          0.244    0.047   0.181    0.308  ...     3.0       4.0      22.0   1.84
mus         0.203    0.042   0.141    0.281  ...     8.0      11.0      22.0   1.25
gamma       0.406    0.281   0.148    0.926  ...     3.0       3.0      22.0   2.18
Is_begin   77.534   59.601   8.187  193.488  ...     4.0       4.0      22.0   1.83
Ia_begin   80.040   54.055  14.252  173.774  ...     6.0       5.0      15.0   1.41
E_begin   227.559  258.450  10.949  728.731  ...     3.0       4.0      20.0   1.70

[8 rows x 11 columns]