5 Divergences 
Passed validation 
Computed from 80 by 385 log-likelihood matrix

         Estimate       SE
elpd_loo -2018.00    41.12
p_loo       41.31        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      354   91.9%
 (0.5, 0.7]   (ok)         16    4.2%
   (0.7, 1]   (bad)        13    3.4%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.275  0.051   0.174    0.347  ...    52.0      54.0      57.0   1.09
pu        0.876  0.026   0.821    0.900  ...   114.0      96.0      20.0   1.01
mu        0.158  0.016   0.130    0.185  ...    33.0      34.0      43.0   1.05
mus       0.182  0.034   0.117    0.244  ...   152.0     141.0      93.0   1.02
gamma     0.225  0.038   0.157    0.289  ...    78.0      84.0      59.0   1.00
Is_begin  0.891  0.810   0.010    2.300  ...    62.0      53.0      59.0   1.02
Ia_begin  0.627  0.528   0.008    1.561  ...   144.0      99.0      76.0   1.00
E_begin   0.659  0.833   0.011    2.820  ...    84.0      57.0      22.0   1.04

[8 rows x 11 columns]