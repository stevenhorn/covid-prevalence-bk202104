0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -854.73    50.82
p_loo       36.56        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      371   96.6%
 (0.5, 0.7]   (ok)          9    2.3%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.231  0.045   0.157    0.308  ...    27.0      30.0      57.0   1.07
pu        0.730  0.017   0.701    0.761  ...   108.0     104.0      57.0   1.00
mu        0.125  0.027   0.088    0.181  ...    64.0      71.0      59.0   1.04
mus       0.166  0.042   0.109    0.236  ...   120.0     124.0      36.0   0.99
gamma     0.194  0.032   0.142    0.261  ...    96.0      92.0      57.0   0.99
Is_begin  0.549  0.546   0.008    1.546  ...   101.0      79.0      60.0   1.01
Ia_begin  1.187  1.768   0.003    3.972  ...    69.0      61.0      59.0   0.99
E_begin   0.482  0.577   0.004    1.639  ...    89.0      56.0      61.0   1.03

[8 rows x 11 columns]