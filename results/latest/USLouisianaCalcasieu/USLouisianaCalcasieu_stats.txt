13 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1923.70    35.44
p_loo       36.42        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      352   91.7%
 (0.5, 0.7]   (ok)         24    6.2%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.230  0.039   0.157    0.289  ...    66.0      63.0      31.0   1.07
pu        0.732  0.015   0.706    0.761  ...    23.0      26.0      49.0   1.07
mu        0.143  0.020   0.102    0.177  ...    11.0      11.0      22.0   1.15
mus       0.183  0.025   0.138    0.234  ...    29.0      27.0      86.0   1.05
gamma     0.254  0.044   0.195    0.341  ...    96.0      98.0     100.0   1.00
Is_begin  0.811  0.584   0.017    1.696  ...    50.0      43.0      55.0   1.16
Ia_begin  1.514  1.299   0.082    3.552  ...    28.0      19.0      40.0   1.09
E_begin   0.987  0.953   0.001    2.792  ...    47.0      34.0      22.0   1.02

[8 rows x 11 columns]