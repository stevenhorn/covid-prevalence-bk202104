0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1421.55    44.17
p_loo       40.20        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      356   92.7%
 (0.5, 0.7]   (ok)         20    5.2%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.256  0.055   0.151    0.336  ...    50.0      71.0      59.0   1.06
pu        0.760  0.029   0.707    0.804  ...    15.0      16.0      32.0   1.13
mu        0.139  0.021   0.102    0.178  ...    27.0      28.0      40.0   1.03
mus       0.186  0.031   0.141    0.247  ...    50.0      44.0      59.0   1.04
gamma     0.240  0.046   0.152    0.323  ...    81.0      68.0      63.0   1.02
Is_begin  0.529  0.517   0.004    1.659  ...   122.0      90.0      79.0   1.04
Ia_begin  0.983  1.166   0.000    3.010  ...    58.0      48.0      48.0   1.01
E_begin   0.420  0.639   0.010    1.584  ...    89.0      63.0      96.0   0.99

[8 rows x 11 columns]