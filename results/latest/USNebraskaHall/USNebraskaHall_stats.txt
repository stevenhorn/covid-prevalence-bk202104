0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1455.92    29.52
p_loo       28.84        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      343   89.3%
 (0.5, 0.7]   (ok)         34    8.9%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.237  0.049   0.162    0.322  ...    41.0      45.0      48.0   1.02
pu        0.728  0.018   0.701    0.756  ...    27.0      25.0      60.0   1.02
mu        0.156  0.022   0.119    0.203  ...    20.0      18.0      55.0   1.09
mus       0.215  0.030   0.164    0.275  ...   152.0     152.0      86.0   1.02
gamma     0.325  0.037   0.258    0.387  ...    96.0     102.0      95.0   0.98
Is_begin  0.910  0.676   0.013    2.011  ...   100.0      69.0      60.0   1.03
Ia_begin  2.593  2.270   0.063    6.829  ...   129.0     115.0      49.0   0.98
E_begin   1.749  2.114   0.015    6.521  ...    86.0      45.0     100.0   1.04

[8 rows x 11 columns]