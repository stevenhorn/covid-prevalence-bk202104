7 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1466.95    39.07
p_loo       28.32        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      357   93.0%
 (0.5, 0.7]   (ok)         24    6.2%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.251  0.050   0.173    0.345  ...    35.0      29.0      21.0   1.05
pu        0.821  0.025   0.777    0.864  ...    29.0      24.0      43.0   1.06
mu        0.119  0.020   0.084    0.163  ...    10.0      10.0      22.0   1.19
mus       0.176  0.032   0.126    0.233  ...    98.0      99.0      81.0   0.99
gamma     0.220  0.035   0.172    0.295  ...    78.0      78.0      73.0   1.01
Is_begin  0.747  0.841   0.006    2.371  ...    27.0      15.0      81.0   1.11
Ia_begin  2.445  2.221   0.251    7.083  ...    66.0      30.0      58.0   1.05
E_begin   1.024  1.116   0.006    3.186  ...    21.0       8.0      22.0   1.23

[8 rows x 11 columns]