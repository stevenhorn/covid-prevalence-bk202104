0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1105.61    47.21
p_loo       45.88        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      362   94.3%
 (0.5, 0.7]   (ok)         18    4.7%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.217  0.041   0.151    0.286  ...    55.0      45.0      60.0   1.04
pu        0.730  0.018   0.701    0.761  ...    94.0      93.0      93.0   1.00
mu        0.144  0.025   0.103    0.193  ...    45.0      45.0      56.0   1.01
mus       0.220  0.042   0.150    0.308  ...    60.0      71.0      55.0   1.04
gamma     0.278  0.050   0.177    0.339  ...   123.0     111.0      91.0   1.00
Is_begin  0.622  0.730   0.003    2.211  ...   113.0      38.0      63.0   1.05
Ia_begin  1.005  0.907   0.032    2.882  ...   111.0     106.0      67.0   0.99
E_begin   0.455  0.590   0.004    1.299  ...   111.0     119.0      79.0   0.99

[8 rows x 11 columns]