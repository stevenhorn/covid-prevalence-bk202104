0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -659.79    27.66
p_loo       30.19        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      356   92.7%
 (0.5, 0.7]   (ok)         21    5.5%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.250  0.059   0.161    0.345  ...    91.0      85.0      60.0   1.03
pu        0.864  0.023   0.819    0.894  ...     8.0       8.0      50.0   1.20
mu        0.146  0.029   0.099    0.192  ...     7.0       6.0      24.0   1.33
mus       0.181  0.031   0.139    0.242  ...   143.0     152.0      70.0   1.06
gamma     0.226  0.044   0.156    0.302  ...    60.0      64.0      22.0   1.06
Is_begin  0.500  0.475   0.008    1.459  ...   102.0      96.0      61.0   1.00
Ia_begin  0.863  0.976   0.014    2.851  ...    55.0      98.0      72.0   0.98
E_begin   0.439  0.411   0.024    1.334  ...    48.0      49.0      60.0   1.01

[8 rows x 11 columns]