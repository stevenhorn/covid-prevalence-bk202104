0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1636.20    38.65
p_loo       34.00        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      355   92.4%
 (0.5, 0.7]   (ok)         21    5.5%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.238  0.048   0.157    0.323  ...   152.0     152.0      53.0   1.04
pu        0.744  0.027   0.700    0.791  ...   152.0     152.0      88.0   0.99
mu        0.148  0.022   0.113    0.192  ...    30.0      32.0      93.0   1.07
mus       0.207  0.037   0.152    0.272  ...   152.0     152.0      97.0   1.04
gamma     0.305  0.044   0.221    0.381  ...   116.0     120.0      93.0   1.00
Is_begin  1.895  1.191   0.126    3.937  ...   133.0     146.0      70.0   1.01
Ia_begin  4.440  3.367   0.009   10.577  ...   120.0      87.0      58.0   1.02
E_begin   3.291  3.272   0.030    9.576  ...   130.0      96.0      17.0   1.00

[8 rows x 11 columns]