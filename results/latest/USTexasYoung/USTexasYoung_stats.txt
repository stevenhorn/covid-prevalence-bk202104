0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1137.87    47.42
p_loo       38.72        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      364   94.8%
 (0.5, 0.7]   (ok)         17    4.4%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.176  0.012   0.161    0.201  ...    29.0      29.0      25.0   1.09
pu        0.711  0.007   0.702    0.724  ...     5.0       5.0      24.0   1.47
mu        0.158  0.022   0.131    0.194  ...     4.0       5.0      45.0   1.42
mus       0.171  0.033   0.102    0.226  ...    14.0      14.0      14.0   1.03
gamma     0.138  0.048   0.091    0.239  ...     3.0       4.0      15.0   1.56
Is_begin  0.591  0.409   0.094    1.262  ...    18.0      19.0      46.0   1.06
Ia_begin  1.302  1.033   0.167    3.153  ...    10.0       9.0      38.0   1.19
E_begin   0.663  0.546   0.029    1.935  ...     7.0       6.0      30.0   1.33

[8 rows x 11 columns]