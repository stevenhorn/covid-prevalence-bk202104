0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -717.42    32.85
p_loo       30.53        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      360   93.8%
 (0.5, 0.7]   (ok)         19    4.9%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.248  0.054   0.158    0.332  ...    59.0      57.0      54.0   1.07
pu        0.746  0.023   0.705    0.777  ...    49.0      52.0      59.0   1.10
mu        0.134  0.022   0.100    0.177  ...    40.0      44.0      48.0   1.02
mus       0.176  0.029   0.131    0.233  ...   124.0     132.0      88.0   1.07
gamma     0.209  0.042   0.142    0.287  ...   131.0     123.0      54.0   1.02
Is_begin  0.449  0.448   0.001    1.358  ...    39.0      26.0      38.0   1.06
Ia_begin  1.246  1.632   0.007    4.526  ...   117.0     140.0      72.0   1.01
E_begin   0.479  0.667   0.001    1.476  ...    81.0      43.0      56.0   1.06

[8 rows x 11 columns]