0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -962.04    37.51
p_loo       33.02        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      366   95.3%
 (0.5, 0.7]   (ok)         14    3.6%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.244  0.062   0.150    0.338  ...    32.0      31.0      22.0   1.01
pu        0.740  0.023   0.701    0.772  ...    37.0      38.0      31.0   1.02
mu        0.145  0.024   0.109    0.186  ...    22.0      25.0      76.0   1.10
mus       0.209  0.039   0.149    0.289  ...    30.0      31.0      37.0   1.06
gamma     0.329  0.047   0.255    0.426  ...    58.0      59.0      60.0   1.02
Is_begin  0.320  0.490   0.003    1.177  ...   102.0      65.0      54.0   1.00
Ia_begin  0.436  0.488   0.016    1.317  ...    85.0      53.0      60.0   1.02
E_begin   0.273  0.309   0.007    0.785  ...    87.0      60.0      91.0   1.01

[8 rows x 11 columns]