0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -940.38    73.10
p_loo       45.60        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      356   92.7%
 (0.5, 0.7]   (ok)         24    6.2%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.182  0.028   0.150    0.229  ...    92.0      42.0      72.0   1.03
pu        0.710  0.011   0.700    0.729  ...   134.0      70.0      60.0   1.07
mu        0.108  0.015   0.078    0.131  ...    36.0      41.0      59.0   1.06
mus       0.268  0.042   0.200    0.353  ...    89.0      93.0      93.0   1.03
gamma     0.523  0.083   0.399    0.657  ...    78.0      88.0      36.0   0.99
Is_begin  0.642  0.722   0.013    1.672  ...    90.0      81.0      59.0   1.03
Ia_begin  0.817  1.030   0.001    2.814  ...    59.0      63.0      93.0   1.06
E_begin   0.357  0.445   0.001    1.380  ...   103.0      87.0      59.0   0.99

[8 rows x 11 columns]