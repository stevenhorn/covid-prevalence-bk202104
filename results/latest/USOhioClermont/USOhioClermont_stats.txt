0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1502.94    43.23
p_loo       30.07        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      355   92.4%
 (0.5, 0.7]   (ok)         25    6.5%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.253  0.057   0.164    0.344  ...    57.0      56.0      43.0   1.03
pu        0.791  0.031   0.718    0.830  ...    29.0      31.0      40.0   1.00
mu        0.106  0.021   0.069    0.143  ...     9.0       9.0      76.0   1.22
mus       0.157  0.033   0.098    0.216  ...   152.0     152.0      96.0   0.99
gamma     0.175  0.043   0.116    0.256  ...    34.0      49.0      40.0   1.04
Is_begin  0.908  0.844   0.003    2.219  ...   123.0      97.0      59.0   1.00
Ia_begin  0.696  0.496   0.022    1.534  ...   151.0     123.0      97.0   0.99
E_begin   0.673  0.660   0.005    2.005  ...   152.0      71.0      24.0   1.02

[8 rows x 11 columns]