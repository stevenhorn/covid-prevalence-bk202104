0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -978.80    59.38
p_loo       45.47        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      366   95.3%
 (0.5, 0.7]   (ok)         14    3.6%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.256  0.064   0.150    0.341  ...    89.0      76.0      17.0   1.01
pu        0.796  0.025   0.760    0.837  ...   104.0     103.0      59.0   1.00
mu        0.126  0.020   0.098    0.166  ...    40.0      41.0      86.0   1.02
mus       0.205  0.033   0.143    0.270  ...   113.0     152.0      60.0   0.99
gamma     0.246  0.041   0.175    0.304  ...   152.0     152.0      22.0   1.07
Is_begin  0.844  0.839   0.011    2.535  ...   101.0     120.0      80.0   0.98
Ia_begin  1.472  1.773   0.019    4.290  ...    41.0     152.0      38.0   1.02
E_begin   0.561  0.661   0.024    1.669  ...    82.0      82.0      59.0   1.00

[8 rows x 11 columns]