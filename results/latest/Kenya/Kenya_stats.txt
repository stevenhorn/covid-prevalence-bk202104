0 Divergences 
Passed validation 
Computed from 80 by 385 log-likelihood matrix

         Estimate       SE
elpd_loo -2151.89    26.63
p_loo       26.85        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      343   89.1%
 (0.5, 0.7]   (ok)         29    7.5%
   (0.7, 1]   (bad)        13    3.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.246  0.050   0.156    0.323  ...    81.0      76.0      57.0   1.05
pu        0.808  0.049   0.710    0.880  ...    30.0      36.0      59.0   1.04
mu        0.151  0.022   0.119    0.191  ...     6.0       6.0      17.0   1.36
mus       0.180  0.029   0.141    0.242  ...    38.0      44.0      43.0   1.04
gamma     0.227  0.047   0.157    0.305  ...   152.0     113.0      56.0   1.00
Is_begin  0.755  0.539   0.015    1.613  ...   138.0     106.0      46.0   1.01
Ia_begin  2.663  1.641   0.045    5.752  ...    43.0      40.0      39.0   1.04
E_begin   2.038  1.613   0.124    5.163  ...    51.0      54.0      60.0   0.99

[8 rows x 11 columns]