0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1041.11    44.25
p_loo       37.65        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      363   94.5%
 (0.5, 0.7]   (ok)         16    4.2%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.262  0.050   0.182    0.346  ...    36.0      34.0      22.0   1.01
pu        0.826  0.019   0.793    0.862  ...    88.0      93.0      45.0   1.01
mu        0.116  0.020   0.084    0.160  ...    44.0      44.0      60.0   1.02
mus       0.163  0.032   0.116    0.218  ...   152.0     152.0     100.0   0.98
gamma     0.198  0.033   0.143    0.254  ...   152.0     152.0      79.0   1.01
Is_begin  0.668  0.598   0.002    1.759  ...   132.0     152.0      60.0   0.98
Ia_begin  1.426  1.284   0.026    4.227  ...    58.0      32.0      51.0   1.09
E_begin   0.640  0.688   0.033    2.004  ...   107.0      35.0      59.0   1.05

[8 rows x 11 columns]