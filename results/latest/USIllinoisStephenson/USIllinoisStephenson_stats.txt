0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1118.80    25.42
p_loo       25.85        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      351   91.4%
 (0.5, 0.7]   (ok)         25    6.5%
   (0.7, 1]   (bad)         8    2.1%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.252  0.054   0.154    0.333  ...    37.0      38.0      35.0   1.03
pu        0.782  0.030   0.725    0.837  ...    11.0      11.0      39.0   1.14
mu        0.128  0.024   0.093    0.182  ...    17.0      19.0      49.0   1.12
mus       0.179  0.033   0.141    0.257  ...    55.0      61.0      69.0   1.03
gamma     0.219  0.036   0.160    0.285  ...    61.0      68.0      59.0   1.05
Is_begin  0.550  0.439   0.010    1.471  ...    52.0      70.0     100.0   1.02
Ia_begin  1.129  1.178   0.021    3.524  ...   121.0     152.0     100.0   1.09
E_begin   0.539  0.667   0.004    2.215  ...   101.0     131.0      87.0   1.05

[8 rows x 11 columns]