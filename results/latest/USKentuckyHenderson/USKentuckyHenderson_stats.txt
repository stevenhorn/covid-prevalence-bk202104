0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1238.19    31.13
p_loo       27.93        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      356   92.7%
 (0.5, 0.7]   (ok)         21    5.5%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.254  0.051   0.181    0.349  ...    47.0      48.0      60.0   1.01
pu        0.775  0.036   0.702    0.820  ...    37.0      36.0      38.0   0.99
mu        0.126  0.026   0.080    0.170  ...    35.0      34.0      57.0   1.00
mus       0.180  0.035   0.125    0.245  ...   101.0      96.0      55.0   1.01
gamma     0.213  0.041   0.137    0.289  ...   152.0     152.0      54.0   1.02
Is_begin  1.054  0.906   0.007    2.996  ...   152.0     152.0      97.0   1.01
Ia_begin  0.654  0.582   0.006    1.898  ...    59.0      37.0      58.0   1.05
E_begin   0.715  0.809   0.016    2.487  ...   100.0     107.0     100.0   0.99

[8 rows x 11 columns]