0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1358.22    31.75
p_loo       23.44        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      349   90.9%
 (0.5, 0.7]   (ok)         28    7.3%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.270  0.058   0.165    0.349  ...    51.0      52.0      38.0   1.06
pu        0.871  0.031   0.818    0.900  ...    26.0      30.0      38.0   1.06
mu        0.134  0.025   0.093    0.187  ...     9.0       9.0      32.0   1.21
mus       0.159  0.031   0.111    0.210  ...   139.0     135.0      91.0   1.00
gamma     0.182  0.042   0.104    0.252  ...    66.0      74.0      40.0   1.02
Is_begin  1.215  1.116   0.020    3.654  ...    95.0      77.0      60.0   0.99
Ia_begin  3.101  2.805   0.201    8.695  ...   104.0     111.0      93.0   1.00
E_begin   1.610  1.916   0.048    6.125  ...   124.0      93.0      60.0   0.99

[8 rows x 11 columns]