0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -913.67    25.75
p_loo       29.51        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      348   90.6%
 (0.5, 0.7]   (ok)         28    7.3%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.251  0.052   0.166    0.338  ...    78.0      82.0      60.0   1.02
pu        0.780  0.024   0.739    0.828  ...     8.0       8.0      40.0   1.20
mu        0.121  0.022   0.081    0.165  ...    40.0      49.0      46.0   1.02
mus       0.172  0.036   0.105    0.246  ...   149.0     152.0      15.0   1.02
gamma     0.194  0.034   0.141    0.263  ...   122.0      86.0      54.0   1.02
Is_begin  0.339  0.338   0.003    1.037  ...    71.0      57.0      88.0   1.02
Ia_begin  0.591  1.005   0.001    2.208  ...    56.0      67.0      60.0   0.98
E_begin   0.322  0.428   0.003    0.993  ...    46.0      45.0      59.0   1.03

[8 rows x 11 columns]