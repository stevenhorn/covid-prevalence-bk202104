0 Divergences 
Failed validation 
Computed from 80 by 247 log-likelihood matrix

         Estimate       SE
elpd_loo  -719.74    29.94
p_loo       22.18        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      220   89.1%
 (0.5, 0.7]   (ok)         21    8.5%
   (0.7, 1]   (bad)         3    1.2%
   (1, Inf)   (very bad)    3    1.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.253  0.061   0.150    0.342  ...    63.0      58.0      22.0   1.10
pu        0.839  0.026   0.797    0.886  ...    25.0      25.0      59.0   1.01
mu        0.108  0.026   0.068    0.158  ...    10.0      10.0      15.0   1.17
mus       0.186  0.034   0.129    0.246  ...    80.0      90.0      58.0   1.00
gamma     0.209  0.043   0.138    0.287  ...    87.0      84.0      49.0   1.06
Is_begin  0.666  0.711   0.009    2.275  ...    51.0      42.0      56.0   1.05
Ia_begin  0.854  0.820   0.016    2.559  ...   126.0      71.0      47.0   1.02
E_begin   0.349  0.382   0.004    1.211  ...    66.0      63.0      93.0   1.02

[8 rows x 11 columns]