0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1046.56    32.63
p_loo       29.14        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      351   91.4%
 (0.5, 0.7]   (ok)         26    6.8%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.253  0.049   0.163    0.332  ...   148.0     152.0      52.0   1.01
pu        0.792  0.025   0.739    0.829  ...    40.0      43.0      32.0   1.11
mu        0.132  0.024   0.089    0.171  ...    55.0      52.0      40.0   1.04
mus       0.171  0.029   0.126    0.237  ...   152.0     152.0      69.0   1.05
gamma     0.211  0.033   0.161    0.272  ...   152.0     152.0      77.0   1.07
Is_begin  0.652  0.558   0.001    1.804  ...   112.0      90.0      59.0   1.00
Ia_begin  1.439  1.388   0.026    4.303  ...   146.0      98.0      95.0   1.02
E_begin   0.596  0.675   0.015    2.226  ...   152.0     152.0     100.0   1.01

[8 rows x 11 columns]