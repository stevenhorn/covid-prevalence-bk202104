0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1201.28    67.73
p_loo       45.76        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      353   91.9%
 (0.5, 0.7]   (ok)         25    6.5%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.258  0.057   0.152    0.336  ...   115.0     114.0      39.0   1.05
pu        0.852  0.021   0.814    0.880  ...    53.0      56.0      42.0   1.02
mu        0.142  0.026   0.104    0.191  ...    21.0      23.0      81.0   1.07
mus       0.251  0.037   0.190    0.315  ...    69.0      57.0      57.0   1.00
gamma     0.364  0.060   0.255    0.463  ...    64.0      65.0      60.0   1.01
Is_begin  1.015  0.974   0.013    2.724  ...   105.0      83.0      60.0   0.99
Ia_begin  1.659  1.322   0.044    4.472  ...   106.0     108.0      88.0   0.99
E_begin   0.872  0.909   0.007    2.570  ...    48.0      52.0      29.0   1.02

[8 rows x 11 columns]