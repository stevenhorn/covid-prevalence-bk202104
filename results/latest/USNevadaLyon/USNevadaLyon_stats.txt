0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1256.53    62.80
p_loo       42.08        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      353   91.9%
 (0.5, 0.7]   (ok)         25    6.5%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.247  0.053   0.167    0.347  ...    92.0      84.0      60.0   1.01
pu        0.873  0.016   0.844    0.895  ...    27.0      23.0      57.0   1.07
mu        0.120  0.019   0.087    0.148  ...     6.0       6.0      43.0   1.33
mus       0.210  0.044   0.129    0.266  ...    67.0      64.0      31.0   1.12
gamma     0.305  0.056   0.211    0.417  ...    77.0      77.0      72.0   1.02
Is_begin  0.813  0.693   0.006    2.084  ...    69.0      38.0      59.0   1.06
Ia_begin  1.482  1.477   0.016    4.003  ...    52.0      44.0      59.0   1.00
E_begin   0.672  0.767   0.018    1.871  ...    59.0      49.0      96.0   1.00

[8 rows x 11 columns]