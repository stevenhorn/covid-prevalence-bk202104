4 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1539.18    20.10
p_loo       25.24        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      344   89.6%
 (0.5, 0.7]   (ok)         36    9.4%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.243  0.042   0.175    0.313  ...   118.0     123.0      91.0   1.01
pu        0.776  0.042   0.716    0.849  ...    63.0      72.0      77.0   1.04
mu        0.124  0.024   0.074    0.163  ...    18.0      20.0      15.0   1.22
mus       0.178  0.029   0.126    0.235  ...    65.0      61.0      60.0   1.03
gamma     0.226  0.042   0.151    0.311  ...   152.0     152.0      68.0   0.98
Is_begin  0.858  0.727   0.002    1.924  ...    62.0      61.0      58.0   1.06
Ia_begin  3.366  1.943   0.173    6.914  ...   122.0     102.0      42.0   1.01
E_begin   2.956  2.488   0.035    8.759  ...    16.0      44.0      22.0   1.05

[8 rows x 11 columns]