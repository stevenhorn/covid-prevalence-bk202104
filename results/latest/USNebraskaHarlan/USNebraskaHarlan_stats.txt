0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -458.42    32.42
p_loo       35.50        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      340   88.5%
 (0.5, 0.7]   (ok)         35    9.1%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.266  0.046   0.180    0.343  ...    81.0      79.0      86.0   0.99
pu        0.868  0.016   0.845    0.897  ...    23.0      26.0      22.0   1.05
mu        0.124  0.023   0.085    0.159  ...    64.0      65.0      59.0   1.00
mus       0.172  0.029   0.120    0.223  ...   109.0     128.0      76.0   1.02
gamma     0.201  0.043   0.129    0.276  ...   120.0     105.0     100.0   1.02
Is_begin  0.704  0.792   0.003    2.208  ...   110.0     104.0      60.0   1.03
Ia_begin  1.181  1.280   0.007    3.827  ...    80.0      51.0      58.0   1.03
E_begin   0.456  0.597   0.003    1.213  ...    63.0      53.0      60.0   1.00

[8 rows x 11 columns]