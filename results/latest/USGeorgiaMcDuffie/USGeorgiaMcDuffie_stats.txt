0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1111.08    58.63
p_loo       41.79        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      357   93.0%
 (0.5, 0.7]   (ok)         22    5.7%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.269  0.054   0.178    0.348  ...    66.0      65.0      60.0   1.01
pu        0.779  0.030   0.717    0.821  ...     5.0       6.0      24.0   1.33
mu        0.151  0.025   0.113    0.200  ...    16.0      20.0      24.0   1.14
mus       0.233  0.041   0.174    0.317  ...    44.0      46.0      43.0   1.05
gamma     0.304  0.052   0.226    0.413  ...    91.0      94.0      59.0   1.00
Is_begin  0.773  0.647   0.003    1.921  ...    71.0      55.0      59.0   0.99
Ia_begin  1.957  1.746   0.119    5.184  ...    86.0      96.0      59.0   0.99
E_begin   0.997  1.201   0.015    2.800  ...    47.0      61.0      60.0   1.03

[8 rows x 11 columns]