0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -613.36    31.66
p_loo       22.72        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      361   94.0%
 (0.5, 0.7]   (ok)         15    3.9%
   (0.7, 1]   (bad)         8    2.1%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.243  0.037   0.184    0.325  ...    46.0      47.0      57.0   1.12
pu        0.747  0.018   0.718    0.774  ...    10.0      12.0      59.0   1.15
mu        0.143  0.036   0.083    0.199  ...     4.0       4.0      38.0   1.72
mus       0.154  0.028   0.085    0.196  ...    10.0      10.0      20.0   1.26
gamma     0.140  0.042   0.085    0.203  ...     4.0       3.0      14.0   2.06
Is_begin  0.231  0.206   0.000    0.592  ...     6.0       6.0      22.0   1.37
Ia_begin  0.423  0.648   0.006    1.995  ...    10.0      10.0      40.0   1.15
E_begin   0.160  0.169   0.003    0.435  ...    12.0       7.0      34.0   1.24

[8 rows x 11 columns]