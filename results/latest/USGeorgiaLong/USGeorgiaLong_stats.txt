0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -830.25    35.98
p_loo       26.49        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      353   91.9%
 (0.5, 0.7]   (ok)         28    7.3%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.263  0.054   0.170    0.348  ...    43.0      44.0      49.0   1.02
pu        0.868  0.024   0.824    0.899  ...    54.0      53.0      91.0   1.03
mu        0.106  0.020   0.078    0.147  ...    36.0      36.0      60.0   1.00
mus       0.154  0.033   0.093    0.207  ...   152.0     152.0      49.0   0.99
gamma     0.170  0.034   0.111    0.238  ...   142.0     129.0      45.0   1.03
Is_begin  0.784  0.666   0.019    2.005  ...    74.0      77.0      58.0   1.01
Ia_begin  1.295  1.249   0.068    4.038  ...    77.0      73.0      86.0   1.01
E_begin   0.662  0.572   0.001    1.795  ...    61.0      41.0      14.0   1.02

[8 rows x 11 columns]