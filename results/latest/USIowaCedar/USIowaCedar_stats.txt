0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -912.89    28.64
p_loo       27.92        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      356   92.7%
 (0.5, 0.7]   (ok)         22    5.7%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.260  0.051   0.179    0.341  ...    34.0      31.0      20.0   1.04
pu        0.780  0.036   0.710    0.836  ...     6.0       6.0      20.0   1.30
mu        0.110  0.022   0.073    0.148  ...    40.0      40.0      72.0   1.03
mus       0.174  0.037   0.120    0.242  ...   152.0     152.0      96.0   0.99
gamma     0.199  0.041   0.121    0.269  ...   152.0     152.0      73.0   1.02
Is_begin  1.038  0.810   0.010    2.500  ...   113.0      82.0      38.0   1.06
Ia_begin  2.074  1.932   0.031    4.731  ...   124.0      95.0      18.0   1.00
E_begin   1.099  1.234   0.006    3.086  ...    78.0      64.0      48.0   1.01

[8 rows x 11 columns]