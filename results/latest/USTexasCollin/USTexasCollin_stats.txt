2 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -2304.46    47.92
p_loo       35.99        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      348   90.6%
 (0.5, 0.7]   (ok)         32    8.3%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.257  0.056   0.173    0.337  ...    61.0      73.0      30.0   1.01
pu        0.801  0.041   0.733    0.865  ...    35.0      37.0      58.0   1.02
mu        0.128  0.021   0.100    0.171  ...    19.0      21.0      42.0   1.04
mus       0.197  0.038   0.143    0.269  ...    87.0     108.0      88.0   1.05
gamma     0.231  0.045   0.148    0.305  ...   152.0     136.0      67.0   1.04
Is_begin  3.437  2.896   0.096    8.973  ...   103.0      71.0      37.0   1.00
Ia_begin  6.714  5.536   0.080   17.169  ...    57.0      46.0      40.0   1.02
E_begin   3.806  4.281   0.025   12.671  ...    95.0      48.0      51.0   1.02

[8 rows x 11 columns]