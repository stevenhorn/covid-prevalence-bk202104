0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -2144.20    32.44
p_loo       27.38        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      363   94.5%
 (0.5, 0.7]   (ok)         17    4.4%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.237  0.054   0.150    0.316  ...   152.0     149.0      40.0   1.16
pu        0.743  0.029   0.705    0.793  ...    92.0     100.0      96.0   0.99
mu        0.105  0.017   0.078    0.134  ...    47.0      46.0      59.0   1.05
mus       0.180  0.024   0.134    0.220  ...    84.0      84.0      59.0   1.08
gamma     0.269  0.041   0.199    0.358  ...   123.0     106.0     100.0   1.03
Is_begin  3.371  2.080   0.250    6.869  ...   138.0     116.0      47.0   1.03
Ia_begin  7.532  4.325   0.744   14.638  ...    90.0      78.0      88.0   1.02
E_begin   9.985  6.912   0.288   23.313  ...   113.0      96.0      40.0   0.98

[8 rows x 11 columns]