0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1157.70    28.27
p_loo       25.69        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      351   91.4%
 (0.5, 0.7]   (ok)         28    7.3%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.270  0.055   0.166    0.349  ...    66.0      67.0      59.0   1.09
pu        0.781  0.022   0.741    0.819  ...    98.0      96.0      99.0   1.00
mu        0.120  0.021   0.092    0.164  ...    34.0      34.0      21.0   1.02
mus       0.152  0.035   0.102    0.234  ...   152.0     152.0      93.0   0.98
gamma     0.163  0.031   0.104    0.218  ...    35.0      63.0      87.0   1.05
Is_begin  0.716  0.702   0.001    1.545  ...    88.0      71.0      91.0   1.02
Ia_begin  1.248  1.268   0.037    3.857  ...   122.0     107.0      96.0   1.02
E_begin   0.487  0.563   0.006    1.296  ...    96.0      63.0      74.0   1.01

[8 rows x 11 columns]