4 Divergences 
Passed validation 
Computed from 80 by 390 log-likelihood matrix

         Estimate       SE
elpd_loo -1468.69    30.16
p_loo       28.19        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      368   94.4%
 (0.5, 0.7]   (ok)         16    4.1%
   (0.7, 1]   (bad)         6    1.5%
   (1, Inf)   (very bad)    0    0.0%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.250   0.050   0.154    0.326  ...    77.0      76.0      58.0   1.03
pu         0.816   0.054   0.719    0.899  ...     8.0       8.0      60.0   1.24
mu         0.138   0.022   0.100    0.183  ...    20.0      19.0      38.0   1.11
mus        0.195   0.033   0.129    0.246  ...    32.0      51.0      38.0   1.04
gamma      0.255   0.036   0.192    0.331  ...    64.0      69.0      40.0   1.02
Is_begin   6.775   5.250   0.104   16.146  ...    91.0      49.0      39.0   1.03
Ia_begin  38.968  22.900   6.357   76.258  ...    23.0      31.0      15.0   1.04
E_begin   23.805  22.025   0.994   66.066  ...     7.0       9.0      30.0   1.20

[8 rows x 11 columns]