0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -997.07    24.47
p_loo       23.88        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      359   93.5%
 (0.5, 0.7]   (ok)         17    4.4%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.261  0.057   0.165    0.347  ...   152.0     152.0      56.0   1.00
pu        0.820  0.041   0.731    0.867  ...    10.0      19.0      17.0   1.08
mu        0.135  0.028   0.085    0.184  ...    17.0      15.0      24.0   1.10
mus       0.168  0.024   0.122    0.206  ...   103.0     106.0      76.0   1.00
gamma     0.228  0.035   0.170    0.290  ...   152.0     152.0      87.0   1.00
Is_begin  0.887  0.829   0.043    2.598  ...   122.0     152.0      91.0   1.01
Ia_begin  1.647  1.537   0.023    4.108  ...    93.0      78.0      33.0   1.02
E_begin   1.090  1.164   0.009    2.915  ...    87.0      52.0      59.0   1.01

[8 rows x 11 columns]