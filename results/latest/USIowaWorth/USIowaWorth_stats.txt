0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -660.53    25.24
p_loo       26.45        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      362   94.3%
 (0.5, 0.7]   (ok)         20    5.2%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.249  0.053   0.156    0.330  ...   128.0     122.0      93.0   1.01
pu        0.823  0.026   0.774    0.870  ...    10.0      12.0      43.0   1.16
mu        0.132  0.025   0.091    0.174  ...    61.0      60.0      60.0   1.01
mus       0.178  0.027   0.133    0.221  ...   147.0     152.0      83.0   0.99
gamma     0.215  0.040   0.161    0.318  ...   152.0     152.0      61.0   1.03
Is_begin  0.498  0.749   0.004    1.408  ...    60.0      86.0      38.0   1.02
Ia_begin  0.880  0.970   0.005    2.616  ...   117.0     118.0      54.0   1.02
E_begin   0.491  0.851   0.001    1.898  ...    98.0      96.0      75.0   0.99

[8 rows x 11 columns]