0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1358.89    28.53
p_loo       25.78        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      358   93.2%
 (0.5, 0.7]   (ok)         22    5.7%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.186  0.024   0.150    0.226  ...   122.0     125.0      83.0   0.98
pu        0.712  0.010   0.700    0.734  ...   152.0     152.0      60.0   0.99
mu        0.109  0.025   0.064    0.162  ...     8.0       7.0      24.0   1.27
mus       0.181  0.037   0.131    0.254  ...    67.0      84.0      96.0   1.01
gamma     0.214  0.051   0.137    0.300  ...    87.0      75.0      54.0   1.06
Is_begin  0.604  0.574   0.040    1.515  ...   112.0     152.0      99.0   1.00
Ia_begin  1.107  1.284   0.014    3.324  ...    50.0      48.0      60.0   1.06
E_begin   0.500  0.539   0.006    1.731  ...    31.0      55.0      39.0   1.04

[8 rows x 11 columns]