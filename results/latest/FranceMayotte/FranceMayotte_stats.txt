0 Divergences 
Failed validation 
Computed from 80 by 385 log-likelihood matrix

         Estimate       SE
elpd_loo -2048.24    36.74
p_loo       38.07        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      359   93.2%
 (0.5, 0.7]   (ok)         17    4.4%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.256  0.054   0.164    0.337  ...    67.0      71.0      60.0   1.00
pu        0.877  0.012   0.856    0.895  ...    51.0      48.0      57.0   1.04
mu        0.129  0.020   0.092    0.160  ...    21.0      20.0      46.0   1.08
mus       0.164  0.024   0.124    0.207  ...   100.0     102.0      88.0   0.98
gamma     0.226  0.035   0.167    0.290  ...   107.0      97.0      88.0   1.00
Is_begin  0.698  0.523   0.067    1.814  ...    41.0     122.0      60.0   1.04
Ia_begin  1.665  1.248   0.100    4.330  ...    70.0      62.0      56.0   1.03
E_begin   1.084  1.218   0.010    3.136  ...    80.0      50.0      35.0   1.02

[8 rows x 11 columns]