1 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -2255.48    25.83
p_loo       19.45        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      357   93.0%
 (0.5, 0.7]   (ok)         22    5.7%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    1    0.3%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.245   0.054   0.165    0.347  ...   152.0     152.0      80.0   1.01
pu         0.803   0.056   0.711    0.883  ...    17.0      15.0      37.0   1.10
mu         0.108   0.017   0.072    0.131  ...    13.0      12.0      22.0   1.15
mus        0.170   0.027   0.124    0.221  ...   152.0     152.0      69.0   1.00
gamma      0.240   0.045   0.156    0.325  ...   104.0     132.0      65.0   1.02
Is_begin  11.721   8.839   0.012   27.142  ...    34.0      22.0      24.0   1.09
Ia_begin  36.845  22.650   3.248   75.274  ...   124.0     117.0      56.0   1.05
E_begin   28.687  24.572   0.497   68.724  ...    64.0      36.0      54.0   1.00

[8 rows x 11 columns]