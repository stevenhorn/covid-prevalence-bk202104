0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1124.12    25.26
p_loo       25.20        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      361   94.0%
 (0.5, 0.7]   (ok)         20    5.2%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.247  0.058   0.154    0.337  ...    34.0      35.0      93.0   1.00
pu        0.780  0.028   0.719    0.815  ...    23.0      28.0      32.0   1.02
mu        0.120  0.019   0.087    0.153  ...    23.0      22.0      49.0   1.04
mus       0.166  0.039   0.107    0.252  ...    80.0     111.0      60.0   1.04
gamma     0.182  0.035   0.135    0.253  ...    38.0      29.0      87.0   1.08
Is_begin  0.526  0.581   0.004    1.459  ...   103.0      69.0      67.0   0.99
Ia_begin  0.767  0.837   0.021    2.698  ...    41.0      18.0      60.0   1.10
E_begin   0.437  0.486   0.009    1.371  ...    69.0      54.0      59.0   1.02

[8 rows x 11 columns]