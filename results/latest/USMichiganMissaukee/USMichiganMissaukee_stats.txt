0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -892.04    33.51
p_loo       29.44        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      356   92.7%
 (0.5, 0.7]   (ok)         23    6.0%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.259  0.057   0.181    0.349  ...    53.0      50.0      48.0   1.08
pu        0.819  0.033   0.740    0.866  ...    23.0      29.0      23.0   1.07
mu        0.132  0.023   0.095    0.173  ...     6.0       6.0      38.0   1.31
mus       0.180  0.033   0.116    0.230  ...   119.0     128.0      59.0   1.08
gamma     0.246  0.048   0.142    0.330  ...   100.0     100.0      59.0   1.01
Is_begin  0.767  0.535   0.046    1.772  ...    26.0      69.0      59.0   1.03
Ia_begin  1.503  1.459   0.022    4.920  ...    69.0      58.0      31.0   1.01
E_begin   0.773  0.880   0.004    2.396  ...    89.0      43.0      86.0   1.00

[8 rows x 11 columns]