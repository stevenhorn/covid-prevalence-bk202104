0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1473.69    28.51
p_loo        7.40        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      379   98.7%
 (0.5, 0.7]   (ok)          4    1.0%
   (0.7, 1]   (bad)         0    0.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.183  0.027   0.153    0.240  ...     3.0       3.0      14.0   2.78
pu        0.760  0.014   0.742    0.781  ...     3.0       3.0      24.0   2.40
mu        0.196  0.003   0.191    0.203  ...     5.0       5.0      40.0   1.46
mus       0.162  0.018   0.138    0.184  ...     3.0       3.0      14.0   2.18
gamma     0.086  0.002   0.082    0.088  ...     3.0       4.0      17.0   1.76
Is_begin  0.525  0.407   0.102    1.236  ...     3.0       4.0      25.0   1.86
Ia_begin  0.794  0.689   0.200    2.501  ...     5.0       5.0      14.0   1.48
E_begin   0.313  0.209   0.010    0.586  ...     3.0       3.0      24.0   2.00

[8 rows x 11 columns]