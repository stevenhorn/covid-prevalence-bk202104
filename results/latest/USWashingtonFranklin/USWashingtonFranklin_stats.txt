1 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1671.59    44.90
p_loo       35.43        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      338   88.0%
 (0.5, 0.7]   (ok)         40   10.4%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.241  0.058   0.151    0.332  ...   141.0     151.0      54.0   1.05
pu        0.750  0.028   0.705    0.800  ...    25.0      29.0     100.0   1.06
mu        0.125  0.024   0.091    0.179  ...     8.0      11.0      20.0   1.14
mus       0.184  0.034   0.127    0.247  ...   106.0      98.0     100.0   0.99
gamma     0.227  0.035   0.160    0.281  ...    73.0      74.0      62.0   1.03
Is_begin  1.401  0.978   0.026    3.136  ...   120.0     130.0      88.0   0.99
Ia_begin  0.845  0.603   0.021    1.843  ...   152.0     152.0      93.0   1.10
E_begin   1.143  1.077   0.039    3.741  ...    95.0      85.0      60.0   1.04

[8 rows x 11 columns]