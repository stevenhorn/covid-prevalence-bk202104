0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -2321.93    34.40
p_loo       29.34        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      332   86.5%
 (0.5, 0.7]   (ok)         37    9.6%
   (0.7, 1]   (bad)        15    3.9%
   (1, Inf)   (very bad)    0    0.0%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.237   0.047   0.155    0.320  ...    81.0      91.0      59.0   1.02
pu         0.750   0.031   0.711    0.813  ...    72.0      67.0      40.0   1.03
mu         0.101   0.016   0.073    0.124  ...    46.0      43.0      39.0   1.03
mus        0.163   0.028   0.116    0.221  ...    75.0      75.0      57.0   1.02
gamma      0.249   0.044   0.162    0.332  ...   118.0     110.0      76.0   0.99
Is_begin  14.549   9.517   0.453   30.881  ...    43.0      36.0      59.0   1.04
Ia_begin  44.323  17.411  13.459   76.700  ...    80.0      79.0     100.0   1.00
E_begin   62.428  46.895   1.678  145.092  ...    41.0      29.0      56.0   1.07

[8 rows x 11 columns]