0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -878.12    42.04
p_loo       33.82        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      357   93.0%
 (0.5, 0.7]   (ok)         20    5.2%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    4    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.253  0.055   0.170    0.341  ...   101.0     119.0      72.0   1.02
pu        0.753  0.025   0.706    0.789  ...    61.0      64.0      60.0   1.01
mu        0.147  0.030   0.102    0.209  ...     6.0       6.0      36.0   1.31
mus       0.188  0.036   0.138    0.259  ...    49.0      44.0      33.0   1.07
gamma     0.221  0.043   0.151    0.302  ...    80.0      84.0      69.0   1.00
Is_begin  0.559  0.728   0.000    2.100  ...    96.0      76.0      43.0   0.98
Ia_begin  1.090  0.973   0.006    3.085  ...    86.0      51.0      26.0   1.06
E_begin   0.407  0.470   0.002    1.149  ...    92.0      81.0      60.0   0.99

[8 rows x 11 columns]