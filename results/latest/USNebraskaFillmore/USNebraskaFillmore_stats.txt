0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -572.12    26.70
p_loo       26.50        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      367   95.6%
 (0.5, 0.7]   (ok)         15    3.9%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.254  0.048   0.170    0.337  ...    70.0      75.0      59.0   1.05
pu        0.803  0.018   0.770    0.835  ...    54.0      54.0      61.0   1.02
mu        0.138  0.023   0.097    0.184  ...    31.0      25.0      60.0   1.06
mus       0.186  0.033   0.142    0.266  ...   111.0     152.0      43.0   1.09
gamma     0.242  0.047   0.160    0.314  ...    76.0     110.0      60.0   1.09
Is_begin  0.539  0.471   0.023    1.487  ...    53.0      45.0      60.0   1.02
Ia_begin  1.025  1.258   0.001    3.421  ...    14.0      14.0      35.0   1.10
E_begin   0.507  0.656   0.002    1.834  ...    26.0      15.0      42.0   1.09

[8 rows x 11 columns]