0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -609.54    35.87
p_loo       32.33        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      357   93.0%
 (0.5, 0.7]   (ok)         20    5.2%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.254  0.056   0.159    0.338  ...   147.0     146.0      73.0   1.05
pu        0.761  0.024   0.715    0.795  ...    26.0      28.0      53.0   1.07
mu        0.147  0.031   0.093    0.204  ...    31.0      29.0      37.0   1.06
mus       0.220  0.033   0.157    0.278  ...   112.0     130.0      83.0   0.99
gamma     0.276  0.043   0.203    0.352  ...   152.0     152.0      60.0   0.99
Is_begin  0.446  0.438   0.009    1.324  ...    22.0      34.0      38.0   1.04
Ia_begin  1.004  1.223   0.005    3.470  ...    46.0      40.0      87.0   1.04
E_begin   0.345  0.403   0.001    0.977  ...    51.0      57.0      40.0   1.00

[8 rows x 11 columns]