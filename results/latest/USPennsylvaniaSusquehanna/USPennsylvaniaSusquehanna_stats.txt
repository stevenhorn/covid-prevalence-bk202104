0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -990.23    27.01
p_loo       30.47        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      352   91.7%
 (0.5, 0.7]   (ok)         30    7.8%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.248  0.049   0.183    0.343  ...    54.0      51.0      39.0   1.00
pu        0.775  0.042   0.709    0.843  ...    34.0      37.0      62.0   1.16
mu        0.142  0.028   0.094    0.188  ...    16.0      22.0      46.0   1.06
mus       0.193  0.032   0.146    0.263  ...    97.0     117.0      60.0   1.02
gamma     0.242  0.050   0.171    0.354  ...    84.0      90.0      80.0   1.03
Is_begin  1.080  0.736   0.046    2.196  ...   109.0      96.0      60.0   1.04
Ia_begin  2.435  2.254   0.046    6.620  ...   100.0      81.0      60.0   1.03
E_begin   1.626  1.656   0.055    5.467  ...   111.0      74.0      59.0   0.99

[8 rows x 11 columns]