0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1159.59    42.75
p_loo       45.21        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      353   91.9%
 (0.5, 0.7]   (ok)         22    5.7%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.248  0.048   0.157    0.330  ...    92.0      93.0      62.0   1.01
pu        0.752  0.023   0.705    0.791  ...    61.0      62.0      18.0   1.01
mu        0.150  0.022   0.108    0.190  ...    14.0      13.0      56.0   1.12
mus       0.195  0.028   0.142    0.258  ...    73.0      50.0      56.0   1.03
gamma     0.265  0.042   0.204    0.354  ...   141.0     152.0      81.0   1.00
Is_begin  0.557  0.544   0.011    1.214  ...   102.0     105.0      40.0   1.00
Ia_begin  1.264  1.450   0.004    3.930  ...    90.0      64.0      43.0   1.06
E_begin   0.591  0.615   0.004    1.671  ...    23.0      34.0      60.0   1.12

[8 rows x 11 columns]