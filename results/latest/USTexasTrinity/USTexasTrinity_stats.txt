0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -860.81    40.46
p_loo       35.38        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      357   93.0%
 (0.5, 0.7]   (ok)         21    5.5%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.294  0.048   0.182    0.348  ...    10.0      15.0      38.0   1.10
pu        0.894  0.007   0.884    0.900  ...    62.0      70.0      88.0   1.10
mu        0.153  0.023   0.119    0.195  ...     7.0       7.0      25.0   1.26
mus       0.173  0.030   0.126    0.235  ...    73.0      85.0      60.0   1.00
gamma     0.242  0.040   0.158    0.309  ...    91.0     102.0      61.0   1.01
Is_begin  0.794  0.820   0.024    2.470  ...    79.0      58.0      60.0   1.00
Ia_begin  2.001  1.887   0.105    4.536  ...    61.0      48.0      43.0   0.99
E_begin   1.015  1.113   0.017    3.147  ...    41.0      38.0      40.0   1.03

[8 rows x 11 columns]