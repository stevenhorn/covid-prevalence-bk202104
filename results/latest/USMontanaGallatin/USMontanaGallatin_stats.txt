0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1557.73    28.52
p_loo       22.08        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      355   92.4%
 (0.5, 0.7]   (ok)         27    7.0%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.219  0.044   0.152    0.313  ...   132.0     116.0      59.0   1.00
pu        0.725  0.015   0.703    0.754  ...   141.0     152.0      21.0   1.01
mu        0.151  0.020   0.121    0.195  ...    41.0      40.0      86.0   1.02
mus       0.206  0.028   0.161    0.260  ...    96.0     101.0     100.0   0.99
gamma     0.289  0.038   0.237    0.364  ...    57.0      71.0      60.0   1.03
Is_begin  0.764  0.596   0.009    1.898  ...   108.0      58.0      40.0   1.06
Ia_begin  2.177  1.643   0.073    5.074  ...    80.0      59.0      59.0   1.00
E_begin   1.295  1.212   0.076    3.817  ...    65.0      68.0      60.0   0.99

[8 rows x 11 columns]