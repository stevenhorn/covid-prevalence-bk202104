0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1518.38    33.98
p_loo       35.95        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      358   93.2%
 (0.5, 0.7]   (ok)         17    4.4%
   (0.7, 1]   (bad)         8    2.1%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.240  0.055   0.154    0.323  ...   152.0     143.0      54.0   1.09
pu        0.743  0.033   0.701    0.809  ...    77.0      68.0      88.0   1.01
mu        0.135  0.019   0.102    0.167  ...    24.0      27.0      43.0   1.08
mus       0.197  0.035   0.143    0.259  ...   152.0     152.0      74.0   1.04
gamma     0.292  0.049   0.218    0.392  ...   113.0     114.0     100.0   1.00
Is_begin  1.541  1.103   0.058    3.846  ...    61.0      61.0      95.0   1.00
Ia_begin  0.723  0.573   0.008    1.813  ...   152.0     100.0      59.0   1.00
E_begin   1.618  1.619   0.011    5.219  ...   133.0     137.0      80.0   1.00

[8 rows x 11 columns]