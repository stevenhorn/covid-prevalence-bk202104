0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1405.64    27.37
p_loo       25.16        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      344   89.6%
 (0.5, 0.7]   (ok)         33    8.6%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.250  0.054   0.169    0.342  ...    46.0      47.0      20.0   1.14
pu        0.775  0.031   0.725    0.830  ...    27.0      24.0      39.0   1.06
mu        0.108  0.019   0.076    0.140  ...     9.0      11.0      88.0   1.14
mus       0.174  0.041   0.106    0.267  ...    60.0      71.0      60.0   1.03
gamma     0.178  0.037   0.119    0.242  ...    88.0      83.0      59.0   1.00
Is_begin  0.987  1.050   0.016    3.268  ...    75.0      34.0      40.0   1.06
Ia_begin  2.967  2.154   0.158    6.994  ...   152.0     152.0      93.0   0.99
E_begin   1.385  1.618   0.011    4.352  ...    69.0      29.0      45.0   1.09

[8 rows x 11 columns]