0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1659.07    67.69
p_loo       44.94        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      352   91.7%
 (0.5, 0.7]   (ok)         26    6.8%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.256  0.050   0.160    0.328  ...    30.0      30.0      38.0   1.06
pu        0.754  0.032   0.704    0.813  ...    21.0      21.0      39.0   1.07
mu        0.127  0.018   0.095    0.154  ...    40.0      43.0      58.0   1.10
mus       0.223  0.036   0.158    0.286  ...    65.0      64.0      79.0   1.03
gamma     0.295  0.053   0.207    0.396  ...    58.0      59.0      61.0   1.02
Is_begin  0.603  0.542   0.012    1.625  ...    92.0      95.0      75.0   1.02
Ia_begin  1.368  1.084   0.036    3.016  ...    55.0      43.0      40.0   1.03
E_begin   0.732  0.699   0.001    2.028  ...    89.0      44.0      22.0   1.05

[8 rows x 11 columns]