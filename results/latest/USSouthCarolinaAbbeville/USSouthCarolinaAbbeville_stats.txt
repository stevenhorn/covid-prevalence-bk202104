0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1140.88    47.17
p_loo       40.10        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      355   92.4%
 (0.5, 0.7]   (ok)         25    6.5%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.256  0.065   0.157    0.343  ...   139.0     123.0      54.0   1.02
pu        0.783  0.025   0.733    0.823  ...    50.0      52.0      33.0   1.01
mu        0.124  0.025   0.081    0.177  ...    12.0      10.0      20.0   1.17
mus       0.190  0.040   0.115    0.263  ...   152.0     137.0      88.0   1.02
gamma     0.213  0.038   0.145    0.280  ...    79.0      90.0      24.0   1.10
Is_begin  0.856  0.648   0.084    2.176  ...   152.0     130.0      60.0   1.00
Ia_begin  1.529  1.347   0.029    4.271  ...   152.0     152.0     100.0   0.99
E_begin   0.666  0.713   0.005    2.025  ...   111.0      77.0      43.0   1.00

[8 rows x 11 columns]