0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1116.02    43.40
p_loo       36.49        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      355   92.4%
 (0.5, 0.7]   (ok)         21    5.5%
   (0.7, 1]   (bad)         8    2.1%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.220  0.038   0.164    0.285  ...    63.0      61.0      56.0   1.03
pu        0.723  0.016   0.701    0.749  ...    37.0      33.0      37.0   1.02
mu        0.138  0.024   0.095    0.181  ...     9.0       9.0      59.0   1.19
mus       0.192  0.040   0.129    0.260  ...    49.0      43.0      40.0   1.05
gamma     0.223  0.035   0.159    0.279  ...    44.0      46.0      60.0   1.02
Is_begin  0.596  0.545   0.005    1.647  ...    73.0      59.0      57.0   1.01
Ia_begin  1.270  1.265   0.015    3.913  ...    75.0      71.0      93.0   1.01
E_begin   0.540  0.572   0.030    1.599  ...    60.0      48.0      53.0   1.02

[8 rows x 11 columns]