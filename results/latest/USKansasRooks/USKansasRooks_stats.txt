0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1008.00    58.39
p_loo       47.72        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      365   95.1%
 (0.5, 0.7]   (ok)         16    4.2%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.238  0.051   0.169    0.343  ...    76.0     132.0      48.0   1.14
pu        0.732  0.022   0.701    0.771  ...    66.0      60.0      40.0   1.02
mu        0.110  0.024   0.073    0.152  ...    63.0      61.0      35.0   1.04
mus       0.210  0.036   0.148    0.274  ...    87.0     106.0      59.0   1.02
gamma     0.264  0.043   0.173    0.326  ...    34.0      47.0      96.0   1.04
Is_begin  0.519  0.631   0.001    1.752  ...    79.0      57.0      60.0   1.01
Ia_begin  0.891  1.208   0.015    2.535  ...    99.0      69.0      43.0   1.03
E_begin   0.442  0.554   0.001    1.492  ...    71.0      47.0      54.0   1.02

[8 rows x 11 columns]