0 Divergences 
Passed validation 
Computed from 80 by 385 log-likelihood matrix

         Estimate       SE
elpd_loo -1196.20    34.08
p_loo       41.17        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      365   94.8%
 (0.5, 0.7]   (ok)         14    3.6%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    4    1.0%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.230   0.054   0.150    0.324  ...    46.0      35.0      24.0   1.07
pu         0.744   0.029   0.705    0.808  ...    62.0      64.0      59.0   1.00
mu         0.226   0.027   0.185    0.280  ...    21.0      21.0      40.0   1.06
mus        0.282   0.033   0.221    0.339  ...    44.0      44.0      59.0   1.08
gamma      0.446   0.064   0.349    0.579  ...   114.0     119.0     100.0   1.00
Is_begin  13.630   6.651   1.588   25.506  ...   100.0      91.0      91.0   1.01
Ia_begin  31.031  14.805   6.423   55.186  ...    15.0      20.0      73.0   1.08
E_begin   25.039  18.305   2.804   50.342  ...    59.0      30.0      25.0   1.07

[8 rows x 11 columns]