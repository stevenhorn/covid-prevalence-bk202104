0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -950.40    26.62
p_loo       25.98        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      366   95.3%
 (0.5, 0.7]   (ok)         11    2.9%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.253  0.055   0.155    0.332  ...    89.0      93.0      56.0   1.02
pu        0.743  0.029   0.702    0.793  ...    47.0      41.0      74.0   1.05
mu        0.131  0.024   0.086    0.169  ...    25.0      18.0      54.0   1.09
mus       0.197  0.030   0.140    0.250  ...    77.0      87.0      60.0   0.98
gamma     0.244  0.046   0.155    0.311  ...   126.0     152.0      86.0   1.01
Is_begin  0.789  0.816   0.020    2.915  ...    80.0      95.0      56.0   0.99
Ia_begin  1.596  1.780   0.033    5.777  ...   152.0     143.0      61.0   1.00
E_begin   0.825  0.919   0.015    3.003  ...   105.0     152.0      61.0   1.03

[8 rows x 11 columns]