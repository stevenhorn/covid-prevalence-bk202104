0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1348.52    62.48
p_loo       44.29        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      356   92.7%
 (0.5, 0.7]   (ok)         21    5.5%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.245  0.048   0.169    0.332  ...    62.0      63.0      95.0   1.00
pu        0.740  0.024   0.701    0.778  ...    38.0      38.0      56.0   1.03
mu        0.134  0.024   0.092    0.176  ...    16.0      13.0      40.0   1.12
mus       0.193  0.032   0.147    0.257  ...    69.0      71.0      42.0   1.00
gamma     0.223  0.052   0.147    0.337  ...    49.0      40.0      60.0   1.03
Is_begin  0.562  0.401   0.006    1.293  ...    61.0      53.0      55.0   1.04
Ia_begin  0.890  1.047   0.006    2.635  ...    74.0      26.0      58.0   1.07
E_begin   0.495  0.667   0.001    1.874  ...    99.0      66.0      60.0   1.00

[8 rows x 11 columns]