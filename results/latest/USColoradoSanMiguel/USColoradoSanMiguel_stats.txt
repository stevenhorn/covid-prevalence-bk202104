0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -828.88    32.58
p_loo       28.98        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      362   94.3%
 (0.5, 0.7]   (ok)         20    5.2%
   (0.7, 1]   (bad)         0    0.0%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.252  0.052   0.165    0.337  ...    86.0      86.0      60.0   1.00
pu        0.760  0.025   0.716    0.807  ...    16.0      16.0      40.0   1.10
mu        0.115  0.018   0.084    0.145  ...    42.0      46.0      93.0   1.02
mus       0.200  0.032   0.146    0.256  ...   152.0     152.0      93.0   0.99
gamma     0.283  0.044   0.221    0.362  ...   148.0     140.0      60.0   1.00
Is_begin  0.971  0.829   0.031    2.753  ...    37.0      36.0      17.0   1.03
Ia_begin  2.081  1.561   0.022    4.635  ...    90.0      78.0      59.0   1.03
E_begin   0.838  0.751   0.016    2.569  ...    54.0      27.0      14.0   1.04

[8 rows x 11 columns]