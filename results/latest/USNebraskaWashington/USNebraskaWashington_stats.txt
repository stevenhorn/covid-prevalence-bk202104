0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1006.96    33.79
p_loo       34.84        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      354   92.2%
 (0.5, 0.7]   (ok)         24    6.2%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.250  0.052   0.178    0.349  ...    74.0      81.0      74.0   1.03
pu        0.764  0.027   0.713    0.808  ...    37.0      38.0      60.0   1.07
mu        0.136  0.026   0.090    0.186  ...    71.0      69.0      57.0   1.02
mus       0.191  0.038   0.127    0.258  ...    92.0     146.0      97.0   1.01
gamma     0.220  0.037   0.155    0.301  ...    84.0      97.0      60.0   0.99
Is_begin  1.233  0.965   0.008    3.232  ...   152.0     117.0      56.0   1.03
Ia_begin  2.940  2.874   0.000    8.359  ...    44.0      20.0      27.0   1.09
E_begin   1.311  1.293   0.013    3.844  ...    42.0      19.0      54.0   1.09

[8 rows x 11 columns]