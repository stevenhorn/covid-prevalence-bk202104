0 Divergences 
Passed validation 
Computed from 80 by 390 log-likelihood matrix

         Estimate       SE
elpd_loo    -0.96    35.07
p_loo       30.54        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      354   90.8%
 (0.5, 0.7]   (ok)         27    6.9%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.268  0.052   0.171    0.348  ...   125.0     113.0      37.0   1.04
pu        0.884  0.013   0.861    0.900  ...    91.0      67.0      33.0   1.05
mu        0.129  0.022   0.093    0.167  ...    82.0      91.0      59.0   1.03
mus       0.217  0.035   0.156    0.275  ...    67.0      64.0      93.0   1.03
gamma     0.242  0.046   0.161    0.324  ...   109.0     136.0      72.0   1.00
Is_begin  0.513  0.430   0.040    1.340  ...    47.0      42.0      65.0   1.04
Ia_begin  0.777  0.810   0.001    2.544  ...   110.0      77.0      33.0   1.00
E_begin   0.341  0.473   0.004    0.977  ...    85.0      76.0      88.0   0.98

[8 rows x 11 columns]