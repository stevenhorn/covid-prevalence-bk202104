0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1346.32    40.56
p_loo       38.80        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      357   93.0%
 (0.5, 0.7]   (ok)         21    5.5%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.228  0.048   0.151    0.304  ...    29.0      25.0      40.0   1.07
pu        0.753  0.027   0.703    0.792  ...    43.0      40.0      60.0   1.04
mu        0.121  0.025   0.079    0.169  ...    12.0      11.0      40.0   1.14
mus       0.177  0.032   0.130    0.240  ...    70.0      87.0      93.0   1.00
gamma     0.216  0.041   0.153    0.297  ...   118.0     112.0      97.0   1.02
Is_begin  0.837  0.738   0.035    2.381  ...   117.0      90.0      55.0   0.98
Ia_begin  1.668  1.877   0.009    5.412  ...   109.0      56.0      59.0   1.01
E_begin   0.785  0.813   0.054    2.508  ...    34.0      21.0      92.0   1.08

[8 rows x 11 columns]