0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -711.16    33.71
p_loo       29.74        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      364   94.8%
 (0.5, 0.7]   (ok)         17    4.4%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.260  0.055   0.175    0.350  ...    67.0      65.0      60.0   1.03
pu        0.786  0.028   0.729    0.834  ...     8.0       8.0      20.0   1.21
mu        0.131  0.026   0.092    0.177  ...    37.0      46.0      45.0   1.05
mus       0.163  0.028   0.114    0.201  ...   122.0     120.0      96.0   1.00
gamma     0.187  0.039   0.136    0.276  ...    93.0     110.0      81.0   1.01
Is_begin  0.589  0.579   0.004    1.718  ...    74.0      68.0      54.0   0.99
Ia_begin  0.860  0.838   0.015    2.614  ...    61.0      43.0      59.0   1.01
E_begin   0.395  0.489   0.008    1.511  ...    93.0      53.0      60.0   1.00

[8 rows x 11 columns]