0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1052.55    36.69
p_loo       32.97        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      365   95.1%
 (0.5, 0.7]   (ok)         14    3.6%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.250  0.053   0.152    0.334  ...    12.0      12.0      24.0   1.14
pu        0.789  0.031   0.730    0.838  ...    44.0      44.0      88.0   1.02
mu        0.126  0.030   0.072    0.176  ...    11.0      12.0      31.0   1.12
mus       0.198  0.048   0.119    0.278  ...    51.0      72.0      58.0   1.00
gamma     0.223  0.038   0.164    0.288  ...    92.0     110.0      72.0   1.00
Is_begin  0.572  0.561   0.004    1.749  ...   152.0     146.0      59.0   1.02
Ia_begin  1.028  1.066   0.012    2.886  ...    70.0      53.0      87.0   1.03
E_begin   0.482  0.561   0.017    1.818  ...    86.0      37.0      87.0   1.04

[8 rows x 11 columns]