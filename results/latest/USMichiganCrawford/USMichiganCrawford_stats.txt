0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -850.08    43.17
p_loo       40.24        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      347   90.4%
 (0.5, 0.7]   (ok)         31    8.1%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.248  0.054   0.164    0.348  ...    64.0      58.0      32.0   1.01
pu        0.833  0.035   0.777    0.888  ...    43.0      47.0      36.0   1.03
mu        0.151  0.023   0.113    0.193  ...    70.0      71.0      59.0   1.05
mus       0.197  0.032   0.141    0.260  ...   112.0     129.0      59.0   1.02
gamma     0.274  0.041   0.202    0.349  ...   152.0     152.0      60.0   0.99
Is_begin  0.951  0.829   0.007    2.487  ...   108.0      66.0      48.0   1.03
Ia_begin  2.183  1.936   0.005    5.606  ...    68.0      63.0      43.0   1.01
E_begin   1.291  1.334   0.003    3.881  ...    58.0      42.0      27.0   1.03

[8 rows x 11 columns]