0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -906.56    34.58
p_loo       26.83        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      356   92.7%
 (0.5, 0.7]   (ok)         20    5.2%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.237  0.046   0.153    0.322  ...    62.0      58.0      60.0   1.02
pu        0.798  0.019   0.769    0.827  ...    27.0      30.0      31.0   1.04
mu        0.125  0.031   0.085    0.203  ...    44.0      38.0      28.0   1.03
mus       0.153  0.031   0.088    0.199  ...   126.0     127.0      19.0   1.15
gamma     0.158  0.035   0.096    0.231  ...   109.0     116.0      60.0   0.99
Is_begin  0.871  0.852   0.008    2.676  ...    88.0     107.0      43.0   1.05
Ia_begin  1.815  1.925   0.010    5.538  ...    51.0      63.0      42.0   1.00
E_begin   0.671  0.844   0.004    2.424  ...   102.0      76.0      38.0   1.05

[8 rows x 11 columns]