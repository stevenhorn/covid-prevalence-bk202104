0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1839.75    41.75
p_loo       33.69        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      340   88.5%
 (0.5, 0.7]   (ok)         37    9.6%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.227  0.051   0.152    0.308  ...    42.0      37.0      47.0   1.05
pu        0.736  0.025   0.701    0.778  ...   117.0     105.0      60.0   1.01
mu        0.126  0.022   0.094    0.170  ...    13.0      16.0      59.0   1.10
mus       0.223  0.030   0.171    0.265  ...    77.0      76.0      96.0   0.99
gamma     0.319  0.052   0.235    0.416  ...    83.0      79.0      93.0   0.98
Is_begin  3.140  1.916   0.705    6.549  ...   152.0     152.0      40.0   1.01
Ia_begin  7.994  4.147   1.102   15.145  ...    98.0      82.0      40.0   0.99
E_begin   9.667  6.718   0.607   21.447  ...    97.0     102.0      60.0   0.98

[8 rows x 11 columns]