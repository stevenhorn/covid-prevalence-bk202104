0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -586.35    37.90
p_loo       36.32        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      354   92.2%
 (0.5, 0.7]   (ok)         25    6.5%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.245  0.051   0.166    0.332  ...   111.0     110.0      96.0   1.00
pu        0.818  0.053   0.710    0.871  ...     3.0       4.0      34.0   1.83
mu        0.153  0.034   0.100    0.226  ...    12.0      12.0      53.0   1.12
mus       0.202  0.039   0.138    0.295  ...    33.0      37.0      31.0   1.05
gamma     0.252  0.055   0.152    0.350  ...   137.0     123.0      66.0   1.12
Is_begin  1.274  1.017   0.030    3.244  ...   125.0      84.0      56.0   1.04
Ia_begin  2.882  2.323   0.243    8.138  ...    91.0      46.0      43.0   1.04
E_begin   1.690  1.508   0.043    5.328  ...    16.0      18.0      40.0   1.11

[8 rows x 11 columns]