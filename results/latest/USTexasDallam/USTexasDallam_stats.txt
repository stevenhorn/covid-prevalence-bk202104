0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1122.12    59.70
p_loo       41.94        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      373   97.1%
 (0.5, 0.7]   (ok)          5    1.3%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.167  0.016   0.150    0.195  ...   146.0     152.0      74.0   0.99
pu        0.705  0.006   0.700    0.714  ...   152.0     152.0      52.0   1.00
mu        0.101  0.014   0.082    0.129  ...    11.0      12.0      44.0   1.12
mus       0.202  0.033   0.154    0.271  ...    83.0     111.0      64.0   1.02
gamma     0.294  0.051   0.185    0.376  ...    70.0      67.0      59.0   1.01
Is_begin  0.614  0.586   0.005    1.604  ...    74.0      48.0      42.0   1.04
Ia_begin  0.952  0.973   0.040    3.374  ...    48.0      23.0      95.0   1.08
E_begin   0.360  0.427   0.001    1.288  ...    18.0      10.0      22.0   1.17

[8 rows x 11 columns]