0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1651.63    30.02
p_loo       25.10        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      351   91.4%
 (0.5, 0.7]   (ok)         28    7.3%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.257  0.052   0.167    0.344  ...    48.0      49.0      18.0   1.14
pu        0.765  0.026   0.724    0.808  ...    10.0      12.0      57.0   1.14
mu        0.109  0.017   0.082    0.137  ...    29.0      31.0      81.0   1.04
mus       0.166  0.026   0.123    0.212  ...   102.0      96.0      91.0   1.00
gamma     0.200  0.030   0.149    0.254  ...   152.0     152.0     100.0   0.99
Is_begin  0.960  0.871   0.007    2.460  ...   131.0      94.0      43.0   1.01
Ia_begin  2.012  1.770   0.034    5.192  ...    84.0      64.0      59.0   0.98
E_begin   1.318  1.290   0.008    4.121  ...    81.0      67.0      41.0   0.99

[8 rows x 11 columns]