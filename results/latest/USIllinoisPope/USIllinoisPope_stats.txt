0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -464.21    27.66
p_loo       24.68        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      344   89.6%
 (0.5, 0.7]   (ok)         33    8.6%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.254  0.058   0.163    0.348  ...    84.0      81.0      59.0   1.03
pu        0.843  0.019   0.812    0.878  ...    53.0      49.0      96.0   1.02
mu        0.134  0.022   0.094    0.170  ...    32.0      27.0      60.0   1.08
mus       0.167  0.028   0.126    0.206  ...   117.0      84.0      60.0   0.99
gamma     0.197  0.043   0.121    0.269  ...   152.0     152.0      88.0   1.11
Is_begin  0.358  0.366   0.000    1.059  ...    58.0      43.0      38.0   1.01
Ia_begin  0.840  1.016   0.016    3.209  ...    92.0      80.0      60.0   1.00
E_begin   0.354  0.437   0.003    1.299  ...    90.0      61.0      93.0   1.02

[8 rows x 11 columns]