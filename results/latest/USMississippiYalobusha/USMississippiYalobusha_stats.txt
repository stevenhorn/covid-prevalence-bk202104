0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -980.05    29.18
p_loo       26.64        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      363   94.5%
 (0.5, 0.7]   (ok)         17    4.4%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.234  0.051   0.158    0.325  ...    83.0      77.0      36.0   1.00
pu        0.734  0.022   0.700    0.778  ...    17.0      17.0      55.0   1.10
mu        0.107  0.023   0.063    0.148  ...    20.0      24.0      40.0   1.07
mus       0.165  0.030   0.115    0.226  ...    87.0      91.0      91.0   0.99
gamma     0.170  0.028   0.118    0.209  ...   125.0     117.0      96.0   1.01
Is_begin  0.701  0.610   0.058    1.870  ...    88.0     100.0      60.0   1.05
Ia_begin  1.182  1.174   0.030    3.691  ...   103.0     114.0      88.0   0.99
E_begin   0.605  0.797   0.000    2.116  ...    87.0      68.0      39.0   1.01

[8 rows x 11 columns]