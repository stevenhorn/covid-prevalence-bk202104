0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1020.09    29.31
p_loo       26.98        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      357   93.0%
 (0.5, 0.7]   (ok)         23    6.0%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.219  0.047   0.152    0.299  ...   152.0     111.0      60.0   1.00
pu        0.727  0.016   0.704    0.754  ...    95.0      81.0      35.0   1.00
mu        0.107  0.021   0.072    0.148  ...    10.0      10.0      68.0   1.18
mus       0.165  0.036   0.091    0.226  ...    27.0      23.0      60.0   1.07
gamma     0.177  0.034   0.119    0.236  ...   133.0     137.0      47.0   1.03
Is_begin  0.660  0.679   0.012    2.006  ...   139.0     152.0      96.0   0.99
Ia_begin  1.050  0.820   0.105    2.634  ...   145.0     152.0     100.0   0.98
E_begin   0.427  0.488   0.005    1.443  ...    95.0      99.0      61.0   1.02

[8 rows x 11 columns]