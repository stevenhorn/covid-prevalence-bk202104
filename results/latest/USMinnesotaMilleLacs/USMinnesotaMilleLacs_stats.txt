0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -899.20    30.16
p_loo       22.88        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      360   93.8%
 (0.5, 0.7]   (ok)         21    5.5%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.244  0.050   0.162    0.328  ...    81.0      77.0      59.0   1.02
pu        0.773  0.024   0.730    0.813  ...    61.0      68.0      42.0   0.99
mu        0.121  0.021   0.090    0.156  ...     7.0       8.0      60.0   1.24
mus       0.181  0.034   0.129    0.252  ...   149.0     152.0      48.0   1.04
gamma     0.229  0.038   0.173    0.304  ...   152.0     142.0      43.0   1.01
Is_begin  0.468  0.477   0.001    1.423  ...   106.0      86.0      79.0   0.99
Ia_begin  0.702  0.841   0.002    2.819  ...   118.0     152.0      88.0   0.99
E_begin   0.387  0.490   0.005    1.249  ...    93.0     141.0      79.0   1.00

[8 rows x 11 columns]