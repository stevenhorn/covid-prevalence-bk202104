0 Divergences 
Passed validation 
Computed from 80 by 390 log-likelihood matrix

         Estimate       SE
elpd_loo -1023.89    37.87
p_loo       32.08        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      362   92.8%
 (0.5, 0.7]   (ok)         24    6.2%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.281  0.050   0.199    0.349  ...    29.0      45.0      60.0   1.05
pu        0.888  0.011   0.868    0.900  ...    30.0      12.0      28.0   1.13
mu        0.174  0.023   0.138    0.217  ...    33.0      22.0      36.0   1.07
mus       0.189  0.039   0.132    0.272  ...   137.0     152.0      60.0   0.98
gamma     0.252  0.047   0.166    0.324  ...   152.0     152.0      74.0   0.99
Is_begin  3.740  3.719   0.016   11.195  ...   131.0     113.0      93.0   0.98
Ia_begin  9.204  8.981   0.093   28.649  ...   103.0      65.0      83.0   1.01
E_begin   4.009  4.974   0.034   13.730  ...    92.0      63.0      56.0   1.01

[8 rows x 11 columns]