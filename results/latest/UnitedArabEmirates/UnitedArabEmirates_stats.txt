24 Divergences 
Passed validation 
Computed from 80 by 385 log-likelihood matrix

         Estimate       SE
elpd_loo -2427.22    26.97
p_loo       95.21        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      339   88.1%
 (0.5, 0.7]   (ok)         33    8.6%
   (0.7, 1]   (bad)        12    3.1%
   (1, Inf)   (very bad)    1    0.3%

             mean       sd  hdi_3%   hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.230    0.056   0.151     0.327  ...     6.0       7.0      24.0   1.26
pu          0.810    0.060   0.730     0.892  ...     3.0       3.0      49.0   1.87
mu          0.179    0.111   0.081     0.386  ...     3.0       3.0      15.0   1.99
mus         0.210    0.045   0.143     0.312  ...    13.0      16.0      57.0   1.24
gamma       0.396    0.078   0.277     0.521  ...     5.0       6.0      49.0   1.31
Is_begin   22.562   12.211   6.815    51.627  ...     5.0       7.0      80.0   1.64
Ia_begin   66.556   47.758  15.619   152.432  ...     3.0       4.0      54.0   1.87
E_begin   380.130  532.225  25.675  1441.747  ...     3.0       3.0      17.0   2.80

[8 rows x 11 columns]