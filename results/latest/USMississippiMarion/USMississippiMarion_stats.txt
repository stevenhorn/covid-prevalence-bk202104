0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1163.35    47.70
p_loo       38.24        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      364   94.8%
 (0.5, 0.7]   (ok)         15    3.9%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.254  0.052   0.165    0.344  ...    95.0      96.0      24.0   1.25
pu        0.762  0.035   0.700    0.815  ...    24.0      32.0      24.0   1.11
mu        0.125  0.021   0.089    0.167  ...     8.0       8.0     100.0   1.26
mus       0.193  0.040   0.128    0.262  ...    90.0      82.0      95.0   1.00
gamma     0.235  0.043   0.174    0.338  ...   100.0     108.0      61.0   1.00
Is_begin  0.925  0.669   0.019    1.918  ...   103.0      85.0      43.0   1.00
Ia_begin  1.940  1.754   0.022    4.781  ...    94.0      74.0      60.0   0.99
E_begin   0.944  0.809   0.002    2.598  ...   100.0      80.0      88.0   1.00

[8 rows x 11 columns]