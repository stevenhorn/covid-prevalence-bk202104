1 Divergences 
Passed validation 
Computed from 80 by 385 log-likelihood matrix

         Estimate       SE
elpd_loo -2418.23    32.61
p_loo       45.21        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      335   87.0%
 (0.5, 0.7]   (ok)         39   10.1%
   (0.7, 1]   (bad)        10    2.6%
   (1, Inf)   (very bad)    1    0.3%

              mean        sd   hdi_3%  ...  ess_bulk  ess_tail  r_hat
pa           0.254     0.053    0.150  ...      41.0      32.0   1.08
pu           0.814     0.042    0.727  ...       4.0      30.0   1.73
mu           0.192     0.034    0.149  ...       5.0      18.0   1.40
mus          0.249     0.038    0.183  ...      14.0      46.0   1.10
gamma        0.351     0.048    0.272  ...      78.0      87.0   1.02
Is_begin   624.079   344.957   42.696  ...      22.0      23.0   1.09
Ia_begin  2219.255   819.517  824.747  ...      36.0      27.0   1.15
E_begin   3653.722  1659.160  124.501  ...       7.0      17.0   1.24

[8 rows x 11 columns]