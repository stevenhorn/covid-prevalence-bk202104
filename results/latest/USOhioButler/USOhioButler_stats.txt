1 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1776.69    41.63
p_loo       27.97        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      350   91.1%
 (0.5, 0.7]   (ok)         28    7.3%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.243  0.049   0.167    0.337  ...    27.0      26.0      22.0   1.05
pu        0.790  0.029   0.741    0.851  ...     8.0       9.0      16.0   1.20
mu        0.111  0.019   0.074    0.151  ...    28.0      28.0      39.0   1.06
mus       0.171  0.031   0.126    0.232  ...   152.0     142.0      93.0   0.99
gamma     0.186  0.032   0.134    0.246  ...    89.0      97.0      77.0   1.02
Is_begin  1.678  1.192   0.084    3.777  ...    40.0      25.0      38.0   1.08
Ia_begin  3.325  2.679   0.124    7.730  ...   116.0      85.0      56.0   1.05
E_begin   2.559  2.343   0.070    7.701  ...    84.0      58.0      49.0   1.03

[8 rows x 11 columns]