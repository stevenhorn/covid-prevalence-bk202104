1 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1378.94    38.67
p_loo       27.79        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      354   92.2%
 (0.5, 0.7]   (ok)         24    6.2%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.235  0.051   0.154    0.308  ...    74.0      90.0      88.0   0.99
pu        0.767  0.037   0.704    0.829  ...    73.0      60.0      58.0   1.03
mu        0.126  0.020   0.087    0.161  ...    41.0      41.0      60.0   1.01
mus       0.192  0.027   0.146    0.236  ...    88.0      89.0      60.0   1.01
gamma     0.255  0.046   0.174    0.332  ...   140.0     129.0      47.0   0.99
Is_begin  1.809  1.219   0.081    3.773  ...    88.0      72.0      42.0   1.02
Ia_begin  5.114  3.382   0.249   10.533  ...    39.0      30.0      33.0   1.02
E_begin   4.824  3.887   0.183   12.798  ...   136.0      99.0      56.0   1.01

[8 rows x 11 columns]