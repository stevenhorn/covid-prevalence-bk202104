0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1503.93    58.04
p_loo       27.31        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      374   97.4%
 (0.5, 0.7]   (ok)          4    1.0%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.172  0.009   0.158    0.186  ...     4.0       3.0      22.0   1.93
pu        0.710  0.006   0.705    0.722  ...     3.0       4.0      22.0   1.71
mu        0.201  0.031   0.157    0.237  ...     3.0       3.0      18.0   2.89
mus       0.228  0.027   0.200    0.281  ...     3.0       3.0      14.0   1.93
gamma     0.265  0.038   0.212    0.342  ...     6.0       6.0      24.0   1.32
Is_begin  0.507  0.422   0.119    1.578  ...     3.0       4.0      14.0   1.63
Ia_begin  0.551  0.266   0.182    0.938  ...     3.0       4.0      25.0   1.82
E_begin   0.253  0.206   0.018    0.534  ...     3.0       3.0      23.0   2.15

[8 rows x 11 columns]