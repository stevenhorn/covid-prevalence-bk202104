0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1440.23    41.77
p_loo       33.33        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      350   91.1%
 (0.5, 0.7]   (ok)         24    6.2%
   (0.7, 1]   (bad)         8    2.1%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.251  0.051   0.164    0.335  ...   152.0     152.0      77.0   1.01
pu        0.780  0.037   0.706    0.826  ...     9.0      11.0      59.0   1.15
mu        0.127  0.023   0.088    0.166  ...    56.0      53.0      58.0   1.00
mus       0.180  0.035   0.131    0.240  ...    65.0      61.0      59.0   1.04
gamma     0.236  0.040   0.151    0.293  ...   130.0     124.0      73.0   1.06
Is_begin  1.132  0.979   0.053    2.786  ...    68.0      68.0      93.0   1.02
Ia_begin  2.581  2.206   0.078    6.282  ...   108.0      58.0      59.0   1.01
E_begin   1.693  1.911   0.012    5.240  ...    65.0      72.0      60.0   1.02

[8 rows x 11 columns]