0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1025.70    51.17
p_loo       38.90        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      350   91.1%
 (0.5, 0.7]   (ok)         29    7.6%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.259  0.051   0.173    0.349  ...    66.0      62.0      40.0   1.03
pu        0.788  0.037   0.716    0.844  ...     6.0       6.0      16.0   1.28
mu        0.147  0.023   0.107    0.188  ...    38.0      40.0      60.0   1.02
mus       0.194  0.032   0.139    0.262  ...    85.0      86.0      69.0   1.02
gamma     0.243  0.046   0.149    0.316  ...   121.0     125.0      60.0   1.00
Is_begin  1.156  0.954   0.020    3.490  ...   152.0     152.0      80.0   1.00
Ia_begin  2.101  2.315   0.004    6.366  ...   110.0      68.0      43.0   1.01
E_begin   1.068  1.458   0.009    3.072  ...    75.0      52.0      86.0   1.00

[8 rows x 11 columns]