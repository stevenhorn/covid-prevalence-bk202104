0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -780.73    71.76
p_loo       45.04        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      356   92.7%
 (0.5, 0.7]   (ok)         18    4.7%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    5    1.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.255  0.052   0.174    0.339  ...    55.0      66.0      25.0   1.05
pu        0.735  0.023   0.700    0.774  ...    27.0      33.0      22.0   1.05
mu        0.083  0.015   0.060    0.107  ...    38.0      37.0      48.0   1.05
mus       0.242  0.028   0.180    0.284  ...   129.0     134.0      72.0   1.04
gamma     0.491  0.071   0.382    0.633  ...    54.0      39.0      61.0   1.06
Is_begin  0.244  0.321   0.002    0.650  ...    70.0      25.0      85.0   1.06
Ia_begin  0.338  0.593   0.000    1.605  ...    53.0      26.0      33.0   1.06
E_begin   0.109  0.115   0.001    0.328  ...    62.0      36.0      58.0   1.02

[8 rows x 11 columns]