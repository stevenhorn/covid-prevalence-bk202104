0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1372.15    50.25
p_loo       44.73        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      351   91.4%
 (0.5, 0.7]   (ok)         23    6.0%
   (0.7, 1]   (bad)         8    2.1%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.213  0.038   0.156    0.283  ...    25.0      22.0      60.0   1.09
pu        0.720  0.015   0.700    0.751  ...    17.0      14.0      72.0   1.12
mu        0.162  0.019   0.127    0.197  ...    35.0      35.0      43.0   1.03
mus       0.329  0.046   0.258    0.430  ...    80.0      83.0      83.0   0.99
gamma     0.510  0.086   0.386    0.664  ...   125.0     129.0     100.0   0.99
Is_begin  1.006  0.934   0.002    2.653  ...    36.0      23.0      54.0   1.07
Ia_begin  1.858  1.562   0.113    4.495  ...    78.0      65.0      60.0   0.99
E_begin   0.782  0.966   0.012    3.121  ...    81.0      62.0      59.0   1.00

[8 rows x 11 columns]