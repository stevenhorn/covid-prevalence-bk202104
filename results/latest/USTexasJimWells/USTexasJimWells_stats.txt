0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1412.96    53.58
p_loo       42.50        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      362   94.3%
 (0.5, 0.7]   (ok)         17    4.4%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.259  0.052   0.170    0.343  ...    42.0      44.0      58.0   1.04
pu        0.771  0.025   0.731    0.824  ...    56.0      57.0      50.0   1.01
mu        0.140  0.038   0.066    0.206  ...     3.0       3.0      19.0   1.96
mus       0.227  0.041   0.163    0.302  ...    18.0      20.0      65.0   1.08
gamma     0.274  0.047   0.184    0.354  ...    78.0      82.0      60.0   1.02
Is_begin  0.466  0.513   0.010    1.537  ...    32.0      27.0      25.0   1.07
Ia_begin  0.930  1.227   0.004    3.276  ...    29.0      19.0      60.0   1.11
E_begin   0.299  0.419   0.003    1.356  ...    87.0      59.0      88.0   1.00

[8 rows x 11 columns]