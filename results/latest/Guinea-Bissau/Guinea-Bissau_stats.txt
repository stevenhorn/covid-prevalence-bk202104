0 Divergences 
Passed validation 
Computed from 80 by 385 log-likelihood matrix

         Estimate       SE
elpd_loo -1506.77    34.15
p_loo       40.14        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      340   88.3%
 (0.5, 0.7]   (ok)         35    9.1%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    6    1.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.271  0.063   0.164    0.349  ...    51.0      61.0      54.0   1.05
pu        0.878  0.019   0.839    0.898  ...    84.0      81.0      97.0   1.00
mu        0.149  0.024   0.118    0.207  ...     8.0      10.0      17.0   1.16
mus       0.175  0.027   0.134    0.237  ...   152.0     152.0      95.0   1.00
gamma     0.268  0.043   0.204    0.356  ...    83.0      78.0      59.0   1.01
Is_begin  0.926  0.700   0.013    2.159  ...    20.0     101.0      53.0   1.09
Ia_begin  1.685  1.949   0.010    5.808  ...   127.0      97.0      95.0   1.02
E_begin   1.001  1.160   0.025    3.774  ...   100.0     108.0      60.0   1.00

[8 rows x 11 columns]