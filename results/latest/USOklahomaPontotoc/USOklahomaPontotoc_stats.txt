0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1246.82    32.70
p_loo       30.07        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      359   93.5%
 (0.5, 0.7]   (ok)         20    5.2%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.240  0.054   0.155    0.327  ...    73.0      73.0      32.0   1.06
pu        0.762  0.025   0.714    0.797  ...     6.0       7.0      59.0   1.29
mu        0.126  0.025   0.074    0.165  ...    12.0      12.0      60.0   1.15
mus       0.162  0.030   0.111    0.218  ...    74.0     118.0      32.0   1.00
gamma     0.166  0.034   0.102    0.220  ...    91.0      89.0      87.0   1.05
Is_begin  0.705  0.752   0.003    2.205  ...    72.0      85.0      35.0   1.00
Ia_begin  1.127  1.153   0.006    3.554  ...    98.0      87.0      60.0   1.01
E_begin   0.586  0.688   0.006    1.987  ...   115.0     112.0     100.0   0.99

[8 rows x 11 columns]