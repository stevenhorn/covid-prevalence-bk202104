0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1347.42    51.19
p_loo       37.93        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      350   91.1%
 (0.5, 0.7]   (ok)         27    7.0%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.249  0.056   0.169    0.347  ...    41.0      42.0      40.0   1.07
pu        0.833  0.018   0.802    0.863  ...    25.0      27.0      45.0   1.06
mu        0.133  0.022   0.091    0.169  ...     6.0       7.0      60.0   1.31
mus       0.202  0.033   0.140    0.250  ...    81.0      83.0      60.0   1.02
gamma     0.263  0.050   0.188    0.354  ...    65.0      46.0      41.0   1.04
Is_begin  0.903  0.815   0.036    2.853  ...   120.0     115.0      77.0   1.00
Ia_begin  2.096  1.840   0.053    5.666  ...   103.0      80.0      61.0   1.02
E_begin   0.822  0.904   0.019    3.361  ...    94.0     124.0      44.0   0.99

[8 rows x 11 columns]