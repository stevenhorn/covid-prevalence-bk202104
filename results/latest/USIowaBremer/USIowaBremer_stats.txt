0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -988.72    35.30
p_loo       35.42        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      351   91.4%
 (0.5, 0.7]   (ok)         27    7.0%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.253  0.053   0.170    0.346  ...    90.0      86.0      60.0   1.08
pu        0.764  0.028   0.715    0.808  ...    48.0      47.0      60.0   1.00
mu        0.151  0.023   0.104    0.191  ...    28.0      22.0      38.0   1.08
mus       0.223  0.028   0.180    0.275  ...   148.0     152.0      86.0   1.01
gamma     0.324  0.050   0.232    0.387  ...   108.0     119.0      87.0   1.00
Is_begin  0.803  0.836   0.007    2.685  ...   152.0     152.0      56.0   0.99
Ia_begin  1.538  1.597   0.013    4.232  ...   109.0     109.0      60.0   1.00
E_begin   0.601  0.578   0.007    1.832  ...   103.0      54.0      56.0   1.04

[8 rows x 11 columns]