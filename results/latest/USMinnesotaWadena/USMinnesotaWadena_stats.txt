0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -797.08    27.32
p_loo       22.42        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      356   92.7%
 (0.5, 0.7]   (ok)         23    6.0%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.261  0.056   0.159    0.342  ...    99.0     108.0      59.0   1.02
pu        0.811  0.022   0.773    0.848  ...    84.0      92.0      48.0   1.00
mu        0.132  0.022   0.098    0.174  ...    84.0      83.0      99.0   1.04
mus       0.167  0.034   0.111    0.223  ...    95.0     152.0      61.0   1.02
gamma     0.183  0.038   0.130    0.256  ...    86.0     115.0      60.0   1.01
Is_begin  0.776  0.647   0.007    1.841  ...   125.0     122.0      59.0   1.01
Ia_begin  1.263  1.395   0.002    4.389  ...    82.0      71.0      56.0   1.04
E_begin   0.551  0.569   0.002    1.479  ...    59.0      56.0      58.0   1.04

[8 rows x 11 columns]