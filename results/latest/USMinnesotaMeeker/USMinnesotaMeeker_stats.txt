0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -880.50    31.81
p_loo       29.21        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      352   91.7%
 (0.5, 0.7]   (ok)         29    7.6%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.235  0.043   0.161    0.318  ...    32.0      31.0      39.0   1.06
pu        0.760  0.021   0.726    0.802  ...    21.0      20.0      60.0   1.09
mu        0.131  0.019   0.099    0.166  ...    42.0      40.0      48.0   1.03
mus       0.207  0.044   0.131    0.283  ...   131.0     137.0      84.0   1.00
gamma     0.264  0.038   0.199    0.341  ...   150.0     132.0      91.0   0.99
Is_begin  0.557  0.496   0.002    1.391  ...    47.0      22.0      40.0   1.07
Ia_begin  1.284  1.189   0.021    3.574  ...   134.0      99.0      97.0   1.00
E_begin   0.523  0.494   0.005    1.507  ...   122.0      74.0     100.0   1.01

[8 rows x 11 columns]