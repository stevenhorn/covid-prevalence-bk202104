0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1043.38    32.35
p_loo       28.46        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      352   91.7%
 (0.5, 0.7]   (ok)         26    6.8%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.231  0.051   0.162    0.330  ...    15.0      11.0      22.0   1.19
pu        0.754  0.026   0.713    0.801  ...    13.0      14.0      18.0   1.15
mu        0.137  0.023   0.093    0.167  ...    15.0      17.0      42.0   1.09
mus       0.193  0.032   0.145    0.250  ...    39.0      40.0      54.0   1.04
gamma     0.253  0.054   0.166    0.348  ...    76.0      72.0      58.0   1.01
Is_begin  0.589  0.548   0.011    1.712  ...    69.0      56.0      54.0   1.02
Ia_begin  0.891  0.769   0.010    2.120  ...    98.0      72.0      60.0   1.00
E_begin   0.430  0.444   0.000    1.262  ...   102.0      91.0      59.0   0.99

[8 rows x 11 columns]