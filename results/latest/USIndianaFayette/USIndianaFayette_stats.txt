0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1031.84    34.22
p_loo       31.26        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      369   96.1%
 (0.5, 0.7]   (ok)         13    3.4%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.251  0.053   0.155    0.333  ...    74.0      76.0      43.0   1.01
pu        0.775  0.023   0.725    0.810  ...    41.0      42.0      93.0   1.03
mu        0.131  0.024   0.087    0.172  ...    31.0      34.0      93.0   1.06
mus       0.180  0.034   0.124    0.247  ...    26.0      26.0      93.0   1.06
gamma     0.213  0.041   0.150    0.303  ...   105.0      96.0      61.0   1.01
Is_begin  1.031  0.906   0.028    2.662  ...   120.0     135.0      91.0   1.04
Ia_begin  1.596  1.483   0.021    5.419  ...    80.0      60.0      60.0   0.99
E_begin   0.798  0.859   0.024    2.773  ...   109.0      59.0      72.0   0.99

[8 rows x 11 columns]