0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -849.58    21.04
p_loo       21.54        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      360   93.8%
 (0.5, 0.7]   (ok)         22    5.7%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.268  0.054   0.166    0.343  ...    72.0      67.0      91.0   1.02
pu        0.817  0.023   0.779    0.859  ...    38.0      34.0      47.0   1.05
mu        0.144  0.029   0.091    0.198  ...    17.0      17.0      40.0   1.12
mus       0.194  0.035   0.138    0.256  ...    90.0      93.0      59.0   1.00
gamma     0.216  0.038   0.152    0.285  ...   152.0     152.0      79.0   1.01
Is_begin  0.878  0.813   0.021    2.479  ...    57.0      40.0      33.0   1.01
Ia_begin  1.222  1.264   0.016    3.533  ...    82.0      50.0      24.0   1.02
E_begin   0.833  0.881   0.007    2.733  ...    73.0      74.0      45.0   1.01

[8 rows x 11 columns]