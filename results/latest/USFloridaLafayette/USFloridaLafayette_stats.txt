1 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -993.63    45.93
p_loo       24.66        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      355   92.4%
 (0.5, 0.7]   (ok)         26    6.8%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.162  0.010   0.151    0.182  ...   106.0     103.0     100.0   1.00
pu        0.704  0.003   0.700    0.711  ...   140.0     133.0      43.0   1.07
mu        0.060  0.009   0.045    0.074  ...   129.0     130.0      31.0   1.05
mus       0.277  0.032   0.226    0.326  ...   152.0     152.0      76.0   0.99
gamma     0.704  0.097   0.523    0.906  ...   117.0     112.0      30.0   1.02
Is_begin  0.413  0.420   0.014    0.951  ...    94.0      64.0      59.0   0.99
Ia_begin  0.486  0.490   0.010    1.467  ...    64.0      40.0      56.0   1.05
E_begin   0.182  0.171   0.001    0.520  ...    55.0      36.0      14.0   0.99

[8 rows x 11 columns]