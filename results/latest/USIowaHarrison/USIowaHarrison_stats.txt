0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -909.50    32.96
p_loo       31.69        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      360   93.8%
 (0.5, 0.7]   (ok)         19    4.9%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.252  0.051   0.167    0.336  ...   116.0     105.0      54.0   1.15
pu        0.764  0.027   0.704    0.803  ...    33.0      32.0      22.0   1.04
mu        0.144  0.030   0.089    0.195  ...    24.0      23.0      42.0   1.08
mus       0.184  0.035   0.131    0.248  ...    80.0      93.0      88.0   1.00
gamma     0.197  0.040   0.130    0.267  ...    84.0      87.0      49.0   1.02
Is_begin  0.717  0.628   0.031    1.797  ...    64.0      24.0      55.0   1.06
Ia_begin  1.686  1.762   0.000    4.993  ...   109.0      55.0      40.0   1.02
E_begin   0.785  1.048   0.002    3.052  ...   107.0      89.0      58.0   0.99

[8 rows x 11 columns]