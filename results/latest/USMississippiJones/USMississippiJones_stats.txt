0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1393.91    29.70
p_loo       27.69        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      365   95.1%
 (0.5, 0.7]   (ok)         18    4.7%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.251  0.053   0.160    0.338  ...    50.0      50.0      60.0   0.99
pu        0.776  0.024   0.726    0.814  ...     8.0       8.0      57.0   1.22
mu        0.117  0.031   0.075    0.189  ...     5.0       5.0      22.0   1.48
mus       0.169  0.032   0.123    0.224  ...    11.0      14.0      42.0   1.13
gamma     0.175  0.030   0.125    0.228  ...    46.0      32.0      37.0   1.04
Is_begin  0.988  0.961   0.001    2.648  ...    45.0      17.0      34.0   1.12
Ia_begin  1.436  2.135   0.025    6.804  ...   105.0      63.0      60.0   1.03
E_begin   0.904  1.118   0.005    3.338  ...   102.0      59.0      75.0   0.99

[8 rows x 11 columns]