0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1114.22    30.50
p_loo       26.11        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      347   90.4%
 (0.5, 0.7]   (ok)         30    7.8%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.240  0.054   0.159    0.329  ...    94.0      92.0      57.0   0.99
pu        0.788  0.023   0.741    0.822  ...    27.0      22.0      40.0   1.08
mu        0.128  0.021   0.092    0.168  ...    34.0      34.0      93.0   0.99
mus       0.177  0.031   0.128    0.238  ...    57.0      60.0     100.0   1.01
gamma     0.185  0.032   0.137    0.241  ...    85.0      83.0      60.0   1.00
Is_begin  0.776  0.633   0.031    1.851  ...    59.0     108.0      52.0   1.03
Ia_begin  1.664  2.019   0.002    5.078  ...   110.0      55.0      58.0   1.04
E_begin   0.892  0.904   0.020    2.520  ...    65.0      76.0      56.0   0.99

[8 rows x 11 columns]