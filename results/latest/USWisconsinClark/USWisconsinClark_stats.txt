0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1087.71    56.85
p_loo       38.22        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      355   92.4%
 (0.5, 0.7]   (ok)         24    6.2%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.233  0.057   0.150    0.319  ...    12.0      10.0      20.0   1.19
pu        0.826  0.018   0.795    0.856  ...    19.0      20.0      53.0   1.10
mu        0.114  0.021   0.081    0.154  ...    37.0      37.0      54.0   1.07
mus       0.159  0.025   0.119    0.205  ...    63.0      63.0      88.0   1.02
gamma     0.175  0.043   0.105    0.250  ...   152.0     152.0      54.0   1.00
Is_begin  0.775  0.664   0.034    2.021  ...    56.0      79.0      60.0   1.01
Ia_begin  1.274  1.142   0.125    3.552  ...   127.0     136.0      59.0   0.98
E_begin   0.781  0.842   0.005    2.254  ...    70.0      77.0      43.0   1.01

[8 rows x 11 columns]