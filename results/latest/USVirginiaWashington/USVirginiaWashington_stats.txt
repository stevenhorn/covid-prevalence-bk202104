0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1156.83    29.45
p_loo       23.95        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      354   92.2%
 (0.5, 0.7]   (ok)         21    5.5%
   (0.7, 1]   (bad)         8    2.1%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.272  0.051   0.164    0.333  ...    20.0      22.0      37.0   1.09
pu        0.766  0.044   0.702    0.828  ...     4.0       6.0      20.0   1.38
mu        0.126  0.025   0.076    0.170  ...     7.0       6.0      17.0   1.32
mus       0.180  0.031   0.129    0.249  ...    95.0      91.0     102.0   1.00
gamma     0.216  0.039   0.154    0.288  ...    70.0      72.0      96.0   1.03
Is_begin  0.736  0.716   0.002    2.086  ...   124.0      76.0      68.0   1.01
Ia_begin  2.010  1.655   0.226    5.445  ...    33.0      16.0      74.0   1.09
E_begin   0.855  0.984   0.016    2.513  ...    51.0      29.0      47.0   1.03

[8 rows x 11 columns]