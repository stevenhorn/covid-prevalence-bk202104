0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1236.13    51.47
p_loo       44.58        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      362   94.3%
 (0.5, 0.7]   (ok)         14    3.6%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.171  0.020   0.150    0.204  ...   152.0      82.0      14.0   1.00
pu        0.707  0.006   0.700    0.720  ...   152.0     144.0      59.0   1.04
mu        0.108  0.013   0.080    0.128  ...    73.0      73.0     102.0   0.99
mus       0.353  0.042   0.271    0.418  ...   151.0     152.0      59.0   0.98
gamma     0.622  0.107   0.423    0.812  ...   152.0     152.0      49.0   1.01
Is_begin  0.829  0.788   0.006    2.220  ...   138.0     113.0     100.0   0.99
Ia_begin  1.110  1.201   0.007    3.891  ...    84.0     102.0      83.0   1.00
E_begin   0.530  0.552   0.001    1.615  ...    40.0      83.0      45.0   1.02

[8 rows x 11 columns]