0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1909.80    64.47
p_loo      136.71        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      338   88.0%
 (0.5, 0.7]   (ok)         23    6.0%
   (0.7, 1]   (bad)        17    4.4%
   (1, Inf)   (very bad)    6    1.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.200  0.037   0.153    0.276  ...     4.0       4.0      20.0   2.30
pu        0.749  0.018   0.707    0.767  ...     5.0       6.0      40.0   1.91
mu        0.169  0.021   0.119    0.190  ...     8.0       9.0      15.0   1.78
mus       0.190  0.016   0.168    0.229  ...    57.0      57.0      53.0   1.62
gamma     0.284  0.043   0.218    0.339  ...     8.0       9.0      57.0   1.22
Is_begin  0.895  0.561   0.019    1.800  ...    27.0      15.0      19.0   1.44
Ia_begin  1.087  1.499   0.011    5.175  ...    12.0      23.0      60.0   1.67
E_begin   0.562  0.709   0.024    1.959  ...    24.0      46.0      38.0   1.60

[8 rows x 11 columns]