0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1565.46    28.29
p_loo       28.11        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      354   92.2%
 (0.5, 0.7]   (ok)         22    5.7%
   (0.7, 1]   (bad)         8    2.1%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.251  0.052   0.163    0.340  ...    58.0      59.0      60.0   0.99
pu        0.804  0.022   0.771    0.845  ...    13.0      13.0      59.0   1.16
mu        0.151  0.023   0.116    0.193  ...    48.0      44.0      56.0   1.02
mus       0.190  0.035   0.120    0.257  ...   152.0     152.0      60.0   1.07
gamma     0.261  0.043   0.206    0.348  ...    56.0      56.0      60.0   1.01
Is_begin  1.723  1.161   0.049    3.674  ...    78.0      65.0      91.0   1.00
Ia_begin  0.842  0.733   0.001    2.270  ...    97.0      67.0      54.0   1.02
E_begin   1.567  1.470   0.008    4.129  ...    81.0      51.0      30.0   0.98

[8 rows x 11 columns]