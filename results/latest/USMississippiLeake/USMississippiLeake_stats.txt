0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1110.20    28.90
p_loo       25.55        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      356   92.7%
 (0.5, 0.7]   (ok)         19    4.9%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.241  0.054   0.165    0.328  ...    94.0      83.0      60.0   1.02
pu        0.775  0.024   0.730    0.810  ...    41.0      40.0      61.0   1.01
mu        0.134  0.023   0.089    0.168  ...     7.0       7.0      14.0   1.27
mus       0.178  0.034   0.118    0.228  ...    64.0      65.0      60.0   1.11
gamma     0.227  0.039   0.163    0.306  ...    67.0      64.0      49.0   1.03
Is_begin  0.955  0.895   0.027    2.259  ...    71.0      65.0      59.0   0.99
Ia_begin  2.002  1.866   0.014    5.075  ...    42.0      35.0      48.0   1.08
E_begin   1.339  1.293   0.022    3.694  ...    72.0      60.0      60.0   0.98

[8 rows x 11 columns]