0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -967.86    26.27
p_loo       23.05        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      357   93.0%
 (0.5, 0.7]   (ok)         21    5.5%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.245  0.053   0.162    0.341  ...    25.0      25.0      40.0   1.06
pu        0.772  0.020   0.734    0.805  ...    45.0      48.0      83.0   1.02
mu        0.128  0.022   0.094    0.165  ...    29.0      23.0      59.0   1.06
mus       0.153  0.034   0.101    0.215  ...    49.0      52.0      60.0   1.04
gamma     0.158  0.026   0.118    0.212  ...    90.0      88.0      91.0   1.03
Is_begin  0.573  0.613   0.002    1.611  ...    50.0      30.0      40.0   1.07
Ia_begin  1.279  1.878   0.018    3.945  ...    50.0      31.0      48.0   1.01
E_begin   0.539  0.667   0.006    1.699  ...    57.0      49.0      26.0   1.03

[8 rows x 11 columns]