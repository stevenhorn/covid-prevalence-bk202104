1 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1656.89    37.69
p_loo       37.12        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      335   87.2%
 (0.5, 0.7]   (ok)         38    9.9%
   (0.7, 1]   (bad)        11    2.9%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.240  0.056   0.154    0.320  ...   152.0     152.0      60.0   1.03
pu        0.739  0.028   0.700    0.783  ...    84.0      80.0      91.0   1.03
mu        0.155  0.023   0.118    0.198  ...    34.0      36.0      43.0   1.04
mus       0.218  0.033   0.170    0.279  ...    81.0      78.0      91.0   1.00
gamma     0.326  0.050   0.222    0.401  ...   110.0     106.0      96.0   1.06
Is_begin  0.888  0.546   0.042    1.789  ...    86.0      73.0      59.0   1.01
Ia_begin  2.003  1.235   0.159    4.079  ...    77.0      76.0      59.0   1.00
E_begin   1.797  1.750   0.015    5.353  ...    59.0      46.0      59.0   1.05

[8 rows x 11 columns]