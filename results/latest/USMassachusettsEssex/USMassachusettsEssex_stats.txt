0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -2154.41    31.39
p_loo       25.46        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      329   85.7%
 (0.5, 0.7]   (ok)         48   12.5%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    0    0.0%

             mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.228   0.048   0.152    0.309  ...    52.0      49.0      60.0   1.00
pu          0.723   0.018   0.701    0.758  ...    80.0      83.0      59.0   0.99
mu          0.111   0.021   0.083    0.154  ...    41.0      38.0      83.0   1.04
mus         0.188   0.036   0.133    0.245  ...    91.0     132.0      60.0   1.01
gamma       0.314   0.059   0.224    0.429  ...   152.0     152.0      51.0   1.00
Is_begin   14.969   8.388   0.922   27.711  ...    29.0      26.0      40.0   1.07
Ia_begin   48.405  13.381  24.543   70.276  ...    66.0      73.0      72.0   1.02
E_begin   133.112  49.689  34.561  218.576  ...    94.0      87.0      33.0   1.00

[8 rows x 11 columns]