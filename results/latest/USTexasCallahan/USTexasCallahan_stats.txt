0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1079.88    59.88
p_loo       44.68        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      366   95.3%
 (0.5, 0.7]   (ok)         10    2.6%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.262  0.054   0.177    0.347  ...    32.0      33.0      59.0   1.01
pu        0.825  0.021   0.794    0.864  ...    26.0      26.0      38.0   1.05
mu        0.151  0.020   0.108    0.182  ...    55.0      55.0      38.0   1.03
mus       0.193  0.043   0.118    0.264  ...    57.0      57.0      58.0   1.05
gamma     0.231  0.038   0.165    0.308  ...    72.0      78.0      48.0   1.03
Is_begin  0.757  0.643   0.018    1.951  ...    35.0      24.0      58.0   1.05
Ia_begin  1.412  1.967   0.063    4.294  ...    29.0      58.0      56.0   1.06
E_begin   0.635  0.670   0.005    1.733  ...    53.0      41.0      54.0   1.04

[8 rows x 11 columns]