0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1907.47    37.60
p_loo       34.48        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      335   87.2%
 (0.5, 0.7]   (ok)         37    9.6%
   (0.7, 1]   (bad)        11    2.9%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.232  0.051   0.152    0.333  ...   152.0     152.0      24.0   1.03
pu        0.735  0.022   0.703    0.774  ...    94.0      60.0      89.0   1.03
mu        0.119  0.023   0.077    0.157  ...    60.0      61.0      40.0   1.02
mus       0.207  0.047   0.131    0.295  ...    73.0     123.0      40.0   1.01
gamma     0.285  0.052   0.200    0.387  ...   113.0     101.0      59.0   1.02
Is_begin  0.872  0.636   0.009    1.919  ...    63.0      45.0      46.0   0.99
Ia_begin  1.783  1.366   0.068    4.418  ...    40.0      19.0      55.0   1.08
E_begin   1.988  2.449   0.002    7.826  ...    91.0      38.0      43.0   1.05

[8 rows x 11 columns]