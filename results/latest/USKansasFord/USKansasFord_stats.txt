0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1599.59    29.48
p_loo       28.36        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      366   95.3%
 (0.5, 0.7]   (ok)         14    3.6%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.225  0.044   0.150    0.307  ...   104.0     103.0      42.0   1.01
pu        0.725  0.017   0.700    0.757  ...   126.0      87.0      24.0   1.03
mu        0.131  0.020   0.105    0.171  ...    25.0      19.0      56.0   1.10
mus       0.172  0.026   0.120    0.215  ...    30.0      41.0      54.0   1.04
gamma     0.231  0.035   0.174    0.293  ...    68.0      65.0      59.0   1.04
Is_begin  0.718  0.594   0.005    1.761  ...   152.0     136.0      72.0   0.99
Ia_begin  1.621  1.644   0.022    5.289  ...    67.0      89.0      60.0   1.00
E_begin   1.070  1.112   0.017    3.520  ...    90.0     118.0      72.0   0.99

[8 rows x 11 columns]