0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -817.93    40.49
p_loo       28.23        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      356   92.7%
 (0.5, 0.7]   (ok)         20    5.2%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.255  0.050   0.153    0.327  ...    69.0      69.0      55.0   1.02
pu        0.817  0.018   0.784    0.841  ...    38.0      42.0      59.0   1.06
mu        0.117  0.016   0.088    0.141  ...    30.0      29.0      40.0   1.12
mus       0.155  0.026   0.120    0.203  ...    80.0      71.0      86.0   1.03
gamma     0.179  0.037   0.118    0.242  ...    97.0     101.0      59.0   1.00
Is_begin  0.838  0.869   0.015    2.496  ...   114.0      94.0      86.0   0.99
Ia_begin  1.760  1.805   0.022    5.050  ...   109.0      80.0      84.0   1.01
E_begin   0.711  0.656   0.016    2.144  ...    72.0      95.0      75.0   1.00

[8 rows x 11 columns]