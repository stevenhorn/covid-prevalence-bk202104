0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -974.70    52.17
p_loo       40.91        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      362   94.3%
 (0.5, 0.7]   (ok)         15    3.9%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    4    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.260  0.053   0.167    0.337  ...    78.0      81.0      59.0   0.98
pu        0.848  0.016   0.818    0.878  ...    36.0      36.0      53.0   1.04
mu        0.140  0.025   0.096    0.187  ...    58.0      63.0      86.0   1.01
mus       0.194  0.033   0.139    0.251  ...    92.0      94.0      58.0   1.02
gamma     0.251  0.042   0.181    0.338  ...   150.0     152.0      59.0   1.01
Is_begin  0.517  0.515   0.009    1.501  ...    98.0     103.0      59.0   1.05
Ia_begin  0.809  0.860   0.005    2.984  ...    59.0      76.0      60.0   1.00
E_begin   0.494  0.680   0.002    1.884  ...    71.0      59.0      60.0   0.99

[8 rows x 11 columns]