0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1459.71    72.83
p_loo       45.73        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      364   94.8%
 (0.5, 0.7]   (ok)         15    3.9%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.179  0.023   0.150    0.223  ...    56.0      38.0      56.0   1.05
pu        0.709  0.009   0.700    0.729  ...   139.0      87.0      59.0   1.02
mu        0.115  0.014   0.088    0.140  ...    61.0      61.0      69.0   1.00
mus       0.239  0.034   0.183    0.298  ...    84.0      93.0      88.0   1.03
gamma     0.480  0.079   0.360    0.649  ...    76.0      79.0      88.0   1.00
Is_begin  0.862  0.813   0.025    2.309  ...   110.0      93.0      59.0   1.00
Ia_begin  1.154  1.224   0.010    3.527  ...    60.0      51.0      22.0   1.03
E_begin   0.498  0.835   0.000    1.622  ...    86.0      56.0      18.0   1.03

[8 rows x 11 columns]