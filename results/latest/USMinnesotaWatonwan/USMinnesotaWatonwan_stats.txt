0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -955.45    37.07
p_loo       33.77        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      361   94.0%
 (0.5, 0.7]   (ok)         18    4.7%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.254  0.052   0.168    0.346  ...   124.0     119.0      60.0   1.05
pu        0.764  0.031   0.712    0.822  ...    20.0      22.0      55.0   1.08
mu        0.140  0.026   0.095    0.178  ...     7.0       8.0      42.0   1.26
mus       0.202  0.037   0.132    0.266  ...    90.0      95.0      60.0   1.00
gamma     0.271  0.051   0.189    0.345  ...    68.0      73.0      87.0   1.00
Is_begin  0.646  0.701   0.019    2.059  ...    88.0      73.0      49.0   1.02
Ia_begin  1.067  1.242   0.011    3.751  ...    62.0     109.0      60.0   1.02
E_begin   0.481  0.628   0.009    1.616  ...    72.0      63.0      59.0   1.01

[8 rows x 11 columns]