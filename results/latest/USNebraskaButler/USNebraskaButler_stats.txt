0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -871.15    49.29
p_loo       42.62        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      364   94.8%
 (0.5, 0.7]   (ok)         15    3.9%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.255  0.051   0.180    0.341  ...    52.0      93.0      59.0   1.02
pu        0.753  0.024   0.713    0.800  ...    84.0      85.0      96.0   1.04
mu        0.147  0.034   0.102    0.220  ...   104.0     106.0      63.0   1.00
mus       0.202  0.042   0.142    0.284  ...    29.0      29.0      79.0   1.07
gamma     0.240  0.049   0.152    0.313  ...   152.0     152.0      60.0   1.00
Is_begin  0.492  0.457   0.001    1.377  ...   124.0      92.0      95.0   1.00
Ia_begin  0.879  0.820   0.005    2.147  ...   104.0     120.0     100.0   1.01
E_begin   0.524  0.473   0.030    1.374  ...   111.0     121.0      88.0   0.99

[8 rows x 11 columns]