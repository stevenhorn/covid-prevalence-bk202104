6 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1537.10    28.25
p_loo       26.05        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      363   94.5%
 (0.5, 0.7]   (ok)         19    4.9%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.256  0.054   0.159    0.334  ...    62.0      60.0      48.0   1.07
pu        0.809  0.051   0.713    0.893  ...    24.0      29.0      59.0   1.06
mu        0.119  0.016   0.088    0.146  ...    26.0      26.0      40.0   1.09
mus       0.172  0.028   0.132    0.219  ...    81.0      94.0      60.0   0.98
gamma     0.209  0.035   0.140    0.274  ...    56.0      65.0      59.0   1.02
Is_begin  1.313  1.325   0.019    3.790  ...    57.0      46.0      43.0   1.02
Ia_begin  4.436  2.471   0.160    8.621  ...    94.0      84.0      35.0   1.03
E_begin   3.224  3.007   0.058    8.773  ...   116.0      68.0      61.0   1.04

[8 rows x 11 columns]