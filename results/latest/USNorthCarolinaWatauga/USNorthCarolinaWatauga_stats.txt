0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1277.07    38.51
p_loo       30.47        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      359   93.5%
 (0.5, 0.7]   (ok)         19    4.9%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.257  0.052   0.172    0.337  ...    58.0      55.0      40.0   1.01
pu        0.833  0.018   0.802    0.863  ...    14.0      18.0      60.0   1.09
mu        0.128  0.028   0.069    0.163  ...     4.0       5.0      34.0   1.50
mus       0.177  0.033   0.128    0.237  ...    59.0      63.0      74.0   1.04
gamma     0.199  0.037   0.144    0.285  ...   135.0     140.0      93.0   1.01
Is_begin  0.776  0.678   0.024    2.360  ...   112.0      91.0      88.0   0.99
Ia_begin  0.601  0.466   0.032    1.371  ...   137.0      87.0      60.0   1.00
E_begin   0.455  0.439   0.015    1.349  ...    93.0      67.0      86.0   1.03

[8 rows x 11 columns]