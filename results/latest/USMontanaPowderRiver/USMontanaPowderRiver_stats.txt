0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -384.47    38.94
p_loo       33.87        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      329   85.7%
 (0.5, 0.7]   (ok)         45   11.7%
   (0.7, 1]   (bad)         8    2.1%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.255  0.052   0.180    0.350  ...    56.0      52.0      58.0   1.00
pu        0.850  0.016   0.821    0.875  ...    57.0      59.0      58.0   1.06
mu        0.133  0.025   0.091    0.181  ...    10.0      10.0      59.0   1.17
mus       0.179  0.030   0.134    0.244  ...    56.0      53.0      57.0   1.04
gamma     0.204  0.037   0.143    0.268  ...    81.0      82.0      86.0   0.99
Is_begin  0.537  0.556   0.017    1.584  ...    80.0      24.0      95.0   1.08
Ia_begin  1.172  1.328   0.014    3.576  ...    52.0      69.0      22.0   1.03
E_begin   0.533  0.721   0.000    1.398  ...    80.0      26.0      19.0   1.07

[8 rows x 11 columns]