0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1303.68    44.07
p_loo       33.18        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      354   92.2%
 (0.5, 0.7]   (ok)         25    6.5%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.248  0.054   0.153    0.330  ...    81.0      72.0      58.0   1.00
pu        0.778  0.032   0.717    0.829  ...    52.0      52.0      60.0   1.01
mu        0.139  0.031   0.090    0.192  ...    20.0      20.0      93.0   1.06
mus       0.200  0.041   0.127    0.265  ...   112.0     133.0      77.0   1.00
gamma     0.272  0.047   0.170    0.356  ...   123.0     115.0      93.0   0.99
Is_begin  0.702  0.665   0.012    2.210  ...   143.0     152.0      59.0   0.98
Ia_begin  1.236  1.049   0.013    3.252  ...    90.0      76.0      57.0   1.03
E_begin   0.646  0.724   0.010    2.149  ...   106.0      62.0      92.0   1.03

[8 rows x 11 columns]