27 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -2057.12    33.13
p_loo       34.26        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      343   89.3%
 (0.5, 0.7]   (ok)         30    7.8%
   (0.7, 1]   (bad)        10    2.6%
   (1, Inf)   (very bad)    1    0.3%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.220   0.045   0.156    0.312  ...    59.0      62.0      45.0   1.00
pu         0.739   0.028   0.705    0.786  ...    31.0      32.0      59.0   1.03
mu         0.100   0.015   0.075    0.127  ...    13.0      13.0      38.0   1.13
mus        0.215   0.039   0.151    0.279  ...    20.0      31.0      17.0   1.03
gamma      0.353   0.053   0.284    0.486  ...    46.0      47.0      40.0   1.06
Is_begin  17.310   9.645   0.274   35.615  ...    36.0      24.0      31.0   1.04
Ia_begin  49.604  18.271  11.681   80.339  ...    62.0      66.0      19.0   1.05
E_begin   98.105  43.945  44.025  183.022  ...    60.0      86.0      53.0   1.06

[8 rows x 11 columns]