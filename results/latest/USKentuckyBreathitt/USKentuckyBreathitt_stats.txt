0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -880.06    42.30
p_loo       31.97        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      356   92.7%
 (0.5, 0.7]   (ok)         25    6.5%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.270  0.045   0.201    0.345  ...    86.0      81.0      40.0   1.03
pu        0.855  0.015   0.835    0.890  ...    95.0      92.0      32.0   1.01
mu        0.129  0.026   0.086    0.176  ...    16.0      13.0      33.0   1.14
mus       0.169  0.034   0.115    0.221  ...   133.0     134.0      81.0   1.02
gamma     0.188  0.041   0.110    0.247  ...   149.0     152.0      75.0   1.01
Is_begin  0.761  0.707   0.008    1.872  ...   113.0     144.0      40.0   1.02
Ia_begin  1.384  1.382   0.001    3.814  ...    82.0      41.0      22.0   1.06
E_begin   0.727  0.793   0.014    2.392  ...   101.0      80.0      93.0   1.01

[8 rows x 11 columns]