0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -869.55    24.56
p_loo       24.05        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      357   93.0%
 (0.5, 0.7]   (ok)         17    4.4%
   (0.7, 1]   (bad)        10    2.6%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.267  0.047   0.192    0.346  ...   142.0     143.0      93.0   1.09
pu        0.886  0.010   0.866    0.900  ...    18.0      23.0      38.0   1.07
mu        0.150  0.023   0.116    0.196  ...    23.0      23.0      36.0   1.10
mus       0.206  0.039   0.130    0.273  ...   115.0     152.0      58.0   1.12
gamma     0.255  0.044   0.174    0.336  ...   152.0     152.0      81.0   1.00
Is_begin  1.183  0.987   0.008    2.959  ...   107.0      92.0      58.0   1.00
Ia_begin  0.772  0.576   0.092    1.935  ...   121.0      94.0      93.0   0.99
E_begin   0.657  0.686   0.019    2.153  ...    78.0     102.0      72.0   1.01

[8 rows x 11 columns]