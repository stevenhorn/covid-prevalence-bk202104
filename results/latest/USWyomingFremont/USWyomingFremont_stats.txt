0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1317.63    38.03
p_loo       28.71        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      369   96.1%
 (0.5, 0.7]   (ok)         12    3.1%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.240  0.054   0.151    0.321  ...    85.0      76.0      56.0   1.03
pu        0.765  0.023   0.720    0.797  ...    27.0      24.0      59.0   1.06
mu        0.139  0.027   0.101    0.194  ...    21.0      19.0      45.0   1.06
mus       0.188  0.036   0.130    0.253  ...    36.0      39.0      55.0   1.05
gamma     0.217  0.040   0.161    0.282  ...    79.0      82.0      88.0   1.06
Is_begin  1.115  0.916   0.036    3.029  ...    79.0      59.0      58.0   1.01
Ia_begin  2.311  2.030   0.010    6.712  ...    47.0      50.0      18.0   1.03
E_begin   1.281  1.542   0.014    5.272  ...    45.0      30.0      96.0   1.06

[8 rows x 11 columns]