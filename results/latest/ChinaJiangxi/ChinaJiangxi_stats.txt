0 Divergences 
Passed validation 
Computed from 80 by 385 log-likelihood matrix

         Estimate       SE
elpd_loo   176.98    85.47
p_loo       79.59        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      365   94.8%
 (0.5, 0.7]   (ok)         14    3.6%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    4    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.252  0.053   0.165    0.333  ...   152.0     152.0      93.0   0.98
pu        0.796  0.031   0.742    0.852  ...    90.0      94.0      93.0   1.00
mu        0.151  0.029   0.107    0.206  ...    29.0      26.0      58.0   1.06
mus       0.270  0.044   0.194    0.355  ...    49.0     124.0      60.0   1.04
gamma     0.292  0.046   0.215    0.389  ...   116.0     116.0      88.0   0.99
Is_begin  0.821  0.679   0.011    1.896  ...   152.0      99.0      53.0   1.01
Ia_begin  1.637  1.411   0.077    4.608  ...    89.0      93.0      69.0   1.00
E_begin   0.568  0.551   0.003    1.721  ...    93.0      68.0      60.0   1.00

[8 rows x 11 columns]