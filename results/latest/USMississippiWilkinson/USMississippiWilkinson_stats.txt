0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -757.48    35.32
p_loo       35.39        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      348   90.6%
 (0.5, 0.7]   (ok)         26    6.8%
   (0.7, 1]   (bad)         8    2.1%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.256  0.063   0.160    0.350  ...    91.0      88.0      57.0   1.06
pu        0.852  0.022   0.818    0.892  ...    13.0      14.0      57.0   1.11
mu        0.152  0.025   0.111    0.200  ...    61.0      69.0      38.0   0.99
mus       0.189  0.037   0.129    0.258  ...    69.0      73.0      88.0   1.03
gamma     0.255  0.040   0.180    0.330  ...    62.0      60.0      93.0   1.01
Is_begin  1.468  1.004   0.057    3.010  ...   121.0     105.0      74.0   1.01
Ia_begin  4.232  3.062   0.009    9.402  ...    94.0      66.0      86.0   1.02
E_begin   2.821  2.691   0.004    7.701  ...    69.0      21.0      37.0   1.09

[8 rows x 11 columns]