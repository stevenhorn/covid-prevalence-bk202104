0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1621.27    41.91
p_loo       28.30        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      358   93.2%
 (0.5, 0.7]   (ok)         21    5.5%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.249  0.047   0.164    0.329  ...    17.0      16.0      30.0   1.10
pu        0.745  0.023   0.708    0.785  ...    39.0      37.0      88.0   1.09
mu        0.114  0.021   0.082    0.152  ...    51.0      53.0      74.0   0.99
mus       0.161  0.034   0.103    0.230  ...    79.0     106.0      83.0   1.04
gamma     0.170  0.037   0.109    0.237  ...    99.0     107.0      53.0   1.00
Is_begin  1.055  0.803   0.062    2.648  ...    51.0      61.0      84.0   1.02
Ia_begin  0.567  0.571   0.016    1.238  ...   109.0      59.0      40.0   1.02
E_begin   0.627  0.666   0.006    2.148  ...   123.0      77.0      40.0   1.01

[8 rows x 11 columns]