0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -913.00    42.31
p_loo       36.48        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      341   88.8%
 (0.5, 0.7]   (ok)         34    8.9%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.249  0.054   0.162    0.338  ...   152.0     152.0      68.0   1.00
pu        0.778  0.038   0.710    0.843  ...    11.0      11.0      88.0   1.17
mu        0.177  0.031   0.123    0.223  ...    35.0      38.0      60.0   1.02
mus       0.224  0.043   0.151    0.299  ...   149.0     139.0      77.0   1.01
gamma     0.287  0.053   0.195    0.370  ...    65.0      73.0      97.0   1.03
Is_begin  0.816  0.847   0.004    2.715  ...   128.0     117.0      87.0   0.99
Ia_begin  1.552  1.504   0.008    3.789  ...   116.0      81.0      38.0   1.00
E_begin   0.648  0.732   0.001    2.221  ...    74.0      88.0      93.0   1.00

[8 rows x 11 columns]