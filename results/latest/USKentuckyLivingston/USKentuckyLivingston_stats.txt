0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -740.48    26.44
p_loo       27.19        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      352   91.7%
 (0.5, 0.7]   (ok)         29    7.6%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.248  0.059   0.153    0.335  ...   152.0     152.0      23.0   1.04
pu        0.851  0.019   0.815    0.885  ...    31.0      33.0      54.0   1.02
mu        0.119  0.021   0.081    0.153  ...    51.0      49.0      75.0   1.05
mus       0.166  0.030   0.114    0.217  ...   152.0     152.0      60.0   0.98
gamma     0.199  0.040   0.137    0.275  ...    75.0      68.0      60.0   1.04
Is_begin  0.587  0.663   0.002    1.826  ...   108.0      49.0      95.0   1.08
Ia_begin  1.237  1.131   0.015    3.269  ...    78.0      91.0      59.0   1.01
E_begin   0.512  0.462   0.029    1.574  ...    38.0      61.0      97.0   1.10

[8 rows x 11 columns]