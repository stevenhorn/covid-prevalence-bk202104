0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1715.04    37.81
p_loo       25.60        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      359   93.5%
 (0.5, 0.7]   (ok)         20    5.2%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.255  0.053   0.163    0.344  ...   152.0     102.0      40.0   1.05
pu        0.801  0.039   0.714    0.849  ...    14.0      24.0      59.0   1.11
mu        0.123  0.022   0.089    0.161  ...    29.0      29.0      60.0   1.04
mus       0.179  0.027   0.126    0.229  ...   107.0     118.0      86.0   1.00
gamma     0.235  0.039   0.178    0.328  ...   152.0     152.0      88.0   1.07
Is_begin  1.367  0.985   0.015    3.111  ...    67.0      57.0      38.0   1.02
Ia_begin  0.789  0.465   0.006    1.492  ...    74.0      59.0      53.0   1.00
E_begin   0.910  0.854   0.092    2.628  ...    61.0      71.0      58.0   1.02

[8 rows x 11 columns]