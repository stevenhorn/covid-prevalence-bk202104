0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1458.93    31.94
p_loo       71.97        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      354   92.2%
 (0.5, 0.7]   (ok)         19    4.9%
   (0.7, 1]   (bad)         8    2.1%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.269  0.051   0.170    0.337  ...    10.0      13.0      43.0   1.14
pu        0.768  0.027   0.711    0.806  ...     8.0       8.0      42.0   1.30
mu        0.218  0.102   0.094    0.325  ...     3.0       3.0      30.0   1.90
mus       0.183  0.026   0.138    0.220  ...     4.0       5.0      46.0   1.51
gamma     0.134  0.077   0.058    0.259  ...     3.0       3.0      24.0   2.07
Is_begin  0.980  0.789   0.026    2.369  ...    20.0      21.0      38.0   1.09
Ia_begin  1.715  1.384   0.039    4.455  ...    20.0      22.0      40.0   1.28
E_begin   1.235  1.135   0.039    3.343  ...     9.0       6.0      58.0   1.34

[8 rows x 11 columns]