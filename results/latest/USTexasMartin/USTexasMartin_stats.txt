0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -906.99    63.63
p_loo       42.19        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      361   94.0%
 (0.5, 0.7]   (ok)         17    4.4%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.243  0.048   0.166    0.333  ...   105.0     126.0      60.0   1.07
pu        0.738  0.022   0.704    0.775  ...   152.0     148.0      60.0   1.00
mu        0.138  0.021   0.100    0.171  ...    92.0      88.0      60.0   0.99
mus       0.192  0.039   0.120    0.260  ...    58.0      73.0      26.0   1.05
gamma     0.224  0.047   0.129    0.289  ...    43.0      70.0      19.0   1.03
Is_begin  0.760  0.596   0.010    1.758  ...    49.0      28.0      97.0   1.06
Ia_begin  1.647  1.570   0.023    4.530  ...    54.0      49.0      48.0   1.04
E_begin   0.635  0.635   0.003    1.746  ...    60.0      55.0      60.0   1.03

[8 rows x 11 columns]