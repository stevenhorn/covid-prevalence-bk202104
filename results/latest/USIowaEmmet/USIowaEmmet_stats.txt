0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -896.32    34.34
p_loo       34.14        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      356   92.7%
 (0.5, 0.7]   (ok)         19    4.9%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    5    1.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.191  0.030   0.152    0.239  ...   120.0     152.0      60.0   1.00
pu        0.714  0.012   0.700    0.738  ...   152.0     152.0      96.0   0.99
mu        0.129  0.028   0.077    0.173  ...    40.0      54.0      81.0   1.07
mus       0.175  0.033   0.117    0.244  ...    55.0      74.0      34.0   1.04
gamma     0.232  0.043   0.167    0.314  ...   152.0     152.0      81.0   1.04
Is_begin  0.224  0.286   0.004    0.670  ...    38.0      43.0      42.0   1.02
Ia_begin  0.410  0.695   0.002    1.463  ...    92.0      59.0      38.0   1.02
E_begin   0.164  0.205   0.001    0.655  ...    82.0      62.0      88.0   0.99

[8 rows x 11 columns]