0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1822.65    36.56
p_loo       26.40        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      352   91.7%
 (0.5, 0.7]   (ok)         26    6.8%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.248  0.047   0.171    0.330  ...   126.0     126.0      44.0   1.02
pu        0.792  0.033   0.726    0.839  ...    69.0      75.0      91.0   1.01
mu        0.117  0.023   0.080    0.169  ...    24.0      51.0      59.0   1.14
mus       0.188  0.036   0.126    0.248  ...    97.0      91.0      72.0   1.03
gamma     0.237  0.038   0.176    0.310  ...    90.0     107.0      59.0   0.99
Is_begin  1.882  1.468   0.102    4.825  ...   152.0     125.0      59.0   1.06
Ia_begin  3.250  2.655   0.171    8.743  ...   119.0     100.0      54.0   0.99
E_begin   2.379  2.339   0.013    6.205  ...    71.0      71.0      59.0   1.03

[8 rows x 11 columns]