10 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1658.38    41.44
p_loo       31.07        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      339   88.3%
 (0.5, 0.7]   (ok)         32    8.3%
   (0.7, 1]   (bad)        12    3.1%
   (1, Inf)   (very bad)    1    0.3%

            mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.249  0.058   0.159    0.347  ...    21.0      21.0      48.0   1.17
pu         0.758  0.036   0.701    0.830  ...    40.0      44.0      46.0   1.01
mu         0.118  0.024   0.079    0.166  ...    11.0      10.0      40.0   1.16
mus        0.169  0.037   0.114    0.228  ...    35.0      52.0      73.0   1.05
gamma      0.227  0.044   0.144    0.305  ...    76.0      89.0     100.0   1.01
Is_begin   4.749  3.530   0.261   10.678  ...    82.0      63.0      60.0   1.03
Ia_begin  14.557  8.242   1.400   29.932  ...    64.0      53.0      60.0   1.06
E_begin   12.947  9.637   0.644   28.289  ...    58.0      51.0      88.0   1.00

[8 rows x 11 columns]