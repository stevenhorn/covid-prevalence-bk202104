40 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -2241.77    41.58
p_loo       26.14        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      296   77.1%
 (0.5, 0.7]   (ok)         59   15.4%
   (0.7, 1]   (bad)        28    7.3%
   (1, Inf)   (very bad)    1    0.3%

             mean       sd   hdi_3%  ...  ess_bulk  ess_tail  r_hat
pa          0.212    0.037    0.152  ...      16.0      22.0   1.68
pu          0.741    0.019    0.708  ...       7.0      30.0   1.75
mu          0.116    0.015    0.081  ...       6.0      25.0   1.74
mus         0.184    0.023    0.136  ...      63.0      32.0   1.97
gamma       0.326    0.026    0.274  ...      18.0      52.0   1.56
Is_begin  146.269   83.082    3.792  ...      29.0      25.0   1.72
Ia_begin  456.199  189.732  288.213  ...       4.0      30.0   2.15
E_begin   785.946  332.063  107.590  ...      49.0      27.0   1.53

[8 rows x 11 columns]