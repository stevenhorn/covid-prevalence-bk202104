0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1451.57    27.23
p_loo       26.32        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      355   92.4%
 (0.5, 0.7]   (ok)         24    6.2%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.241  0.058   0.150    0.331  ...   113.0      93.0      39.0   1.02
pu        0.757  0.029   0.706    0.799  ...    10.0      12.0      86.0   1.16
mu        0.106  0.019   0.066    0.140  ...    43.0      42.0      42.0   1.07
mus       0.172  0.030   0.108    0.215  ...   135.0     149.0     100.0   1.00
gamma     0.226  0.043   0.152    0.292  ...    93.0      91.0      60.0   1.01
Is_begin  1.338  0.962   0.021    2.956  ...   108.0     111.0     100.0   0.99
Ia_begin  3.306  2.552   0.131    7.756  ...   126.0      91.0      88.0   1.00
E_begin   2.139  2.344   0.013    5.762  ...    92.0      72.0      40.0   1.01

[8 rows x 11 columns]