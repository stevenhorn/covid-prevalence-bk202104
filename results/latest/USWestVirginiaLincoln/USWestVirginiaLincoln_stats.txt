0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -849.51    26.58
p_loo       25.96        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      360   93.8%
 (0.5, 0.7]   (ok)         23    6.0%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.253  0.051   0.160    0.328  ...   102.0     117.0      97.0   1.00
pu        0.835  0.027   0.793    0.878  ...    58.0      57.0      59.0   1.03
mu        0.131  0.026   0.096    0.180  ...    46.0      48.0      60.0   1.00
mus       0.178  0.028   0.123    0.232  ...    75.0      79.0      56.0   1.02
gamma     0.235  0.046   0.141    0.303  ...   152.0     152.0     100.0   0.98
Is_begin  0.567  0.673   0.006    1.971  ...    96.0      76.0      84.0   0.99
Ia_begin  1.046  1.035   0.018    2.830  ...    97.0      63.0      93.0   1.01
E_begin   0.558  0.733   0.002    2.466  ...    59.0      41.0      67.0   1.01

[8 rows x 11 columns]