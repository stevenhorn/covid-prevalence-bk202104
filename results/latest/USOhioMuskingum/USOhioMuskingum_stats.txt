0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1283.07    34.93
p_loo       24.01        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      362   94.3%
 (0.5, 0.7]   (ok)         18    4.7%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.262  0.045   0.193    0.341  ...    62.0      64.0      93.0   0.99
pu        0.788  0.020   0.758    0.825  ...    61.0      64.0      65.0   1.03
mu        0.130  0.023   0.094    0.178  ...    31.0      31.0     100.0   1.05
mus       0.165  0.040   0.102    0.254  ...   105.0     152.0      28.0   0.99
gamma     0.168  0.028   0.120    0.224  ...    95.0      91.0     100.0   0.99
Is_begin  0.731  0.677   0.016    2.038  ...    94.0      77.0      91.0   1.00
Ia_begin  1.615  1.545   0.045    4.469  ...    99.0     152.0      96.0   1.01
E_begin   0.645  0.618   0.013    1.706  ...    97.0     132.0      63.0   1.00

[8 rows x 11 columns]