0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -976.53    30.51
p_loo       29.37        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      351   91.4%
 (0.5, 0.7]   (ok)         23    6.0%
   (0.7, 1]   (bad)        10    2.6%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.274  0.050   0.185    0.346  ...    33.0      33.0      48.0   1.03
pu        0.884  0.015   0.862    0.900  ...    15.0      15.0      58.0   1.12
mu        0.152  0.026   0.109    0.196  ...    10.0      10.0      56.0   1.20
mus       0.179  0.034   0.120    0.247  ...   152.0     146.0      91.0   0.98
gamma     0.217  0.042   0.137    0.287  ...    97.0     120.0      79.0   1.01
Is_begin  0.918  0.903   0.003    2.793  ...    68.0      57.0      43.0   1.05
Ia_begin  0.633  0.597   0.007    1.895  ...    52.0      40.0      59.0   1.03
E_begin   0.597  0.769   0.000    1.868  ...    43.0      14.0      24.0   1.14

[8 rows x 11 columns]