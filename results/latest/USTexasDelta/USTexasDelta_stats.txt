0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -659.60    69.51
p_loo       46.97        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      364   94.8%
 (0.5, 0.7]   (ok)         14    3.6%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.261  0.045   0.182    0.340  ...    97.0     102.0      62.0   1.02
pu        0.882  0.012   0.863    0.900  ...    24.0      23.0      60.0   1.06
mu        0.121  0.019   0.089    0.154  ...    19.0      16.0      59.0   1.11
mus       0.192  0.034   0.131    0.252  ...    86.0      94.0      46.0   1.01
gamma     0.259  0.041   0.188    0.328  ...    98.0     114.0      48.0   0.98
Is_begin  0.609  0.589   0.038    1.622  ...   135.0     152.0      84.0   0.99
Ia_begin  1.331  1.526   0.008    4.373  ...    81.0     152.0      88.0   1.09
E_begin   0.619  0.632   0.010    2.043  ...   136.0     111.0      60.0   0.98

[8 rows x 11 columns]