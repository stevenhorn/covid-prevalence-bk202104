0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1944.12    34.14
p_loo       24.44        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      354   92.2%
 (0.5, 0.7]   (ok)         23    6.0%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.248  0.060   0.150    0.341  ...    30.0      18.0      20.0   1.08
pu        0.788  0.035   0.722    0.840  ...    37.0      47.0      40.0   1.02
mu        0.120  0.020   0.092    0.165  ...    55.0      53.0      59.0   1.02
mus       0.169  0.029   0.103    0.214  ...    88.0      77.0      72.0   1.01
gamma     0.202  0.041   0.118    0.277  ...   152.0     152.0      91.0   1.00
Is_begin  1.319  1.075   0.077    3.430  ...    62.0      44.0      60.0   1.04
Ia_begin  0.615  0.495   0.007    1.605  ...    49.0      32.0      48.0   1.03
E_begin   0.859  1.047   0.005    2.589  ...   118.0      75.0      55.0   1.00

[8 rows x 11 columns]