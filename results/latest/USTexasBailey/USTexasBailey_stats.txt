0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -915.19    44.22
p_loo       35.05        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      364   94.8%
 (0.5, 0.7]   (ok)         15    3.9%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.237  0.042   0.167    0.302  ...    55.0      65.0      60.0   1.02
pu        0.737  0.020   0.705    0.776  ...    94.0      88.0      40.0   1.03
mu        0.122  0.016   0.096    0.155  ...    37.0      40.0      59.0   1.11
mus       0.182  0.028   0.147    0.256  ...    58.0      75.0      93.0   1.01
gamma     0.244  0.043   0.183    0.336  ...    51.0      53.0      59.0   1.01
Is_begin  0.306  0.393   0.000    1.077  ...    67.0      27.0      57.0   1.06
Ia_begin  0.411  0.409   0.011    1.175  ...    30.0      26.0      24.0   1.04
E_begin   0.200  0.241   0.001    0.619  ...    56.0      61.0      88.0   1.02

[8 rows x 11 columns]