0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -2798.54    40.03
p_loo       24.74        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      343   89.3%
 (0.5, 0.7]   (ok)         33    8.6%
   (0.7, 1]   (bad)         8    2.1%
   (1, Inf)   (very bad)    0    0.0%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.250   0.048   0.169    0.330  ...    94.0      96.0      93.0   1.00
pu         0.776   0.032   0.714    0.821  ...    30.0      31.0      60.0   1.06
mu         0.106   0.019   0.082    0.149  ...    12.0      11.0      33.0   1.15
mus        0.173   0.041   0.107    0.252  ...    88.0     104.0      25.0   0.99
gamma      0.205   0.035   0.148    0.262  ...    25.0      22.0      56.0   1.07
Is_begin  14.331  10.357   1.254   28.703  ...    64.0      58.0      78.0   1.07
Ia_begin  41.728  28.031   4.968   84.497  ...    68.0      71.0      40.0   1.02
E_begin   29.926  23.516   1.079   63.510  ...    95.0      64.0      57.0   1.03

[8 rows x 11 columns]