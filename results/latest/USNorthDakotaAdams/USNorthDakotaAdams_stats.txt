0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -465.55    29.06
p_loo       30.72        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      367   95.6%
 (0.5, 0.7]   (ok)          9    2.3%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.249  0.048   0.170    0.336  ...    24.0      23.0      96.0   1.07
pu        0.755  0.026   0.706    0.796  ...     6.0       6.0      22.0   1.35
mu        0.157  0.025   0.103    0.202  ...    18.0      19.0      89.0   1.09
mus       0.212  0.035   0.140    0.269  ...    79.0      75.0      88.0   1.04
gamma     0.273  0.045   0.215    0.370  ...   152.0     152.0      49.0   1.02
Is_begin  0.361  0.472   0.003    1.300  ...    79.0      67.0      40.0   0.99
Ia_begin  0.538  0.762   0.007    2.027  ...    67.0      58.0      59.0   1.01
E_begin   0.248  0.304   0.005    0.740  ...    87.0      72.0      54.0   0.99

[8 rows x 11 columns]