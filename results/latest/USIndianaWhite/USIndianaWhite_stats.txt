0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1019.12    28.19
p_loo       29.89        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      353   91.9%
 (0.5, 0.7]   (ok)         20    5.2%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    4    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.245  0.054   0.165    0.349  ...    61.0      61.0      38.0   1.11
pu        0.762  0.027   0.707    0.804  ...     9.0      10.0      40.0   1.18
mu        0.130  0.024   0.089    0.170  ...    13.0      11.0      59.0   1.16
mus       0.182  0.031   0.126    0.235  ...   152.0     152.0      49.0   1.00
gamma     0.227  0.041   0.161    0.318  ...    79.0     114.0      35.0   0.99
Is_begin  0.474  0.437   0.012    1.247  ...    94.0      91.0      80.0   1.00
Ia_begin  0.780  0.957   0.001    2.850  ...   112.0      61.0      69.0   1.02
E_begin   0.399  0.428   0.021    1.135  ...    64.0      53.0      93.0   1.02

[8 rows x 11 columns]