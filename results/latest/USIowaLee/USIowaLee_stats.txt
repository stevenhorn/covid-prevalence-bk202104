0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1202.96    60.30
p_loo       42.15        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      363   94.5%
 (0.5, 0.7]   (ok)         15    3.9%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.211  0.042   0.156    0.291  ...    73.0      82.0      60.0   0.98
pu        0.723  0.015   0.701    0.747  ...   112.0     102.0      60.0   1.00
mu        0.119  0.025   0.080    0.160  ...    19.0      11.0      58.0   1.16
mus       0.206  0.034   0.151    0.268  ...   120.0     141.0      44.0   1.03
gamma     0.268  0.052   0.194    0.406  ...   152.0     152.0      59.0   0.98
Is_begin  0.724  0.707   0.036    1.959  ...    55.0      50.0      46.0   1.02
Ia_begin  1.403  1.401   0.016    3.920  ...    67.0      94.0      38.0   1.00
E_begin   0.584  0.491   0.008    1.733  ...    77.0      73.0      93.0   1.02

[8 rows x 11 columns]