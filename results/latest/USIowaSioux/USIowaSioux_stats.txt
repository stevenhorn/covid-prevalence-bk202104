0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1255.77    38.34
p_loo       31.60        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      354   92.2%
 (0.5, 0.7]   (ok)         26    6.8%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.218  0.046   0.152    0.293  ...    66.0      61.0      60.0   1.04
pu        0.722  0.014   0.702    0.744  ...    97.0      88.0      96.0   0.99
mu        0.122  0.025   0.085    0.181  ...    29.0      30.0      59.0   1.08
mus       0.174  0.031   0.128    0.237  ...    42.0      59.0      46.0   1.03
gamma     0.209  0.042   0.148    0.292  ...    96.0      90.0      60.0   1.06
Is_begin  0.420  0.476   0.001    1.416  ...    93.0     133.0      31.0   1.00
Ia_begin  0.583  0.585   0.006    1.631  ...    90.0      86.0      54.0   1.02
E_begin   0.292  0.286   0.012    0.910  ...    64.0      19.0      48.0   1.08

[8 rows x 11 columns]