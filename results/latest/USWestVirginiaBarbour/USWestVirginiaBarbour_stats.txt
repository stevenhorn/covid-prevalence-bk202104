0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1017.79    68.66
p_loo       43.06        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      360   93.8%
 (0.5, 0.7]   (ok)         19    4.9%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.241  0.049   0.165    0.328  ...    77.0      77.0      96.0   1.01
pu        0.763  0.030   0.705    0.810  ...    37.0      37.0      44.0   1.05
mu        0.131  0.023   0.083    0.169  ...    20.0      20.0      23.0   1.12
mus       0.288  0.052   0.209    0.373  ...    53.0      80.0      38.0   1.00
gamma     0.411  0.071   0.306    0.556  ...    50.0      64.0      60.0   1.07
Is_begin  0.876  0.822   0.012    2.689  ...    70.0      65.0      60.0   1.01
Ia_begin  1.622  1.836   0.009    4.449  ...    96.0      85.0      46.0   1.01
E_begin   0.799  0.902   0.015    2.484  ...    85.0     141.0      93.0   1.00

[8 rows x 11 columns]