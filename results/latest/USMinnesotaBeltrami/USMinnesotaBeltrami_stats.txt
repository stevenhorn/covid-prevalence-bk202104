0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1053.84    30.85
p_loo       27.68        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      358   93.2%
 (0.5, 0.7]   (ok)         22    5.7%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.260  0.057   0.164    0.348  ...    98.0     136.0      49.0   1.02
pu        0.804  0.028   0.754    0.846  ...    37.0      37.0      60.0   1.03
mu        0.125  0.027   0.086    0.175  ...     5.0       5.0      38.0   1.43
mus       0.187  0.032   0.144    0.256  ...    75.0      76.0      92.0   1.00
gamma     0.244  0.041   0.166    0.304  ...    87.0      91.0      60.0   1.00
Is_begin  0.706  0.811   0.000    2.312  ...   106.0      40.0      15.0   1.00
Ia_begin  1.508  1.225   0.034    3.963  ...   114.0     109.0      80.0   1.01
E_begin   0.605  0.624   0.004    1.629  ...    88.0      67.0      88.0   0.99

[8 rows x 11 columns]