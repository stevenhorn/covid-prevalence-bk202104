0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -671.75    32.72
p_loo       35.13        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      361   94.0%
 (0.5, 0.7]   (ok)         17    4.4%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.301  0.043   0.217    0.348  ...    65.0      71.0      49.0   1.08
pu        0.893  0.007   0.877    0.899  ...   131.0     101.0      46.0   0.99
mu        0.173  0.021   0.140    0.208  ...    26.0      25.0      16.0   1.11
mus       0.197  0.032   0.146    0.263  ...    52.0      50.0      57.0   1.02
gamma     0.283  0.047   0.203    0.369  ...    83.0      76.0      54.0   1.02
Is_begin  0.711  0.554   0.012    1.680  ...    85.0      62.0      39.0   1.00
Ia_begin  1.309  1.001   0.007    3.216  ...    30.0      18.0      24.0   1.10
E_begin   0.797  0.771   0.008    2.250  ...    60.0      47.0      40.0   1.04

[8 rows x 11 columns]