0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -492.20    35.72
p_loo       31.90        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      366   95.3%
 (0.5, 0.7]   (ok)         11    2.9%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.244  0.047   0.155    0.319  ...    72.0      73.0      55.0   0.99
pu        0.802  0.023   0.754    0.835  ...    50.0      56.0      54.0   1.03
mu        0.120  0.022   0.087    0.148  ...    56.0      66.0      97.0   1.03
mus       0.170  0.036   0.119    0.249  ...   112.0     141.0      45.0   1.02
gamma     0.197  0.049   0.126    0.287  ...    56.0      96.0      43.0   1.13
Is_begin  0.310  0.364   0.000    0.994  ...    73.0      58.0      72.0   0.98
Ia_begin  0.529  0.642   0.010    1.669  ...    58.0      57.0      59.0   1.01
E_begin   0.281  0.405   0.002    1.140  ...    81.0      59.0      33.0   1.02

[8 rows x 11 columns]