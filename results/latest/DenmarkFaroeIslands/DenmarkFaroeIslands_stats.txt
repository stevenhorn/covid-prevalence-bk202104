0 Divergences 
Passed validation 
Computed from 80 by 385 log-likelihood matrix

         Estimate       SE
elpd_loo  -795.53    41.47
p_loo       44.37        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      358   93.0%
 (0.5, 0.7]   (ok)         22    5.7%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    1    0.3%

             mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.253   0.052   0.182    0.348  ...    87.0      86.0      60.0   1.07
pu          0.841   0.037   0.771    0.897  ...    79.0      79.0      46.0   1.06
mu          0.260   0.032   0.204    0.312  ...    21.0      25.0      60.0   1.06
mus         0.289   0.037   0.240    0.372  ...    73.0      82.0      44.0   1.02
gamma       0.481   0.087   0.330    0.640  ...    94.0     119.0      60.0   1.02
Is_begin   35.141  18.923   5.397   65.863  ...    94.0      90.0      88.0   1.01
Ia_begin   95.658  40.578  25.657  158.492  ...    90.0      71.0      60.0   1.04
E_begin   176.338  63.868  82.000  286.212  ...    72.0      62.0      41.0   1.01

[8 rows x 11 columns]