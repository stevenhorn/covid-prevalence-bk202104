0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1030.61    40.61
p_loo       36.33        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      359   93.5%
 (0.5, 0.7]   (ok)         19    4.9%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.249  0.055   0.163    0.337  ...   111.0     112.0      96.0   0.99
pu        0.806  0.027   0.759    0.855  ...    58.0      58.0      60.0   1.01
mu        0.126  0.025   0.090    0.176  ...    22.0      21.0      60.0   1.06
mus       0.165  0.027   0.125    0.221  ...   115.0     113.0      88.0   1.01
gamma     0.195  0.033   0.135    0.252  ...   139.0     132.0      68.0   0.98
Is_begin  0.654  0.666   0.000    2.022  ...    74.0      44.0      14.0   1.03
Ia_begin  1.497  1.726   0.002    4.041  ...    77.0      21.0      22.0   1.07
E_begin   0.657  0.649   0.006    2.068  ...    54.0      56.0      58.0   0.99

[8 rows x 11 columns]