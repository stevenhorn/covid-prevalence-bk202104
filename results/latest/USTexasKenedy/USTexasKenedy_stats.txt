0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -185.35    51.61
p_loo       46.29        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      360   93.8%
 (0.5, 0.7]   (ok)         20    5.2%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.257  0.045   0.195    0.343  ...   142.0     137.0      57.0   0.99
pu        0.801  0.030   0.743    0.846  ...   152.0     106.0      85.0   1.01
mu        0.129  0.026   0.087    0.181  ...   140.0     141.0      59.0   1.01
mus       0.216  0.040   0.147    0.281  ...   152.0     152.0      60.0   0.99
gamma     0.235  0.047   0.150    0.327  ...   152.0     152.0     100.0   1.07
Is_begin  0.444  0.461   0.003    1.431  ...   115.0     126.0      34.0   1.01
Ia_begin  0.631  0.819   0.005    2.403  ...    89.0      90.0     100.0   0.99
E_begin   0.301  0.370   0.015    0.937  ...   114.0     104.0      96.0   1.00

[8 rows x 11 columns]