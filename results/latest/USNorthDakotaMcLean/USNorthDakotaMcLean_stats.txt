0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -789.49    28.15
p_loo       26.96        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      354   92.2%
 (0.5, 0.7]   (ok)         26    6.8%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.250  0.057   0.168    0.347  ...    21.0      24.0      16.0   1.07
pu        0.756  0.023   0.711    0.788  ...    30.0      41.0      22.0   1.04
mu        0.153  0.026   0.104    0.199  ...    11.0      12.0      58.0   1.13
mus       0.182  0.037   0.116    0.247  ...    70.0      84.0      48.0   1.01
gamma     0.206  0.037   0.136    0.277  ...   152.0     152.0      65.0   1.01
Is_begin  0.738  0.687   0.014    1.897  ...   106.0     133.0      59.0   1.02
Ia_begin  1.448  1.321   0.025    4.368  ...    56.0      54.0      60.0   1.01
E_begin   0.532  0.535   0.002    1.517  ...    91.0      53.0      39.0   1.02

[8 rows x 11 columns]