0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1163.71    39.95
p_loo       36.05        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      357   93.0%
 (0.5, 0.7]   (ok)         21    5.5%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.252  0.055   0.163    0.331  ...    30.0      44.0      56.0   1.05
pu        0.781  0.032   0.731    0.837  ...     9.0       9.0      38.0   1.22
mu        0.125  0.023   0.082    0.168  ...    31.0      30.0      40.0   1.04
mus       0.184  0.032   0.138    0.246  ...   108.0     127.0     100.0   0.99
gamma     0.227  0.038   0.167    0.294  ...   152.0     152.0      58.0   1.01
Is_begin  1.029  0.915   0.013    2.683  ...    61.0      42.0      65.0   1.01
Ia_begin  2.692  2.439   0.056    8.668  ...    89.0      78.0      78.0   1.03
E_begin   1.714  2.202   0.007    6.042  ...    58.0      41.0      41.0   1.00

[8 rows x 11 columns]