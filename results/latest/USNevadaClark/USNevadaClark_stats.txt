0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -2439.55    25.39
p_loo       26.61        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      349   90.9%
 (0.5, 0.7]   (ok)         32    8.3%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    0    0.0%

             mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.232   0.044   0.153    0.301  ...    42.0      27.0      44.0   1.06
pu          0.743   0.028   0.702    0.789  ...    31.0      33.0      57.0   1.05
mu          0.118   0.023   0.082    0.155  ...     4.0       4.0      24.0   1.64
mus         0.191   0.025   0.150    0.236  ...    36.0      56.0      60.0   1.07
gamma       0.261   0.048   0.192    0.367  ...   118.0     136.0     100.0   1.02
Is_begin   42.744  20.972   9.337   75.991  ...    51.0      44.0      40.0   1.05
Ia_begin   84.192  43.354   5.648  166.782  ...    17.0      18.0      40.0   1.13
E_begin   137.207  60.287  50.023  246.439  ...    75.0      70.0      58.0   1.00

[8 rows x 11 columns]