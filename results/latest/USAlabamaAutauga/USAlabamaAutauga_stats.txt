0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1362.87    40.03
p_loo       25.86        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      342   89.1%
 (0.5, 0.7]   (ok)         32    8.3%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.252  0.057   0.164    0.342  ...    17.0      14.0      19.0   1.13
pu        0.783  0.024   0.724    0.811  ...    26.0      32.0      32.0   1.04
mu        0.105  0.026   0.070    0.158  ...     6.0       6.0      16.0   1.38
mus       0.143  0.031   0.083    0.196  ...    72.0      60.0      33.0   1.04
gamma     0.145  0.028   0.098    0.199  ...    28.0      24.0      56.0   1.05
Is_begin  0.617  0.579   0.020    1.964  ...    79.0      96.0     102.0   0.98
Ia_begin  1.097  0.932   0.069    2.594  ...    93.0      74.0      36.0   1.07
E_begin   0.593  0.607   0.002    2.000  ...    90.0      68.0      40.0   0.99

[8 rows x 11 columns]