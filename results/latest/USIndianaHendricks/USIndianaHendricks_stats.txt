0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1471.47    31.94
p_loo       27.68        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      357   93.0%
 (0.5, 0.7]   (ok)         23    6.0%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.238  0.058   0.154    0.340  ...    71.0      81.0      60.0   1.01
pu        0.748  0.028   0.700    0.797  ...    93.0      91.0      43.0   1.02
mu        0.115  0.018   0.086    0.143  ...    34.0      35.0      56.0   1.04
mus       0.182  0.029   0.119    0.221  ...   152.0     152.0      72.0   0.99
gamma     0.277  0.040   0.206    0.335  ...   109.0     108.0      91.0   1.03
Is_begin  0.883  0.573   0.109    1.953  ...    71.0      77.0      93.0   1.00
Ia_begin  2.267  1.479   0.095    4.811  ...   152.0      66.0      40.0   1.04
E_begin   2.072  1.779   0.010    5.620  ...    79.0      63.0      48.0   1.10

[8 rows x 11 columns]