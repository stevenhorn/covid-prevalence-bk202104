0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1871.71    37.49
p_loo       32.52        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      356   92.7%
 (0.5, 0.7]   (ok)         21    5.5%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.239  0.044   0.172    0.321  ...   152.0     152.0      95.0   1.02
pu        0.746  0.028   0.701    0.792  ...    37.0      28.0      14.0   1.04
mu        0.122  0.020   0.081    0.153  ...    33.0      42.0      56.0   1.06
mus       0.180  0.029   0.125    0.230  ...   104.0     127.0      83.0   1.00
gamma     0.259  0.049   0.179    0.336  ...   105.0      96.0      83.0   0.98
Is_begin  1.842  1.310   0.091    4.026  ...   102.0      56.0      83.0   1.02
Ia_begin  3.348  2.719   0.008    7.782  ...    54.0      29.0      36.0   1.05
E_begin   3.163  2.740   0.134    8.356  ...    91.0      79.0      86.0   1.05

[8 rows x 11 columns]