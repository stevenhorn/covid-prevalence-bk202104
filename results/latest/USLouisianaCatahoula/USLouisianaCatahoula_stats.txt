0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1109.42    41.90
p_loo       44.69        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      359   93.5%
 (0.5, 0.7]   (ok)         14    3.6%
   (0.7, 1]   (bad)         8    2.1%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.243  0.051   0.163    0.335  ...    51.0      49.0      40.0   1.02
pu        0.798  0.028   0.738    0.850  ...    25.0      32.0      29.0   1.07
mu        0.114  0.017   0.084    0.145  ...    28.0      28.0      60.0   1.04
mus       0.171  0.035   0.120    0.235  ...    50.0      53.0      60.0   1.00
gamma     0.200  0.043   0.131    0.281  ...    88.0      94.0      93.0   1.07
Is_begin  0.949  0.926   0.063    2.983  ...   101.0     152.0      57.0   1.01
Ia_begin  1.470  1.472   0.052    4.691  ...   120.0      99.0      57.0   1.02
E_begin   0.757  0.848   0.005    2.410  ...    61.0      49.0      59.0   1.03

[8 rows x 11 columns]