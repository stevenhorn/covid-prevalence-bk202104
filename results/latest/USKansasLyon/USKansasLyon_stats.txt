0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1494.50    36.96
p_loo       36.30        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      359   93.5%
 (0.5, 0.7]   (ok)         17    4.4%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.193  0.027   0.155    0.238  ...    66.0      68.0      97.0   1.02
pu        0.715  0.011   0.701    0.735  ...    89.0      65.0      59.0   1.04
mu        0.127  0.020   0.092    0.153  ...    16.0      18.0      54.0   1.12
mus       0.173  0.030   0.129    0.229  ...    38.0      38.0      43.0   1.00
gamma     0.237  0.033   0.165    0.290  ...   119.0     124.0      72.0   1.02
Is_begin  1.033  0.935   0.006    2.242  ...    74.0      41.0      31.0   1.13
Ia_begin  2.122  1.831   0.010    5.485  ...    34.0      20.0      38.0   1.13
E_begin   1.293  1.148   0.004    3.910  ...    40.0      48.0      60.0   1.02

[8 rows x 11 columns]