0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1103.44    43.46
p_loo       31.34        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      355   92.4%
 (0.5, 0.7]   (ok)         25    6.5%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.246  0.050   0.168    0.331  ...    71.0      68.0      56.0   1.00
pu        0.825  0.017   0.799    0.857  ...    52.0      46.0      60.0   1.05
mu        0.135  0.030   0.080    0.189  ...    29.0      37.0      47.0   1.06
mus       0.178  0.028   0.133    0.225  ...    19.0      19.0      60.0   1.10
gamma     0.210  0.047   0.134    0.300  ...    91.0     100.0      60.0   1.02
Is_begin  0.805  0.828   0.001    2.119  ...   106.0      83.0      22.0   1.01
Ia_begin  1.307  1.150   0.040    3.258  ...    75.0      95.0      95.0   1.05
E_begin   0.660  0.670   0.002    2.269  ...    76.0      31.0      31.0   1.02

[8 rows x 11 columns]