0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -767.06    57.54
p_loo       37.15        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      364   94.8%
 (0.5, 0.7]   (ok)         15    3.9%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.245  0.058   0.154    0.332  ...    48.0      39.0      39.0   1.02
pu        0.749  0.031   0.700    0.802  ...    30.0      30.0      14.0   1.02
mu        0.133  0.024   0.097    0.174  ...    50.0      54.0      95.0   1.01
mus       0.244  0.042   0.170    0.313  ...    24.0      24.0      60.0   1.09
gamma     0.336  0.050   0.247    0.428  ...    10.0       9.0      42.0   1.23
Is_begin  0.445  0.348   0.004    1.120  ...    30.0      26.0      95.0   1.05
Ia_begin  0.730  0.677   0.004    2.447  ...    68.0      98.0      52.0   1.00
E_begin   0.329  0.368   0.001    1.160  ...    24.0      15.0      38.0   1.13

[8 rows x 11 columns]