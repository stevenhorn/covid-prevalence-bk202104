3 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -2308.56    32.77
p_loo       28.53        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      352   91.7%
 (0.5, 0.7]   (ok)         28    7.3%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    1    0.3%

             mean       sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.244    0.045   0.170    0.336  ...   152.0     152.0      60.0   0.99
pu          0.784    0.053   0.700    0.869  ...    62.0      66.0      93.0   1.05
mu          0.143    0.026   0.090    0.183  ...    10.0      11.0      32.0   1.20
mus         0.188    0.039   0.110    0.263  ...   138.0     152.0      61.0   1.02
gamma       0.253    0.052   0.159    0.343  ...   152.0     152.0      84.0   1.02
Is_begin   60.676   35.884   7.926  119.226  ...    86.0      84.0      96.0   1.00
Ia_begin  143.300   62.704   6.397  229.008  ...    29.0      37.0      14.0   1.06
E_begin   166.846  105.981   7.829  391.168  ...    76.0      67.0      60.0   1.01

[8 rows x 11 columns]