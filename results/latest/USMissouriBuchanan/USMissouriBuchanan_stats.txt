0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1789.09    67.80
p_loo       42.31        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      367   95.6%
 (0.5, 0.7]   (ok)         12    3.1%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.197  0.037   0.150    0.277  ...    60.0      48.0      47.0   1.03
pu        0.717  0.015   0.700    0.740  ...    94.0      95.0      55.0   1.02
mu        0.122  0.015   0.100    0.154  ...    18.0      15.0      38.0   1.10
mus       0.234  0.039   0.165    0.292  ...     9.0       9.0      40.0   1.21
gamma     0.508  0.089   0.354    0.689  ...    67.0      38.0      19.0   1.04
Is_begin  0.659  0.677   0.003    1.886  ...    59.0      36.0      17.0   1.03
Ia_begin  1.272  1.311   0.020    4.013  ...    88.0      51.0      95.0   1.04
E_begin   0.487  0.494   0.015    1.210  ...    43.0      44.0      35.0   0.99

[8 rows x 11 columns]