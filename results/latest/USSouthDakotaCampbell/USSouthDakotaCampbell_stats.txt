0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -315.44    27.35
p_loo       26.45        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      305   79.4%
 (0.5, 0.7]   (ok)         65   16.9%
   (0.7, 1]   (bad)        13    3.4%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.256  0.050   0.173    0.346  ...    46.0      44.0      38.0   1.00
pu        0.849  0.013   0.826    0.870  ...    38.0      38.0      88.0   1.05
mu        0.138  0.023   0.100    0.180  ...    19.0      18.0      38.0   1.11
mus       0.174  0.037   0.119    0.256  ...    87.0      89.0      88.0   1.01
gamma     0.200  0.033   0.141    0.258  ...    64.0      60.0      93.0   1.00
Is_begin  0.432  0.422   0.003    1.201  ...    54.0      58.0      58.0   1.01
Ia_begin  0.818  0.850   0.020    2.565  ...    59.0      62.0      54.0   1.04
E_begin   0.327  0.328   0.001    1.007  ...    74.0      35.0      31.0   1.07

[8 rows x 11 columns]