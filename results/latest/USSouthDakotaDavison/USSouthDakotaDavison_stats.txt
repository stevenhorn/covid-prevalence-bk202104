0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -979.61    30.43
p_loo       28.71        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      347   90.4%
 (0.5, 0.7]   (ok)         30    7.8%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.175  0.021   0.150    0.214  ...   117.0     104.0      96.0   0.99
pu        0.707  0.006   0.700    0.716  ...   151.0     152.0      76.0   1.02
mu        0.104  0.016   0.074    0.128  ...    68.0      66.0     100.0   0.99
mus       0.199  0.028   0.164    0.262  ...    89.0      97.0      59.0   0.99
gamma     0.258  0.037   0.188    0.324  ...   114.0     132.0      69.0   0.99
Is_begin  0.663  0.801   0.004    2.175  ...   120.0      65.0      56.0   1.03
Ia_begin  1.198  1.272   0.035    3.994  ...   112.0     103.0      60.0   1.05
E_begin   0.466  0.474   0.003    1.143  ...   109.0      96.0      96.0   1.00

[8 rows x 11 columns]