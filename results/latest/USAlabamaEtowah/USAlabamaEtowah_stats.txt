0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1605.36    48.75
p_loo       39.70        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      353   91.9%
 (0.5, 0.7]   (ok)         20    5.2%
   (0.7, 1]   (bad)         8    2.1%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.220  0.049   0.152    0.306  ...    52.0      53.0      43.0   0.99
pu        0.726  0.018   0.700    0.761  ...    65.0      34.0      40.0   1.05
mu        0.109  0.017   0.077    0.139  ...    21.0      23.0      59.0   1.09
mus       0.175  0.028   0.123    0.225  ...   119.0     109.0      51.0   0.99
gamma     0.199  0.040   0.138    0.284  ...    90.0     110.0      88.0   1.01
Is_begin  0.947  0.906   0.003    2.193  ...    79.0      63.0      59.0   0.99
Ia_begin  1.958  1.369   0.010    4.394  ...    58.0      58.0      43.0   1.02
E_begin   0.991  0.851   0.001    2.498  ...    24.0      24.0      40.0   1.05

[8 rows x 11 columns]