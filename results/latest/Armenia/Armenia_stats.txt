0 Divergences 
Passed validation 
Computed from 80 by 385 log-likelihood matrix

         Estimate       SE
elpd_loo -2240.63    23.54
p_loo       27.29        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      338   87.8%
 (0.5, 0.7]   (ok)         39   10.1%
   (0.7, 1]   (bad)         8    2.1%
   (1, Inf)   (very bad)    0    0.0%

             mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.242   0.049   0.164    0.335  ...   107.0     101.0      60.0   1.00
pu          0.756   0.029   0.707    0.811  ...    76.0      77.0      69.0   1.01
mu          0.144   0.023   0.107    0.191  ...     9.0       9.0      33.0   1.19
mus         0.195   0.032   0.135    0.246  ...   152.0     152.0      69.0   1.01
gamma       0.252   0.040   0.178    0.334  ...   106.0     134.0      49.0   0.99
Is_begin   46.559  24.953   5.148   84.652  ...    53.0      52.0      56.0   1.01
Ia_begin  101.962  48.145  19.049  185.311  ...    72.0      72.0      57.0   1.03
E_begin   118.712  77.917   2.886  291.489  ...    22.0      15.0      17.0   1.12

[8 rows x 11 columns]