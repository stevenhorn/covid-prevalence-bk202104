0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1140.90    31.57
p_loo       26.45        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      365   95.1%
 (0.5, 0.7]   (ok)         12    3.1%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.256  0.058   0.157    0.350  ...    99.0      96.0      60.0   1.00
pu        0.834  0.019   0.798    0.867  ...    59.0      67.0      56.0   1.04
mu        0.129  0.026   0.088    0.188  ...    28.0      30.0      46.0   1.03
mus       0.166  0.040   0.105    0.245  ...   152.0     152.0      91.0   0.99
gamma     0.177  0.038   0.102    0.244  ...   117.0     113.0      59.0   1.00
Is_begin  0.698  0.651   0.010    2.024  ...    87.0      69.0      43.0   0.99
Ia_begin  1.719  1.865   0.022    5.276  ...   130.0     144.0      40.0   0.99
E_begin   0.802  0.870   0.024    2.496  ...   116.0     103.0      95.0   0.98

[8 rows x 11 columns]