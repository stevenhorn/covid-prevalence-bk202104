0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1730.50    73.86
p_loo       46.84        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      355   92.4%
 (0.5, 0.7]   (ok)         24    6.2%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.182  0.035   0.150    0.260  ...    89.0      55.0      43.0   1.03
pu        0.713  0.012   0.700    0.738  ...   104.0      64.0      57.0   0.99
mu        0.099  0.015   0.074    0.128  ...    12.0      14.0      40.0   1.13
mus       0.246  0.040   0.180    0.326  ...     8.0       8.0      56.0   1.25
gamma     0.437  0.071   0.304    0.560  ...    10.0      11.0      91.0   1.17
Is_begin  0.857  0.912   0.000    2.803  ...    60.0      33.0      22.0   1.03
Ia_begin  1.284  1.919   0.003    6.360  ...    74.0      58.0      59.0   1.01
E_begin   0.776  1.331   0.008    4.452  ...    60.0      57.0      29.0   1.03

[8 rows x 11 columns]