0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -963.44    30.21
p_loo       24.92        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      359   93.5%
 (0.5, 0.7]   (ok)         21    5.5%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.268  0.057   0.167    0.347  ...    41.0      35.0      40.0   1.02
pu        0.870  0.023   0.822    0.897  ...    30.0      26.0      57.0   1.07
mu        0.151  0.025   0.105    0.193  ...    28.0      31.0      59.0   1.06
mus       0.178  0.028   0.131    0.235  ...   152.0     152.0      86.0   1.06
gamma     0.245  0.047   0.173    0.338  ...   123.0     115.0      65.0   1.05
Is_begin  1.499  1.201   0.031    3.641  ...    88.0      62.0      39.0   1.02
Ia_begin  4.160  2.804   0.195    8.225  ...    52.0      42.0      34.0   1.06
E_begin   2.722  2.742   0.003    8.244  ...    23.0      14.0      40.0   1.13

[8 rows x 11 columns]