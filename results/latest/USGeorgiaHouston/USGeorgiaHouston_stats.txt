0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1596.26    56.14
p_loo       38.77        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      358   93.2%
 (0.5, 0.7]   (ok)         23    6.0%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.256  0.062   0.160    0.350  ...   152.0     131.0      25.0   1.12
pu        0.789  0.035   0.729    0.857  ...    14.0      13.0      56.0   1.14
mu        0.133  0.026   0.094    0.175  ...    12.0      14.0      60.0   1.13
mus       0.221  0.041   0.150    0.290  ...    81.0      43.0      91.0   1.04
gamma     0.296  0.052   0.224    0.403  ...    81.0      81.0     100.0   0.99
Is_begin  1.473  0.983   0.074    3.535  ...   145.0     118.0      48.0   1.01
Ia_begin  0.738  0.543   0.080    1.827  ...    34.0      30.0      93.0   1.06
E_begin   1.337  1.106   0.022    3.652  ...    95.0     152.0      32.0   0.99

[8 rows x 11 columns]