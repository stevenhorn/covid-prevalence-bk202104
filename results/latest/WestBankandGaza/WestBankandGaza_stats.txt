0 Divergences 
Passed validation 
Computed from 80 by 385 log-likelihood matrix

         Estimate       SE
elpd_loo -2125.90    32.30
p_loo       33.02        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      345   89.6%
 (0.5, 0.7]   (ok)         32    8.3%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.254  0.051   0.168    0.334  ...    74.0      69.0      60.0   1.01
pu        0.796  0.048   0.706    0.865  ...    44.0      45.0      39.0   1.00
mu        0.150  0.017   0.115    0.184  ...    33.0      32.0      40.0   1.07
mus       0.221  0.034   0.163    0.292  ...    99.0      96.0      59.0   0.99
gamma     0.357  0.049   0.271    0.449  ...    79.0      78.0      80.0   1.03
Is_begin  3.336  1.856   0.092    6.023  ...    95.0      70.0      60.0   1.02
Ia_begin  7.521  4.536   1.087   14.369  ...    82.0      69.0      96.0   1.00
E_begin   6.198  5.254   0.129   15.136  ...   116.0      87.0      60.0   0.98

[8 rows x 11 columns]