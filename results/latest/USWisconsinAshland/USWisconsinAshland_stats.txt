0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -818.46    39.32
p_loo       30.94        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      364   94.8%
 (0.5, 0.7]   (ok)         17    4.4%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.257  0.052   0.176    0.338  ...   152.0     152.0      43.0   1.02
pu        0.845  0.017   0.807    0.872  ...    18.0      18.0      79.0   1.11
mu        0.133  0.027   0.087    0.181  ...    19.0      19.0      69.0   1.10
mus       0.166  0.033   0.111    0.244  ...   152.0     152.0      60.0   0.98
gamma     0.186  0.036   0.135    0.260  ...   113.0     115.0      69.0   1.00
Is_begin  0.792  0.740   0.007    2.101  ...   105.0     152.0      72.0   1.00
Ia_begin  1.450  1.737   0.020    4.455  ...    95.0     152.0      60.0   1.02
E_begin   0.557  0.512   0.006    1.560  ...    95.0     108.0      91.0   0.98

[8 rows x 11 columns]