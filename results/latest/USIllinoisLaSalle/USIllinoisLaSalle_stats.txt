0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1342.12    28.70
p_loo       33.45        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      347   90.4%
 (0.5, 0.7]   (ok)         31    8.1%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.251  0.050   0.178    0.341  ...   108.0     121.0      88.0   1.01
pu        0.752  0.027   0.704    0.799  ...    61.0      64.0      58.0   1.04
mu        0.123  0.021   0.093    0.171  ...    26.0      22.0      20.0   1.14
mus       0.211  0.033   0.155    0.260  ...   104.0     105.0      93.0   1.01
gamma     0.237  0.038   0.167    0.304  ...   127.0     120.0     100.0   1.01
Is_begin  0.865  0.728   0.004    2.321  ...    96.0      61.0      32.0   1.03
Ia_begin  1.689  1.727   0.003    4.738  ...    69.0      21.0      15.0   1.08
E_begin   0.781  0.723   0.000    2.173  ...    28.0      16.0      15.0   1.09

[8 rows x 11 columns]