0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -770.02    56.98
p_loo       43.01        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      359   93.5%
 (0.5, 0.7]   (ok)         17    4.4%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.248  0.050   0.150    0.323  ...   101.0      97.0      15.0   1.15
pu        0.787  0.021   0.754    0.826  ...    79.0      80.0      80.0   1.07
mu        0.137  0.028   0.089    0.183  ...   111.0      92.0      52.0   1.03
mus       0.203  0.029   0.152    0.252  ...    63.0      59.0      40.0   1.00
gamma     0.257  0.048   0.171    0.330  ...   113.0     137.0      58.0   1.00
Is_begin  0.687  0.716   0.006    2.060  ...   101.0     112.0      60.0   0.99
Ia_begin  1.025  1.026   0.001    2.930  ...   100.0      73.0      27.0   1.02
E_begin   0.562  0.645   0.025    2.118  ...   127.0     102.0      83.0   1.00

[8 rows x 11 columns]