0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -926.88    46.91
p_loo       41.14        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      353   91.9%
 (0.5, 0.7]   (ok)         22    5.7%
   (0.7, 1]   (bad)         8    2.1%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.253  0.054   0.176    0.346  ...    19.0      20.0      59.0   1.09
pu        0.837  0.018   0.798    0.865  ...    12.0      11.0      21.0   1.14
mu        0.135  0.022   0.088    0.169  ...    18.0      24.0      14.0   1.11
mus       0.168  0.035   0.120    0.238  ...    44.0      46.0      56.0   1.05
gamma     0.192  0.040   0.130    0.255  ...    99.0      87.0      60.0   1.00
Is_begin  0.789  0.796   0.013    2.017  ...    84.0      93.0      36.0   1.02
Ia_begin  1.724  1.591   0.106    4.910  ...    23.0      25.0      53.0   1.08
E_begin   0.771  0.848   0.032    2.630  ...    50.0      53.0      59.0   0.99

[8 rows x 11 columns]