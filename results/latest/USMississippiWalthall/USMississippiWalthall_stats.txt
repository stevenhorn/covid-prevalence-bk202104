0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -917.71    33.55
p_loo       33.56        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      353   91.9%
 (0.5, 0.7]   (ok)         24    6.2%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.251  0.058   0.163    0.344  ...    52.0      52.0      60.0   1.00
pu        0.830  0.016   0.796    0.855  ...    35.0      41.0      59.0   1.02
mu        0.148  0.025   0.103    0.197  ...     7.0       7.0      31.0   1.28
mus       0.167  0.030   0.126    0.236  ...    53.0      39.0      21.0   1.03
gamma     0.182  0.036   0.128    0.253  ...    51.0      57.0      43.0   1.02
Is_begin  0.963  0.724   0.034    2.317  ...    76.0      73.0      60.0   1.03
Ia_begin  2.430  2.254   0.011    6.870  ...    86.0     109.0      77.0   1.02
E_begin   1.398  1.526   0.013    4.041  ...    20.0      22.0      80.0   1.08

[8 rows x 11 columns]