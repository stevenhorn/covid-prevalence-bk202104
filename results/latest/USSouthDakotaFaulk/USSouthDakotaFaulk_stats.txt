0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -626.14    44.53
p_loo       44.65        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      355   92.4%
 (0.5, 0.7]   (ok)         23    6.0%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.237  0.044   0.164    0.336  ...    84.0      76.0      59.0   1.00
pu        0.733  0.021   0.700    0.763  ...    76.0      73.0      80.0   1.03
mu        0.138  0.026   0.105    0.195  ...    33.0      43.0      40.0   1.04
mus       0.215  0.031   0.164    0.272  ...   131.0     122.0      96.0   1.00
gamma     0.243  0.047   0.185    0.338  ...    80.0     138.0      60.0   0.98
Is_begin  0.571  0.588   0.005    1.553  ...   110.0      85.0      93.0   0.98
Ia_begin  1.041  1.059   0.005    3.255  ...   119.0      76.0      56.0   1.05
E_begin   0.479  0.521   0.002    1.324  ...    82.0      53.0      59.0   1.01

[8 rows x 11 columns]