0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1354.67    45.55
p_loo       33.34        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      370   96.4%
 (0.5, 0.7]   (ok)         12    3.1%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.259  0.053   0.168    0.350  ...    65.0      68.0      54.0   1.01
pu        0.837  0.015   0.807    0.863  ...    47.0      33.0      15.0   1.06
mu        0.124  0.021   0.096    0.172  ...    19.0      20.0      67.0   1.07
mus       0.148  0.028   0.101    0.204  ...   130.0     121.0      51.0   1.01
gamma     0.165  0.034   0.113    0.239  ...    91.0     134.0      65.0   1.02
Is_begin  0.684  0.730   0.005    2.006  ...    93.0      58.0      88.0   1.02
Ia_begin  1.478  1.380   0.054    4.014  ...   136.0     130.0      88.0   1.01
E_begin   0.637  0.714   0.003    1.965  ...    64.0      74.0      33.0   1.00

[8 rows x 11 columns]