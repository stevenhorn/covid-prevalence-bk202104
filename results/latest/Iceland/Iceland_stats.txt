0 Divergences 
Passed validation 
Computed from 80 by 385 log-likelihood matrix

         Estimate       SE
elpd_loo -1228.28    28.98
p_loo       35.85        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      345   89.6%
 (0.5, 0.7]   (ok)         31    8.1%
   (0.7, 1]   (bad)         9    2.3%
   (1, Inf)   (very bad)    0    0.0%

             mean       sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.261    0.044   0.192    0.345  ...    95.0     112.0      59.0   1.04
pu          0.830    0.052   0.728    0.897  ...     6.0       6.0      24.0   1.35
mu          0.225    0.025   0.182    0.269  ...     8.0       8.0      65.0   1.23
mus         0.269    0.032   0.227    0.348  ...    67.0      63.0      38.0   1.02
gamma       0.452    0.064   0.345    0.567  ...   129.0     151.0      96.0   1.00
Is_begin   91.932   53.710   4.202  190.219  ...    69.0      72.0      56.0   1.00
Ia_begin  265.237  111.868  68.936  473.357  ...    17.0      17.0      52.0   1.09
E_begin   418.133  212.309  66.603  784.624  ...     6.0       6.0      40.0   1.31

[8 rows x 11 columns]