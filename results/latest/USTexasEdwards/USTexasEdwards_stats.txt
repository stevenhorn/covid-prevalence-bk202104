0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -763.49    61.76
p_loo       60.33        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      367   95.6%
 (0.5, 0.7]   (ok)         11    2.9%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.224  0.048   0.150    0.302  ...    56.0      53.0      43.0   1.02
pu        0.728  0.025   0.701    0.768  ...    21.0      34.0      54.0   1.06
mu        0.187  0.031   0.125    0.242  ...    43.0      40.0      21.0   1.00
mus       0.286  0.051   0.193    0.394  ...    70.0      69.0      81.0   1.00
gamma     0.381  0.059   0.289    0.490  ...    71.0      70.0      87.0   0.99
Is_begin  0.434  0.530   0.012    1.415  ...    33.0      28.0      59.0   1.04
Ia_begin  0.691  0.866   0.027    2.026  ...    51.0      44.0      60.0   1.00
E_begin   0.415  0.656   0.035    1.057  ...    58.0      32.0      88.0   1.02

[8 rows x 11 columns]