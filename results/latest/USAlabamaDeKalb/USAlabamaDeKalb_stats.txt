0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1454.50    47.50
p_loo       34.63        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      352   91.7%
 (0.5, 0.7]   (ok)         24    6.2%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.224  0.052   0.156    0.318  ...    90.0      98.0      47.0   1.02
pu        0.731  0.022   0.701    0.772  ...    74.0      91.0      84.0   1.01
mu        0.122  0.024   0.083    0.167  ...    24.0      22.0      49.0   1.03
mus       0.193  0.031   0.141    0.245  ...    37.0      31.0      57.0   1.07
gamma     0.228  0.037   0.166    0.300  ...    60.0      99.0      46.0   1.04
Is_begin  0.512  0.535   0.007    1.542  ...    95.0     107.0      60.0   1.02
Ia_begin  1.316  1.607   0.008    3.277  ...   103.0     143.0      58.0   0.99
E_begin   0.592  0.667   0.006    2.100  ...   118.0      87.0      60.0   0.99

[8 rows x 11 columns]