0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1203.11    37.95
p_loo       24.03        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      360   93.8%
 (0.5, 0.7]   (ok)         20    5.2%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.262  0.051   0.167    0.346  ...    37.0      38.0      51.0   1.05
pu        0.776  0.024   0.736    0.820  ...    11.0      12.0      24.0   1.15
mu        0.129  0.024   0.084    0.175  ...    19.0      16.0      59.0   1.10
mus       0.163  0.032   0.114    0.226  ...   101.0     100.0      88.0   1.01
gamma     0.174  0.035   0.120    0.238  ...    86.0      80.0      74.0   0.99
Is_begin  0.631  0.539   0.019    1.782  ...    15.0      11.0      19.0   1.16
Ia_begin  1.356  1.395   0.006    4.162  ...    81.0     152.0      57.0   1.02
E_begin   0.589  0.554   0.007    1.774  ...    65.0      78.0      72.0   1.01

[8 rows x 11 columns]