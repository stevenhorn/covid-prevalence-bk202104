61 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -764.60    41.21
p_loo       30.22        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      357   93.0%
 (0.5, 0.7]   (ok)         19    4.9%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.224  0.057   0.157    0.314  ...     3.0       3.0      23.0   2.14
pu        0.860  0.016   0.835    0.879  ...     3.0       4.0      15.0   1.83
mu        0.150  0.023   0.115    0.186  ...     3.0       3.0      20.0   2.25
mus       0.196  0.025   0.148    0.229  ...     8.0       9.0      17.0   1.20
gamma     0.194  0.022   0.161    0.229  ...     5.0       5.0      41.0   1.39
Is_begin  1.178  0.926   0.007    2.575  ...     5.0       5.0      15.0   1.47
Ia_begin  1.492  1.411   0.113    5.288  ...     5.0       4.0      15.0   1.70
E_begin   0.402  0.392   0.037    1.065  ...     9.0       5.0      34.0   1.39

[8 rows x 11 columns]