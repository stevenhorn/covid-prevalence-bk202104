0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1453.99    31.74
p_loo       30.33        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      351   91.4%
 (0.5, 0.7]   (ok)         27    7.0%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.247  0.049   0.160    0.320  ...    63.0      63.0      33.0   1.07
pu        0.760  0.028   0.709    0.815  ...    64.0      66.0      55.0   1.05
mu        0.122  0.030   0.070    0.171  ...     7.0       7.0      32.0   1.24
mus       0.180  0.037   0.119    0.252  ...    25.0      47.0      69.0   1.05
gamma     0.217  0.047   0.154    0.309  ...    55.0      62.0      89.0   1.05
Is_begin  1.354  1.028   0.114    3.055  ...    66.0      51.0      54.0   1.02
Ia_begin  3.142  2.646   0.002    8.548  ...    95.0      44.0      22.0   1.14
E_begin   2.017  1.910   0.020    4.834  ...    98.0      68.0      59.0   1.00

[8 rows x 11 columns]