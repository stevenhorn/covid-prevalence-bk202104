0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -606.79    35.40
p_loo       27.88        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      357   93.0%
 (0.5, 0.7]   (ok)         22    5.7%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.268  0.042   0.186    0.338  ...    26.0      32.0      44.0   1.08
pu        0.883  0.010   0.867    0.899  ...    20.0      17.0      24.0   1.13
mu        0.143  0.024   0.111    0.191  ...    28.0      31.0      71.0   1.04
mus       0.158  0.031   0.103    0.208  ...   126.0     126.0      59.0   1.00
gamma     0.164  0.036   0.119    0.241  ...    10.0      10.0      37.0   1.15
Is_begin  0.700  0.605   0.031    1.848  ...    63.0      58.0      24.0   1.00
Ia_begin  1.667  1.627   0.029    4.907  ...    78.0      70.0      60.0   1.03
E_begin   0.792  1.006   0.010    2.362  ...    76.0      62.0      72.0   1.06

[8 rows x 11 columns]