0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -848.39    32.40
p_loo       31.96        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      358   93.2%
 (0.5, 0.7]   (ok)         21    5.5%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.250  0.050   0.171    0.329  ...    49.0      58.0      60.0   1.07
pu        0.766  0.026   0.716    0.810  ...    25.0      26.0      54.0   1.10
mu        0.129  0.024   0.084    0.172  ...    49.0      50.0      96.0   1.03
mus       0.173  0.031   0.106    0.227  ...    15.0      18.0      26.0   1.10
gamma     0.189  0.037   0.126    0.255  ...   148.0     135.0      93.0   1.04
Is_begin  0.718  0.783   0.007    2.034  ...    96.0      82.0      77.0   1.00
Ia_begin  1.031  0.881   0.007    2.956  ...   101.0      64.0      60.0   1.01
E_begin   0.523  0.708   0.003    2.198  ...   107.0      47.0      38.0   1.05

[8 rows x 11 columns]