0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1193.56    64.08
p_loo       45.77        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      356   92.7%
 (0.5, 0.7]   (ok)         20    5.2%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.172  0.020   0.150    0.220  ...   136.0     152.0      72.0   0.98
pu        0.706  0.005   0.700    0.717  ...   144.0     123.0      51.0   0.99
mu        0.125  0.018   0.093    0.161  ...    87.0      84.0      59.0   1.00
mus       0.253  0.048   0.177    0.354  ...    92.0     103.0      76.0   1.03
gamma     0.433  0.071   0.319    0.551  ...    88.0      90.0      97.0   1.02
Is_begin  0.523  0.516   0.002    1.494  ...    99.0      90.0      24.0   0.99
Ia_begin  0.862  1.029   0.002    3.766  ...    99.0     123.0      43.0   1.01
E_begin   0.406  0.705   0.005    1.597  ...    96.0     142.0      24.0   1.08

[8 rows x 11 columns]