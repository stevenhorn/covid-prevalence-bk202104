0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1284.63    41.40
p_loo       35.71        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      367   95.6%
 (0.5, 0.7]   (ok)         14    3.6%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.255  0.052   0.165    0.339  ...   108.0     116.0      21.0   1.02
pu        0.802  0.021   0.754    0.828  ...    60.0      63.0      59.0   1.00
mu        0.110  0.020   0.077    0.145  ...     8.0       8.0      21.0   1.27
mus       0.155  0.035   0.099    0.226  ...   119.0     114.0      87.0   1.00
gamma     0.165  0.031   0.112    0.216  ...   146.0     142.0      54.0   0.99
Is_begin  0.748  0.607   0.012    1.957  ...   152.0     143.0      59.0   0.98
Ia_begin  1.520  1.493   0.020    4.247  ...    98.0     130.0      96.0   0.99
E_begin   0.674  0.679   0.015    2.084  ...    84.0      76.0      96.0   0.99

[8 rows x 11 columns]