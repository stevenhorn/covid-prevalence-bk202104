0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -508.06    30.36
p_loo       31.11        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      360   93.8%
 (0.5, 0.7]   (ok)         21    5.5%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.280  0.051   0.190    0.350  ...   103.0      91.0      58.0   1.00
pu        0.888  0.009   0.868    0.900  ...    90.0      85.0      40.0   1.01
mu        0.153  0.025   0.111    0.203  ...    87.0      90.0      69.0   1.01
mus       0.176  0.031   0.121    0.224  ...   152.0     152.0      86.0   0.99
gamma     0.233  0.041   0.159    0.301  ...   152.0     152.0     100.0   0.98
Is_begin  0.647  0.540   0.004    1.755  ...   106.0     116.0      56.0   0.99
Ia_begin  1.769  2.264   0.029    6.052  ...   105.0      76.0      57.0   0.99
E_begin   0.681  0.854   0.001    2.389  ...   100.0      75.0      53.0   1.03

[8 rows x 11 columns]