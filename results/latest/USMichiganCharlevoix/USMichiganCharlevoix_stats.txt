0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -860.72    29.90
p_loo       27.86        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      365   95.1%
 (0.5, 0.7]   (ok)         17    4.4%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.298  0.029   0.237    0.336  ...     4.0       4.0      25.0   1.57
pu        0.835  0.012   0.820    0.865  ...     4.0       5.0      39.0   1.49
mu        0.183  0.029   0.132    0.219  ...     3.0       4.0      17.0   1.80
mus       0.206  0.035   0.153    0.274  ...     7.0       7.0      16.0   1.23
gamma     0.253  0.038   0.170    0.318  ...    40.0      40.0      57.0   1.15
Is_begin  1.238  0.947   0.040    3.186  ...    17.0      13.0      40.0   1.13
Ia_begin  1.462  1.276   0.033    4.927  ...    57.0      45.0      30.0   1.19
E_begin   0.514  0.813   0.029    1.957  ...    23.0       9.0      57.0   1.22

[8 rows x 11 columns]