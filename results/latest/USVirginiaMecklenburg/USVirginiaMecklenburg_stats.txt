0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1148.19    33.25
p_loo       32.76        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      366   95.3%
 (0.5, 0.7]   (ok)         10    2.6%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.246  0.058   0.166    0.344  ...    78.0      69.0      37.0   1.04
pu        0.836  0.027   0.787    0.880  ...    30.0      35.0      60.0   1.05
mu        0.145  0.029   0.085    0.199  ...    21.0      21.0      40.0   1.06
mus       0.191  0.034   0.134    0.251  ...   100.0      91.0      48.0   1.02
gamma     0.262  0.045   0.174    0.342  ...   152.0     152.0      96.0   1.01
Is_begin  1.235  0.943   0.097    3.138  ...   132.0     101.0      77.0   1.00
Ia_begin  3.344  2.911   0.112    9.259  ...   121.0     101.0      59.0   1.00
E_begin   1.831  2.265   0.026    6.002  ...   114.0     129.0      93.0   0.98

[8 rows x 11 columns]