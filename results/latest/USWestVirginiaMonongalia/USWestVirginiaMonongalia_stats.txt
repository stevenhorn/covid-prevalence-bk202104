0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1328.73    27.80
p_loo       34.07        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      343   89.3%
 (0.5, 0.7]   (ok)         31    8.1%
   (0.7, 1]   (bad)         9    2.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.240  0.057   0.150    0.328  ...    28.0      20.0      22.0   1.08
pu        0.792  0.032   0.732    0.840  ...    10.0      11.0      59.0   1.15
mu        0.185  0.025   0.144    0.232  ...    28.0      28.0      54.0   1.17
mus       0.248  0.037   0.186    0.318  ...    43.0      54.0      55.0   1.03
gamma     0.426  0.058   0.313    0.516  ...    89.0      89.0      93.0   1.00
Is_begin  1.603  1.411   0.014    4.565  ...    95.0      70.0      42.0   1.03
Ia_begin  4.095  2.891   0.189    9.950  ...    48.0      39.0      88.0   1.02
E_begin   2.241  2.066   0.137    5.469  ...    62.0      54.0      88.0   1.02

[8 rows x 11 columns]