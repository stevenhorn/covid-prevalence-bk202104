0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -481.91    59.64
p_loo       66.98        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      357   93.0%
 (0.5, 0.7]   (ok)         19    4.9%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    4    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.256  0.059   0.164    0.342  ...    25.0      20.0      23.0   1.07
pu        0.860  0.021   0.818    0.893  ...    18.0      17.0      69.0   1.09
mu        0.111  0.024   0.074    0.153  ...    19.0      22.0      48.0   1.08
mus       0.260  0.048   0.193    0.366  ...    14.0      16.0      60.0   1.12
gamma     0.424  0.081   0.298    0.573  ...    18.0      19.0      53.0   1.08
Is_begin  1.052  0.962   0.033    3.081  ...    93.0      71.0      60.0   0.99
Ia_begin  1.685  1.438   0.049    4.624  ...    60.0      77.0      61.0   1.07
E_begin   0.676  0.647   0.004    2.091  ...    55.0      50.0      33.0   1.02

[8 rows x 11 columns]