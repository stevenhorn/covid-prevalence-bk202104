0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -458.49    30.71
p_loo       32.60        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      360   93.8%
 (0.5, 0.7]   (ok)         15    3.9%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.265  0.044   0.177    0.336  ...    58.0      55.0      65.0   0.99
pu        0.815  0.024   0.760    0.854  ...    19.0      42.0      15.0   1.05
mu        0.128  0.026   0.083    0.178  ...    10.0      10.0      39.0   1.17
mus       0.173  0.026   0.127    0.213  ...   106.0     116.0      76.0   0.99
gamma     0.198  0.043   0.128    0.277  ...    46.0      97.0      37.0   1.02
Is_begin  0.436  0.492   0.002    1.422  ...    62.0      51.0      49.0   1.00
Ia_begin  0.684  0.711   0.001    1.979  ...    58.0      41.0      40.0   1.01
E_begin   0.336  0.468   0.003    1.335  ...    47.0      36.0      52.0   1.02

[8 rows x 11 columns]