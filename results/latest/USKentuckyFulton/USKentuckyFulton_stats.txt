0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -643.03    20.94
p_loo       22.98        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      363   94.5%
 (0.5, 0.7]   (ok)         16    4.2%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.268  0.044   0.194    0.343  ...    49.0      57.0      38.0   1.01
pu        0.848  0.029   0.784    0.883  ...     5.0      10.0      42.0   1.44
mu        0.128  0.027   0.073    0.180  ...    46.0      49.0      40.0   1.04
mus       0.172  0.028   0.124    0.218  ...   112.0     132.0      59.0   1.02
gamma     0.213  0.047   0.124    0.292  ...   152.0     152.0      53.0   1.05
Is_begin  0.465  0.463   0.005    1.222  ...    71.0      55.0      46.0   1.04
Ia_begin  0.844  0.838   0.009    2.628  ...    42.0      51.0      40.0   1.03
E_begin   0.455  0.455   0.002    1.395  ...    67.0      82.0      56.0   1.01

[8 rows x 11 columns]