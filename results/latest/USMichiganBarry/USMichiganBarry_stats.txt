0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1218.33    33.20
p_loo       31.02        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      355   92.4%
 (0.5, 0.7]   (ok)         26    6.8%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.257  0.063   0.150    0.341  ...    58.0      50.0      40.0   1.01
pu        0.795  0.028   0.749    0.845  ...    52.0      57.0      93.0   1.02
mu        0.121  0.022   0.090    0.161  ...    60.0      58.0      97.0   1.10
mus       0.171  0.030   0.118    0.221  ...    78.0      86.0      49.0   1.03
gamma     0.216  0.043   0.134    0.279  ...    62.0      85.0      49.0   1.02
Is_begin  0.927  0.571   0.071    1.805  ...   105.0     100.0      93.0   1.02
Ia_begin  1.918  1.855   0.063    5.884  ...    85.0      67.0      95.0   1.04
E_begin   0.858  0.760   0.018    2.218  ...    93.0     113.0      73.0   1.00

[8 rows x 11 columns]