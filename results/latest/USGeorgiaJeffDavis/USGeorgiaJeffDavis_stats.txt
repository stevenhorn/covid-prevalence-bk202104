0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1101.78    66.54
p_loo       49.16        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      364   94.8%
 (0.5, 0.7]   (ok)         16    4.2%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.240  0.056   0.153    0.331  ...    55.0      49.0      42.0   1.02
pu        0.777  0.018   0.739    0.810  ...    54.0      53.0      60.0   1.02
mu        0.160  0.019   0.121    0.191  ...     6.0       6.0      34.0   1.31
mus       0.233  0.034   0.179    0.290  ...    75.0      78.0      91.0   1.01
gamma     0.328  0.058   0.239    0.437  ...    70.0      68.0      79.0   0.99
Is_begin  0.522  0.504   0.005    1.647  ...    59.0      37.0      59.0   1.03
Ia_begin  1.166  1.101   0.013    2.947  ...    38.0      21.0      19.0   1.05
E_begin   0.464  0.559   0.001    1.537  ...    14.0       9.0      24.0   1.21

[8 rows x 11 columns]