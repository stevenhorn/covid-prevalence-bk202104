0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -890.85    30.98
p_loo       27.49        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      348   90.6%
 (0.5, 0.7]   (ok)         32    8.3%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.258  0.061   0.161    0.350  ...    93.0     107.0      60.0   0.99
pu        0.835  0.021   0.800    0.878  ...    74.0      79.0      49.0   1.03
mu        0.131  0.027   0.083    0.176  ...    34.0      34.0      60.0   1.06
mus       0.180  0.032   0.124    0.243  ...   152.0     152.0      96.0   1.01
gamma     0.213  0.039   0.150    0.267  ...    87.0      99.0      58.0   0.99
Is_begin  0.796  0.830   0.006    2.441  ...   138.0     152.0     100.0   0.99
Ia_begin  1.456  1.224   0.016    3.857  ...    77.0      71.0      74.0   1.00
E_begin   0.581  0.518   0.009    1.508  ...    45.0      20.0      75.0   1.09

[8 rows x 11 columns]