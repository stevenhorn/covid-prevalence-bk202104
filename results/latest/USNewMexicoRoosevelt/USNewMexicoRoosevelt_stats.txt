0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -958.36    33.14
p_loo       29.48        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      348   90.6%
 (0.5, 0.7]   (ok)         29    7.6%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.255  0.056   0.176    0.350  ...    64.0      67.0      59.0   1.01
pu        0.811  0.022   0.775    0.850  ...    11.0      12.0      53.0   1.13
mu        0.115  0.025   0.069    0.159  ...    16.0      14.0      59.0   1.10
mus       0.150  0.029   0.105    0.200  ...    73.0     107.0      88.0   1.04
gamma     0.162  0.033   0.109    0.218  ...   152.0     152.0      96.0   0.99
Is_begin  0.539  0.488   0.026    1.539  ...    63.0      68.0      58.0   1.04
Ia_begin  1.325  1.154   0.001    3.518  ...    80.0      83.0      59.0   1.01
E_begin   0.620  0.623   0.001    1.989  ...    73.0      77.0      60.0   1.01

[8 rows x 11 columns]