0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -841.42    37.77
p_loo       34.57        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      355   92.4%
 (0.5, 0.7]   (ok)         22    5.7%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.252  0.054   0.164    0.340  ...   100.0      95.0      59.0   0.99
pu        0.825  0.017   0.786    0.852  ...    61.0      71.0     100.0   1.02
mu        0.123  0.026   0.077    0.162  ...    24.0      22.0      24.0   1.09
mus       0.195  0.027   0.152    0.246  ...   152.0     152.0      74.0   1.06
gamma     0.218  0.039   0.144    0.281  ...   128.0     126.0     100.0   1.01
Is_begin  0.797  0.772   0.024    2.174  ...    45.0      33.0      58.0   1.08
Ia_begin  1.744  1.530   0.020    4.487  ...    95.0     102.0      80.0   1.00
E_begin   0.634  0.575   0.025    1.639  ...    70.0      63.0      59.0   0.99

[8 rows x 11 columns]