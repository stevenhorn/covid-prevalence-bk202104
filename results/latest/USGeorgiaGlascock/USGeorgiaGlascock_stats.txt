0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -613.12    48.69
p_loo       41.21        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      357   93.0%
 (0.5, 0.7]   (ok)         21    5.5%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    4    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.241  0.045   0.163    0.306  ...    59.0      58.0      59.0   1.04
pu        0.831  0.017   0.799    0.861  ...    51.0      54.0      95.0   1.04
mu        0.124  0.021   0.080    0.157  ...    78.0      88.0      47.0   1.02
mus       0.203  0.046   0.126    0.291  ...    66.0      73.0     100.0   1.03
gamma     0.270  0.060   0.171    0.394  ...    28.0      30.0      48.0   1.08
Is_begin  0.534  0.560   0.016    1.639  ...    41.0      34.0      84.0   1.06
Ia_begin  0.773  1.144   0.004    2.720  ...    94.0      50.0      20.0   1.03
E_begin   0.371  0.451   0.008    1.120  ...   112.0     104.0      59.0   0.99

[8 rows x 11 columns]