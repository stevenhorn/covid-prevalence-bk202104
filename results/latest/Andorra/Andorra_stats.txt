0 Divergences 
Passed validation 
Computed from 80 by 385 log-likelihood matrix

         Estimate       SE
elpd_loo -1700.24    45.84
p_loo       30.92        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      347   90.1%
 (0.5, 0.7]   (ok)         28    7.3%
   (0.7, 1]   (bad)         9    2.3%
   (1, Inf)   (very bad)    1    0.3%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.189   0.032   0.151    0.244  ...    86.0      61.0      59.0   1.11
pu         0.710   0.009   0.700    0.728  ...   101.0      68.0      57.0   1.01
mu         0.130   0.021   0.096    0.163  ...    12.0      11.0      40.0   1.16
mus        0.216   0.033   0.165    0.288  ...    84.0      83.0      60.0   1.01
gamma      0.304   0.050   0.216    0.395  ...    92.0     105.0      88.0   1.00
Is_begin  19.412  13.942   0.013   39.471  ...    10.0       8.0      14.0   1.22
Ia_begin  56.135  25.999   9.944   99.604  ...    97.0      87.0      46.0   0.99
E_begin   40.583  25.569   5.460   79.349  ...   128.0      98.0      49.0   1.02

[8 rows x 11 columns]