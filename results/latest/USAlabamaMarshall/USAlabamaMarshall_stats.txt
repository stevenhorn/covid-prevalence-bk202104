0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1693.80    46.54
p_loo       37.38        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      342   89.1%
 (0.5, 0.7]   (ok)         36    9.4%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.225  0.048   0.156    0.310  ...   152.0     152.0      60.0   0.98
pu        0.728  0.018   0.700    0.757  ...   102.0      80.0      22.0   1.00
mu        0.114  0.017   0.086    0.146  ...    39.0      33.0      38.0   1.05
mus       0.186  0.026   0.138    0.233  ...    42.0      45.0      33.0   1.03
gamma     0.250  0.048   0.176    0.354  ...   152.0     152.0      80.0   0.99
Is_begin  1.053  1.009   0.015    2.781  ...   108.0      89.0      41.0   1.01
Ia_begin  2.097  1.708   0.260    5.897  ...    60.0      55.0      59.0   1.01
E_begin   1.235  1.132   0.099    3.063  ...    72.0      58.0      74.0   1.03

[8 rows x 11 columns]