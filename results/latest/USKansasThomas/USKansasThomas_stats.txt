0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -953.99    29.72
p_loo       16.61        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      372   96.9%
 (0.5, 0.7]   (ok)         10    2.6%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.168  0.013   0.154    0.194  ...    21.0      25.0      58.0   1.04
pu        0.705  0.004   0.701    0.712  ...    16.0      12.0      20.0   1.14
mu        0.152  0.050   0.103    0.279  ...     4.0       5.0      15.0   1.46
mus       0.157  0.030   0.103    0.204  ...     5.0       6.0      14.0   1.33
gamma     0.067  0.015   0.046    0.101  ...     3.0       4.0      15.0   1.81
Is_begin  0.140  0.209   0.005    0.459  ...    26.0      10.0      18.0   1.16
Ia_begin  0.768  0.601   0.109    2.002  ...     3.0       4.0      18.0   1.76
E_begin   0.245  0.259   0.026    0.504  ...    24.0      24.0      39.0   1.06

[8 rows x 11 columns]