0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -884.97    25.09
p_loo       22.96        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      365   95.1%
 (0.5, 0.7]   (ok)         15    3.9%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.277  0.041   0.211    0.348  ...    28.0      27.0      33.0   1.08
pu        0.860  0.024   0.803    0.894  ...     9.0       7.0      17.0   1.26
mu        0.137  0.024   0.095    0.175  ...     6.0       6.0      25.0   1.29
mus       0.164  0.030   0.119    0.220  ...    15.0      16.0      47.0   1.10
gamma     0.189  0.032   0.140    0.256  ...    37.0      38.0      39.0   1.03
Is_begin  0.966  0.658   0.197    2.620  ...    71.0      71.0      56.0   0.99
Ia_begin  2.653  2.465   0.409    7.602  ...    38.0      58.0      57.0   1.02
E_begin   1.416  1.449   0.027    4.707  ...    41.0      16.0      22.0   1.13

[8 rows x 11 columns]