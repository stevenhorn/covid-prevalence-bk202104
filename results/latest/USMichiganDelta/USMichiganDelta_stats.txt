0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1100.48    32.94
p_loo       38.74        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      349   90.9%
 (0.5, 0.7]   (ok)         24    6.2%
   (0.7, 1]   (bad)        11    2.9%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.219  0.042   0.162    0.307  ...    67.0      70.0      33.0   1.03
pu        0.733  0.020   0.705    0.762  ...    96.0     106.0      83.0   1.00
mu        0.109  0.018   0.079    0.143  ...    36.0      35.0      53.0   1.08
mus       0.184  0.032   0.118    0.241  ...    63.0      68.0      72.0   1.06
gamma     0.226  0.039   0.161    0.294  ...    88.0     108.0      60.0   0.99
Is_begin  0.778  0.612   0.049    1.859  ...    72.0      69.0      60.0   1.01
Ia_begin  1.290  1.428   0.005    3.468  ...    85.0      42.0      14.0   1.04
E_begin   0.656  0.629   0.005    1.752  ...    69.0      55.0     100.0   1.02

[8 rows x 11 columns]