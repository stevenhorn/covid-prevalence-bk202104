0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -897.78    51.80
p_loo       39.59        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      360   93.8%
 (0.5, 0.7]   (ok)         20    5.2%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.244  0.061   0.151    0.337  ...    74.0      81.0      38.0   0.98
pu        0.817  0.022   0.783    0.859  ...    63.0      66.0      46.0   1.00
mu        0.132  0.019   0.101    0.173  ...    75.0      77.0      16.0   1.05
mus       0.200  0.038   0.133    0.253  ...    93.0      96.0      87.0   1.03
gamma     0.234  0.050   0.157    0.340  ...    40.0      41.0      44.0   1.05
Is_begin  0.656  0.659   0.005    1.932  ...   137.0      57.0      55.0   1.03
Ia_begin  1.218  1.156   0.043    3.679  ...   112.0     107.0      99.0   1.00
E_begin   0.595  0.593   0.013    1.694  ...    15.0      13.0      40.0   1.14

[8 rows x 11 columns]