0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1478.08    35.55
p_loo       25.25        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      357   93.0%
 (0.5, 0.7]   (ok)         22    5.7%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.250  0.050   0.164    0.333  ...    80.0      81.0      46.0   1.06
pu        0.761  0.032   0.706    0.818  ...    33.0      32.0      60.0   1.03
mu        0.121  0.026   0.084    0.170  ...     9.0       8.0      92.0   1.21
mus       0.159  0.034   0.112    0.215  ...   116.0      88.0      40.0   1.08
gamma     0.200  0.033   0.158    0.272  ...   130.0     141.0     100.0   1.05
Is_begin  1.252  0.808   0.111    2.702  ...    68.0      64.0      60.0   0.98
Ia_begin  3.456  2.192   0.317    7.627  ...   106.0     122.0      88.0   1.01
E_begin   2.042  1.905   0.029    5.418  ...    56.0      74.0      58.0   1.00

[8 rows x 11 columns]