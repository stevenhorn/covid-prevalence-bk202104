0 Divergences 
Passed validation 
Computed from 80 by 385 log-likelihood matrix

         Estimate       SE
elpd_loo -1808.53    57.69
p_loo       58.20        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      375   97.4%
 (0.5, 0.7]   (ok)          3    0.8%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    4    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.289  0.055   0.183    0.343  ...     3.0       3.0      20.0   2.17
pu        0.889  0.002   0.885    0.893  ...     4.0       4.0      22.0   1.65
mu        0.243  0.007   0.234    0.253  ...     3.0       3.0      14.0   2.46
mus       0.249  0.013   0.226    0.271  ...    10.0      11.0      14.0   1.18
gamma     0.217  0.013   0.201    0.235  ...     3.0       3.0      22.0   2.03
Is_begin  0.617  0.385   0.090    1.218  ...     3.0       3.0      17.0   2.07
Ia_begin  1.932  0.774   0.999    3.182  ...     3.0       3.0      18.0   2.36
E_begin   0.917  0.204   0.487    1.223  ...    16.0      16.0      14.0   1.02

[8 rows x 11 columns]