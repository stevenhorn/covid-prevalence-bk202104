0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -2788.35    41.25
p_loo       43.80        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      342   89.1%
 (0.5, 0.7]   (ok)         32    8.3%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.241  0.056   0.154    0.329  ...    77.0      77.0      24.0   1.01
pu        0.764  0.031   0.707    0.811  ...    49.0      53.0      43.0   1.04
mu        0.155  0.031   0.105    0.213  ...    21.0      21.0      60.0   1.04
mus       0.216  0.038   0.148    0.275  ...    38.0      43.0      60.0   1.01
gamma     0.283  0.049   0.211    0.376  ...   146.0     120.0      97.0   1.00
Is_begin  2.673  2.656   0.007    7.192  ...    71.0     152.0      43.0   1.01
Ia_begin  3.920  4.453   0.003   11.412  ...    50.0      27.0      22.0   1.07
E_begin   2.331  2.559   0.035    6.392  ...    75.0     105.0      72.0   1.03

[8 rows x 11 columns]