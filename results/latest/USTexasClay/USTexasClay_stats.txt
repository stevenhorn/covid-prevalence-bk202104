0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -911.09    31.88
p_loo       27.43        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      359   93.5%
 (0.5, 0.7]   (ok)         19    4.9%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.257  0.049   0.171    0.348  ...    16.0      17.0      51.0   1.12
pu        0.781  0.021   0.746    0.820  ...    11.0      11.0      59.0   1.16
mu        0.130  0.024   0.089    0.177  ...    79.0      80.0      93.0   0.98
mus       0.162  0.029   0.112    0.214  ...    56.0      78.0      60.0   1.01
gamma     0.173  0.038   0.111    0.245  ...    79.0      83.0      69.0   1.01
Is_begin  0.642  0.678   0.002    2.272  ...    53.0      49.0      69.0   1.01
Ia_begin  1.367  2.047   0.005    4.641  ...    61.0       7.0      14.0   1.22
E_begin   0.528  0.767   0.000    1.815  ...    92.0      26.0      40.0   1.05

[8 rows x 11 columns]