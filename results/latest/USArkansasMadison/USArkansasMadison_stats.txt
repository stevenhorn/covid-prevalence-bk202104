0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -894.25    34.39
p_loo       33.99        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      367   95.6%
 (0.5, 0.7]   (ok)         15    3.9%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.246  0.055   0.153    0.326  ...    65.0      70.0      59.0   1.03
pu        0.798  0.030   0.747    0.842  ...    13.0      14.0      24.0   1.15
mu        0.130  0.019   0.090    0.160  ...    66.0      66.0      59.0   1.01
mus       0.193  0.038   0.108    0.256  ...    74.0     101.0      53.0   0.99
gamma     0.233  0.047   0.162    0.311  ...   120.0     127.0      46.0   1.02
Is_begin  0.494  0.615   0.001    1.405  ...    73.0      72.0      66.0   1.02
Ia_begin  0.725  0.808   0.004    2.123  ...    51.0      51.0      59.0   1.07
E_begin   0.322  0.412   0.008    1.304  ...   121.0     152.0     100.0   0.98

[8 rows x 11 columns]