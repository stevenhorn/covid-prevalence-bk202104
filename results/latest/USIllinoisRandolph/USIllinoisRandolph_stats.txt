0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1177.56    23.79
p_loo       24.24        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      352   91.7%
 (0.5, 0.7]   (ok)         29    7.6%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.245  0.050   0.154    0.327  ...    69.0      69.0      80.0   1.07
pu        0.732  0.022   0.706    0.782  ...    58.0      45.0      54.0   1.07
mu        0.140  0.019   0.114    0.178  ...    51.0      64.0      65.0   1.03
mus       0.182  0.025   0.127    0.223  ...   105.0      98.0      55.0   0.99
gamma     0.254  0.040   0.189    0.324  ...    93.0      86.0      59.0   1.02
Is_begin  0.990  0.873   0.008    2.368  ...   102.0      68.0      59.0   1.08
Ia_begin  1.625  1.262   0.030    3.822  ...   130.0     113.0      60.0   0.98
E_begin   1.163  1.209   0.004    3.435  ...    99.0      93.0      96.0   0.99

[8 rows x 11 columns]