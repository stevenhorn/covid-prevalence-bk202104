0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -628.82    26.56
p_loo       27.94        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      361   94.0%
 (0.5, 0.7]   (ok)         18    4.7%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.247  0.049   0.178    0.344  ...   120.0     105.0      68.0   1.00
pu        0.861  0.021   0.824    0.897  ...    57.0      39.0      95.0   1.05
mu        0.148  0.027   0.098    0.189  ...    53.0      49.0      59.0   0.99
mus       0.194  0.042   0.114    0.264  ...    98.0      99.0      59.0   1.04
gamma     0.227  0.057   0.128    0.340  ...   152.0     152.0      93.0   1.07
Is_begin  0.975  0.902   0.031    2.600  ...   152.0     152.0      96.0   0.99
Ia_begin  1.817  1.320   0.039    4.216  ...    66.0      34.0      91.0   1.05
E_begin   1.022  1.061   0.028    3.211  ...   105.0      60.0      97.0   1.03

[8 rows x 11 columns]