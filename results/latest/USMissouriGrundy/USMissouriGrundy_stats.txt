0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1021.03    70.35
p_loo       41.93        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      369   96.1%
 (0.5, 0.7]   (ok)         11    2.9%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.171  0.021   0.150    0.220  ...    84.0      56.0      27.0   1.01
pu        0.706  0.006   0.700    0.720  ...    77.0      40.0      29.0   1.00
mu        0.107  0.014   0.087    0.136  ...    28.0      22.0      59.0   1.08
mus       0.231  0.034   0.176    0.312  ...    79.0     130.0      59.0   1.04
gamma     0.444  0.072   0.330    0.579  ...    60.0      95.0      60.0   1.05
Is_begin  0.299  0.413   0.005    1.367  ...    16.0      26.0      93.0   1.10
Ia_begin  0.381  0.441   0.001    1.375  ...   111.0      83.0      71.0   1.02
E_begin   0.206  0.272   0.001    0.798  ...    69.0      47.0      37.0   1.05

[8 rows x 11 columns]