0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -547.46    46.92
p_loo       39.91        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      349   90.9%
 (0.5, 0.7]   (ok)         27    7.0%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.257  0.044   0.183    0.341  ...    76.0      74.0      59.0   0.99
pu        0.773  0.025   0.729    0.816  ...    89.0      93.0      54.0   1.05
mu        0.124  0.022   0.078    0.162  ...    88.0      91.0      59.0   1.01
mus       0.177  0.026   0.130    0.225  ...   121.0     109.0      93.0   1.04
gamma     0.266  0.043   0.200    0.352  ...    83.0      97.0     100.0   1.00
Is_begin  0.378  0.377   0.031    0.940  ...   118.0     133.0      93.0   0.99
Ia_begin  0.677  0.797   0.003    2.208  ...    29.0      52.0      22.0   1.02
E_begin   0.369  0.426   0.001    1.210  ...    36.0      67.0      40.0   1.06

[8 rows x 11 columns]