0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1448.67    36.38
p_loo       24.29        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      339   88.3%
 (0.5, 0.7]   (ok)         40   10.4%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.242  0.051   0.168    0.331  ...   152.0     152.0     100.0   1.00
pu        0.794  0.044   0.709    0.854  ...    43.0      51.0      30.0   0.99
mu        0.118  0.025   0.075    0.163  ...    47.0      45.0      59.0   1.12
mus       0.172  0.030   0.122    0.234  ...    55.0      58.0      55.0   1.07
gamma     0.195  0.036   0.140    0.266  ...    59.0      52.0      57.0   0.99
Is_begin  1.661  1.427   0.057    4.491  ...   101.0      92.0      43.0   0.99
Ia_begin  4.275  3.309   0.016   10.248  ...    74.0      67.0      31.0   1.02
E_begin   3.587  3.317   0.069   10.859  ...    77.0      49.0      40.0   1.03

[8 rows x 11 columns]