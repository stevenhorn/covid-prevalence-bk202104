0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1551.56    41.81
p_loo       34.92        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      358   93.2%
 (0.5, 0.7]   (ok)         17    4.4%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.258  0.054   0.162    0.341  ...    40.0      40.0      60.0   1.03
pu        0.770  0.022   0.725    0.803  ...    31.0      30.0      40.0   1.06
mu        0.112  0.017   0.086    0.140  ...    41.0      44.0     100.0   0.98
mus       0.195  0.032   0.136    0.256  ...   132.0     135.0      93.0   1.00
gamma     0.230  0.040   0.165    0.308  ...   124.0     138.0      60.0   1.00
Is_begin  0.514  0.544   0.002    1.520  ...   110.0      74.0      17.0   0.99
Ia_begin  0.804  1.034   0.006    3.666  ...   109.0     112.0     100.0   0.98
E_begin   0.389  0.430   0.009    1.076  ...   106.0      99.0      40.0   1.00

[8 rows x 11 columns]