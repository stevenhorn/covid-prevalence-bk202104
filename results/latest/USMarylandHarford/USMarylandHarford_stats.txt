0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1419.31    19.37
p_loo       25.54        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      350   91.1%
 (0.5, 0.7]   (ok)         27    7.0%
   (0.7, 1]   (bad)         7    1.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.254  0.055   0.164    0.333  ...    78.0      73.0      60.0   1.00
pu        0.774  0.048   0.701    0.849  ...    26.0      25.0      40.0   1.06
mu        0.115  0.016   0.091    0.150  ...    63.0      57.0      24.0   1.16
mus       0.183  0.035   0.127    0.237  ...   112.0     111.0      95.0   1.01
gamma     0.269  0.047   0.207    0.376  ...    58.0      78.0      74.0   1.03
Is_begin  1.728  1.074   0.118    3.668  ...   138.0     140.0      68.0   0.98
Ia_begin  0.685  0.614   0.003    1.703  ...    71.0      48.0      40.0   0.98
E_begin   1.305  1.201   0.002    3.860  ...    62.0      32.0      15.0   1.03

[8 rows x 11 columns]