0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1210.94    31.32
p_loo       27.09        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      356   92.7%
 (0.5, 0.7]   (ok)         25    6.5%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.255  0.057   0.166    0.337  ...    87.0     101.0     100.0   0.98
pu        0.868  0.014   0.838    0.887  ...    60.0      59.0      66.0   1.02
mu        0.135  0.023   0.094    0.178  ...     6.0       6.0      32.0   1.35
mus       0.159  0.027   0.110    0.203  ...    76.0      79.0      59.0   1.01
gamma     0.157  0.040   0.081    0.218  ...    22.0      27.0      60.0   1.07
Is_begin  1.136  0.884   0.012    2.693  ...   139.0     148.0      91.0   0.98
Ia_begin  2.291  1.942   0.011    5.924  ...    60.0      55.0     100.0   1.08
E_begin   1.120  0.931   0.050    2.862  ...    35.0      32.0      60.0   1.02

[8 rows x 11 columns]