0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1058.78    36.01
p_loo       24.77        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      351   91.4%
 (0.5, 0.7]   (ok)         25    6.5%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.239  0.048   0.155    0.312  ...   108.0     100.0     100.0   1.04
pu        0.799  0.022   0.765    0.841  ...    24.0      22.0      56.0   1.08
mu        0.119  0.024   0.089    0.157  ...    76.0      65.0      48.0   0.99
mus       0.165  0.032   0.112    0.220  ...    64.0      80.0      96.0   1.02
gamma     0.186  0.043   0.108    0.268  ...    86.0      84.0     100.0   1.04
Is_begin  0.801  0.665   0.024    2.149  ...    71.0      81.0      23.0   1.06
Ia_begin  1.426  1.518   0.036    4.272  ...    95.0      76.0     100.0   1.05
E_begin   0.644  0.748   0.003    1.806  ...    95.0      78.0      51.0   0.99

[8 rows x 11 columns]