41 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1522.41    39.61
p_loo       23.18        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      278   72.4%
 (0.5, 0.7]   (ok)         14    3.6%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)   86   22.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.223  0.044   0.151    0.312  ...    10.0      17.0      25.0   2.22
pu        0.816  0.028   0.752    0.855  ...    28.0      48.0      48.0   2.22
mu        0.159  0.030   0.100    0.186  ...     3.0       4.0      23.0   3.30
mus       0.181  0.021   0.143    0.238  ...    83.0      95.0      76.0   2.22
gamma     0.292  0.050   0.198    0.341  ...     4.0       4.0      39.0   2.23
Is_begin  0.797  0.352   0.313    1.721  ...   107.0      99.0      80.0   2.23
Ia_begin  2.149  0.728   0.890    3.688  ...   102.0     120.0      86.0   2.22
E_begin   2.078  0.856   0.436    3.478  ...    29.0      52.0      38.0   2.27

[8 rows x 11 columns]