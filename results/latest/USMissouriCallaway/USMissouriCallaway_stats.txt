0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1373.71    70.19
p_loo       45.88        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      359   93.5%
 (0.5, 0.7]   (ok)         18    4.7%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.197  0.031   0.152    0.255  ...    89.0      90.0     100.0   1.00
pu        0.713  0.012   0.700    0.733  ...   115.0      89.0      56.0   1.01
mu        0.105  0.015   0.079    0.130  ...    46.0      46.0      30.0   1.00
mus       0.216  0.036   0.157    0.292  ...    71.0      76.0      56.0   1.02
gamma     0.317  0.061   0.201    0.407  ...    93.0     104.0     100.0   1.01
Is_begin  0.707  0.771   0.023    2.414  ...   119.0     102.0      59.0   1.00
Ia_begin  1.186  1.071   0.030    3.379  ...   127.0     152.0     100.0   1.00
E_begin   0.541  0.557   0.002    1.761  ...   124.0      76.0      53.0   1.00

[8 rows x 11 columns]