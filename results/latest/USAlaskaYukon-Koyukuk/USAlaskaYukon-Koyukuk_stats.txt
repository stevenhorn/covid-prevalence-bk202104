0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -749.37    56.93
p_loo       43.11        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      357   93.0%
 (0.5, 0.7]   (ok)         19    4.9%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.254  0.050   0.180    0.345  ...    55.0      56.0      53.0   1.01
pu        0.832  0.023   0.787    0.864  ...    39.0      43.0      38.0   0.99
mu        0.125  0.025   0.081    0.171  ...    11.0      12.0      35.0   1.12
mus       0.224  0.046   0.150    0.299  ...    34.0      38.0      39.0   1.03
gamma     0.286  0.060   0.191    0.384  ...    62.0      67.0      59.0   1.02
Is_begin  0.872  0.854   0.017    2.383  ...    42.0      31.0      43.0   1.05
Ia_begin  1.445  1.655   0.009    4.778  ...    67.0      84.0      72.0   1.04
E_begin   0.614  0.762   0.004    2.071  ...    79.0      53.0      53.0   1.07

[8 rows x 11 columns]