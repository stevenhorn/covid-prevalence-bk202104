0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1211.85    31.27
p_loo       30.80        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      362   94.3%
 (0.5, 0.7]   (ok)         17    4.4%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.258  0.056   0.163    0.348  ...    71.0      85.0      40.0   1.06
pu        0.800  0.024   0.761    0.835  ...    23.0      33.0      58.0   1.06
mu        0.122  0.025   0.083    0.166  ...     9.0       9.0      57.0   1.18
mus       0.191  0.034   0.140    0.252  ...    66.0      83.0      97.0   1.01
gamma     0.212  0.045   0.152    0.311  ...    37.0      45.0      60.0   1.05
Is_begin  0.783  0.716   0.002    2.350  ...    62.0      48.0      59.0   1.05
Ia_begin  1.285  1.455   0.003    3.938  ...    94.0      67.0      57.0   1.00
E_begin   0.594  0.538   0.002    1.736  ...    70.0      46.0      43.0   1.02

[8 rows x 11 columns]