0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1806.29    71.12
p_loo       26.90        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      352   91.7%
 (0.5, 0.7]   (ok)         14    3.6%
   (0.7, 1]   (bad)        14    3.6%
   (1, Inf)   (very bad)    4    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.181  0.029   0.152    0.222  ...     3.0       3.0      24.0   1.93
pu        0.711  0.004   0.706    0.717  ...     3.0       3.0      23.0   2.73
mu        0.158  0.016   0.140    0.178  ...     3.0       3.0      21.0   1.92
mus       0.177  0.011   0.164    0.191  ...     3.0       3.0      14.0   2.15
gamma     0.252  0.033   0.209    0.295  ...     3.0       3.0      32.0   3.03
Is_begin  0.899  0.241   0.511    1.281  ...     3.0       3.0      20.0   2.31
Ia_begin  0.331  0.094   0.192    0.457  ...     3.0       3.0      18.0   2.06
E_begin   0.468  0.037   0.398    0.517  ...    11.0      11.0      39.0   1.13

[8 rows x 11 columns]