0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1847.04    27.63
p_loo       30.39        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      357   93.0%
 (0.5, 0.7]   (ok)         24    6.2%
   (0.7, 1]   (bad)         2    0.5%
   (1, Inf)   (very bad)    1    0.3%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.220   0.044   0.151    0.307  ...    68.0      57.0      30.0   1.00
pu         0.743   0.033   0.702    0.812  ...    30.0      27.0      41.0   1.05
mu         0.112   0.019   0.080    0.149  ...    27.0      24.0      27.0   1.08
mus        0.178   0.032   0.136    0.259  ...   152.0     152.0      60.0   0.99
gamma      0.298   0.050   0.218    0.390  ...   107.0     120.0      43.0   1.01
Is_begin   8.428   4.859   0.252   16.500  ...   128.0     111.0      40.0   1.01
Ia_begin  24.824   9.318   6.772   41.624  ...   127.0     145.0      24.0   1.01
E_begin   39.721  24.745   1.075   89.165  ...    76.0      68.0      86.0   0.99

[8 rows x 11 columns]