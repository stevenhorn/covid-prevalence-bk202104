0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -565.21    26.14
p_loo       24.28        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      353   91.9%
 (0.5, 0.7]   (ok)         26    6.8%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.258  0.054   0.171    0.347  ...   103.0     106.0      60.0   1.02
pu        0.805  0.023   0.767    0.849  ...    70.0      78.0      59.0   1.04
mu        0.117  0.023   0.079    0.161  ...    86.0      81.0      88.0   1.01
mus       0.181  0.031   0.133    0.244  ...    95.0     110.0      60.0   1.10
gamma     0.203  0.039   0.143    0.283  ...    84.0      76.0      78.0   1.04
Is_begin  0.311  0.351   0.002    1.042  ...    98.0      71.0      43.0   1.03
Ia_begin  0.554  0.782   0.003    1.813  ...    97.0      83.0      60.0   1.00
E_begin   0.175  0.197   0.000    0.543  ...   100.0      75.0      42.0   1.00

[8 rows x 11 columns]