0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1172.21    68.50
p_loo       50.00        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      368   95.8%
 (0.5, 0.7]   (ok)         13    3.4%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.254  0.055   0.174    0.348  ...   100.0     111.0      40.0   1.00
pu        0.780  0.035   0.716    0.836  ...    16.0      17.0      60.0   1.12
mu        0.135  0.031   0.086    0.197  ...    10.0      12.0      36.0   1.15
mus       0.225  0.037   0.164    0.287  ...    64.0      55.0      42.0   1.05
gamma     0.279  0.051   0.193    0.366  ...    74.0      69.0      60.0   1.00
Is_begin  0.656  0.668   0.010    1.880  ...   104.0     100.0      87.0   1.06
Ia_begin  1.013  1.171   0.027    3.570  ...    70.0      59.0      42.0   1.01
E_begin   0.411  0.534   0.000    1.403  ...   107.0      67.0      60.0   1.02

[8 rows x 11 columns]