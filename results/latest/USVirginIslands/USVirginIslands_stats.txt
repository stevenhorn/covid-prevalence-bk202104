1 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1330.55    41.62
p_loo       37.13        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      355   92.4%
 (0.5, 0.7]   (ok)         16    4.2%
   (0.7, 1]   (bad)        11    2.9%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.283  0.045   0.199    0.348  ...    66.0      70.0      60.0   1.01
pu        0.886  0.013   0.857    0.900  ...    94.0      74.0      59.0   0.99
mu        0.162  0.029   0.121    0.220  ...     4.0       4.0      26.0   1.59
mus       0.178  0.035   0.120    0.252  ...    67.0      71.0      60.0   1.02
gamma     0.245  0.039   0.176    0.309  ...    72.0      73.0      77.0   0.99
Is_begin  0.718  0.600   0.003    1.922  ...    32.0      18.0      17.0   1.09
Ia_begin  1.968  1.615   0.084    5.434  ...   103.0      86.0      60.0   1.02
E_begin   0.976  1.064   0.023    2.879  ...    73.0      42.0      96.0   1.04

[8 rows x 11 columns]