0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -811.75    28.59
p_loo       26.79        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      357   93.0%
 (0.5, 0.7]   (ok)         20    5.2%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.258  0.055   0.165    0.344  ...    59.0      61.0      58.0   1.02
pu        0.824  0.019   0.785    0.852  ...    46.0      49.0      58.0   1.08
mu        0.139  0.026   0.098    0.183  ...    12.0      11.0      59.0   1.13
mus       0.204  0.040   0.135    0.280  ...    42.0      66.0      60.0   1.04
gamma     0.242  0.041   0.162    0.303  ...    93.0      92.0      93.0   1.01
Is_begin  0.746  0.617   0.007    2.087  ...    78.0      74.0      69.0   1.01
Ia_begin  1.492  1.359   0.001    4.025  ...    67.0      60.0      22.0   1.04
E_begin   0.781  0.827   0.007    2.473  ...    66.0      61.0      59.0   1.00

[8 rows x 11 columns]