0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -946.09    52.42
p_loo       40.18        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      367   95.6%
 (0.5, 0.7]   (ok)         13    3.4%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.258  0.056   0.163    0.332  ...    90.0      80.0     100.0   0.99
pu        0.772  0.047   0.703    0.845  ...    17.0      16.0      33.0   1.08
mu        0.137  0.026   0.097    0.192  ...    45.0      47.0      60.0   1.00
mus       0.187  0.044   0.104    0.264  ...   108.0      82.0      16.0   1.01
gamma     0.256  0.046   0.186    0.361  ...   141.0     152.0      79.0   1.03
Is_begin  0.669  0.622   0.016    2.105  ...   109.0      95.0      93.0   0.99
Ia_begin  1.519  1.931   0.060    4.452  ...    95.0      99.0      99.0   1.00
E_begin   0.755  1.017   0.001    2.989  ...    98.0      39.0      58.0   1.03

[8 rows x 11 columns]