0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -827.20    48.08
p_loo       33.20        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      354   92.2%
 (0.5, 0.7]   (ok)         24    6.2%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.258  0.049   0.173    0.331  ...    44.0      47.0      60.0   1.06
pu        0.806  0.019   0.776    0.840  ...    18.0      17.0      23.0   1.09
mu        0.157  0.031   0.105    0.212  ...     6.0       7.0      25.0   1.27
mus       0.238  0.039   0.167    0.300  ...    34.0      53.0      32.0   1.12
gamma     0.296  0.062   0.186    0.387  ...   152.0     152.0      67.0   1.03
Is_begin  0.739  0.798   0.002    2.564  ...    82.0      75.0      61.0   1.07
Ia_begin  1.053  1.085   0.000    2.962  ...    74.0      26.0      22.0   1.07
E_begin   0.543  0.532   0.020    1.751  ...    42.0      43.0      57.0   1.03

[8 rows x 11 columns]