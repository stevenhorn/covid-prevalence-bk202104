0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -967.56    36.45
p_loo       31.68        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      352   91.7%
 (0.5, 0.7]   (ok)         27    7.0%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    2    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.264  0.049   0.165    0.334  ...    33.0      41.0      83.0   1.06
pu        0.833  0.018   0.797    0.863  ...    52.0      54.0      31.0   1.03
mu        0.138  0.026   0.097    0.180  ...    23.0      25.0      25.0   1.07
mus       0.195  0.038   0.131    0.269  ...   128.0     129.0      38.0   1.03
gamma     0.230  0.047   0.163    0.327  ...   152.0     152.0      93.0   0.99
Is_begin  1.011  1.059   0.017    2.747  ...    91.0      61.0      39.0   1.01
Ia_begin  1.916  1.929   0.023    6.128  ...    60.0      27.0      59.0   1.08
E_begin   0.912  0.970   0.022    2.689  ...    61.0      26.0      40.0   1.06

[8 rows x 11 columns]