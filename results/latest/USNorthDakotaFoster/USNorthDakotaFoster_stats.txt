0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -676.30    35.50
p_loo       38.70        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      360   93.8%
 (0.5, 0.7]   (ok)         16    4.2%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    3    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.212  0.043   0.151    0.290  ...    64.0      52.0      59.0   1.03
pu        0.721  0.014   0.700    0.748  ...    95.0     101.0     100.0   0.98
mu        0.102  0.018   0.070    0.137  ...    79.0      91.0      43.0   1.06
mus       0.212  0.036   0.148    0.281  ...   152.0     152.0      61.0   1.12
gamma     0.268  0.048   0.167    0.341  ...   152.0     152.0     100.0   1.00
Is_begin  0.392  0.483   0.004    1.514  ...    59.0      97.0      76.0   1.01
Ia_begin  0.624  0.717   0.008    1.787  ...    82.0      29.0      15.0   1.05
E_begin   0.272  0.324   0.002    0.747  ...    64.0      96.0      83.0   1.03

[8 rows x 11 columns]