0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -981.64    39.68
p_loo       34.80        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      363   94.5%
 (0.5, 0.7]   (ok)         16    4.2%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.265  0.050   0.167    0.330  ...    95.0      97.0      59.0   1.01
pu        0.869  0.019   0.840    0.898  ...    62.0      53.0      43.0   1.08
mu        0.132  0.029   0.073    0.175  ...    52.0      49.0      36.0   1.01
mus       0.183  0.026   0.142    0.240  ...    69.0      70.0      93.0   1.02
gamma     0.239  0.042   0.161    0.308  ...   102.0     103.0      18.0   0.99
Is_begin  0.932  0.727   0.050    2.598  ...   118.0     102.0      57.0   0.98
Ia_begin  1.687  1.294   0.122    3.756  ...    49.0      60.0      59.0   1.00
E_begin   0.866  0.900   0.051    2.119  ...    82.0      81.0      96.0   0.99

[8 rows x 11 columns]