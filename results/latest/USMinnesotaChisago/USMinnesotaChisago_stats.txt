0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1086.74    32.14
p_loo       24.77        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      355   92.4%
 (0.5, 0.7]   (ok)         26    6.8%
   (0.7, 1]   (bad)         3    0.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.267  0.052   0.185    0.349  ...    48.0      44.0      38.0   1.03
pu        0.803  0.025   0.762    0.843  ...     8.0       9.0      14.0   1.18
mu        0.108  0.022   0.066    0.144  ...     8.0       7.0      54.0   1.29
mus       0.171  0.033   0.100    0.214  ...    67.0      66.0      86.0   1.05
gamma     0.183  0.037   0.124    0.270  ...    85.0     105.0      57.0   1.00
Is_begin  0.725  0.620   0.010    1.766  ...    80.0      67.0      24.0   1.01
Ia_begin  1.807  2.074   0.002    5.308  ...    72.0      29.0      40.0   1.07
E_begin   0.649  0.699   0.001    2.328  ...    40.0      43.0      59.0   1.06

[8 rows x 11 columns]