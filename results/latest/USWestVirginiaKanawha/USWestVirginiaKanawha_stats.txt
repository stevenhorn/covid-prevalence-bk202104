0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1440.90    25.76
p_loo       26.98        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      346   90.1%
 (0.5, 0.7]   (ok)         32    8.3%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.248  0.055   0.166    0.339  ...   114.0     143.0      78.0   1.03
pu        0.766  0.033   0.708    0.816  ...    48.0      48.0      34.0   1.03
mu        0.129  0.019   0.093    0.157  ...    20.0      22.0      61.0   1.07
mus       0.177  0.035   0.123    0.233  ...    93.0     118.0      59.0   1.02
gamma     0.232  0.040   0.156    0.306  ...   111.0     128.0      93.0   0.99
Is_begin  1.537  1.099   0.175    3.780  ...   139.0      78.0      16.0   1.09
Ia_begin  0.791  0.561   0.033    1.859  ...    87.0      74.0      40.0   0.99
E_begin   1.234  1.125   0.097    3.443  ...   106.0     152.0      81.0   1.00

[8 rows x 11 columns]