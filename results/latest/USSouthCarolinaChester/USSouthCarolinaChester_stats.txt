0 Divergences 
Failed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1155.58    25.69
p_loo       20.42        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      347   90.4%
 (0.5, 0.7]   (ok)         31    8.1%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.256  0.052   0.167    0.327  ...    23.0      20.0      91.0   1.09
pu        0.759  0.025   0.711    0.800  ...     7.0       7.0      24.0   1.28
mu        0.114  0.022   0.082    0.156  ...    10.0       9.0      37.0   1.20
mus       0.147  0.029   0.094    0.187  ...    95.0      75.0      45.0   1.00
gamma     0.152  0.029   0.103    0.208  ...    91.0      95.0      60.0   0.99
Is_begin  0.825  0.745   0.002    2.180  ...    48.0      30.0      59.0   1.05
Ia_begin  1.635  1.706   0.013    5.567  ...    40.0      38.0      57.0   1.08
E_begin   0.870  1.278   0.001    2.315  ...    85.0      97.0      83.0   1.00

[8 rows x 11 columns]