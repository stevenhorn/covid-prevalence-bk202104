0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1202.36    31.19
p_loo       25.01        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      348   90.6%
 (0.5, 0.7]   (ok)         31    8.1%
   (0.7, 1]   (bad)         5    1.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.245  0.050   0.155    0.329  ...   141.0     152.0      43.0   1.03
pu        0.798  0.026   0.743    0.842  ...    57.0      79.0      93.0   1.14
mu        0.127  0.025   0.085    0.172  ...    43.0      54.0      38.0   1.23
mus       0.175  0.032   0.118    0.220  ...    74.0     122.0      36.0   1.10
gamma     0.227  0.037   0.164    0.298  ...   106.0     105.0      61.0   1.05
Is_begin  0.851  0.761   0.013    2.388  ...    58.0      81.0      77.0   1.00
Ia_begin  1.344  1.236   0.003    3.526  ...   105.0     122.0      59.0   0.99
E_begin   0.848  0.924   0.012    2.645  ...   105.0      98.0      60.0   1.02

[8 rows x 11 columns]