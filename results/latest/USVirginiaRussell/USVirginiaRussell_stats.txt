0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo  -984.80    28.26
p_loo       27.68        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      353   91.9%
 (0.5, 0.7]   (ok)         25    6.5%
   (0.7, 1]   (bad)         6    1.6%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.253  0.054   0.170    0.332  ...   105.0      94.0      58.0   1.02
pu        0.806  0.019   0.770    0.838  ...    82.0      76.0     100.0   1.01
mu        0.142  0.025   0.102    0.190  ...    11.0      14.0      91.0   1.12
mus       0.191  0.033   0.124    0.243  ...    95.0     110.0      99.0   1.01
gamma     0.233  0.045   0.166    0.326  ...   152.0     152.0     100.0   0.98
Is_begin  0.709  0.644   0.016    2.067  ...   150.0     152.0      65.0   0.98
Ia_begin  1.074  1.117   0.005    3.246  ...   106.0      94.0      60.0   1.00
E_begin   0.580  0.604   0.007    2.006  ...    83.0      59.0      38.0   1.00

[8 rows x 11 columns]