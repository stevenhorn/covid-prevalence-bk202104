0 Divergences 
Passed validation 
Computed from 80 by 385 log-likelihood matrix

         Estimate       SE
elpd_loo  -543.18    30.64
p_loo       39.37        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      344   89.4%
 (0.5, 0.7]   (ok)         28    7.3%
   (0.7, 1]   (bad)         9    2.3%
   (1, Inf)   (very bad)    4    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.238  0.049   0.155    0.313  ...    11.0      15.0      48.0   1.10
pu        0.773  0.049   0.707    0.854  ...     5.0       4.0      23.0   1.52
mu        0.170  0.030   0.109    0.217  ...     7.0       8.0      43.0   1.25
mus       0.230  0.034   0.161    0.283  ...   152.0     152.0      93.0   1.03
gamma     0.305  0.047   0.221    0.394  ...    15.0      15.0      25.0   1.12
Is_begin  1.579  1.282   0.061    4.310  ...    82.0      53.0      81.0   0.99
Ia_begin  4.751  2.995   0.372    9.367  ...   119.0     111.0      83.0   0.99
E_begin   3.227  2.815   0.003    9.136  ...    40.0      42.0      56.0   1.04

[8 rows x 11 columns]