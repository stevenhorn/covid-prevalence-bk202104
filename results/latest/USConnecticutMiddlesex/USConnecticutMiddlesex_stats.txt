0 Divergences 
Passed validation 
Computed from 80 by 384 log-likelihood matrix

         Estimate       SE
elpd_loo -1672.55    30.98
p_loo       28.67        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      355   92.4%
 (0.5, 0.7]   (ok)         25    6.5%
   (0.7, 1]   (bad)         4    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.247  0.051   0.164    0.336  ...   115.0     100.0      18.0   1.02
pu        0.773  0.032   0.710    0.822  ...    29.0      29.0      40.0   1.04
mu        0.130  0.021   0.097    0.165  ...    62.0      66.0      58.0   1.03
mus       0.160  0.031   0.113    0.221  ...   124.0     137.0      23.0   1.01
gamma     0.245  0.036   0.192    0.303  ...    64.0      61.0      65.0   1.02
Is_begin  0.830  0.678   0.001    1.801  ...    80.0      59.0      40.0   0.99
Ia_begin  1.655  1.268   0.042    3.929  ...    79.0      51.0      60.0   1.02
E_begin   1.652  1.638   0.055    5.173  ...    91.0      50.0      93.0   1.02

[8 rows x 11 columns]